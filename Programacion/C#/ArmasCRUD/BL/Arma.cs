﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BL
{
    public class Arma
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Marca { get; set; }
        public int Disparos { get; set; }
        public int Potencia { get; set; }
        private DAO dAO = new DAO();

        public bool Guardar()
        {
            string mSQL = "";
            if (Id>0)
            {
                mSQL = "UPDATE Arma SET Arma_Id=" + Id + ", Arma_Nombre='" + Nombre + "Arma_Marca='" + Marca + ", Arma_Disparos=" + ", Arma_Potencia=" + Potencia + "where Arma_Id =" + Id;
            }
            else
            {
                int mId = dAO.ProximoId("Arma");
                mId += 1;
                mSQL = "INSERT into Arma (Arma_Id, Arma_Nombre, Arma_Marca, Arma_Disparos, Arma_Potencia) VALUES (" + mId + ", '" + Nombre + "', '" + Marca + "', " + Disparos + ", " + Potencia + ")";
            }
            return dAO.Ejecutar(mSQL) >0;
        }


        public bool Eliminar()
        {
            string mSQL = "DELETE FROM Arma WHERE Arma_Id = " + Id;


            return dAO.Ejecutar(mSQL) > 0;
        }

        public void Obtener()
        {
            if (Id > 0)
            {
                string mSQL = "SELECT * FROM Arma WHERE Arma_Id=" + Id;;
                DataSet mDS = dAO.Obtener(mSQL);

                if (mDS.Tables.Count > 0 && mDS.Tables[0].Rows.Count > 0)
                {
                    Valorizar(mDS.Tables[0].Rows[0], this);
                }
            }
        }

        public List<Arma> ListarArmas()
        {
            string mSQL = "SELECT * FROM Arma";
            DataSet mDS = dAO.Obtener(mSQL);

            List<Arma> mLista = new List<Arma>();
            if (mDS.Tables.Count > 0 && mDS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow mDr in mDS.Tables[0].Rows)
                {
                    Arma mArma = new Arma();
                    Valorizar(mDr, mArma);
                    mLista.Add(mArma);
                }
                return mLista;
            }
            else
                return null;
        }
        private void Valorizar(DataRow pDr, Arma pArma)
        {
            pArma.Id = int.Parse(pDr["Arma_Id"].ToString());
            pArma.Nombre = pDr["Arma_Nombre"].ToString();
            pArma.Marca = pDr["Arma_Marca"].ToString();
            pArma.Disparos = int.Parse(pDr["Arma_Disparos"].ToString());
            pArma.Potencia = int.Parse(pDr["Arma_Potencia"].ToString());
        }
    }
}
