﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace BL
{
    class DAO
    {
        private  SqlConnection sqlConnection =new SqlConnection(@"Data Source = MANUELMARIN\MANUELSQLSERVER; Initial Catalog = Armas; Integrated Security = True");

        public int Ejecutar (string pSQL)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(pSQL, sqlConnection);
                sqlConnection.Open();
                return sqlCommand.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public DataSet Obtener(string pSQL)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(pSQL, sqlConnection);
            DataSet dataSet = new DataSet();

            try
            {
                sqlDataAdapter.Fill(dataSet);
                return dataSet;
            }
            catch (Exception)
            {
                throw;
            }
            
        }
        public int ProximoId(string pTabla)
        {
            string mSQL = "SELECT ISNULL(MAX(" + pTabla + "_Id), 0) FROM " + pTabla;
            DataSet mDs = Obtener(mSQL);

            return int.Parse(mDs.Tables[0].Rows[0][0].ToString());
        }
    }
}
