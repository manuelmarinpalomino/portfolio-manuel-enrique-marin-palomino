﻿
namespace PrimerParcial_ManuelEnriqueMarinPalomino
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdArma = new System.Windows.Forms.DataGridView();
            this.lbl_NombeArma = new System.Windows.Forms.Label();
            this.Txt_NombreArma = new System.Windows.Forms.TextBox();
            this.Lbl_ModeloArma = new System.Windows.Forms.Label();
            this.Lbl_DisparosArma = new System.Windows.Forms.Label();
            this.Lbl_PotenciaArma = new System.Windows.Forms.Label();
            this.Txt_ModeloArma = new System.Windows.Forms.TextBox();
            this.Txt_DisparosArma = new System.Windows.Forms.TextBox();
            this.TxT_PotenciaArma = new System.Windows.Forms.TextBox();
            this.Btn_Guardar = new System.Windows.Forms.Button();
            this.Btn_Nuevo = new System.Windows.Forms.Button();
            this.Btn_Eliminar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdArma)).BeginInit();
            this.SuspendLayout();
            // 
            // grdArma
            // 
            this.grdArma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdArma.Location = new System.Drawing.Point(35, 27);
            this.grdArma.Name = "grdArma";
            this.grdArma.Size = new System.Drawing.Size(729, 236);
            this.grdArma.TabIndex = 0;
            // 
            // lbl_NombeArma
            // 
            this.lbl_NombeArma.AutoSize = true;
            this.lbl_NombeArma.Location = new System.Drawing.Point(32, 285);
            this.lbl_NombeArma.Name = "lbl_NombeArma";
            this.lbl_NombeArma.Size = new System.Drawing.Size(93, 13);
            this.lbl_NombeArma.TabIndex = 1;
            this.lbl_NombeArma.Text = "Nombre Del Arma:";
            // 
            // Txt_NombreArma
            // 
            this.Txt_NombreArma.Location = new System.Drawing.Point(129, 282);
            this.Txt_NombreArma.Name = "Txt_NombreArma";
            this.Txt_NombreArma.Size = new System.Drawing.Size(173, 20);
            this.Txt_NombreArma.TabIndex = 2;
            // 
            // Lbl_ModeloArma
            // 
            this.Lbl_ModeloArma.AutoSize = true;
            this.Lbl_ModeloArma.Location = new System.Drawing.Point(32, 316);
            this.Lbl_ModeloArma.Name = "Lbl_ModeloArma";
            this.Lbl_ModeloArma.Size = new System.Drawing.Size(91, 13);
            this.Lbl_ModeloArma.TabIndex = 3;
            this.Lbl_ModeloArma.Text = "Modelo Del Arma:";
            // 
            // Lbl_DisparosArma
            // 
            this.Lbl_DisparosArma.AutoSize = true;
            this.Lbl_DisparosArma.Location = new System.Drawing.Point(32, 348);
            this.Lbl_DisparosArma.Name = "Lbl_DisparosArma";
            this.Lbl_DisparosArma.Size = new System.Drawing.Size(97, 13);
            this.Lbl_DisparosArma.TabIndex = 4;
            this.Lbl_DisparosArma.Text = "Disparos Del Arma:";
            // 
            // Lbl_PotenciaArma
            // 
            this.Lbl_PotenciaArma.AutoSize = true;
            this.Lbl_PotenciaArma.Location = new System.Drawing.Point(32, 376);
            this.Lbl_PotenciaArma.Name = "Lbl_PotenciaArma";
            this.Lbl_PotenciaArma.Size = new System.Drawing.Size(98, 13);
            this.Lbl_PotenciaArma.TabIndex = 5;
            this.Lbl_PotenciaArma.Text = "Potencia Del Arma:";
            // 
            // Txt_ModeloArma
            // 
            this.Txt_ModeloArma.Location = new System.Drawing.Point(129, 313);
            this.Txt_ModeloArma.Name = "Txt_ModeloArma";
            this.Txt_ModeloArma.Size = new System.Drawing.Size(173, 20);
            this.Txt_ModeloArma.TabIndex = 6;
            // 
            // Txt_DisparosArma
            // 
            this.Txt_DisparosArma.Location = new System.Drawing.Point(129, 345);
            this.Txt_DisparosArma.Name = "Txt_DisparosArma";
            this.Txt_DisparosArma.Size = new System.Drawing.Size(173, 20);
            this.Txt_DisparosArma.TabIndex = 7;
            // 
            // TxT_PotenciaArma
            // 
            this.TxT_PotenciaArma.Location = new System.Drawing.Point(129, 373);
            this.TxT_PotenciaArma.Name = "TxT_PotenciaArma";
            this.TxT_PotenciaArma.Size = new System.Drawing.Size(173, 20);
            this.TxT_PotenciaArma.TabIndex = 8;
            // 
            // Btn_Guardar
            // 
            this.Btn_Guardar.Location = new System.Drawing.Point(520, 318);
            this.Btn_Guardar.Name = "Btn_Guardar";
            this.Btn_Guardar.Size = new System.Drawing.Size(146, 30);
            this.Btn_Guardar.TabIndex = 9;
            this.Btn_Guardar.Text = "Guardar";
            this.Btn_Guardar.UseVisualStyleBackColor = true;
            this.Btn_Guardar.Click += new System.EventHandler(this.Btn_Guardar_Click);
            // 
            // Btn_Nuevo
            // 
            this.Btn_Nuevo.Location = new System.Drawing.Point(520, 282);
            this.Btn_Nuevo.Name = "Btn_Nuevo";
            this.Btn_Nuevo.Size = new System.Drawing.Size(146, 30);
            this.Btn_Nuevo.TabIndex = 10;
            this.Btn_Nuevo.Text = "Nuevo";
            this.Btn_Nuevo.UseVisualStyleBackColor = true;
            this.Btn_Nuevo.Click += new System.EventHandler(this.Btn_Nuevo_Click);
            // 
            // Btn_Eliminar
            // 
            this.Btn_Eliminar.Location = new System.Drawing.Point(520, 354);
            this.Btn_Eliminar.Name = "Btn_Eliminar";
            this.Btn_Eliminar.Size = new System.Drawing.Size(146, 30);
            this.Btn_Eliminar.TabIndex = 11;
            this.Btn_Eliminar.Text = "Eliminar";
            this.Btn_Eliminar.UseVisualStyleBackColor = true;
            this.Btn_Eliminar.Click += new System.EventHandler(this.Btn_Eliminar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Btn_Eliminar);
            this.Controls.Add(this.Btn_Nuevo);
            this.Controls.Add(this.Btn_Guardar);
            this.Controls.Add(this.TxT_PotenciaArma);
            this.Controls.Add(this.Txt_DisparosArma);
            this.Controls.Add(this.Txt_ModeloArma);
            this.Controls.Add(this.Lbl_PotenciaArma);
            this.Controls.Add(this.Lbl_DisparosArma);
            this.Controls.Add(this.Lbl_ModeloArma);
            this.Controls.Add(this.Txt_NombreArma);
            this.Controls.Add(this.lbl_NombeArma);
            this.Controls.Add(this.grdArma);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdArma)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdArma;
        private System.Windows.Forms.Label lbl_NombeArma;
        private System.Windows.Forms.TextBox Txt_NombreArma;
        private System.Windows.Forms.Label Lbl_ModeloArma;
        private System.Windows.Forms.Label Lbl_DisparosArma;
        private System.Windows.Forms.Label Lbl_PotenciaArma;
        private System.Windows.Forms.TextBox Txt_ModeloArma;
        private System.Windows.Forms.TextBox Txt_DisparosArma;
        private System.Windows.Forms.TextBox TxT_PotenciaArma;
        private System.Windows.Forms.Button Btn_Guardar;
        private System.Windows.Forms.Button Btn_Nuevo;
        private System.Windows.Forms.Button Btn_Eliminar;
    }
}

