﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace PrimerParcial_ManuelEnriqueMarinPalomino
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            Txt_DisparosArma.Text = "";
            Txt_ModeloArma.Text = "";
            Txt_NombreArma.Text = "";
            TxT_PotenciaArma.Text = "";
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            Arma mArma = new Arma();
            mArma.Nombre = Txt_NombreArma.Text;
            mArma.Marca = Txt_ModeloArma.Text;
            mArma.Disparos = int.Parse(Txt_DisparosArma.Text);
            mArma.Potencia = int.Parse(TxT_PotenciaArma.Text);

            if (mArma.Guardar())
            {
                MessageBox.Show("Arma guardada");
            }
            Actualizar();
        }
        private void Actualizar()
        {
            Arma mArma = new Arma();
            grdArma.DataSource = null;
            grdArma.DataSource = mArma.ListarArmas();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Actualizar();

        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {

        }
    }
}
