﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public abstract class Arma
    {
         protected bool mPreparada;
        public abstract bool preparada { get; }
        public string nombre { get; set; }
        public bool ensangrentada { get; set; }

        public virtual void Matar()
        {
            if (this.mPreparada)
            {
                ensangrentada = true;
               
            }
        }
    }
}
