﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Cuchilla:Arma
    {
        public override bool preparada  {
            get
            {
                if(this.mPreparada == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            
        }
        public Cuchilla()
        {
            this.mPreparada = true;
        }
        public override void Matar()
        {
            if (this.mPreparada)
            {
                mPreparada = false;
            }
        }

    }
}
