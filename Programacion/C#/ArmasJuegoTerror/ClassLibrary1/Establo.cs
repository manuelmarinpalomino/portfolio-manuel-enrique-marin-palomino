﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Establo:IEnumerable<Arma>
    {
        private List<Arma> armas = new List<Arma>();
        public int CantidadArmasPreparadas 
        {
            get
            {
                var armaspreparadas = from a in armas where a.preparada select a;
                int cantidad = 0;
                    foreach (var item in armaspreparadas)
                    {
                        cantidad++;
                    }
                return cantidad;
            }
        }

        public IEnumerator<Arma> GetEnumerator()
        {
            return this.armas.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.armas.GetEnumerator();
        }
        public void AgregarArma(Arma arma)
        {
            DProbarArmas += arma.Matar;
            this.armas.Add(arma);

        }
        public delegate void probarArmasDelegate();
        public probarArmasDelegate DProbarArmas;
        public void probarArmas()
        {
            DProbarArmas();
        }
    }
}
