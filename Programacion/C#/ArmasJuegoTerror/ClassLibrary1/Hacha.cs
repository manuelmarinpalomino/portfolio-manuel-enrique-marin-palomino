﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Hacha :Arma
    {
        public override bool preparada {
            get
            {
                if (afilada)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool afilada { get; set; }
        public override void Matar()
        {
            if (this.mPreparada)
            {
                this.afilada = false;
                ensangrentada = true;
            }
        }
        public Hacha()
        {
            mPreparada = true;
        }
    }
}
