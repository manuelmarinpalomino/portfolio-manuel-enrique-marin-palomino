﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Machete : Arma
    {
        public override bool preparada {
            get
            {
                return true;
            } 
        }

        public Machete()
        {
            mPreparada = true;
        }
    }
}
