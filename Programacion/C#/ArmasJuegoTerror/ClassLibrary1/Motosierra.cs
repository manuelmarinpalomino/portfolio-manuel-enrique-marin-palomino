﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Motosierra : Arma
    {
        public override bool preparada {
            get
            {
                this.mPreparada = false;
                if (nivelCombustible >0)
                {
                    this.mPreparada = true;
                }
                return this.mPreparada;
            }
        }
        public override void Matar()
        {
            if (this.mPreparada)
            {
                ensangrentada = true;
                nivelCombustible -= 10;
            }
        }
        public int nivelCombustible { get; set; }
    }
}
