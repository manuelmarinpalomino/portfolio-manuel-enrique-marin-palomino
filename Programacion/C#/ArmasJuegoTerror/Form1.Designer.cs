﻿
namespace Parcial2ManuelMarin
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdEstablo = new System.Windows.Forms.DataGridView();
            this.btnPrepararHachas = new System.Windows.Forms.Button();
            this.btnPrepararMotosierra = new System.Windows.Forms.Button();
            this.btnProbarArmas = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdEstablo)).BeginInit();
            this.SuspendLayout();
            // 
            // grdEstablo
            // 
            this.grdEstablo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEstablo.Location = new System.Drawing.Point(12, 12);
            this.grdEstablo.Name = "grdEstablo";
            this.grdEstablo.Size = new System.Drawing.Size(776, 343);
            this.grdEstablo.TabIndex = 0;
            // 
            // btnPrepararHachas
            // 
            this.btnPrepararHachas.Location = new System.Drawing.Point(96, 393);
            this.btnPrepararHachas.Name = "btnPrepararHachas";
            this.btnPrepararHachas.Size = new System.Drawing.Size(111, 31);
            this.btnPrepararHachas.TabIndex = 1;
            this.btnPrepararHachas.Text = "PrepararHachas";
            this.btnPrepararHachas.UseVisualStyleBackColor = true;
            this.btnPrepararHachas.Click += new System.EventHandler(this.btnPrepararHachas_Click);
            // 
            // btnPrepararMotosierra
            // 
            this.btnPrepararMotosierra.Location = new System.Drawing.Point(329, 397);
            this.btnPrepararMotosierra.Name = "btnPrepararMotosierra";
            this.btnPrepararMotosierra.Size = new System.Drawing.Size(128, 23);
            this.btnPrepararMotosierra.TabIndex = 2;
            this.btnPrepararMotosierra.Text = "PrepararMotosierras";
            this.btnPrepararMotosierra.UseVisualStyleBackColor = true;
            this.btnPrepararMotosierra.Click += new System.EventHandler(this.btnPrepararMotosierra_Click);
            // 
            // btnProbarArmas
            // 
            this.btnProbarArmas.Location = new System.Drawing.Point(556, 397);
            this.btnProbarArmas.Name = "btnProbarArmas";
            this.btnProbarArmas.Size = new System.Drawing.Size(75, 23);
            this.btnProbarArmas.TabIndex = 3;
            this.btnProbarArmas.Text = "ProbarArmas";
            this.btnProbarArmas.UseVisualStyleBackColor = true;
            this.btnProbarArmas.Click += new System.EventHandler(this.btnProbarArmas_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 367);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnProbarArmas);
            this.Controls.Add(this.btnPrepararMotosierra);
            this.Controls.Add(this.btnPrepararHachas);
            this.Controls.Add(this.grdEstablo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdEstablo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdEstablo;
        private System.Windows.Forms.Button btnPrepararHachas;
        private System.Windows.Forms.Button btnPrepararMotosierra;
        private System.Windows.Forms.Button btnProbarArmas;
        private System.Windows.Forms.Label label1;
    }
}

