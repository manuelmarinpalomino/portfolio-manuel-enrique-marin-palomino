﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Parcial2ManuelMarin
{
    public partial class Form1 : Form
    {
        Establo establo = new Establo();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            establo.AgregarArma(new Machete() {nombre = "Machete" });
            establo.AgregarArma(new Hacha() { afilada = true, nombre = "Hacha" });
            establo.AgregarArma(new Cuchilla() { nombre = "Cuchilla" });
            establo.AgregarArma(new Motosierra() { nivelCombustible = 100, nombre = "Motosierra" });
            grdEstablo.Columns.Add("Nombre", "Nombre");
            grdEstablo.Columns.Add("Preparada", "Preparada");
            grdEstablo.Columns.Add("Ensangrentada", "Ensangrentada");
            grdEstablo.Columns.Add("Afilada", "Afilada");
            grdEstablo.Columns.Add("nivelCombustible", "nivelCombustible");
            Actualizar();
        }
        public void Actualizar()
        {
            grdEstablo.Rows.Clear();
            foreach (var armas in establo)
            {
                if(armas is Hacha)
                {
                    grdEstablo.Rows.Add(armas.nombre, armas.preparada, armas.ensangrentada, ((Hacha)armas).afilada, "-");
                }
                else if(armas is Motosierra)
                {
                    grdEstablo.Rows.Add(armas.nombre, armas.preparada, armas.ensangrentada, "-",((Motosierra)armas).nivelCombustible);
                }
                else
                {
                    grdEstablo.Rows.Add(armas.nombre, armas.preparada, armas.ensangrentada, "-", "-");
                }
            }
            label1.Text = establo.CantidadArmasPreparadas.ToString();
        }

        private void btnProbarArmas_Click(object sender, EventArgs e)
        {
            establo.probarArmas();
            Actualizar();
        }

        private void btnPrepararHachas_Click(object sender, EventArgs e)
        {
            foreach (var item in establo)
            {
                if(item is Hacha)
                {
                    ((Hacha)item).afilada = true;
                }
                Actualizar();
            }
        }

        private void btnPrepararMotosierra_Click(object sender, EventArgs e)
        {
            foreach (var item in establo)
            {
                if (item is Motosierra)
                {
                    ((Motosierra)item).nivelCombustible = 100;
                }
            }
            Actualizar();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
