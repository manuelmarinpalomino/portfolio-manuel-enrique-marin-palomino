﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace parcial1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Core.Jugar jugar = new Core.Jugar();
            jugar.Menu();
        }
    }
}
namespace Core
{
    public class Jugar
    {
        public void Menu()
        {
            bool salirDelJuego = false;
            do
            {
                bool salir = false;
                int op = 0;
                string o;
                do
                {
                    Console.Clear();
                    Console.WriteLine("Batalla Naval");
                    Console.WriteLine("1-Jugar");
                    Console.WriteLine("2-Ayuda");
                    Console.WriteLine("3-Salir");
                    Console.Write("que opción quiere elegir? ");
                    o = Console.ReadLine();
                    if (o.Length == 1 && (o == "1" || o == "2" || o == "3"))
                    {
                        op = Convert.ToInt32(o);
                        salir = true;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("ha marcado una opción incorrecta por favor intentelo nuevamente.(Enter para continuar.)");
                        Console.ReadLine();
                    }
                } while (!salir);
                switch (op)
                {
                    case 1:
                        jugar();
                        break;
                    case 2:
                        Ayuda();
                        break;
                    case 3:
                        salirDelJuego = true;
                        break;
                    default:
                        break;
                }
            } while (!salirDelJuego);
        }



        private void Ayuda()
        {
            Console.Clear();
            Console.WriteLine("Solo tienes que ingresar bien la columna (A-J) y la fila(1-10) que quieres.");
            Console.ReadLine();
        }

        private void jugar()
        {
            string columna;
            int fila;
            jugador j1 = new jugador();
            Tablero t1 = new Tablero();
            bool salir = false;
            int posx = 0;
            int posy = 0;
            int cosasDestruidas;
            Console.Clear();
            Console.Write("ingrese su nombre: ");
            j1.Nombre = Console.ReadLine();
            j1.Puntaje = 0;
            Console.Clear();

            do
            {
                
                t1.Dibujar(j1);

                Console.Write("ingrese la columna que cree que hay un barco(S para salir.): ");
                columna = Console.ReadLine();
                
                if(columna == "S" || columna == "s")
                {
                    salir = true;
                }
                else if(columna.Length==1)
                {
                    if (char.ToUpper(Convert.ToChar(columna)) == 'A' || char.ToUpper(Convert.ToChar(columna)) == 'B' || char.ToUpper(Convert.ToChar(columna)) == 'C' || char.ToUpper(Convert.ToChar(columna)) == 'D' || char.ToUpper(Convert.ToChar(columna)) == 'E' || char.ToUpper(Convert.ToChar(columna)) == 'F' || char.ToUpper(Convert.ToChar(columna)) == 'G' || char.ToUpper(Convert.ToChar(columna)) == 'H' || char.ToUpper(Convert.ToChar(columna)) == 'I' || char.ToUpper(Convert.ToChar(columna)) == 'J')
                    {


                        Console.Write("Escriba la fila que quiere(S para salir): ");
                        string fila1 = Console.ReadLine();
                        if(fila1== "s" || fila1 == "S")
                        {
                            salir = true;
                        }
                        else if (fila1.Length <= 2)
                        {
                            if (fila1 == "1" || fila1 == "2" || fila1 == "3" || fila1 == "4" || fila1 == "5" || fila1 == "6" || fila1 == "7" || fila1 == "8" || fila1 == "9" || fila1 == "10")
                            {
                                fila = Convert.ToInt32(fila1);
                                char columna1 = char.ToUpper(Convert.ToChar(columna));
                                posx = t1.convertirFila(fila);
                                posy = t1.convertirColumna(columna1);
                                if ((t1.GetTablero(posx, posy) != "X") && (t1.GetTablero(posx, posy) != "."))
                                {


                                    if (t1.estaEnLaPosicion(posx, posy))
                                    {
                                        Console.Write("Le diste a un barco");
                                        j1.Puntaje = j1.Puntaje + 2;
                                        Console.ReadLine();
                                    }
                                    else
                                    {
                                        Console.Write("le diste al agua");
                                        if (j1.Puntaje > 0)
                                        {
                                            j1.Puntaje = j1.Puntaje - 1;
                                        }
                                        Console.ReadLine();
                                    }
                                    cosasDestruidas = 0;
                                    cosasDestruidas = t1.destruisteEmbarcacion(t1.GetTablero(), t1.GetTableroOculto());
                                    if (cosasDestruidas == 7)
                                    {
                                        Console.Clear();
                                        Console.ForegroundColor = ConsoleColor.Green;
                                        Console.WriteLine($"Felicitaciones{j1.Nombre} Ganaste¡¡¡¡");
                                        Console.WriteLine($"Tu puntuaje es: {j1.Puntaje}");
                                        Console.ReadLine();
                                        Console.ForegroundColor = ConsoleColor.White;
                                        salir = true;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("esa celda esta ocupada ingrese otra por favor");
                                    Console.ReadLine();
                                }
                            }
                            
                        }
                        else
                        {
                            Console.Write("ingresaste un valor no valido por favor ingrese nuevamente los datos.");
                            Console.ReadLine();
                        }
                    }
                }
                else
                {
                    Console.Write("ingresaste un valor no valido por favor ingrese nuevamente los datos.");
                    Console.ReadLine();
                }
                
            } while (!salir);
            
        }

    }
    public class jugador
    {
        public string Nombre { get; set; }
        public int Puntaje { get; set; }
    }
    public class Tablero
    {
        #region Constructor
        public Tablero()
        {

            for (int i = 0; i < 10; i++)
            {
                for (int y = 0; y < 10; y++)
                {
                    tableroOculto[i, y] = " ";
                    tablero[i, y] = " ";

                }

            }
           
            portaAviones.Ubicarbarcos(tableroOculto, portaAviones);
            
            buque.Ubicarbarcos(tableroOculto, buque);
            
            submarino.Ubicarbarcos(tableroOculto, submarino);
            
            submarino1.Ubicarbarcos(tableroOculto, submarino1);
            
            crucero.Ubicarbarcos(tableroOculto, crucero);
            
            lancha.Ubicarbarcos(tableroOculto, lancha);
            
            lancha1.Ubicarbarcos(tableroOculto, lancha1);



        }
        #endregion
        #region Estados
        private string[,] tableroOculto = new string[10, 10];
        private string[,] tablero = new string[10, 10];
        private  PortaAviones portaAviones = new PortaAviones();
        private Buque buque = new Buque();
        private Submarino submarino = new Submarino();
        private Submarino submarino1 = new Submarino();
        private Crucero crucero = new Crucero();
        private Lancha lancha = new Lancha();
        private Lancha lancha1 = new Lancha();


        #endregion
        #region metodos

        
        public string[,] GetTablero()
        {
             
            return tablero;
        }
        public string GetTablero(int x,int y)
        {

            return tablero[x,y];
        }
        public string[,] GetTableroOculto()
        {
            return tableroOculto;
        }
        public void SetTableroOculto (int x, int y, string value)
        {
            tableroOculto[x, y] = value;
        }
        public void Dibujar(jugador jugador)
        {
            
            Console.Clear();
            Console.WriteLine($"jugador: {jugador.Nombre}. Puntaje: {jugador.Puntaje} ");
            Console.WriteLine("   | A | B | C | D | E | F | G | H | I | J |");
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            Console.WriteLine("1  | {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", tablero[0, 0], tablero[0, 1], tablero[0, 2], tablero[0, 3], tablero[0, 4], tablero[0, 5], tablero[0, 6], tablero[0, 7], tablero[0, 8], tablero[0, 9]);
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            Console.WriteLine("2  | {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", tablero[1, 0], tablero[1, 1], tablero[1, 2], tablero[1, 3], tablero[1, 4], tablero[1, 5], tablero[1, 6], tablero[1, 7], tablero[1, 8], tablero[1, 9]);
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            Console.WriteLine("3  | {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", tablero[2, 0], tablero[2, 1], tablero[2, 2], tablero[2, 3], tablero[2, 4], tablero[2, 5], tablero[2, 6], tablero[2, 7], tablero[2, 8], tablero[2, 9]);
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            Console.WriteLine("4  | {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", tablero[3, 0], tablero[3, 1], tablero[3, 2], tablero[3, 3], tablero[3, 4], tablero[3, 5], tablero[3, 6], tablero[3, 7], tablero[3, 8], tablero[3, 9]);
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            Console.WriteLine("5  | {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", tablero[4, 0], tablero[4, 1], tablero[4, 2], tablero[4, 3], tablero[4, 4], tablero[4, 5], tablero[4, 6], tablero[4, 7], tablero[4, 8], tablero[4, 9]);
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            Console.WriteLine("6  | {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", tablero[5, 0], tablero[5, 1], tablero[5, 2], tablero[5, 3], tablero[5, 4], tablero[5, 5], tablero[5, 6], tablero[5, 7], tablero[5, 8], tablero[5, 9]);
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            Console.WriteLine("7  | {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", tablero[6, 0], tablero[6, 1], tablero[6, 2], tablero[6, 3], tablero[6, 4], tablero[6, 5], tablero[6, 6], tablero[6, 7], tablero[6, 8], tablero[6, 9]);
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            Console.WriteLine("8  | {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", tablero[7, 0], tablero[7, 1], tablero[7, 2], tablero[7, 3], tablero[7, 4], tablero[7, 5], tablero[7, 6], tablero[7, 7], tablero[7, 8], tablero[7, 9]);
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            Console.WriteLine("9  | {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", tablero[8, 0], tablero[8, 1], tablero[8, 2], tablero[8, 3], tablero[8, 4], tablero[8, 5], tablero[8, 6], tablero[8, 7], tablero[8, 8], tablero[8, 9]);
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            Console.WriteLine("10 | {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |", tablero[9, 0], tablero[9, 1], tablero[9, 2], tablero[9, 3], tablero[9, 4], tablero[9, 5], tablero[9, 6], tablero[9, 7], tablero[9, 8], tablero[9, 9]);
            Console.WriteLine("   |---|---|---|---|---|---|---|---|---|---|");
            

        }
        public int convertirFila( int fila)
        {
            int posx=0;
            
            
            if (fila == 1)
            {
                posx = 0;
            }
            if (fila == 2)
            {
                posx = 1;
            }
            if (fila == 3)
            {
                posx = 2;
            }
            if (fila == 4)
            {
                posx = 3;
            }
            if (fila == 5)
            {
                posx = 4;
            }
            if (fila == 6)
            {
                posx = 5;
            }
            if (fila == 7)
            {
                posx = 6;
            }
            if (fila == 8)
            {
                posx = 7;
            }
            if (fila == 9)
            {
                posx = 8;
            }
            if (fila == 10)
            {
                posx = 9;
            }
            return posx;
        }
        public int convertirColumna(char columna)
        {
            int posy = 0;
            if (columna == 'A')
            {
                posy = 0;
            }
            if (columna == 'B')
            {
                posy = 1;
            }
            if (columna == 'C')
            {
                posy = 2;
            }
            if (columna == 'D')
            {
                posy = 3;
            }
            if (columna == 'E')
            {
                posy = 4;
            }
            if (columna == 'F')
            {
                posy = 5;
            }
            if (columna == 'G')
            {
                posy = 6;
            }
            if (columna == 'H')
            {
                posy = 7;
            }
            if (columna == 'I')
            {
                posy = 8;
            }
            if (columna == 'J')
            {
                posy = 9;
            }
            return posy;
        }
       public bool estaEnLaPosicion(int posx, int posy)
        {
            if (tableroOculto[posx, posy] == "X")
            {
                tablero[posx, posy] = "X";
                return true;
            }
            else
            {
                tablero[posx, posy] = ".";
                return false;
            }
        }
        public int destruisteEmbarcacion(string[,] tablero, string[,] tableroOculto)
        {
            int cosasDestruidas = 0;
            if ((tablero[portaAviones.Posx1,portaAviones.Posy1]==tableroOculto[portaAviones.Posx1, portaAviones.Posy1])&& (tablero[portaAviones.Posx2, portaAviones.Posy2] == tableroOculto[portaAviones.Posx2, portaAviones.Posy2])&& (tablero[portaAviones.Posx3, portaAviones.Posy3] == tableroOculto[portaAviones.Posx3, portaAviones.Posy3])&& (tablero[portaAviones.Posx4, portaAviones.Posy4] == tableroOculto[portaAviones.Posx4, portaAviones.Posy4])&& (tablero[portaAviones.Posx5, portaAviones.Posy5] == tableroOculto[portaAviones.Posx5, portaAviones.Posy5]))
            {
                Console.WriteLine("Destruiste el porta-avión.");
                cosasDestruidas++;
                
            }
            else
            {
                Console.WriteLine("Aún no destruiste el porta-avión.");
                
            }
            if ((tablero[buque.Posx1, buque.Posy1] == tableroOculto[buque.Posx1,buque.Posy1])&& (tablero[buque.Posx2, buque.Posy2] == tableroOculto[buque.Posx2, buque.Posy2])&& (tablero[buque.Posx3, buque.Posy3] == tableroOculto[buque.Posx3, buque.Posy3])&& (tablero[buque.Posx4, buque.Posy4] == tableroOculto[buque.Posx4, buque.Posy4]))
            {
                Console.WriteLine("Destruiste el buque.");
                cosasDestruidas++;
            }
            else
            {
                Console.WriteLine("Aún no destruiste el buque.");
                
            }
            if((tablero[submarino.Posx1,submarino.Posy1]==tableroOculto[submarino.Posx1,submarino.Posy1])&& (tablero[submarino.Posx2, submarino.Posy2] == tableroOculto[submarino.Posx2, submarino.Posy2])&& (tablero[submarino.Posx3, submarino.Posy3] == tableroOculto[submarino.Posx3, submarino.Posy3]))
            {
                Console.WriteLine("Destruiste el primer submarino.");
                cosasDestruidas++;
            }
            else
            {
                Console.WriteLine("Aún no destruiste el primer submarino.");
                
            }
            if ((tablero[submarino1.Posx1, submarino1.Posy1] == tableroOculto[submarino1.Posx1, submarino1.Posy1]) && (tablero[submarino1.Posx2, submarino1.Posy2] == tableroOculto[submarino1.Posx2, submarino1.Posy2]) && (tablero[submarino1.Posx3, submarino1.Posy3] == tableroOculto[submarino1.Posx3, submarino1.Posy3]))
            {
                Console.WriteLine("Destruiste el segundo submarino.");
                cosasDestruidas++;
            }
            else
            {
                Console.WriteLine("Aún no destruiste el segundo submarino.");
                
            }
            if((tablero[crucero.Posx1,crucero.Posy1]==tableroOculto[crucero.Posx1,crucero.Posy1])&& (tablero[crucero.Posx2, crucero.Posy2] == tableroOculto[crucero.Posx2, crucero.Posy2]))
            {
                Console.WriteLine("Destruiste el crucero.");
                cosasDestruidas++;
            }
            else
            {
                Console.WriteLine("Aún no destruiste el crucero.");
                
            }
            if (tablero[lancha.Posx1, lancha.Posy1] == tableroOculto[lancha.Posx1, lancha.Posy1])
            {
                Console.WriteLine("Destruiste la primera lancha.");
                cosasDestruidas++;
            }
            else
            {
                Console.WriteLine("Aún no destruiste la primera lancha.");
                
            }
            if (tablero[lancha1.Posx1, lancha1.Posy1] == tableroOculto[lancha1.Posx1, lancha1.Posy1])
            {
                Console.WriteLine("Destruiste la segunda lancha.");
                cosasDestruidas++;
            }
            else
            {
                Console.WriteLine("Aún no destruiste la segunda lancha.");
                
            }
            Console.ReadLine();
            return cosasDestruidas;
        }
        #endregion
    }
    public class Barcos
    {
        public int Estado { get; set; }
        
    }
    public class PortaAviones:Barcos
    {
        #region Estados
        public int Posx1 { get; set; }
        public int Posx2 { get; set; }
        public int Posx3 { get; set; }
        public int Posx4 { get; set; }
        public int Posx5 { get; set; }
        public int Posy1 { get; set; }
        public int Posy2 { get; set; }
        public int Posy3 { get; set; }
        public int Posy4 { get; set; }
        public int Posy5 { get; set; }
        #endregion



        public void Ubicarbarcos(string[,] tableroOculto, PortaAviones barco)
        {
            Random random = new Random();
            int r = random.Next(0, 10);
            barco.Estado = r;
            bool salir = false;
            do
            {
                int posx = random.Next(0, 10);
                int posy = random.Next(0, 10);
                if (barco.Estado == 0|| barco.Estado ==2||barco.Estado==6)
                {
                    if (posy != 6 && posy != 7 && posy != 8 && posy != 9)
                    {
                        if (tableroOculto[posx, posy] == " ")
                        {
                           

                            posy++;

                            if (tableroOculto[posx, posy] == " ")
                            {
                               
                                posy++;
                                if (tableroOculto[posx, posy] == " ")
                                { 
                                    posy++;
                                    if (tableroOculto[posx, posy] == " ")
                                    {
                                        
                                        posy++;
                                        if (tableroOculto[posx, posy] == " ")
                                        {
                                            tableroOculto[posx, posy] = "X";
                                            Posx1 = posx;
                                            Posy1 = posy;
                                            posy--;
                                            tableroOculto[posx, posy] = "X";
                                            Posx2 = posx;
                                            Posy2 = posy;
                                            posy--;
                                            tableroOculto[posx, posy] = "X";
                                            Posx3 = posx;
                                            Posy3 = posy;
                                            posy--;
                                            tableroOculto[posx, posy] = "X";
                                            Posx4 = posx;
                                            Posy4 = posy;
                                            posy--;
                                            tableroOculto[posx, posy] = "X";
                                            Posx5 = posx;
                                            Posy5 = posy;
                                            salir = true;

                                        }
                                    }
                                }
                            }


                        }

                        
                    }
                }
                if (barco.Estado == 1 || barco.Estado == 3 || barco.Estado == 4 || barco.Estado == 8 || barco.Estado == 5 || barco.Estado == 7 || barco.Estado == 9)
                {
                    if (posx != 6 && posx != 7 && posx != 8 && posx != 9)
                    {
                        if (tableroOculto[posx, posy] == " ")
                        {
                            

                            posx++;

                            if (tableroOculto[posx, posy] == " ")
                            {
                                
                                posx++;
                                if (tableroOculto[posx, posy] == " ")
                                {
                                    
                                    posx++;
                                    if (tableroOculto[posx, posy] == " ")
                                    {
                                        
                                        posx++;
                                        if (tableroOculto[posx, posy] == " ")
                                        {
                                            tableroOculto[posx, posy] = "X";
                                            Posx1 = posx;
                                            Posy1 = posy;
                                            posx--;
                                            tableroOculto[posx, posy] = "X";
                                            Posx2 = posx;
                                            Posy2 = posy;
                                            posx--;
                                            tableroOculto[posx, posy] = "X";
                                            Posx3 = posx;
                                            Posy3 = posy;
                                            posx--;
                                            tableroOculto[posx, posy] = "X";
                                            Posx4 = posx;
                                            Posy4 = posy;
                                            posx--;
                                            tableroOculto[posx, posy] = "X";
                                            Posx5 = posx;
                                            Posy5 = posy;
                                            salir = true;

                                        }
                                    }
                                }
                            }


                        }


                    }
                }
            } while (!salir);
        }


    }
    public class Buque : Barcos
    {
        #region Estados
        public int Posx1 { get; set; }
        public int Posx2 { get; set; }
        public int Posx3 { get; set; }
        public int Posx4 { get; set; }
        public int Posy1 { get; set; }
        public int Posy2 { get; set; }
        public int Posy3 { get; set; }
        public int Posy4 { get; set; }
        #endregion

        public void Ubicarbarcos(string[,] tableroOculto, Buque barco)
        {
            Random ram = new Random();
            int f = ram.Next(0, 10);
            barco.Estado = f;
            bool salir = false;
            do
            {
                int posx = ram.Next(0, 10);
                int posy = ram.Next(0, 10);
                if (barco.Estado == 1 || barco.Estado == 3 || barco.Estado == 4 || barco.Estado == 8 || barco.Estado == 5 || barco.Estado == 7 || barco.Estado == 9)
                {
                    if (posy != 7 && posy != 8 && posy != 9)
                    {
                        if (tableroOculto[posx, posy] == " ")
                        {
                            

                            posy++;

                            if (tableroOculto[posx, posy] == " ")
                            {
                                
                                posy++;
                                if (tableroOculto[posx, posy] == " ")
                                {
                                    
                                    posy++;
                                    if (tableroOculto[posx, posy] == " ")
                                    {
                                        tableroOculto[posx, posy] = "X";
                                        Posx1 = posx;
                                        Posy1 = posy;
                                        posy--;
                                        tableroOculto[posx, posy] = "X";
                                        Posx2 = posx;
                                        Posy2 = posy;
                                        posy--;
                                        tableroOculto[posx, posy] = "X";
                                        Posx3 = posx;
                                        Posy3 = posy;
                                        posy--;
                                        tableroOculto[posx, posy] = "X";
                                        Posx4 = posx;
                                        Posy4 = posy;
                                        salir = true;
                                    }
                                }
                            }


                        }


                    }
                }
                if (barco.Estado == 0 || barco.Estado == 2 || barco.Estado == 6)
                {
                    if ( posx != 7 && posx != 8 && posx != 9)
                    {
                        if (tableroOculto[posx, posy] == " ")
                        {
                            

                            posx++;

                            if (tableroOculto[posx, posy] == " ")
                            {
                                
                                posx++;
                                if (tableroOculto[posx, posy] == " ")
                                {
                                    
                                    posx++;
                                    if (tableroOculto[posx, posy] == " ")
                                    {
                                        tableroOculto[posx, posy] = "X";
                                        Posx1 = posx;
                                        Posy1 = posy;
                                        posx--;
                                        tableroOculto[posx, posy] = "X";
                                        Posx2 = posx;
                                        Posy2 = posy;
                                        posx--;
                                        tableroOculto[posx, posy] = "X";
                                        Posx3 = posx;
                                        Posy3 = posy;
                                        posx--;
                                        tableroOculto[posx, posy] = "X";
                                        Posx4 = posx;
                                        Posy4 = posy;
                                        salir = true;
                                    }
                                }
                            }


                        }


                    }
                }
            } while (!salir);
        }


    }
    public class Submarino : Barcos
    {
        #region Estados
        public int Posx1 { get; set; }
        public int Posx2 { get; set; }
        public int Posx3 { get; set; }
        public int Posy1 { get; set; }
        public int Posy2 { get; set; }
        public int Posy3 { get; set; }
        #endregion

        public void Ubicarbarcos(string[,] tableroOculto, Submarino barco)
        {
            Random ra = new Random();
            int n = ra.Next(0, 10);
            barco.Estado = n;
            bool salir = false;
            do
            {
                int posx = ra.Next(0, 10);
                int posy = ra.Next(0, 10);
                if (barco.Estado == 0 || barco.Estado == 2 || barco.Estado == 6)
                {
                    if (posy != 8 && posy != 9)
                    {
                        if (tableroOculto[posx, posy] == " ")
                        {
                            

                            posy++;

                            if (tableroOculto[posx, posy] == " ")
                            {
                                
                                posy++;
                                if (tableroOculto[posx, posy] == " ")
                                {
                                    tableroOculto[posx, posy] = "X";
                                    Posx1 = posx;
                                    Posy1 = posy;
                                    posy--;
                                    tableroOculto[posx, posy] = "X";
                                    Posx2 = posx;
                                    Posy2 = posy;
                                    posy--; 
                                    tableroOculto[posx, posy] = "X";
                                    Posx3 = posx;
                                    Posy3 = posy;

                                    salir = true;
                                }
                            }


                        }


                    }
                }
                if (barco.Estado == 1 || barco.Estado == 3 || barco.Estado == 4 || barco.Estado == 5 || barco.Estado == 7 || barco.Estado == 8 || barco.Estado == 9)
                {
                    if (posx != 8 && posx != 9)
                    {
                        if (tableroOculto[posx, posy] == " ")
                        {
                            

                            posx++;

                            if (tableroOculto[posx, posy] == " ")
                            {
                                
                                posx++;
                                if (tableroOculto[posx, posy] == " ")
                                {
                                    tableroOculto[posx, posy] = "X";
                                    Posx1 = posx;
                                    Posy1 = posy;
                                    posx--;
                                    tableroOculto[posx, posy] = "X";
                                    Posx2 = posx;
                                    Posy2 = posy;
                                    posx--;
                                    tableroOculto[posx, posy] = "X";
                                    Posx3 = posx;
                                    Posy3 = posy;


                                    salir = true;
                                }
                            }


                        }


                    }
                }
            } while (!salir);
        }

    }
    public class Crucero : Barcos
    {
        #region Estados
        public int Posx1 { get; set; }
        public int Posx2 { get; set; }
        public int Posy1 { get; set; }
        public int Posy2 { get; set; }
        #endregion

        public void Ubicarbarcos(string[,] tableroOculto, Crucero barco)
        {
            Random ra = new Random();
            int n = ra.Next(0, 10);
            barco.Estado = n;
            bool salir = false;
            do
            {
                int posx = ra.Next(0, 10);
                int posy = ra.Next(0, 10);
                if (barco.Estado == 0 || barco.Estado == 2 || barco.Estado == 4 || barco.Estado == 6 || barco.Estado == 8)
                {
                    if ( posy != 9)
                    {
                        if (tableroOculto[posx, posy] == " ")
                        {

                            posy++;

                            if (tableroOculto[posx, posy] == " ")
                            {
                                tableroOculto[posx, posy] = "X";
                                Posx1 = posx;
                                Posy1 = posy;
                                posy--;
                                tableroOculto[posx, posy] = "X";
                                Posx2 = posx;
                                Posy2 = posy;
                                salir = true;
                            }


                        }


                    }
                }
                if (barco.Estado == 1 || barco.Estado == 3  || barco.Estado == 5 || barco.Estado == 7 || barco.Estado == 9)
                {
                    if ( posx != 9)
                    {
                        if (tableroOculto[posx, posy] == " ")
                        {
                            

                            posx++;

                            if (tableroOculto[posx, posy] == " ")
                            {
                                tableroOculto[posx, posy] = "X";
                                Posx1 = posx;
                                Posy1 = posy;
                                posx--;
                                tableroOculto[posx, posy] = "X";
                                Posx2 = posx;
                                Posy2 = posy;
                                salir = true;
                            }


                        }


                    }
                }
            } while (!salir);
        }
    }
    public class Lancha : Barcos
    {
        #region Estados
        public int Posx1 { get; set; }
        public int Posy1 { get; set; }
        #endregion

        public void Ubicarbarcos(string[,] tableroOculto, Lancha barco)
        {
            Random random = new Random();
            bool salir = false;
            do
            {
                int posx = random.Next(0, 10);
                int posy = random.Next(0, 10);
                if (tableroOculto[posx, posy] == " ")
                {
                    tableroOculto[posx, posy] = "X";
                    Posx1 = posx;
                    Posy1 = posy;
                    salir = true;

                }
            } while (!salir);
        }
    }
}
