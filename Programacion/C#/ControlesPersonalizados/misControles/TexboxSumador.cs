﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;


namespace misControles
{
    internal class TextBoxErrorEventArgs
    {
        public TextBox textBox { get; set; }
        public string MensajeError { get; set; }
    }
    public class TexboxSumador : TextBox
    {
        private List<TextBox> txtList = new List<TextBox>();
        [Category("Errores"), Description("Mensaje de error")]
        public string MensajeError { get; set; }
        [Category("Errores"), Description("Indicara si el error aparece en el textbox")]
        public bool MostrarMensajeEnTextBox { get; set; }
        [Category("Errores"), Description("ESTE ES EL COLOR ERRONEO")]
        public Color ColorErrorTextBox { get; set; }

        private List<Color> listColorTexBoxOriginales = new List<Color>();
        protected override void InitLayout()
        {
            base.InitLayout();

        }
        public void AgregarTexbox(TextBox txt)
        {
            txtList.Add(txt);
            txt.TextChanged += sumarTotalTextBox;
            txt.LostFocus += ValidarTexto;
            listColorTexBoxOriginales.Add(txt.BackColor);
        }
        public void sumarTotalTextBox(object sender, EventArgs e)
        {
            int numero = 0;
            foreach (var item in txtList)
            {
                if(item.Text != "" && System.Text.RegularExpressions.Regex.IsMatch(item.Text, "[0-9]"))
                {
                    numero += int.Parse(item.Text);
                    item.BackColor = QueColorEsOriginal(item); // como no podemos acceder a que posicion estamos dentro del foreach creamos este metodo para obtener el color origial de ese textbox
                }
            }
            this.Text = numero.ToString();
        }
        public void ValidarTexto (Object sender, EventArgs e)
        {
            foreach (var item in txtList)
            {
                if ( item.Text == "" || !System.Text.RegularExpressions.Regex.IsMatch(item.Text, "[0-9]"))
                {
                    errorText += TextBoxError;
                    
                    errorText?.Invoke(item, new TextBoxErrorEventArgs() { textBox = (TextBox)sender, MensajeError = " no contiene un numero valido" });
                    item.BackColor = ColorErrorTextBox;
                    mGr = Parent.CreateGraphics();
                    int x = item.Left - 5;
                    int y = item.Top - 5;
                    int ancho = item.Width + 10;
                    int alto = item.Height + 10;
                    mGr.DrawEllipse(Pens.Red, x, y, ancho, alto);
                    mGr.Dispose();
                }
                else
                {
                    item.BackColor=QueColorEsOriginal(item); // como no podemos acceder a que posicion estamos dentro del foreach creamos este metodo para obtener el color origial de ese textbox
                }
            }
        }

        public Color QueColorEsOriginal(TextBox txt) //aca recorremos con un for la lista de los txtbox si hay coincidencia devolvemos el color original de ese txt
        {
            for (int i = 0; i < txtList.Count; i++)
            {
                if (txt == txtList[i])
                {
                    return listColorTexBoxOriginales[i];
                }
            }
            return Color.Transparent;
        }
        delegate void ErrorText (object sender, TextBoxErrorEventArgs e);
         ErrorText errorText;
        private Graphics mGr;

        void TextBoxError (object sender,TextBoxErrorEventArgs e )
        {
            MensajeError = e.MensajeError;
            this.Text = "El " + e.textBox.Name + e.MensajeError;
        }
    }
}
