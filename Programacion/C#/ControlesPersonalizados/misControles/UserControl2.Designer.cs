﻿
namespace misControles
{
    partial class UserControl2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbltext = new System.Windows.Forms.Label();
            this.txtBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbltext
            // 
            this.lbltext.AutoSize = true;
            this.lbltext.Location = new System.Drawing.Point(146, 103);
            this.lbltext.Name = "lbltext";
            this.lbltext.Size = new System.Drawing.Size(35, 13);
            this.lbltext.TabIndex = 1;
            this.lbltext.Text = "label1";
            // 
            // txtBox
            // 
            this.txtBox.Location = new System.Drawing.Point(149, 58);
            this.txtBox.Name = "txtBox";
            this.txtBox.Size = new System.Drawing.Size(161, 20);
            this.txtBox.TabIndex = 2;
            // 
            // UserControl2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtBox);
            this.Controls.Add(this.lbltext);
            this.Name = "UserControl2";
            this.Size = new System.Drawing.Size(457, 272);
            this.Load += new System.EventHandler(this.UserControl2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbltext;
        private System.Windows.Forms.TextBox txtBox;
    }
}
