﻿using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX.DirectInput;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        Microsoft.DirectX.Direct3D.Device device;
        Microsoft.DirectX.DirectInput.Device keyboard;
        Texture texture, texture2,texture3,texture4;
        Microsoft.DirectX.Direct3D.Font font;
        int x = 10, y = 0, xEnemigo=10, yEnemigo=10, vida=3;
        float rotation = 0;
        int fps = 0, frames = 0;
        long timeStarted = Environment.TickCount;
        Thread thread;
        Random random = new Random();
        int numeroR = 0;
        
        public Form1()
        {
            InitializeComponent();
            InitDevice();
            InitKeyboard();
            InitFont();
            LoadTexture();

        }
     
        private void LoadTexture()
        {
            texture = TextureLoader.FromFile(device, "fondoJuego.jpg", device.Viewport.Width, device.Viewport.Height, 1, 0, Format.A8R8G8B8, Pool.Managed, Filter.Point, Filter.Point, Color.Transparent.ToArgb());
            texture2 = TextureLoader.FromFile(device, "Asteroide.png", 100, 100, 1, 0, Format.A8R8G8B8, Pool.Managed, Filter.Point, Filter.Point, Color.Transparent.ToArgb());
            texture3 = TextureLoader.FromFile(device, "Nave.png", 100, 100, 1, 0, Format.A8R8G8B8, Pool.Managed, Filter.Point, Filter.Point, Color.Transparent.ToArgb());
            texture4 = TextureLoader.FromFile(device, "FondoGameOver.jpg", device.Viewport.Width, device.Viewport.Height, 1, 0, Format.A8R8G8B8, Pool.Managed, Filter.Point, Filter.Point, Color.Transparent.ToArgb());
        }
        private void InitDevice()
        {
            PresentParameters pp = new PresentParameters();
            pp.Windowed = true;
            pp.SwapEffect = SwapEffect.Discard;
            device = new Microsoft.DirectX.Direct3D.Device(0, Microsoft.DirectX.Direct3D.DeviceType.Hardware, this, CreateFlags.HardwareVertexProcessing, pp);

        }
        
        private void InitKeyboard()
        {
            keyboard = new Microsoft.DirectX.DirectInput.Device(SystemGuid.Keyboard);
            keyboard.SetCooperativeLevel(this, CooperativeLevelFlags.NonExclusive | CooperativeLevelFlags.Background);
            keyboard.Acquire();
        }
        private void InitFont()
        {
            System.Drawing.Font f = new System.Drawing.Font("Arial", 16f, FontStyle.Regular);
            font = new Microsoft.DirectX.Direct3D.Font(device, f);
        }
        private void UpdateInput()
        {
            foreach(Key k in keyboard.GetPressedKeys())
            {

                if(k == Key.D)
                {
                    x += 2;
                }
                if (k == Key.A)
                {
                    x -= 2;
                }
                
              
            }
        }
        private void UpdateEnemigo()
        {
            if (((xEnemigo-50<=x+50&&xEnemigo+50>=x-50)||(xEnemigo - 50 >= x + 50 && xEnemigo + 50 <= x - 50)) && yEnemigo==400)
            {
                vida -= 1;
                xEnemigo = random.Next(0, 800);
                yEnemigo = -10;
                x = 50;
                
            }
            else {
                if (yEnemigo < 650)
                {
                    yEnemigo += 2;
                }
                else
                {
                    xEnemigo = random.Next(0, 800);
                    yEnemigo = -10;

                }
            }
        }


        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            StartThread();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopThread();
        }

        private void Render()
        {
            while (true)
            {

                UpdateInput();
                UpdateEnemigo();
                device.Clear(ClearFlags.Target, Color.CornflowerBlue, 0, 1);
                device.BeginScene();
                if (vida >= 0)
                {
                    using (Sprite s = new Sprite(device))
                    {

                        s.Begin(SpriteFlags.AlphaBlend);
                        s.Draw2D(texture, new Rectangle(0, 0, 0, 0), new Rectangle(0, 0, device.Viewport.Width, device.Viewport.Height), new Point(0, 0), 0f, new Point(0, 0), Color.White);
                        Matrix matrix = new Matrix();
                        matrix = Matrix.Transformation2D(new Vector2(0, 0), 0.0f, new Vector2(1.0f, 1.0f), new Vector2(x + 50, y + 50), rotation, new Vector2(0, 0));
                        s.Transform = matrix;

                        // s.Draw2D(texture2, new Rectangle(0, 0, 0, 0), new Rectangle(0, 0, 200, 200), new Point(0, 0), 0f, new Point(x, y), Color.White);
                        s.Draw(texture3, new Rectangle(0, 0, 0, 0), new Vector3(10, -450, 0), new Vector3(x, 0, 0), Color.White);
                        s.Draw(texture2, new Rectangle(0, 0, 0, 0), new Vector3(10, 10, 0), new Vector3(xEnemigo, yEnemigo, 0), Color.White);
                        //UpdateCamara();
                        s.End();
                    }
                    using (Sprite b = new Sprite(device))
                    {
                        b.Begin(SpriteFlags.AlphaBlend);
                        font.DrawText(b, "vida: " + vida, new Point(0, 0), Color.Gold);
                        font.DrawText(b, fps + "FPS", new Point(0, 30), Color.Gold);
                        b.End();
                    }
                }
                else
                {
                    using(Sprite c = new Sprite(device))
                    {
                        c.Begin(SpriteFlags.AlphaBlend);
                        c.Draw2D(texture4, new Rectangle(0, 0, 0, 0), new Rectangle(0, 0, device.Viewport.Width, device.Viewport.Height), new Point(0, 0), 0f, new Point(0, 0), Color.White);
                        c.End();

                    }
                    using(Sprite d = new Sprite(device))
                    {
                        d.Begin(SpriteFlags.AlphaBlend);
                        font.DrawText(d, "Game Over", new Point(350, 250), Color.Gold);
                        d.End();
                    }
                }
                device.EndScene();
                device.Present();
                if (Environment.TickCount >= timeStarted + 1000)
                {
                    fps = frames;
                    frames = 0;
                    timeStarted = Environment.TickCount;
                }
                frames++;
            }
        }
        private void StartThread()
        {
            thread = new Thread(new ThreadStart(Render));
            thread.Start();
        }
        private void StopThread()
        {
            thread.Abort();

        }
     

       
    }
}

