﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX;
using Microsoft.DirectX.DirectInput;
using System.Threading;
namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        Microsoft.DirectX.Direct3D.Device device;
        Microsoft.DirectX.DirectInput.Device keyboard;
        Texture texture, texture2;
        Microsoft.DirectX.Direct3D.Font font;
        int x = 0, y = 0;
        float rotation = 0;
        int fps = 0, frames = 0;
        long timeStarted = Environment.TickCount;
        Thread thread;
        float camaraX, camaraY, camaraZ;
        
        public Form1()
        {
            InitializeComponent();
            InitDevice();
            InitKeyboard();
            InitFont();
            LoadTexture();

        }
        private void UpdateCamara()
        {
            camaraX = x;
            camaraY = y;
            //device.Transform.Projection = Matrix.OrthoLH(device.Viewport.Width, device.Viewport.Height, 0.1f, 1000f);
            //device.Transform.View = Matrix.LookAtLH(new Vector3(camaraX, camaraY, 50), new Vector3(x, y, 0), new Vector3(0, -1, 0));
            device.Transform.Projection = Matrix.PerspectiveFovLH((int)Math.PI / 3, device.Viewport.Width / device.Viewport.Height, 0.1f, 1000f);
            device.Transform.View = Matrix.LookAtLH(new Vector3(camaraX, camaraY, 500), new Vector3(x, y, 0), new Vector3(-1, -1, 0));
        }
        private void LoadTexture()
        {
            texture = TextureLoader.FromFile(device, "fondo-abstracto-3d.jpg", device.Viewport.Width, device.Viewport.Height, 1, 0, Format.A8R8G8B8, Pool.Managed, Filter.Point, Filter.Point, Color.Transparent.ToArgb());
            texture2 = TextureLoader.FromFile(device, "Guerrero.png", 100, 100, 1, 0, Format.A8R8G8B8, Pool.Managed, Filter.Point, Filter.Point, Color.Transparent.ToArgb());
        }
        private void InitDevice()
        {
            PresentParameters pp = new PresentParameters();
            pp.Windowed = true;
            pp.SwapEffect = SwapEffect.Discard;
            device = new Microsoft.DirectX.Direct3D.Device(0, Microsoft.DirectX.Direct3D.DeviceType.Hardware, this, CreateFlags.HardwareVertexProcessing, pp);

        }
        private void InitKeyboard()
        {
            keyboard = new Microsoft.DirectX.DirectInput.Device(SystemGuid.Keyboard);
            keyboard.SetCooperativeLevel(this, CooperativeLevelFlags.NonExclusive | CooperativeLevelFlags.Background);
            keyboard.Acquire();
        }
        private void InitFont()
        {
            System.Drawing.Font f = new System.Drawing.Font("Arial", 16f, FontStyle.Regular);
            font = new Microsoft.DirectX.Direct3D.Font(device, f);
        }
        private void UpdateInput()
        {
            foreach(Key k in keyboard.GetPressedKeys())
            {
                if(k == Key.D)
                {
                    x += 2;
                }
                if (k == Key.S)
                {
                    y += 2;
                }
                if (k == Key.A)
                {
                    x -= 2;
                }
                if (k == Key.W)
                {
                    y -= 2;
                }
                if (k == Key.Left)
                {
                    rotation-=0.1f;
                }
                if (k == Key.Right)
                {
                    rotation += 0.1f;
                }
           
            }
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            StartThread();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopThread();
        }

        private void Render()
        {
            while (true)
            {

                UpdateInput();
                device.Clear(ClearFlags.Target, Color.CornflowerBlue, 0, 1);
                device.BeginScene();
                using (Sprite s = new Sprite(device))
                {
                    
                    s.Begin(SpriteFlags.AlphaBlend);
                    s.Draw2D(texture, new Rectangle(0, 0, 0, 0), new Rectangle(0, 0, device.Viewport.Width, device.Viewport.Height), new Point(0, 0), 0f, new Point(0, 0), Color.White);
                    Matrix matrix = new Matrix();
                    matrix = Matrix.Transformation2D(new Vector2(0, 0), 0.0f, new Vector2(1.0f, 1.0f), new Vector2(x + 50, y + 50), rotation, new Vector2(0, 0));
                    s.Transform = matrix;

                    // s.Draw2D(texture2, new Rectangle(0, 0, 0, 0), new Rectangle(0, 0, 200, 200), new Point(0, 0), 0f, new Point(x, y), Color.White);
                    s.Draw(texture2, new Rectangle(0, 0, 0, 0), new Vector3(0, 0, 0), new Vector3(x, 0, 0), Color.White);
                    UpdateCamara();
                    s.End();
                }
                using (Sprite b = new Sprite(device))
                {
                    b.Begin(SpriteFlags.AlphaBlend);
                    font.DrawText(b, "Golden Crown", new Point(0, 0), Color.Gold);
                    font.DrawText(b, fps+ "FPS", new Point(0, 30), Color.Gold);
                    b.End();
                }
                device.EndScene();
                device.Present();
                if (Environment.TickCount >= timeStarted + 1000)
                {
                    fps = frames;
                    frames = 0;
                    timeStarted = Environment.TickCount;
                }
                frames++;
            }
        }
        private void StartThread()
        {
            thread = new Thread(new ThreadStart(Render));
            thread.Start();
        }
        private void StopThread()
        {
            thread.Abort();

        }
     

       
    }
}

