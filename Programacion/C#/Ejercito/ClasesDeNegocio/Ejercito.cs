﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ClasesDeNegocio
{
    public class Ejercito
    {
        private List<Guerrero> mGuerreros = new List<Guerrero>();
         
        public List<Guerrero> guerreros
        {
            get
            {
                return mGuerreros;
            }
            set
            {
                mGuerreros = value;
            }
        }

        public void AgregarGuerrero(TipoGuerrero tipoGuerrero)
        {
            Guerrero guerrero = new Guerrero(tipoGuerrero);
            mGuerreros.Add(guerrero);
            EAtacar += guerrero.EventAtacar;
            EReplegarse += guerrero.EventReplegar;
            
        }
        public event EventHandler EAtacar;
        public event EventHandler EReplegarse;
        public void Atacar()
        {
            foreach (Guerrero item in mGuerreros)
            {
                EAtacar(item,null);
            }
        }
        public void Atacar(TipoGuerrero tipoGuerrero)
        {
            foreach (Guerrero item in mGuerreros)
            {
                if(item.tipoGuerrero1 == tipoGuerrero)
                {
                    EAtacar(item, null);
                }
            }
        }
        public void Replegar (TipoGuerrero tipoGuerrero)
        {
            foreach (Guerrero item in mGuerreros)
            {
                if (item.tipoGuerrero1 == tipoGuerrero)
                {
                    EReplegarse(item,null);
                }
            }
        }
    }
}
