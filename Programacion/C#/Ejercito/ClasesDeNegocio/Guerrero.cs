﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClasesDeNegocio
{
    public enum TipoGuerrero
    {
        Soldado,
        Arquero,
        Caballero
    }


     public class Guerrero
    {
        private TipoGuerrero mTipoGuerrero1;
        public TipoGuerrero tipoGuerrero1
        {
            get
            {
                return mTipoGuerrero1;
            }
        }
        private bool mAtacando;
        public bool Atacando
        {
            get
            {
                return mAtacando;
            }
            internal set
            {
                mAtacando = value;
            }
        }


        public Guerrero (TipoGuerrero tipoGuerrero)
        {
            mTipoGuerrero1 = tipoGuerrero;

        }

        public void EventAtacar(object sender , EventArgs e)
        {
            ((Guerrero)sender).mAtacando = true;
        }
        public void EventReplegar (object sender, EventArgs e)
        {
            ((Guerrero)sender).mAtacando = false;
        }
    }
}
