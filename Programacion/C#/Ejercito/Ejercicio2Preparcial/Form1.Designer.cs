﻿
namespace Ejercicio2Preparcial
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdGuerreros = new System.Windows.Forms.DataGridView();
            this.BtnAgregarEjercito = new System.Windows.Forms.Button();
            this.cmbTipoEjercito = new System.Windows.Forms.ComboBox();
            this.cmbAtacarReplegarse = new System.Windows.Forms.ComboBox();
            this.btnAtacar = new System.Windows.Forms.Button();
            this.btnReplegarse = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdGuerreros)).BeginInit();
            this.SuspendLayout();
            // 
            // grdGuerreros
            // 
            this.grdGuerreros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGuerreros.Location = new System.Drawing.Point(81, 49);
            this.grdGuerreros.Name = "grdGuerreros";
            this.grdGuerreros.Size = new System.Drawing.Size(629, 251);
            this.grdGuerreros.TabIndex = 0;
            // 
            // BtnAgregarEjercito
            // 
            this.BtnAgregarEjercito.Location = new System.Drawing.Point(176, 355);
            this.BtnAgregarEjercito.Name = "BtnAgregarEjercito";
            this.BtnAgregarEjercito.Size = new System.Drawing.Size(100, 23);
            this.BtnAgregarEjercito.TabIndex = 1;
            this.BtnAgregarEjercito.Text = "Agregar Ejercito";
            this.BtnAgregarEjercito.UseVisualStyleBackColor = true;
            this.BtnAgregarEjercito.Click += new System.EventHandler(this.BtnAgregarEjercito_Click);
            // 
            // cmbTipoEjercito
            // 
            this.cmbTipoEjercito.FormattingEnabled = true;
            this.cmbTipoEjercito.Location = new System.Drawing.Point(12, 357);
            this.cmbTipoEjercito.Name = "cmbTipoEjercito";
            this.cmbTipoEjercito.Size = new System.Drawing.Size(151, 21);
            this.cmbTipoEjercito.TabIndex = 2;
            this.cmbTipoEjercito.Text = "Tipo de ejercito a agregar";
            // 
            // cmbAtacarReplegarse
            // 
            this.cmbAtacarReplegarse.FormattingEnabled = true;
            this.cmbAtacarReplegarse.Location = new System.Drawing.Point(301, 357);
            this.cmbAtacarReplegarse.Name = "cmbAtacarReplegarse";
            this.cmbAtacarReplegarse.Size = new System.Drawing.Size(262, 21);
            this.cmbAtacarReplegarse.TabIndex = 3;
            this.cmbAtacarReplegarse.Text = "Tipo de ejercito que tiene que atacar o replegarse";
            // 
            // btnAtacar
            // 
            this.btnAtacar.Location = new System.Drawing.Point(569, 355);
            this.btnAtacar.Name = "btnAtacar";
            this.btnAtacar.Size = new System.Drawing.Size(75, 23);
            this.btnAtacar.TabIndex = 4;
            this.btnAtacar.Text = "Atacar";
            this.btnAtacar.UseVisualStyleBackColor = true;
            this.btnAtacar.Click += new System.EventHandler(this.btnAtacar_Click);
            // 
            // btnReplegarse
            // 
            this.btnReplegarse.Location = new System.Drawing.Point(663, 355);
            this.btnReplegarse.Name = "btnReplegarse";
            this.btnReplegarse.Size = new System.Drawing.Size(75, 23);
            this.btnReplegarse.TabIndex = 5;
            this.btnReplegarse.Text = "Replegarse";
            this.btnReplegarse.UseVisualStyleBackColor = true;
            this.btnReplegarse.Click += new System.EventHandler(this.btnReplegarse_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnReplegarse);
            this.Controls.Add(this.btnAtacar);
            this.Controls.Add(this.cmbAtacarReplegarse);
            this.Controls.Add(this.cmbTipoEjercito);
            this.Controls.Add(this.BtnAgregarEjercito);
            this.Controls.Add(this.grdGuerreros);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdGuerreros)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grdGuerreros;
        private System.Windows.Forms.Button BtnAgregarEjercito;
        private System.Windows.Forms.ComboBox cmbTipoEjercito;
        private System.Windows.Forms.ComboBox cmbAtacarReplegarse;
        private System.Windows.Forms.Button btnAtacar;
        private System.Windows.Forms.Button btnReplegarse;
    }
}

