﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClasesDeNegocio;
namespace Ejercicio2Preparcial
{
    public partial class Form1 : Form
    {
        Ejercito ejercito = new Ejercito();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cmbTipoEjercito.Items.Add(TipoGuerrero.Arquero);
            cmbTipoEjercito.Items.Add(TipoGuerrero.Caballero);
            cmbTipoEjercito.Items.Add(TipoGuerrero.Soldado);
            cmbAtacarReplegarse.Items.Add(TipoGuerrero.Arquero);
            cmbAtacarReplegarse.Items.Add(TipoGuerrero.Caballero);
            cmbAtacarReplegarse.Items.Add(TipoGuerrero.Soldado);
            cmbAtacarReplegarse.Items.Add("Todos");
            actualizar();
        }

        private void BtnAgregarEjercito_Click(object sender, EventArgs e)
        {
            if (cmbTipoEjercito.SelectedItem.ToString() == "Arquero")
            {
                ejercito.AgregarGuerrero(TipoGuerrero.Arquero);
            }
            else if (cmbTipoEjercito.SelectedItem.ToString() == "Caballero")
            {
                ejercito.AgregarGuerrero(TipoGuerrero.Caballero);
            }
            else if (cmbTipoEjercito.SelectedItem.ToString() == "Soldado")
            {
                ejercito.AgregarGuerrero(TipoGuerrero.Soldado);
            }
            actualizar();
        }
        void actualizar()
        {
            grdGuerreros.DataSource = null;
            grdGuerreros.DataSource = ejercito.guerreros;
        }

        private void btnAtacar_Click(object sender, EventArgs e)
        {
            if (cmbAtacarReplegarse.SelectedItem.ToString() == "Arquero")
            {
                ejercito.Atacar(TipoGuerrero.Arquero);
            }
            else if (cmbAtacarReplegarse.SelectedItem.ToString() == "Caballero")
            {
                ejercito.Atacar(TipoGuerrero.Caballero);
            }
            else if (cmbAtacarReplegarse.SelectedItem.ToString() == "Soldado")
            {
                ejercito.Atacar(TipoGuerrero.Soldado);
            }
            else if(cmbAtacarReplegarse.SelectedItem.ToString()== "Todos")
            {
                ejercito.Atacar();
            }
            actualizar();
        }

        private void btnReplegarse_Click(object sender, EventArgs e)
        {
            if (cmbAtacarReplegarse.SelectedItem.ToString() == "Arquero")
            {
                ejercito.Replegar(TipoGuerrero.Arquero);
            }
            else if (cmbAtacarReplegarse.SelectedItem.ToString() == "Caballero")
            {
                ejercito.Replegar(TipoGuerrero.Caballero);
            }
            else if (cmbAtacarReplegarse.SelectedItem.ToString() == "Soldado")
            {
                ejercito.Replegar(TipoGuerrero.Soldado);
            }
            else if (cmbAtacarReplegarse.SelectedItem.ToString() == "Todos")
            {
                ejercito.Replegar(TipoGuerrero.Arquero);
                ejercito.Replegar(TipoGuerrero.Caballero);
                ejercito.Replegar(TipoGuerrero.Soldado);

            }
            actualizar();
        }
    }
}
