﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Delantero: Jugador
    {
        public override int regateo
        {
            get
            {
                return 4 * habilidad;
            }
        }

        public override int posesion
        {
            get
            {
                return 3 * habilidad;
            }
        }

        public override int potencia
        {
            get
            {
                return 5 * habilidad;
            }
        }

        public override int velocidad
        {
            get
            {
                return 4 * habilidad;
            }
        }

        public override int agilidad
        {
            get
            {
                return 4 * habilidad;
            }
        }
        public Delantero()
        {
            this.Resistencia = 100;
            this.habilidad = 5;
        }
        public Delantero(int habilidad, int posicion, string nombre)
        {
            this.habilidad = habilidad;
            this.posicion = posicion;
            this.nombre = nombre;
            this.Resistencia = 100;   
        }

    }
}
