﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Equipo:IEnumerable<Jugador>
    {
        private List<Jugador> mJugadores =new List<Jugador>();
        public Entrenador entrenador;
        public int Regateo
        {
            get
            {
                var mRegateo = from a in mJugadores  select a.regateo;
                return (int)(mRegateo.Average()+entrenador.Experiencia*0.1);
                }
            }
        
        public int posesion
        {
            get
            {
                var mPosesion = from a in mJugadores select a.posesion;
                return (int)(mPosesion.Average() + entrenador.Experiencia * 0.1);
            }
        }
        public int Potencia
        {
            get
            {
                var mPotencia = from a in mJugadores select a.potencia;
                return (int)(mPotencia.Average() + entrenador.Experiencia * 0.1);
            }
        }
        public int Velocidad
        {
            get
            {
                var mVelocidad = from a in mJugadores select a.velocidad;
                return (int)(mVelocidad.Average() + entrenador.Experiencia * 0.1);
            }
        }
        public int Agilidad
        {
            get
            {
                var mAgilidad = from a in mJugadores select a.agilidad;
                return (int)(mAgilidad.Average() + entrenador.Experiencia * 0.1);
            }
        }
        public event EventHandler EJugar;
        public void jugar()
        {
            EJugar(this, null);
        }

        public IEnumerator<Jugador> GetEnumerator()
        { 
            return mJugadores.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return mJugadores.GetEnumerator();
        }
        public void AgregarJugador(Jugador jugador)
        {
            EJugar += jugador.Jugar;
            mJugadores.Add(jugador);
        }
    }
}
