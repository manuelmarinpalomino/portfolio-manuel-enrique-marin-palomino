﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public abstract class Jugador
    {
        public static string Equipo;
        public int Resistencia { get; set; }
        public int habilidad { get; set; }
        public int posicion { get; set; }
        public string nombre { get; set; }
        public abstract int regateo { get;  }
        public abstract int posesion { get; }
        public abstract int potencia { get; }
        public abstract int velocidad { get; }
        public abstract int agilidad { get; }
        
        public void Jugar(object e , EventArgs eventArgs)
        { 
            Resistencia -= (int)(velocidad + agilidad / 2);
            if(Resistencia <= 0)
            {
                Resistencia = 0;
            }
        }
    }
}
