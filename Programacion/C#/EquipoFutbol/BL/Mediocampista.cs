﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Mediocampista:Jugador
    {
        public override int regateo
        {
            get
            {
                return 5 * habilidad;
            }
        } 

        public override int posesion {
            get
            {
                return 5 * habilidad;
            }
        }

        public override int potencia {
            get
            {
                return 3 * habilidad;
            }
        }

        public override int velocidad
        {
            get
            {
                return 3 * habilidad;
            }
        }

        public override int agilidad
        {
            get
            {
                return 4 * habilidad;
            }
        }
        public Mediocampista()
        {
            this.Resistencia = 100;
            this.habilidad = 5;
            posicion = 9;
        }
        public Mediocampista(int habilidad, int posicion, string nombre)
        {
            this.habilidad = habilidad;
            this.posicion = posicion;
            this.nombre = nombre;
            this.Resistencia = 100;
        }
    }
}
