﻿
namespace Ejercicio1Preparcial2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdEquipo = new System.Windows.Forms.DataGridView();
            this.btnJugar = new System.Windows.Forms.Button();
            this.lblInfoEquipo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdEquipo)).BeginInit();
            this.SuspendLayout();
            // 
            // grdEquipo
            // 
            this.grdEquipo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEquipo.Location = new System.Drawing.Point(12, 12);
            this.grdEquipo.Name = "grdEquipo";
            this.grdEquipo.Size = new System.Drawing.Size(902, 354);
            this.grdEquipo.TabIndex = 0;
            // 
            // btnJugar
            // 
            this.btnJugar.BackColor = System.Drawing.SystemColors.Control;
            this.btnJugar.Location = new System.Drawing.Point(370, 402);
            this.btnJugar.Name = "btnJugar";
            this.btnJugar.Size = new System.Drawing.Size(75, 23);
            this.btnJugar.TabIndex = 1;
            this.btnJugar.Text = "Jugar";
            this.btnJugar.UseVisualStyleBackColor = false;
            this.btnJugar.Click += new System.EventHandler(this.btnJugar_Click);
            // 
            // lblInfoEquipo
            // 
            this.lblInfoEquipo.AutoSize = true;
            this.lblInfoEquipo.Location = new System.Drawing.Point(505, 379);
            this.lblInfoEquipo.Name = "lblInfoEquipo";
            this.lblInfoEquipo.Size = new System.Drawing.Size(35, 13);
            this.lblInfoEquipo.TabIndex = 2;
            this.lblInfoEquipo.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 514);
            this.Controls.Add(this.lblInfoEquipo);
            this.Controls.Add(this.btnJugar);
            this.Controls.Add(this.grdEquipo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdEquipo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdEquipo;
        private System.Windows.Forms.Button btnJugar;
        private System.Windows.Forms.Label lblInfoEquipo;
    }
}

