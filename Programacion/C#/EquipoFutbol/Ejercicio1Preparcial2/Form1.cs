﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;


namespace Ejercicio1Preparcial2
{
    public partial class Form1 : Form
    {
        public Equipo equipo = new Equipo();
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {

            Jugador.Equipo = "Epsilon";
            Entrenador entrenador = new Entrenador() { Experiencia = 10, nombre = "Ihuen" };
            equipo.entrenador = entrenador;
            equipo.AgregarJugador(new Arquero(10, 1, "Juan"));
            equipo.AgregarJugador(new Defensor(7, 2, "Pedro"));
            equipo.AgregarJugador(new Defensor(7, 3, "Luis"));
            equipo.AgregarJugador(new Defensor(7, 4, "Juan"));
            equipo.AgregarJugador(new Mediocampista(8, 6, "Pedro"));
            equipo.AgregarJugador(new Mediocampista(8, 7, "Manuel"));
            equipo.AgregarJugador(new Mediocampista(8, 8, "Andres"));
            equipo.AgregarJugador(new Mediocampista(8, 9, "Gianluca"));
            equipo.AgregarJugador(new Delantero(100, 10, "Angel"));
            equipo.AgregarJugador(new Delantero(10, 11, "Lazaro"));
            equipo.AgregarJugador(new Defensor(7, 5, "Octavio"));
            grdEquipo.Columns.Add("Nombre", "Nombre");
            grdEquipo.Columns.Add("Posicion", "Posicion");
            grdEquipo.Columns.Add("Resistencia", "Resistencia");
            grdEquipo.Columns.Add("Habilidad", "Habilidad");
            grdEquipo.Columns.Add("Regateo", "Regateo");
            grdEquipo.Columns.Add("Posesion", "Posesion");
            grdEquipo.Columns.Add("Potencia", "Potencia");
            grdEquipo.Columns.Add("Velocidad", "Velocidad");
            grdEquipo.Columns.Add("Agilidad", "Agilidad");

            ActualizarGrdEquipo();
        }
        private void ActualizarGrdEquipo()
        {
            grdEquipo.Rows.Clear();
            foreach(Jugador j in equipo)
            {
                grdEquipo.Rows.Add(j.nombre, j.posicion, j.Resistencia, j.habilidad, j.regateo, j.posesion, j.potencia, j.velocidad, j.agilidad);
            }
            lblInfoEquipo.Text = "DT: " + equipo.entrenador.nombre + "Exp: " + equipo.entrenador.Experiencia + Environment.NewLine +
            "Datos del equipo: " + Environment.NewLine + "  Potencia: " + equipo.Potencia + Environment.NewLine + "    Posesion: " + equipo.posesion + Environment.NewLine + "Regateo    " + equipo.Regateo + Environment.NewLine + "    Velocidad" + equipo.Velocidad
            + Environment.NewLine + "    Agilidad: " + equipo.Agilidad;

        }

        private void btnJugar_Click(object sender, EventArgs e)
        {
            equipo.jugar();
            ActualizarGrdEquipo();
        }
    }
}
