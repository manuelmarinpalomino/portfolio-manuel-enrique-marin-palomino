﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraficosGDI
{
    public partial class GraficoPersistente : Form
    {
        public GraficoPersistente()
        {
            InitializeComponent();
        }

        private void GraficoPersistente_Paint(object sender, PaintEventArgs e)
        {
             Graphics mGr = e.Graphics;
             mGr.DrawEllipse(Pens.Blue, 20, 20, 100, 200);
        }

        private void GraficoPersistente_Load(object sender, EventArgs e)
        {

        }
    }
}
