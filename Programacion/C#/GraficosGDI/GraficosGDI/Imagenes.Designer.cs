﻿namespace GraficosGDI
{
    partial class Imagenes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button2 = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Button2
            // 
            this.Button2.Location = new System.Drawing.Point(26, 392);
            this.Button2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(285, 55);
            this.Button2.TabIndex = 17;
            this.Button2.Text = "Transparentar color";
            this.Button2.UseVisualStyleBackColor = true;
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(26, 75);
            this.Button1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(285, 55);
            this.Button1.TabIndex = 10;
            this.Button1.Text = "Cargar imagen";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Imagenes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2078, 1285);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Name = "Imagenes";
            this.Text = "Imagenes";
            this.Load += new System.EventHandler(this.Imagenes_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Imaging_MouseClick);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button1;
    }
}