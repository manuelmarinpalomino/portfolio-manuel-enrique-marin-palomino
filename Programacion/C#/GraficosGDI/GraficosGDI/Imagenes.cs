﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace GraficosGDI
{
    public partial class Imagenes : Form
    {
        public Imagenes()
        {
            InitializeComponent();
        }

        Bitmap mBMP; 

        private void Button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog mOf= new OpenFileDialog();
         
            mOf.Multiselect = false;
            mOf.CheckFileExists = true;
            mOf.ShowReadOnly = false;
            mOf.Filter = @"Todos los archivos|*.*|Imagenes|*.bmp;*.gif;*.jpg";
            mOf.FilterIndex = 2;
            if(mOf.ShowDialog() == DialogResult.OK)
            {
                mBMP = new Bitmap(mOf.FileName);

                Graphics mGr  = this.CreateGraphics();

                mGr.DrawImage(mBMP, 300, 100);

                mGr.Dispose();
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
             if (mBMP != null) 
             {
                ColorDialog mCS  = new ColorDialog ();
               
                    mCS.AllowFullOpen = true;
                    mCS.AnyColor = true;
                    if(mCS.ShowDialog() ==  System.Windows.Forms.DialogResult.OK)
                    {
                        mBMP.MakeTransparent(mCS.Color);
                        Graphics mGr = this.CreateGraphics();
                        mGr.DrawImage(mBMP, 300, 100);
                        mGr.Dispose();
                    }

             }

        }

        private void Imaging_MouseClick(object sender, MouseEventArgs e)
        {
            Color mColor; 
            if(e.X >= 300 && e.X <= 300 + mBMP.Width && e.Y >= 100 && e.Y <= 100 + mBMP.Height)
            {
                mColor = mBMP.GetPixel(e.X - 300, e.Y - 100);
                mBMP.MakeTransparent(mColor);
                Graphics mGr = this.CreateGraphics();
                mGr.Clear(BackColor);
                mGr.DrawImage(mBMP, 300, 100);
                mGr.Dispose();
            }
               
        }

        private void Imagenes_Load(object sender, EventArgs e)
        {

        }
    }
}
