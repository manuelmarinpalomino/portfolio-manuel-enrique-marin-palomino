﻿namespace GraficosGDI
{
    partial class Lienzo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(632, 401);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(44, 21);
            this.Button1.TabIndex = 1;
            this.Button1.Text = "Borrar";
            this.Button1.UseVisualStyleBackColor = true;
            // 
            // Lienzo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 432);
            this.Controls.Add(this.Button1);
            this.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.Name = "Lienzo";
            this.Text = "Lienzo";
            this.Load += new System.EventHandler(this.Lienzo_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Lienzo_MouseClick);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button Button1;
    }
}