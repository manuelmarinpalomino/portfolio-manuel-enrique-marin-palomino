﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraficosGDI
{
    public partial class Lienzo : Form
    {

        public bool CapturarPuntos = false;

        public Lienzo()
        {
            InitializeComponent();
        }

        private void Lienzo_Load(object sender, EventArgs e)
        {
            Form.CheckForIllegalCrossThreadCalls = false;
        }

        private void Lienzo_MouseClick(object sender, MouseEventArgs e)
        {
             if(CapturarPuntos)
             {
               ((Vectoriales) ((Principal)Owner).ActiveMdiChild).lstPuntos.Items.Add(e.X.ToString() + "," + e.Y.ToString());
             }
                
        }

         public void MostrarMensaje(string pMensaje)
         {
             MessageBox.Show(pMensaje);
         }

    }
}
