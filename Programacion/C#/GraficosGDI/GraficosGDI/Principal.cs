﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraficosGDI
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void vectorialesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vectoriales mForm = new Vectoriales();
            mForm.MdiParent = this;
            mForm.WindowState = FormWindowState.Maximized;
            mForm.Show();
        }

        private void imágenesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Imagenes mForm = new Imagenes();
            mForm.MdiParent = this;
            mForm.WindowState = FormWindowState.Maximized;
            mForm.Show();
        }

        private void texoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Texto mForm = new Texto();
            mForm.MdiParent = this;
            mForm.WindowState = FormWindowState.Maximized;
            mForm.Show();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            this.Height = 600;
            this.Width = 1000;
        }
    }
}
