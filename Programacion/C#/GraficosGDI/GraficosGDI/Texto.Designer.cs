﻿namespace GraficosGDI
{
    partial class Texto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label4 = new System.Windows.Forms.Label();
            this.txtTexto = new System.Windows.Forms.TextBox();
            this.chkBold = new System.Windows.Forms.CheckBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.btnEscribir = new System.Windows.Forms.Button();
            this.cmbFont = new System.Windows.Forms.ComboBox();
            this.cmbColor = new System.Windows.Forms.ComboBox();
            this.txtSize = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(76, 56);
            this.Label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(86, 32);
            this.Label4.TabIndex = 20;
            this.Label4.Text = "Texto";
            // 
            // txtTexto
            // 
            this.txtTexto.Location = new System.Drawing.Point(182, 49);
            this.txtTexto.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtTexto.Name = "txtTexto";
            this.txtTexto.Size = new System.Drawing.Size(1007, 38);
            this.txtTexto.TabIndex = 19;
            // 
            // chkBold
            // 
            this.chkBold.AutoSize = true;
            this.chkBold.Location = new System.Drawing.Point(889, 218);
            this.chkBold.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.chkBold.Name = "chkBold";
            this.chkBold.Size = new System.Drawing.Size(111, 36);
            this.chkBold.TabIndex = 18;
            this.chkBold.Text = "Bold";
            this.chkBold.UseVisualStyleBackColor = true;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(513, 220);
            this.Label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(71, 32);
            this.Label3.TabIndex = 17;
            this.Label3.Text = "Size";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(84, 220);
            this.Label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(83, 32);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "Color";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(84, 156);
            this.Label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(72, 32);
            this.Label1.TabIndex = 15;
            this.Label1.Text = "Font";
            // 
            // btnEscribir
            // 
            this.btnEscribir.Location = new System.Drawing.Point(596, 344);
            this.btnEscribir.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnEscribir.Name = "btnEscribir";
            this.btnEscribir.Size = new System.Drawing.Size(200, 55);
            this.btnEscribir.TabIndex = 14;
            this.btnEscribir.Text = "Escribir";
            this.btnEscribir.UseVisualStyleBackColor = true;
            this.btnEscribir.Click += new System.EventHandler(this.btnEscribir_Click);
            // 
            // cmbFont
            // 
            this.cmbFont.FormattingEnabled = true;
            this.cmbFont.Location = new System.Drawing.Point(182, 149);
            this.cmbFont.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.cmbFont.Name = "cmbFont";
            this.cmbFont.Size = new System.Drawing.Size(1007, 39);
            this.cmbFont.TabIndex = 13;
            // 
            // cmbColor
            // 
            this.cmbColor.FormattingEnabled = true;
            this.cmbColor.Location = new System.Drawing.Point(182, 213);
            this.cmbColor.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.cmbColor.Name = "cmbColor";
            this.cmbColor.Size = new System.Drawing.Size(284, 39);
            this.cmbColor.TabIndex = 12;
            // 
            // txtSize
            // 
            this.txtSize.Location = new System.Drawing.Point(630, 213);
            this.txtSize.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtSize.Name = "txtSize";
            this.txtSize.Size = new System.Drawing.Size(159, 38);
            this.txtSize.TabIndex = 11;
            // 
            // Texto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1686, 932);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.txtTexto);
            this.Controls.Add(this.chkBold);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.btnEscribir);
            this.Controls.Add(this.cmbFont);
            this.Controls.Add(this.cmbColor);
            this.Controls.Add(this.txtSize);
            this.Name = "Texto";
            this.Text = "Texto";
            this.Load += new System.EventHandler(this.Texto_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox txtTexto;
        internal System.Windows.Forms.CheckBox chkBold;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btnEscribir;
        internal System.Windows.Forms.ComboBox cmbFont;
        internal System.Windows.Forms.ComboBox cmbColor;
        internal System.Windows.Forms.TextBox txtSize;
    }
}