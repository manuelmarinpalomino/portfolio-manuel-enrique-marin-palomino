﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;

namespace GraficosGDI
{
    public partial class Texto : Form
    {
        public Texto()
        {
            InitializeComponent();
        }

        private void Texto_Load(object sender, EventArgs e)
        {
             InstalledFontCollection mInstFonts = new InstalledFontCollection();
              FontFamily[] mFonts  = mInstFonts.Families;

        foreach(FontFamily mFam in mFonts)
            cmbFont.Items.Add(mFam.Name);

        cmbColor.Items.Add(Color.Blue);
        cmbColor.Items.Add(Color.Black);
        cmbColor.Items.Add(Color.White);
        cmbColor.Items.Add(Color.Pink);
        cmbColor.Items.Add(Color.Red);
        cmbColor.Items.Add(Color.Green);
        }

        private void btnEscribir_Click(object sender, EventArgs e)
        {
            Graphics mGr  = this.CreateGraphics();

            FontStyle mFS;
            if (chkBold.Checked) 
                mFS = FontStyle.Bold; 
            else
                mFS = FontStyle.Regular;

            mGr.DrawString(txtTexto.Text, new Font(cmbFont.SelectedItem.ToString(), int.Parse(txtSize.Text), mFS, GraphicsUnit.Pixel), ObtenerBrush(), 100, 200);

            mGr.Dispose();
        }

        private Brush ObtenerBrush()
        {
            Color mCol  = (Color)cmbColor.SelectedItem;
        if(mCol != null) 
        {
            switch(mCol.Name)
            {
                case "Blue":
                    return Brushes.Blue;
                case "Black":
                    return Brushes.Black;
                case "White":
                    return Brushes.White;
                case "Pink":
                    return Brushes.Pink;
                case "Red":
                    return Brushes.Red;
                case "Green":
                    return Brushes.Green;
                default:
                    return Brushes.Black;
            }
            
        }
        else
            return Brushes.Black;
        }
       
    }
}
