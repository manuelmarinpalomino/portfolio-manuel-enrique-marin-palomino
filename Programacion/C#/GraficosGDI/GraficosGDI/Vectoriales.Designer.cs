﻿namespace GraficosGDI
{
    partial class Vectoriales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button18 = new System.Windows.Forms.Button();
            this.GroupBox8 = new System.Windows.Forms.GroupBox();
            this.Button17 = new System.Windows.Forms.Button();
            this.Button16 = new System.Windows.Forms.Button();
            this.GroupBox7 = new System.Windows.Forms.GroupBox();
            this.txtTransAncho = new System.Windows.Forms.TextBox();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.txtTransRot = new System.Windows.Forms.TextBox();
            this.Label17 = new System.Windows.Forms.Label();
            this.txtTransAlto = new System.Windows.Forms.TextBox();
            this.txtTransX = new System.Windows.Forms.TextBox();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.txtTransY = new System.Windows.Forms.TextBox();
            this.Button12 = new System.Windows.Forms.Button();
            this.GroupBox6 = new System.Windows.Forms.GroupBox();
            this.chkAlternar = new System.Windows.Forms.CheckBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.cmbTipoRelleno = new System.Windows.Forms.ComboBox();
            this.Button13 = new System.Windows.Forms.Button();
            this.Button14 = new System.Windows.Forms.Button();
            this.Button15 = new System.Windows.Forms.Button();
            this.Label11 = new System.Windows.Forms.Label();
            this.Button11 = new System.Windows.Forms.Button();
            this.Button10 = new System.Windows.Forms.Button();
            this.pctImagen = new System.Windows.Forms.PictureBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.txtTension = new System.Windows.Forms.TextBox();
            this.Button9 = new System.Windows.Forms.Button();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.cmbEstilo = new System.Windows.Forms.ComboBox();
            this.txtAnchoLapiz = new System.Windows.Forms.TextBox();
            this.cmbColor = new System.Windows.Forms.ComboBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.Button4 = new System.Windows.Forms.Button();
            this.Button7 = new System.Windows.Forms.Button();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.txtAnguloFin = new System.Windows.Forms.TextBox();
            this.txtAnguloComienzo = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.Button8 = new System.Windows.Forms.Button();
            this.Button6 = new System.Windows.Forms.Button();
            this.Button5 = new System.Windows.Forms.Button();
            this.lstPuntos = new System.Windows.Forms.ListBox();
            this.Button3 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.Label5 = new System.Windows.Forms.Label();
            this.txtCentroY = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.txtAlto = new System.Windows.Forms.TextBox();
            this.txtAncho = new System.Windows.Forms.TextBox();
            this.txtCentroX = new System.Windows.Forms.TextBox();
            this.Button1 = new System.Windows.Forms.Button();
            this.GroupBox8.SuspendLayout();
            this.GroupBox7.SuspendLayout();
            this.GroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctImagen)).BeginInit();
            this.GroupBox1.SuspendLayout();
            this.GroupBox5.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.GroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Button18
            // 
            this.Button18.Location = new System.Drawing.Point(534, 166);
            this.Button18.Name = "Button18";
            this.Button18.Size = new System.Drawing.Size(115, 23);
            this.Button18.TabIndex = 6;
            this.Button18.Text = "Grafico persistente";
            this.Button18.UseVisualStyleBackColor = true;
            this.Button18.Click += new System.EventHandler(this.Button18_Click);
            // 
            // GroupBox8
            // 
            this.GroupBox8.Controls.Add(this.Button17);
            this.GroupBox8.Controls.Add(this.Button16);
            this.GroupBox8.Location = new System.Drawing.Point(502, 17);
            this.GroupBox8.Name = "GroupBox8";
            this.GroupBox8.Size = new System.Drawing.Size(181, 128);
            this.GroupBox8.TabIndex = 8;
            this.GroupBox8.TabStop = false;
            this.GroupBox8.Text = "Movimiento";
            // 
            // Button17
            // 
            this.Button17.Location = new System.Drawing.Point(32, 66);
            this.Button17.Name = "Button17";
            this.Button17.Size = new System.Drawing.Size(115, 23);
            this.Button17.TabIndex = 1;
            this.Button17.Text = "Detener pelota";
            this.Button17.UseVisualStyleBackColor = true;
            this.Button17.Click += new System.EventHandler(this.Button17_Click);
            // 
            // Button16
            // 
            this.Button16.Location = new System.Drawing.Point(32, 41);
            this.Button16.Name = "Button16";
            this.Button16.Size = new System.Drawing.Size(115, 23);
            this.Button16.TabIndex = 0;
            this.Button16.Text = "Rebotar pelota";
            this.Button16.UseVisualStyleBackColor = true;
            this.Button16.Click += new System.EventHandler(this.Button16_Click);
            // 
            // GroupBox7
            // 
            this.GroupBox7.Controls.Add(this.txtTransAncho);
            this.GroupBox7.Controls.Add(this.Label15);
            this.GroupBox7.Controls.Add(this.Label16);
            this.GroupBox7.Controls.Add(this.txtTransRot);
            this.GroupBox7.Controls.Add(this.Label17);
            this.GroupBox7.Controls.Add(this.txtTransAlto);
            this.GroupBox7.Controls.Add(this.txtTransX);
            this.GroupBox7.Controls.Add(this.Label13);
            this.GroupBox7.Controls.Add(this.Label14);
            this.GroupBox7.Controls.Add(this.txtTransY);
            this.GroupBox7.Controls.Add(this.Button12);
            this.GroupBox7.Location = new System.Drawing.Point(271, 252);
            this.GroupBox7.Name = "GroupBox7";
            this.GroupBox7.Size = new System.Drawing.Size(225, 160);
            this.GroupBox7.TabIndex = 7;
            this.GroupBox7.TabStop = false;
            this.GroupBox7.Text = "Transformaciones";
            // 
            // txtTransAncho
            // 
            this.txtTransAncho.Location = new System.Drawing.Point(96, 45);
            this.txtTransAncho.Name = "txtTransAncho";
            this.txtTransAncho.Size = new System.Drawing.Size(31, 20);
            this.txtTransAncho.TabIndex = 33;
            this.txtTransAncho.Text = "0,1";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(21, 74);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(36, 13);
            this.Label15.TabIndex = 28;
            this.Label15.Text = "Rotar:";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(21, 48);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(78, 13);
            this.Label16.TabIndex = 32;
            this.Label16.Text = "Escala (ancho)";
            // 
            // txtTransRot
            // 
            this.txtTransRot.Location = new System.Drawing.Point(63, 71);
            this.txtTransRot.Name = "txtTransRot";
            this.txtTransRot.Size = new System.Drawing.Size(31, 20);
            this.txtTransRot.TabIndex = 29;
            this.txtTransRot.Text = "0";
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Location = new System.Drawing.Point(124, 48);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(30, 13);
            this.Label17.TabIndex = 31;
            this.Label17.Text = "(alto)";
            // 
            // txtTransAlto
            // 
            this.txtTransAlto.Location = new System.Drawing.Point(153, 45);
            this.txtTransAlto.Name = "txtTransAlto";
            this.txtTransAlto.Size = new System.Drawing.Size(28, 20);
            this.txtTransAlto.TabIndex = 30;
            this.txtTransAlto.Text = "0,1";
            // 
            // txtTransX
            // 
            this.txtTransX.Location = new System.Drawing.Point(96, 19);
            this.txtTransX.Name = "txtTransX";
            this.txtTransX.Size = new System.Drawing.Size(31, 20);
            this.txtTransX.TabIndex = 27;
            this.txtTransX.Text = "100";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(21, 22);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(73, 13);
            this.Label13.TabIndex = 26;
            this.Label13.Text = "Trasladar:  (X)";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(129, 22);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(20, 13);
            this.Label14.TabIndex = 25;
            this.Label14.Text = "(Y)";
            // 
            // txtTransY
            // 
            this.txtTransY.Location = new System.Drawing.Point(153, 19);
            this.txtTransY.Name = "txtTransY";
            this.txtTransY.Size = new System.Drawing.Size(28, 20);
            this.txtTransY.TabIndex = 24;
            this.txtTransY.Text = "100";
            // 
            // Button12
            // 
            this.Button12.Location = new System.Drawing.Point(69, 121);
            this.Button12.Name = "Button12";
            this.Button12.Size = new System.Drawing.Size(72, 23);
            this.Button12.TabIndex = 23;
            this.Button12.Text = "Rectángulo";
            this.Button12.UseVisualStyleBackColor = true;
            this.Button12.Click += new System.EventHandler(this.Button12_Click);
            // 
            // GroupBox6
            // 
            this.GroupBox6.Controls.Add(this.chkAlternar);
            this.GroupBox6.Controls.Add(this.Label12);
            this.GroupBox6.Controls.Add(this.cmbTipoRelleno);
            this.GroupBox6.Controls.Add(this.Button13);
            this.GroupBox6.Controls.Add(this.Button14);
            this.GroupBox6.Controls.Add(this.Button15);
            this.GroupBox6.Controls.Add(this.Label11);
            this.GroupBox6.Controls.Add(this.Button11);
            this.GroupBox6.Controls.Add(this.Button10);
            this.GroupBox6.Controls.Add(this.pctImagen);
            this.GroupBox6.Location = new System.Drawing.Point(271, 17);
            this.GroupBox6.Name = "GroupBox6";
            this.GroupBox6.Size = new System.Drawing.Size(225, 229);
            this.GroupBox6.TabIndex = 5;
            this.GroupBox6.TabStop = false;
            this.GroupBox6.Text = "Formas y rellenos";
            // 
            // chkAlternar
            // 
            this.chkAlternar.AutoSize = true;
            this.chkAlternar.Location = new System.Drawing.Point(6, 153);
            this.chkAlternar.Name = "chkAlternar";
            this.chkAlternar.Size = new System.Drawing.Size(96, 17);
            this.chkAlternar.TabIndex = 22;
            this.chkAlternar.Text = "Alternar relleno";
            this.chkAlternar.UseVisualStyleBackColor = true;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(10, 63);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(80, 13);
            this.Label12.TabIndex = 21;
            this.Label12.Text = "Tipo de relleno:";
            // 
            // cmbTipoRelleno
            // 
            this.cmbTipoRelleno.FormattingEnabled = true;
            this.cmbTipoRelleno.Location = new System.Drawing.Point(96, 60);
            this.cmbTipoRelleno.Name = "cmbTipoRelleno";
            this.cmbTipoRelleno.Size = new System.Drawing.Size(123, 21);
            this.cmbTipoRelleno.TabIndex = 9;
            // 
            // Button13
            // 
            this.Button13.Location = new System.Drawing.Point(145, 187);
            this.Button13.Name = "Button13";
            this.Button13.Size = new System.Drawing.Size(75, 23);
            this.Button13.TabIndex = 20;
            this.Button13.Text = "Polígono";
            this.Button13.UseVisualStyleBackColor = true;
            this.Button13.Click += new System.EventHandler(this.Button13_Click);
            // 
            // Button14
            // 
            this.Button14.Location = new System.Drawing.Point(69, 187);
            this.Button14.Name = "Button14";
            this.Button14.Size = new System.Drawing.Size(72, 23);
            this.Button14.TabIndex = 19;
            this.Button14.Text = "Rectángulo";
            this.Button14.UseVisualStyleBackColor = true;
            this.Button14.Click += new System.EventHandler(this.Button14_Click);
            // 
            // Button15
            // 
            this.Button15.Location = new System.Drawing.Point(6, 187);
            this.Button15.Name = "Button15";
            this.Button15.Size = new System.Drawing.Size(60, 23);
            this.Button15.TabIndex = 18;
            this.Button15.Text = "Ellipse";
            this.Button15.UseVisualStyleBackColor = true;
            this.Button15.Click += new System.EventHandler(this.Button15_Click);
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(6, 91);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(94, 13);
            this.Label11.TabIndex = 9;
            this.Label11.Text = "Imagen de relleno:";
            // 
            // Button11
            // 
            this.Button11.Location = new System.Drawing.Point(6, 118);
            this.Button11.Name = "Button11";
            this.Button11.Size = new System.Drawing.Size(88, 23);
            this.Button11.TabIndex = 7;
            this.Button11.Text = "Seleccionar";
            this.Button11.UseVisualStyleBackColor = true;
            this.Button11.Click += new System.EventHandler(this.Button11_Click);
            // 
            // Button10
            // 
            this.Button10.Location = new System.Drawing.Point(34, 19);
            this.Button10.Name = "Button10";
            this.Button10.Size = new System.Drawing.Size(122, 23);
            this.Button10.TabIndex = 6;
            this.Button10.Text = "Cubo (con Path)";
            this.Button10.UseVisualStyleBackColor = true;
            this.Button10.Click += new System.EventHandler(this.Button10_Click);
            // 
            // pctImagen
            // 
            this.pctImagen.Location = new System.Drawing.Point(100, 91);
            this.pctImagen.Name = "pctImagen";
            this.pctImagen.Size = new System.Drawing.Size(119, 50);
            this.pctImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pctImagen.TabIndex = 0;
            this.pctImagen.TabStop = false;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.GroupBox5);
            this.GroupBox1.Controls.Add(this.Button9);
            this.GroupBox1.Controls.Add(this.GroupBox4);
            this.GroupBox1.Controls.Add(this.Button4);
            this.GroupBox1.Controls.Add(this.Button7);
            this.GroupBox1.Controls.Add(this.GroupBox2);
            this.GroupBox1.Controls.Add(this.GroupBox3);
            this.GroupBox1.Controls.Add(this.Button3);
            this.GroupBox1.Controls.Add(this.Button2);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.txtCentroY);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Controls.Add(this.txtAlto);
            this.GroupBox1.Controls.Add(this.txtAncho);
            this.GroupBox1.Controls.Add(this.txtCentroX);
            this.GroupBox1.Controls.Add(this.Button1);
            this.GroupBox1.Location = new System.Drawing.Point(10, 17);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(255, 396);
            this.GroupBox1.TabIndex = 4;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Curvas, lineas, elipses y polígonos";
            // 
            // GroupBox5
            // 
            this.GroupBox5.Controls.Add(this.Label10);
            this.GroupBox5.Controls.Add(this.txtTension);
            this.GroupBox5.Location = new System.Drawing.Point(11, 296);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Size = new System.Drawing.Size(228, 41);
            this.GroupBox5.TabIndex = 1;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Text = "Splines cardinales";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(6, 17);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(48, 13);
            this.Label10.TabIndex = 3;
            this.Label10.Text = "Tensión:";
            // 
            // txtTension
            // 
            this.txtTension.Location = new System.Drawing.Point(60, 14);
            this.txtTension.Name = "txtTension";
            this.txtTension.Size = new System.Drawing.Size(27, 20);
            this.txtTension.TabIndex = 2;
            this.txtTension.Text = "0,5";
            // 
            // Button9
            // 
            this.Button9.Location = new System.Drawing.Point(166, 367);
            this.Button9.Name = "Button9";
            this.Button9.Size = new System.Drawing.Size(75, 23);
            this.Button9.TabIndex = 18;
            this.Button9.Text = "Bezier";
            this.Button9.UseVisualStyleBackColor = true;
            this.Button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.Label8);
            this.GroupBox4.Controls.Add(this.Label9);
            this.GroupBox4.Controls.Add(this.cmbEstilo);
            this.GroupBox4.Controls.Add(this.txtAnchoLapiz);
            this.GroupBox4.Controls.Add(this.cmbColor);
            this.GroupBox4.Controls.Add(this.Label7);
            this.GroupBox4.Location = new System.Drawing.Point(11, 74);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(228, 71);
            this.GroupBox4.TabIndex = 1;
            this.GroupBox4.TabStop = false;
            this.GroupBox4.Text = "Lápiz";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(6, 41);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(35, 13);
            this.Label8.TabIndex = 2;
            this.Label8.Text = "Estilo:";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(149, 44);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(41, 13);
            this.Label9.TabIndex = 3;
            this.Label9.Text = "Ancho:";
            // 
            // cmbEstilo
            // 
            this.cmbEstilo.FormattingEnabled = true;
            this.cmbEstilo.Location = new System.Drawing.Point(46, 38);
            this.cmbEstilo.Name = "cmbEstilo";
            this.cmbEstilo.Size = new System.Drawing.Size(97, 21);
            this.cmbEstilo.TabIndex = 4;
            // 
            // txtAnchoLapiz
            // 
            this.txtAnchoLapiz.Location = new System.Drawing.Point(196, 38);
            this.txtAnchoLapiz.Name = "txtAnchoLapiz";
            this.txtAnchoLapiz.Size = new System.Drawing.Size(26, 20);
            this.txtAnchoLapiz.TabIndex = 5;
            this.txtAnchoLapiz.Text = "1";
            // 
            // cmbColor
            // 
            this.cmbColor.FormattingEnabled = true;
            this.cmbColor.Location = new System.Drawing.Point(46, 13);
            this.cmbColor.Name = "cmbColor";
            this.cmbColor.Size = new System.Drawing.Size(176, 21);
            this.cmbColor.TabIndex = 10;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(6, 16);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(34, 13);
            this.Label7.TabIndex = 14;
            this.Label7.Text = "Color:";
            // 
            // Button4
            // 
            this.Button4.Location = new System.Drawing.Point(89, 367);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(75, 23);
            this.Button4.TabIndex = 2;
            this.Button4.Text = "Cardinal";
            this.Button4.UseVisualStyleBackColor = true;
            this.Button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // Button7
            // 
            this.Button7.Location = new System.Drawing.Point(11, 367);
            this.Button7.Name = "Button7";
            this.Button7.Size = new System.Drawing.Size(75, 23);
            this.Button7.TabIndex = 17;
            this.Button7.Text = "Polígono";
            this.Button7.UseVisualStyleBackColor = true;
            this.Button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.txtAnguloFin);
            this.GroupBox2.Controls.Add(this.txtAnguloComienzo);
            this.GroupBox2.Controls.Add(this.Label4);
            this.GroupBox2.Controls.Add(this.Label6);
            this.GroupBox2.Location = new System.Drawing.Point(11, 151);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(228, 53);
            this.GroupBox2.TabIndex = 1;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Arcos (angulo)";
            // 
            // txtAnguloFin
            // 
            this.txtAnguloFin.Location = new System.Drawing.Point(169, 20);
            this.txtAnguloFin.Name = "txtAnguloFin";
            this.txtAnguloFin.Size = new System.Drawing.Size(35, 20);
            this.txtAnguloFin.TabIndex = 17;
            this.txtAnguloFin.Text = "90";
            // 
            // txtAnguloComienzo
            // 
            this.txtAnguloComienzo.Location = new System.Drawing.Point(73, 19);
            this.txtAnguloComienzo.Name = "txtAnguloComienzo";
            this.txtAnguloComienzo.Size = new System.Drawing.Size(35, 20);
            this.txtAnguloComienzo.TabIndex = 1;
            this.txtAnguloComienzo.Text = "0";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(138, 23);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(24, 13);
            this.Label4.TabIndex = 11;
            this.Label4.Text = "Fin:";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(10, 23);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(56, 13);
            this.Label6.TabIndex = 13;
            this.Label6.Text = "Comienzo:";
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.Button8);
            this.GroupBox3.Controls.Add(this.Button6);
            this.GroupBox3.Controls.Add(this.Button5);
            this.GroupBox3.Controls.Add(this.lstPuntos);
            this.GroupBox3.Location = new System.Drawing.Point(11, 208);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(228, 88);
            this.GroupBox3.TabIndex = 1;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Polígonos y splines (puntos)";
            // 
            // Button8
            // 
            this.Button8.Location = new System.Drawing.Point(113, 60);
            this.Button8.Name = "Button8";
            this.Button8.Size = new System.Drawing.Size(109, 22);
            this.Button8.TabIndex = 3;
            this.Button8.Text = "Borrar";
            this.Button8.UseVisualStyleBackColor = true;
            this.Button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // Button6
            // 
            this.Button6.Location = new System.Drawing.Point(113, 37);
            this.Button6.Name = "Button6";
            this.Button6.Size = new System.Drawing.Size(109, 22);
            this.Button6.TabIndex = 2;
            this.Button6.Text = "Detener captura";
            this.Button6.UseVisualStyleBackColor = true;
            this.Button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // Button5
            // 
            this.Button5.Location = new System.Drawing.Point(112, 13);
            this.Button5.Name = "Button5";
            this.Button5.Size = new System.Drawing.Size(110, 22);
            this.Button5.TabIndex = 1;
            this.Button5.Text = "Comenzar captura";
            this.Button5.UseVisualStyleBackColor = true;
            this.Button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // lstPuntos
            // 
            this.lstPuntos.FormattingEnabled = true;
            this.lstPuntos.Location = new System.Drawing.Point(6, 13);
            this.lstPuntos.Name = "lstPuntos";
            this.lstPuntos.Size = new System.Drawing.Size(100, 69);
            this.lstPuntos.TabIndex = 0;
            // 
            // Button3
            // 
            this.Button3.Location = new System.Drawing.Point(166, 343);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(75, 23);
            this.Button3.TabIndex = 16;
            this.Button3.Text = "Arco";
            this.Button3.UseVisualStyleBackColor = true;
            this.Button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // Button2
            // 
            this.Button2.Location = new System.Drawing.Point(89, 343);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(75, 23);
            this.Button2.TabIndex = 15;
            this.Button2.Text = "Rectángulo";
            this.Button2.UseVisualStyleBackColor = true;
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(129, 51);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(25, 13);
            this.Label5.TabIndex = 12;
            this.Label5.Text = "Alto";
            // 
            // txtCentroY
            // 
            this.txtCentroY.Location = new System.Drawing.Point(192, 16);
            this.txtCentroY.Name = "txtCentroY";
            this.txtCentroY.Size = new System.Drawing.Size(47, 20);
            this.txtCentroY.TabIndex = 9;
            this.txtCentroY.Text = "50";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(129, 19);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(57, 13);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "Limite sup:";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(8, 19);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(53, 13);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "Limite izq:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(8, 51);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(38, 13);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "Ancho";
            // 
            // txtAlto
            // 
            this.txtAlto.Location = new System.Drawing.Point(192, 48);
            this.txtAlto.Name = "txtAlto";
            this.txtAlto.Size = new System.Drawing.Size(47, 20);
            this.txtAlto.TabIndex = 2;
            this.txtAlto.Text = "200";
            // 
            // txtAncho
            // 
            this.txtAncho.Location = new System.Drawing.Point(71, 48);
            this.txtAncho.Name = "txtAncho";
            this.txtAncho.Size = new System.Drawing.Size(47, 20);
            this.txtAncho.TabIndex = 3;
            this.txtAncho.Text = "200";
            // 
            // txtCentroX
            // 
            this.txtCentroX.Location = new System.Drawing.Point(71, 16);
            this.txtCentroX.Name = "txtCentroX";
            this.txtCentroX.Size = new System.Drawing.Size(47, 20);
            this.txtCentroX.TabIndex = 4;
            this.txtCentroX.Text = "300";
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(11, 343);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(75, 23);
            this.Button1.TabIndex = 5;
            this.Button1.Text = "Ellipse";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Vectoriales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 455);
            this.Controls.Add(this.Button18);
            this.Controls.Add(this.GroupBox8);
            this.Controls.Add(this.GroupBox7);
            this.Controls.Add(this.GroupBox6);
            this.Controls.Add(this.GroupBox1);
            this.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.Name = "Vectoriales";
            this.Text = "Vectoriales";
            this.Load += new System.EventHandler(this.Vectoriales_Load);
            this.GroupBox8.ResumeLayout(false);
            this.GroupBox7.ResumeLayout(false);
            this.GroupBox7.PerformLayout();
            this.GroupBox6.ResumeLayout(false);
            this.GroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctImagen)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.GroupBox5.ResumeLayout(false);
            this.GroupBox5.PerformLayout();
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.GroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button Button18;
        internal System.Windows.Forms.GroupBox GroupBox8;
        internal System.Windows.Forms.Button Button17;
        internal System.Windows.Forms.Button Button16;
        internal System.Windows.Forms.GroupBox GroupBox7;
        internal System.Windows.Forms.TextBox txtTransAncho;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.TextBox txtTransRot;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.TextBox txtTransAlto;
        internal System.Windows.Forms.TextBox txtTransX;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TextBox txtTransY;
        internal System.Windows.Forms.Button Button12;
        internal System.Windows.Forms.GroupBox GroupBox6;
        internal System.Windows.Forms.CheckBox chkAlternar;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.ComboBox cmbTipoRelleno;
        internal System.Windows.Forms.Button Button13;
        internal System.Windows.Forms.Button Button14;
        internal System.Windows.Forms.Button Button15;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Button Button11;
        internal System.Windows.Forms.Button Button10;
        internal System.Windows.Forms.PictureBox pctImagen;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.GroupBox GroupBox5;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.TextBox txtTension;
        internal System.Windows.Forms.Button Button9;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.ComboBox cmbEstilo;
        internal System.Windows.Forms.TextBox txtAnchoLapiz;
        internal System.Windows.Forms.ComboBox cmbColor;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.Button Button7;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.TextBox txtAnguloFin;
        internal System.Windows.Forms.TextBox txtAnguloComienzo;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.Button Button8;
        internal System.Windows.Forms.Button Button6;
        internal System.Windows.Forms.Button Button5;
        internal System.Windows.Forms.ListBox lstPuntos;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox txtCentroY;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txtAlto;
        internal System.Windows.Forms.TextBox txtAncho;
        internal System.Windows.Forms.TextBox txtCentroX;
        internal System.Windows.Forms.Button Button1;
    }
}