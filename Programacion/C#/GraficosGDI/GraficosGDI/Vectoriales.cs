﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace GraficosGDI
{
    public partial class Vectoriales : Form
    {
        public Vectoriales()
        {
            InitializeComponent();
        }

        Lienzo mLienzo;

        private void Vectoriales_Load(object sender, EventArgs e)
        {
       
            cmbColor.Items.Add("Negro");
            cmbColor.Items.Add("Azul");
            cmbColor.Items.Add("Marrón");
            cmbColor.Items.Add("Gris");
            cmbColor.Items.Add("Verde");
            cmbColor.Items.Add("Celeste");
            cmbColor.Items.Add("Naranja");
            cmbColor.Items.Add("Rojo");
            cmbColor.Items.Add("Blanco");
            cmbColor.Items.Add("Amarillo");
            cmbColor.SelectedIndex = 0;

        
            cmbEstilo.Items.Add("Sólido");
            cmbEstilo.Items.Add("Guión");
            cmbEstilo.Items.Add("Punto");
            cmbEstilo.Items.Add("Guión punto");
            cmbEstilo.Items.Add("Guión punto punto");
            cmbEstilo.SelectedIndex = 0;

        
            cmbTipoRelleno.Items.Add("Sólido");
            cmbTipoRelleno.Items.Add("Líneas oblicuas");
            cmbTipoRelleno.Items.Add("Líneas verticales");
            cmbTipoRelleno.Items.Add("Ladrillos");
            cmbTipoRelleno.Items.Add("Damero");
            cmbTipoRelleno.Items.Add("Imagen");
            cmbTipoRelleno.SelectedIndex = 0;
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            mLienzo = new Lienzo();
            mLienzo.CapturarPuntos = true;
            mLienzo.Show(this);
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            mLienzo.CapturarPuntos = false;
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            lstPuntos.Items.Clear();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //UN OBJETO GRAPHICS CARECE DE CONSTRUCTOR PUBLICO, POR LO CUAL PODEMOS OBTENERLO DESDE LA INSTANCIA DEL FORMULARIO, DE UN CONTROL, 
            //O DESDE LOS EVENTARGS DEL EVENTO PAINT DEL FORMULARIO
            mLienzo = new Lienzo();
            mLienzo.Show(this);
            Graphics mGr = mLienzo.CreateGraphics();    
            mGr.DrawEllipse(ArmarLapiz(), int.Parse(txtCentroX.Text), int.Parse(txtCentroY.Text), int.Parse(txtAncho.Text), int.Parse(txtAlto.Text));
            mGr.Dispose();
        }



        private Pen ArmarLapiz()
        {
            Pen mP = new System.Drawing.Pen (ObtenerColor().Color, float.Parse(txtAnchoLapiz.Text));
            mP.DashStyle = ObtenerEstilo();
            return mP;
        }

        private Pen ObtenerColor()
         {
             switch (cmbColor.SelectedItem.ToString())
             {
                 case "Negro":
                     return System.Drawing.Pens.Black;

                     case "Azul":
                     return System.Drawing.Pens.Blue;
                  
                     case "Marrón":
                     return System.Drawing.Pens.Brown;
                 
                     case "Gris":
                     return System.Drawing.Pens.Gray;
                    
                     case "Verde":
                     return System.Drawing.Pens.Green;
                    
                     case "Celeste":
                     return System.Drawing.Pens.LightBlue;
                    
                     case "Naranja":
                     return System.Drawing.Pens.Orange;
                  
                     case "Rojo":
                     return System.Drawing.Pens.Red;
                   
                     case "Blanco":
                     return System.Drawing.Pens.White;
                    
                     case "Amarillo":
                     return System.Drawing.Pens.Yellow;
                    
                    default:
                     return System.Drawing.Pens.Black;
                   
             }
         }
      

        private DashStyle ObtenerEstilo()
    {
        switch(cmbEstilo.SelectedItem.ToString())
        {
            case "Sólido":
            return System.Drawing.Drawing2D.DashStyle.Solid;
                 
            case "Guión punto":
            return System.Drawing.Drawing2D.DashStyle.DashDot;
                
            case "Guión punto punto":
            return System.Drawing.Drawing2D.DashStyle.DashDotDot;
                
            case "Guión":
            return System.Drawing.Drawing2D.DashStyle.Dash;
                
            case "Punto":
            return System.Drawing.Drawing2D.DashStyle.Dot;

            default:
            return System.Drawing.Drawing2D.DashStyle.Solid;
                
        }
            
    }

         private Brush ObtenerPincel()
         {
             switch(cmbTipoRelleno.SelectedItem.ToString())
             {
                 case "Sólido":
                     return new SolidBrush(ObtenerColor().Color);
                 case "Líneas oblicuas":
                     return new HatchBrush(HatchStyle.BackwardDiagonal, ObtenerColor().Color);
                 case "Líneas verticales":
                     return new HatchBrush(HatchStyle.DarkVertical, ObtenerColor().Color);
                 case "Ladrillos":
                     return new HatchBrush(HatchStyle.HorizontalBrick, ObtenerColor().Color);
                 case "Damero":
                     return new HatchBrush(HatchStyle.LargeCheckerBoard, ObtenerColor().Color);
                 case "Imagen":
                     return new TextureBrush(pctImagen.Image, WrapMode.Tile);
                 default:
                      return new SolidBrush(ObtenerColor().Color);
             }
         }

        private void Button2_Click(object sender, EventArgs e)
        {
            mLienzo = new Lienzo();
            mLienzo.Show(this);
            Graphics mGr = mLienzo.CreateGraphics();
            mGr.DrawRectangle(ArmarLapiz(), int.Parse(txtCentroX.Text), int.Parse(txtCentroY.Text), int.Parse(txtAncho.Text), int.Parse(txtAlto.Text));
            mGr.Dispose();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            mLienzo = new Lienzo();
            mLienzo.Show(this);
            Graphics mGr = mLienzo.CreateGraphics();
            mGr.DrawArc(ArmarLapiz(), int.Parse(txtCentroX.Text), int.Parse(txtCentroY.Text), int.Parse(txtAncho.Text), int.Parse(txtAlto.Text), int.Parse(txtAnguloComienzo.Text), int.Parse(txtAnguloFin.Text));
            mGr.Dispose();
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            if(lstPuntos.Items.Count > 0)
            {
            Point[] mPuntos = new Point[lstPuntos.Items.Count];
            int x = 0;
            foreach(string mPunto in lstPuntos.Items)
            {
                string[] mCoord = mPunto.Split(',');
                mPuntos[x] = new Point(int.Parse(mCoord[0]), int.Parse(mCoord[1]));
                x += 1;
            }
            mLienzo = new Lienzo();
            mLienzo.Show();
            Graphics mGr = mLienzo.CreateGraphics();
            mGr.DrawPolygon(ArmarLapiz(), mPuntos);
            mGr.Dispose();
            }
           
            else
            {
                mLienzo = new Lienzo();
                mLienzo.Show();
                mLienzo.MostrarMensaje("Debe haber puntos definidos para dibujar un polígono");
            }

        }

        private void Button4_Click(object sender, EventArgs e)
        {
            if (lstPuntos.Items.Count > 0)
            {
                Point[] mPuntos = new Point[lstPuntos.Items.Count];
                int x = 0;
                foreach (string mPunto in lstPuntos.Items)
                {
                    string[] mCoord = mPunto.Split(',');
                    mPuntos[x] = new Point(int.Parse(mCoord[0]), int.Parse(mCoord[1]));
                    x += 1;
                }
                mLienzo = new Lienzo();
                mLienzo.Show();
                Graphics mGr = mLienzo.CreateGraphics();
                mGr.DrawCurve(ArmarLapiz(), mPuntos, float.Parse ( txtTension.Text)  );
                mGr.Dispose();
            }

            else
            {
                mLienzo = new Lienzo();
                mLienzo.Show();
                mLienzo.MostrarMensaje("Debe haber puntos definidos para dibujar un spline cardinal");
            }
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            if (lstPuntos.Items.Count == 4)
            {
                Point[] mPuntos = new Point[lstPuntos.Items.Count];
                int x = 0;
                foreach (string mPunto in lstPuntos.Items)
                {
                    string[] mCoord = mPunto.Split(',');
                    mPuntos[x] = new Point(int.Parse(mCoord[0]), int.Parse(mCoord[1]));
                    x += 1;
                }
                mLienzo = new Lienzo();
                mLienzo.Show();
                Graphics mGr = mLienzo.CreateGraphics();
                mGr.DrawBezier(ArmarLapiz(), mPuntos[0], mPuntos[1], mPuntos[2], mPuntos[3]);
                mGr.Dispose();
            }

            else
            {
                mLienzo = new Lienzo();
                mLienzo.Show();
                mLienzo.MostrarMensaje("Debe haber 4 puntos definidos para dibujar un spline de Bezier");
            }
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            GraphicsPath mP = new GraphicsPath();
            mP.StartFigure();
            mP.AddRectangle(new Rectangle(20, 20, 200, 150));
            mP.AddRectangle(new Rectangle(50, 50, 200, 150));
            mP.AddLine(20, 20, 50, 50);
            mP.StartFigure();
            mP.AddLine(220, 20, 250, 50);
            mP.StartFigure();
            mP.AddLine(220, 170, 250, 200);
            mP.StartFigure();
            mP.AddLine(20, 170, 50, 200);
            mLienzo = new Lienzo ();
            mLienzo.Show();
            Graphics mGr = mLienzo.CreateGraphics();
            mGr.DrawPath(ArmarLapiz(), mP);
            mGr.Dispose();
            mP.Dispose();
        }

        private void Button11_Click(object sender, EventArgs e)
        {
            OpenFileDialog mOf = new OpenFileDialog();
       
            mOf.Multiselect = false;
            mOf.CheckFileExists = true;
            mOf.ShowReadOnly = false;
            mOf.Filter = "All Files|*.*|Bitmap Files (*)|*.bmp;*.gif;*.jpg";
            mOf.FilterIndex = 2;
            if(mOf.ShowDialog() == DialogResult.OK)
                pctImagen.Image = Image.FromFile(mOf.FileName);
        }

        private void Button15_Click(object sender, EventArgs e)
        {
            mLienzo = new Lienzo ();
             mLienzo.Show();
            Graphics mGr  = mLienzo.CreateGraphics();

            mGr.FillEllipse(ObtenerPincel(), int.Parse(txtCentroX.Text), int.Parse(txtCentroY.Text), int.Parse(txtAncho.Text), int.Parse(txtAlto.Text));
            mGr.Dispose();
        }

        private void Button14_Click(object sender, EventArgs e)
        {
            mLienzo = new Lienzo();
            mLienzo.Show();
            Graphics mGr = mLienzo.CreateGraphics();

            mGr.FillRectangle(ObtenerPincel(), int.Parse(txtCentroX.Text), int.Parse(txtCentroY.Text), int.Parse(txtAncho.Text), int.Parse(txtAlto.Text));
            mGr.Dispose();
        }

        private void Button13_Click(object sender, EventArgs e)
        {
            if (lstPuntos.Items.Count > 0)
            {
                Point[] mPuntos = new Point[lstPuntos.Items.Count];
                int x = 0;
                foreach (string mPunto in lstPuntos.Items)
                {
                    string[] mCoord = mPunto.Split(',');
                    mPuntos[x] = new Point(int.Parse(mCoord[0]), int.Parse(mCoord[1]));
                    x += 1;
                }
                mLienzo = new Lienzo();
                mLienzo.Show();
                Graphics mGr = mLienzo.CreateGraphics();
                mGr.FillPolygon(ObtenerPincel(), mPuntos);
                mGr.Dispose();
            }

            else
            {
                mLienzo = new Lienzo();
                mLienzo.Show();
                mLienzo.MostrarMensaje("Debe haber puntos definidos para dibujar un polígono");
            }
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            GraphicsPath mPa = new GraphicsPath();
            mPa.AddRectangle(new Rectangle(int.Parse(txtCentroX.Text), int.Parse(txtCentroY.Text), int.Parse(txtAncho.Text), int.Parse(txtAlto.Text)));
            mLienzo = new Lienzo ();
            mLienzo.Show();
            Graphics mGr  = mLienzo.CreateGraphics();

            mGr.DrawPath(ArmarLapiz(), mPa);
        
            mGr.TranslateTransform(int.Parse(txtTransX.Text), int.Parse(txtTransY.Text));
            mGr.ScaleTransform(Single.Parse(txtTransAncho.Text), Single.Parse(txtTransAlto.Text));
            mGr.RotateTransform(int.Parse(txtTransRot.Text));

            mGr.DrawPath(ArmarLapiz(), mPa);

            mGr.Dispose();
            mPa.Dispose();
        }

        private void Button18_Click(object sender, EventArgs e)
        {
            GraficoPersistente mForm = new GraficoPersistente();
            mForm.Show();
        }


        bool mRebotar = false;

    private void RebotarPelota()
    {
        mLienzo = new Lienzo ();

        int mVelocidadY = 2;
        int mVelocidadX = 2;

        int mPosY  = mLienzo.Height / 2;
        int mPosX  = mLienzo.Width / 2;

        int LimiteX = mLienzo.Width;
        int LimiteY = mLienzo.Height;

        mLienzo.Show();

        Graphics mGr  = mLienzo.CreateGraphics();
            while(mRebotar)
            {
                mGr.Clear(mLienzo.BackColor);
                mGr.FillEllipse(Brushes.Blue, mPosX, mPosY, 50, 50);
                if(mPosX <= 0 || mPosX + 60 >= mLienzo.Width)
                    mVelocidadX *= -1;

                if( mPosY <= 0 || mPosY + 80 >= mLienzo.Height)
                    mVelocidadY *= -1;

                mPosX += mVelocidadX;
                mPosY += mVelocidadY;

                System.Threading.Thread.Sleep(10);
                Application.DoEvents();

            }
        mGr.Dispose();

    }

    private void Button16_Click(object sender, EventArgs e)
    {
        mRebotar = true;
        RebotarPelota();
    }

    private void Button17_Click(object sender, EventArgs e)
    {
        mRebotar = false;
    }

    }
}
