﻿namespace TpIntegrado
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalir = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.CkUnitarios = new System.Windows.Forms.CheckBox();
            this.CkFederales = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.PuntajeFederales_BTN = new System.Windows.Forms.Button();
            this.PuntajeFederales = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalir.Font = new System.Drawing.Font("Harlow Solid Italic", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Location = new System.Drawing.Point(1066, 379);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(95, 38);
            this.btnSalir.TabIndex = 0;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Info;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Harlow Solid Italic", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(512, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(239, 37);
            this.label3.TabIndex = 3;
            this.label3.Text = "A que bando apoyas";
            this.label3.Enter += new System.EventHandler(this.label3_Enter);
            // 
            // CkUnitarios
            // 
            this.CkUnitarios.AutoSize = true;
            this.CkUnitarios.BackColor = System.Drawing.Color.Red;
            this.CkUnitarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CkUnitarios.Font = new System.Drawing.Font("Harlow Solid Italic", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CkUnitarios.Location = new System.Drawing.Point(37, 180);
            this.CkUnitarios.Name = "CkUnitarios";
            this.CkUnitarios.Size = new System.Drawing.Size(109, 29);
            this.CkUnitarios.TabIndex = 4;
            this.CkUnitarios.Text = "Unitarios";
            this.CkUnitarios.UseVisualStyleBackColor = false;
            this.CkUnitarios.CheckedChanged += new System.EventHandler(this.CkUnitarios_CheckedChanged);
            this.CkUnitarios.Click += new System.EventHandler(this.CkUnitarios_Click);
            // 
            // CkFederales
            // 
            this.CkFederales.AutoSize = true;
            this.CkFederales.BackColor = System.Drawing.Color.RoyalBlue;
            this.CkFederales.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CkFederales.Font = new System.Drawing.Font("Harlow Solid Italic", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CkFederales.Location = new System.Drawing.Point(1068, 185);
            this.CkFederales.Name = "CkFederales";
            this.CkFederales.Size = new System.Drawing.Size(112, 29);
            this.CkFederales.TabIndex = 5;
            this.CkFederales.Text = "Federales";
            this.CkFederales.UseVisualStyleBackColor = false;
            this.CkFederales.CheckedChanged += new System.EventHandler(this.CkFederales_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Harlow Solid Italic", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(32, 379);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 38);
            this.button1.TabIndex = 6;
            this.button1.Text = "Jugar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // PuntajeFederales_BTN
            // 
            this.PuntajeFederales_BTN.BackColor = System.Drawing.Color.DodgerBlue;
            this.PuntajeFederales_BTN.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PuntajeFederales_BTN.Location = new System.Drawing.Point(1081, 220);
            this.PuntajeFederales_BTN.Name = "PuntajeFederales_BTN";
            this.PuntajeFederales_BTN.Size = new System.Drawing.Size(99, 31);
            this.PuntajeFederales_BTN.TabIndex = 7;
            this.PuntajeFederales_BTN.Text = "Puntaje";
            this.PuntajeFederales_BTN.UseVisualStyleBackColor = false;
            this.PuntajeFederales_BTN.Click += new System.EventHandler(this.PuntajeFederales_BTN_Click);
            // 
            // PuntajeFederales
            // 
            this.PuntajeFederales.BackColor = System.Drawing.Color.DarkRed;
            this.PuntajeFederales.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PuntajeFederales.Location = new System.Drawing.Point(37, 220);
            this.PuntajeFederales.Name = "PuntajeFederales";
            this.PuntajeFederales.Size = new System.Drawing.Size(99, 31);
            this.PuntajeFederales.TabIndex = 8;
            this.PuntajeFederales.Text = "Puntaje";
            this.PuntajeFederales.UseVisualStyleBackColor = false;
            this.PuntajeFederales.Click += new System.EventHandler(this.PuntajeFederales_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TpIntegrado.Properties.Resources.Fondo_con_gorro_rojo_y_azul;
            this.ClientSize = new System.Drawing.Size(1262, 453);
            this.Controls.Add(this.PuntajeFederales);
            this.Controls.Add(this.PuntajeFederales_BTN);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.CkFederales);
            this.Controls.Add(this.CkUnitarios);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSalir);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox CkUnitarios;
        private System.Windows.Forms.CheckBox CkFederales;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button PuntajeFederales_BTN;
        private System.Windows.Forms.Button PuntajeFederales;
    }
}

