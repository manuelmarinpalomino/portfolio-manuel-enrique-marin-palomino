﻿namespace TpIntegrado
{
    partial class GamePlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Siguiente = new System.Windows.Forms.Button();
            this.Intro_Txt = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Btm_Salir = new System.Windows.Forms.Button();
            this.Lbl_TextoResponder = new System.Windows.Forms.Label();
            this.ok_btn = new System.Windows.Forms.Button();
            this.Respuesta1 = new System.Windows.Forms.TextBox();
            this.LblABC = new System.Windows.Forms.Label();
            this.Ck_Si = new System.Windows.Forms.CheckBox();
            this.Ck_No = new System.Windows.Forms.CheckBox();
            this.Respuesta2_Txt = new System.Windows.Forms.TextBox();
            this.Responder_btn = new System.Windows.Forms.Button();
            this.BTN_Final = new System.Windows.Forms.Button();
            this.LblMensajes = new System.Windows.Forms.Label();
            this.TextoResponder4_Txt = new System.Windows.Forms.TextBox();
            this.Responder_BTN3 = new System.Windows.Forms.Button();
            this.BTN_Seguir2 = new System.Windows.Forms.Button();
            this.Texto2 = new System.Windows.Forms.TextBox();
            this.Siguiente3 = new System.Windows.Forms.Button();
            this.Btn_Responder4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Btn_Siguiente
            // 
            this.Btn_Siguiente.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Btn_Siguiente.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Siguiente.Location = new System.Drawing.Point(1070, 410);
            this.Btn_Siguiente.Name = "Btn_Siguiente";
            this.Btn_Siguiente.Size = new System.Drawing.Size(100, 31);
            this.Btn_Siguiente.TabIndex = 0;
            this.Btn_Siguiente.Text = "Siguiente";
            this.Btn_Siguiente.UseVisualStyleBackColor = false;
            this.Btn_Siguiente.Click += new System.EventHandler(this.button1_Click);
            // 
            // Intro_Txt
            // 
            this.Intro_Txt.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Intro_Txt.Location = new System.Drawing.Point(13, 13);
            this.Intro_Txt.Multiline = true;
            this.Intro_Txt.Name = "Intro_Txt";
            this.Intro_Txt.Size = new System.Drawing.Size(794, 227);
            this.Intro_Txt.TabIndex = 1;
            this.Intro_Txt.Enter += new System.EventHandler(this.Intro_Txt_Enter);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::TpIntegrado.Properties.Resources._11___Rosas;
            this.pictureBox1.Location = new System.Drawing.Point(859, 51);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(311, 250);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // Btm_Salir
            // 
            this.Btm_Salir.BackColor = System.Drawing.Color.Red;
            this.Btm_Salir.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btm_Salir.Location = new System.Drawing.Point(1070, 12);
            this.Btm_Salir.Name = "Btm_Salir";
            this.Btm_Salir.Size = new System.Drawing.Size(100, 32);
            this.Btm_Salir.TabIndex = 3;
            this.Btm_Salir.Text = "Salir";
            this.Btm_Salir.UseVisualStyleBackColor = false;
            this.Btm_Salir.Click += new System.EventHandler(this.Btm_Salir_Click);
            // 
            // Lbl_TextoResponder
            // 
            this.Lbl_TextoResponder.AutoSize = true;
            this.Lbl_TextoResponder.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Lbl_TextoResponder.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_TextoResponder.Location = new System.Drawing.Point(45, 243);
            this.Lbl_TextoResponder.Name = "Lbl_TextoResponder";
            this.Lbl_TextoResponder.Size = new System.Drawing.Size(89, 24);
            this.Lbl_TextoResponder.TabIndex = 4;
            this.Lbl_TextoResponder.Text = "Respuesta";
            this.Lbl_TextoResponder.Visible = false;
            // 
            // ok_btn
            // 
            this.ok_btn.BackColor = System.Drawing.Color.Red;
            this.ok_btn.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ok_btn.Location = new System.Drawing.Point(213, 273);
            this.ok_btn.Name = "ok_btn";
            this.ok_btn.Size = new System.Drawing.Size(124, 35);
            this.ok_btn.TabIndex = 6;
            this.ok_btn.Text = "Responder";
            this.ok_btn.UseVisualStyleBackColor = false;
            this.ok_btn.Visible = false;
            this.ok_btn.Click += new System.EventHandler(this.ok_btn_Click);
            // 
            // Respuesta1
            // 
            this.Respuesta1.Location = new System.Drawing.Point(12, 279);
            this.Respuesta1.Name = "Respuesta1";
            this.Respuesta1.Size = new System.Drawing.Size(195, 22);
            this.Respuesta1.TabIndex = 7;
            this.Respuesta1.Visible = false;
            // 
            // LblABC
            // 
            this.LblABC.AutoSize = true;
            this.LblABC.Font = new System.Drawing.Font("Harlow Solid Italic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblABC.Location = new System.Drawing.Point(140, 246);
            this.LblABC.Name = "LblABC";
            this.LblABC.Size = new System.Drawing.Size(77, 19);
            this.LblABC.TabIndex = 9;
            this.LblABC.Text = "(A)(B)(C)";
            this.LblABC.Visible = false;
            // 
            // Ck_Si
            // 
            this.Ck_Si.BackColor = System.Drawing.Color.Brown;
            this.Ck_Si.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ck_Si.Location = new System.Drawing.Point(13, 279);
            this.Ck_Si.Name = "Ck_Si";
            this.Ck_Si.Size = new System.Drawing.Size(66, 24);
            this.Ck_Si.TabIndex = 10;
            this.Ck_Si.Text = "Si";
            this.Ck_Si.UseVisualStyleBackColor = false;
            this.Ck_Si.Visible = false;
            this.Ck_Si.CheckedChanged += new System.EventHandler(this.Ck_Si_CheckedChanged);
            // 
            // Ck_No
            // 
            this.Ck_No.BackColor = System.Drawing.Color.Brown;
            this.Ck_No.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ck_No.Location = new System.Drawing.Point(12, 326);
            this.Ck_No.Name = "Ck_No";
            this.Ck_No.Size = new System.Drawing.Size(67, 24);
            this.Ck_No.TabIndex = 11;
            this.Ck_No.Text = "No";
            this.Ck_No.UseVisualStyleBackColor = false;
            this.Ck_No.Visible = false;
            this.Ck_No.CheckedChanged += new System.EventHandler(this.Ck_No_CheckedChanged);
            // 
            // Respuesta2_Txt
            // 
            this.Respuesta2_Txt.Location = new System.Drawing.Point(12, 279);
            this.Respuesta2_Txt.Name = "Respuesta2_Txt";
            this.Respuesta2_Txt.Size = new System.Drawing.Size(195, 22);
            this.Respuesta2_Txt.TabIndex = 13;
            this.Respuesta2_Txt.Visible = false;
            // 
            // Responder_btn
            // 
            this.Responder_btn.BackColor = System.Drawing.Color.Red;
            this.Responder_btn.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Responder_btn.Location = new System.Drawing.Point(213, 273);
            this.Responder_btn.Name = "Responder_btn";
            this.Responder_btn.Size = new System.Drawing.Size(124, 35);
            this.Responder_btn.TabIndex = 12;
            this.Responder_btn.Text = "Responder";
            this.Responder_btn.UseVisualStyleBackColor = false;
            this.Responder_btn.Visible = false;
            this.Responder_btn.Click += new System.EventHandler(this.Responder_btn_Click);
            // 
            // BTN_Final
            // 
            this.BTN_Final.BackColor = System.Drawing.Color.Red;
            this.BTN_Final.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Final.Location = new System.Drawing.Point(1070, 410);
            this.BTN_Final.Name = "BTN_Final";
            this.BTN_Final.Size = new System.Drawing.Size(100, 31);
            this.BTN_Final.TabIndex = 14;
            this.BTN_Final.Text = "Final";
            this.BTN_Final.UseVisualStyleBackColor = false;
            this.BTN_Final.Visible = false;
            this.BTN_Final.Click += new System.EventHandler(this.BTN_Final_Click);
            // 
            // LblMensajes
            // 
            this.LblMensajes.BackColor = System.Drawing.Color.IndianRed;
            this.LblMensajes.Font = new System.Drawing.Font("Harlow Solid Italic", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMensajes.Location = new System.Drawing.Point(12, 13);
            this.LblMensajes.Name = "LblMensajes";
            this.LblMensajes.Size = new System.Drawing.Size(427, 77);
            this.LblMensajes.TabIndex = 15;
            this.LblMensajes.Text = "Inician los eventos finales ";
            this.LblMensajes.Visible = false;
            // 
            // TextoResponder4_Txt
            // 
            this.TextoResponder4_Txt.Location = new System.Drawing.Point(12, 281);
            this.TextoResponder4_Txt.Name = "TextoResponder4_Txt";
            this.TextoResponder4_Txt.Size = new System.Drawing.Size(195, 22);
            this.TextoResponder4_Txt.TabIndex = 17;
            this.TextoResponder4_Txt.Visible = false;
            // 
            // Responder_BTN3
            // 
            this.Responder_BTN3.BackColor = System.Drawing.Color.Red;
            this.Responder_BTN3.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Responder_BTN3.Location = new System.Drawing.Point(214, 273);
            this.Responder_BTN3.Name = "Responder_BTN3";
            this.Responder_BTN3.Size = new System.Drawing.Size(124, 35);
            this.Responder_BTN3.TabIndex = 16;
            this.Responder_BTN3.Text = "Responder";
            this.Responder_BTN3.UseVisualStyleBackColor = false;
            this.Responder_BTN3.Visible = false;
            this.Responder_BTN3.Click += new System.EventHandler(this.Responder_BTN3_Click);
            // 
            // BTN_Seguir2
            // 
            this.BTN_Seguir2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BTN_Seguir2.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Seguir2.Location = new System.Drawing.Point(1070, 410);
            this.BTN_Seguir2.Name = "BTN_Seguir2";
            this.BTN_Seguir2.Size = new System.Drawing.Size(100, 31);
            this.BTN_Seguir2.TabIndex = 18;
            this.BTN_Seguir2.Text = "Seguir ";
            this.BTN_Seguir2.UseVisualStyleBackColor = false;
            this.BTN_Seguir2.Visible = false;
            this.BTN_Seguir2.Click += new System.EventHandler(this.BTN_Seguir2_Click);
            // 
            // Texto2
            // 
            this.Texto2.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Texto2.Location = new System.Drawing.Point(6, 12);
            this.Texto2.Multiline = true;
            this.Texto2.Name = "Texto2";
            this.Texto2.Size = new System.Drawing.Size(794, 227);
            this.Texto2.TabIndex = 19;
            this.Texto2.Visible = false;
            // 
            // Siguiente3
            // 
            this.Siguiente3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Siguiente3.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Siguiente3.Location = new System.Drawing.Point(1070, 410);
            this.Siguiente3.Name = "Siguiente3";
            this.Siguiente3.Size = new System.Drawing.Size(100, 31);
            this.Siguiente3.TabIndex = 20;
            this.Siguiente3.Text = "Seguir ";
            this.Siguiente3.UseVisualStyleBackColor = false;
            this.Siguiente3.Visible = false;
            this.Siguiente3.Click += new System.EventHandler(this.Siguiente3_Click);
            // 
            // Btn_Responder4
            // 
            this.Btn_Responder4.BackColor = System.Drawing.Color.Red;
            this.Btn_Responder4.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Responder4.Location = new System.Drawing.Point(214, 273);
            this.Btn_Responder4.Name = "Btn_Responder4";
            this.Btn_Responder4.Size = new System.Drawing.Size(124, 35);
            this.Btn_Responder4.TabIndex = 21;
            this.Btn_Responder4.Text = "Responder";
            this.Btn_Responder4.UseVisualStyleBackColor = false;
            this.Btn_Responder4.Visible = false;
            this.Btn_Responder4.Click += new System.EventHandler(this.Btn_Responder4_Click);
            // 
            // GamePlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 453);
            this.Controls.Add(this.Btn_Responder4);
            this.Controls.Add(this.Siguiente3);
            this.Controls.Add(this.Texto2);
            this.Controls.Add(this.BTN_Seguir2);
            this.Controls.Add(this.TextoResponder4_Txt);
            this.Controls.Add(this.Responder_BTN3);
            this.Controls.Add(this.LblMensajes);
            this.Controls.Add(this.BTN_Final);
            this.Controls.Add(this.Respuesta2_Txt);
            this.Controls.Add(this.Responder_btn);
            this.Controls.Add(this.Ck_No);
            this.Controls.Add(this.Ck_Si);
            this.Controls.Add(this.LblABC);
            this.Controls.Add(this.Respuesta1);
            this.Controls.Add(this.ok_btn);
            this.Controls.Add(this.Lbl_TextoResponder);
            this.Controls.Add(this.Btm_Salir);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Intro_Txt);
            this.Controls.Add(this.Btn_Siguiente);
            this.Name = "GamePlay";
            this.Text = "GamePlay";
            this.Load += new System.EventHandler(this.GamePlay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_Siguiente;
        private System.Windows.Forms.TextBox Intro_Txt;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Btm_Salir;
        private System.Windows.Forms.Label Lbl_TextoResponder;
        private System.Windows.Forms.Button ok_btn;
        private System.Windows.Forms.TextBox Respuesta1;
        private System.Windows.Forms.Label LblABC;
        private System.Windows.Forms.CheckBox Ck_Si;
        private System.Windows.Forms.CheckBox Ck_No;
        private System.Windows.Forms.TextBox Respuesta2_Txt;
        private System.Windows.Forms.Button Responder_btn;
        private System.Windows.Forms.Button BTN_Final;
        private System.Windows.Forms.Label LblMensajes;
        private System.Windows.Forms.TextBox TextoResponder4_Txt;
        private System.Windows.Forms.Button Responder_BTN3;
        private System.Windows.Forms.Button BTN_Seguir2;
        private System.Windows.Forms.TextBox Texto2;
        private System.Windows.Forms.Button Siguiente3;
        private System.Windows.Forms.Button Btn_Responder4;
    }
}