﻿namespace TpIntegrado
{
    partial class Gameplay2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.intro = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Siguiente = new System.Windows.Forms.Button();
            this.Lvl1 = new System.Windows.Forms.Label();
            this.Respuesta1 = new System.Windows.Forms.TextBox();
            this.ok_btn = new System.Windows.Forms.Button();
            this.SalirGame2 = new System.Windows.Forms.Button();
            this.CkSi = new System.Windows.Forms.CheckBox();
            this.CkNo = new System.Windows.Forms.CheckBox();
            this.Btn_Ok2 = new System.Windows.Forms.Button();
            this.Respuesta3 = new System.Windows.Forms.TextBox();
            this.LblABC = new System.Windows.Forms.Label();
            this.EventoFinal = new System.Windows.Forms.Label();
            this.Final_Btn = new System.Windows.Forms.Button();
            this.BTN_responder2 = new System.Windows.Forms.Button();
            this.Responder4 = new System.Windows.Forms.TextBox();
            this.Final2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // intro
            // 
            this.intro.Location = new System.Drawing.Point(12, 12);
            this.intro.Multiline = true;
            this.intro.Name = "intro";
            this.intro.Size = new System.Drawing.Size(736, 190);
            this.intro.TabIndex = 0;
            this.intro.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.intro.Enter += new System.EventHandler(this.intro_Enter);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::TpIntegrado.Properties.Resources._8___Urquiza;
            this.pictureBox1.Location = new System.Drawing.Point(754, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 197);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Siguiente
            // 
            this.Siguiente.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Siguiente.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Siguiente.Location = new System.Drawing.Point(1052, 404);
            this.Siguiente.Name = "Siguiente";
            this.Siguiente.Size = new System.Drawing.Size(102, 37);
            this.Siguiente.TabIndex = 2;
            this.Siguiente.Text = "Siguiente";
            this.Siguiente.UseVisualStyleBackColor = false;
            this.Siguiente.Click += new System.EventHandler(this.Siguiente_Click);
            // 
            // Lvl1
            // 
            this.Lvl1.AutoSize = true;
            this.Lvl1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Lvl1.Font = new System.Drawing.Font("Harlow Solid Italic", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lvl1.Location = new System.Drawing.Point(27, 209);
            this.Lvl1.Name = "Lvl1";
            this.Lvl1.Size = new System.Drawing.Size(94, 25);
            this.Lvl1.TabIndex = 3;
            this.Lvl1.Text = "Respuesta";
            this.Lvl1.Visible = false;
            // 
            // Respuesta1
            // 
            this.Respuesta1.Location = new System.Drawing.Point(13, 249);
            this.Respuesta1.Name = "Respuesta1";
            this.Respuesta1.Size = new System.Drawing.Size(195, 22);
            this.Respuesta1.TabIndex = 4;
            this.Respuesta1.Visible = false;
            // 
            // ok_btn
            // 
            this.ok_btn.BackColor = System.Drawing.Color.Red;
            this.ok_btn.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ok_btn.Location = new System.Drawing.Point(214, 243);
            this.ok_btn.Name = "ok_btn";
            this.ok_btn.Size = new System.Drawing.Size(124, 35);
            this.ok_btn.TabIndex = 5;
            this.ok_btn.Text = "Responder";
            this.ok_btn.UseVisualStyleBackColor = false;
            this.ok_btn.Visible = false;
            this.ok_btn.Click += new System.EventHandler(this.button1_Click);
            // 
            // SalirGame2
            // 
            this.SalirGame2.BackColor = System.Drawing.Color.Red;
            this.SalirGame2.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalirGame2.Location = new System.Drawing.Point(1095, 2);
            this.SalirGame2.Name = "SalirGame2";
            this.SalirGame2.Size = new System.Drawing.Size(75, 29);
            this.SalirGame2.TabIndex = 7;
            this.SalirGame2.Text = "Salir";
            this.SalirGame2.UseVisualStyleBackColor = false;
            this.SalirGame2.Click += new System.EventHandler(this.SalirGame2_Click);
            // 
            // CkSi
            // 
            this.CkSi.BackColor = System.Drawing.Color.Firebrick;
            this.CkSi.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CkSi.Location = new System.Drawing.Point(13, 237);
            this.CkSi.Name = "CkSi";
            this.CkSi.Size = new System.Drawing.Size(59, 29);
            this.CkSi.TabIndex = 8;
            this.CkSi.Text = "Si";
            this.CkSi.UseVisualStyleBackColor = false;
            this.CkSi.Visible = false;
            this.CkSi.CheckedChanged += new System.EventHandler(this.CkSi_CheckedChanged);
            // 
            // CkNo
            // 
            this.CkNo.AutoSize = true;
            this.CkNo.BackColor = System.Drawing.Color.Firebrick;
            this.CkNo.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CkNo.Location = new System.Drawing.Point(13, 279);
            this.CkNo.Name = "CkNo";
            this.CkNo.Size = new System.Drawing.Size(59, 28);
            this.CkNo.TabIndex = 9;
            this.CkNo.Text = "No";
            this.CkNo.UseVisualStyleBackColor = false;
            this.CkNo.Visible = false;
            this.CkNo.CheckedChanged += new System.EventHandler(this.CkNo_CheckedChanged);
            // 
            // Btn_Ok2
            // 
            this.Btn_Ok2.BackColor = System.Drawing.Color.Red;
            this.Btn_Ok2.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Ok2.Location = new System.Drawing.Point(213, 238);
            this.Btn_Ok2.Name = "Btn_Ok2";
            this.Btn_Ok2.Size = new System.Drawing.Size(124, 35);
            this.Btn_Ok2.TabIndex = 11;
            this.Btn_Ok2.Text = "Responder";
            this.Btn_Ok2.UseVisualStyleBackColor = false;
            this.Btn_Ok2.Visible = false;
            this.Btn_Ok2.Click += new System.EventHandler(this.Btn_Ok2_Click);
            // 
            // Respuesta3
            // 
            this.Respuesta3.Location = new System.Drawing.Point(12, 244);
            this.Respuesta3.Name = "Respuesta3";
            this.Respuesta3.Size = new System.Drawing.Size(195, 22);
            this.Respuesta3.TabIndex = 10;
            this.Respuesta3.Visible = false;
            // 
            // LblABC
            // 
            this.LblABC.AutoSize = true;
            this.LblABC.Font = new System.Drawing.Font("Harlow Solid Italic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblABC.Location = new System.Drawing.Point(140, 213);
            this.LblABC.Name = "LblABC";
            this.LblABC.Size = new System.Drawing.Size(77, 19);
            this.LblABC.TabIndex = 12;
            this.LblABC.Text = "(A)(B)(C)";
            this.LblABC.Visible = false;
            // 
            // EventoFinal
            // 
            this.EventoFinal.BackColor = System.Drawing.Color.Firebrick;
            this.EventoFinal.Font = new System.Drawing.Font("Harlow Solid Italic", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EventoFinal.Location = new System.Drawing.Point(74, 37);
            this.EventoFinal.Name = "EventoFinal";
            this.EventoFinal.Size = new System.Drawing.Size(472, 55);
            this.EventoFinal.TabIndex = 13;
            this.EventoFinal.Text = "Inicio de eventos finales";
            this.EventoFinal.Visible = false;
            // 
            // Final_Btn
            // 
            this.Final_Btn.BackColor = System.Drawing.Color.Red;
            this.Final_Btn.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Final_Btn.Location = new System.Drawing.Point(1052, 404);
            this.Final_Btn.Name = "Final_Btn";
            this.Final_Btn.Size = new System.Drawing.Size(102, 37);
            this.Final_Btn.TabIndex = 14;
            this.Final_Btn.Text = "Final";
            this.Final_Btn.UseVisualStyleBackColor = false;
            this.Final_Btn.Visible = false;
            this.Final_Btn.Click += new System.EventHandler(this.Final_Btn_Click);
            // 
            // BTN_responder2
            // 
            this.BTN_responder2.BackColor = System.Drawing.Color.Red;
            this.BTN_responder2.Font = new System.Drawing.Font("Harlow Solid Italic", 10.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_responder2.Location = new System.Drawing.Point(213, 243);
            this.BTN_responder2.Name = "BTN_responder2";
            this.BTN_responder2.Size = new System.Drawing.Size(124, 35);
            this.BTN_responder2.TabIndex = 16;
            this.BTN_responder2.Text = "Responder";
            this.BTN_responder2.UseVisualStyleBackColor = false;
            this.BTN_responder2.Visible = false;
            this.BTN_responder2.Click += new System.EventHandler(this.BTN_responder2_Click);
            // 
            // Responder4
            // 
            this.Responder4.Location = new System.Drawing.Point(12, 249);
            this.Responder4.Name = "Responder4";
            this.Responder4.Size = new System.Drawing.Size(195, 22);
            this.Responder4.TabIndex = 15;
            this.Responder4.Visible = false;
            // 
            // Final2
            // 
            this.Final2.BackColor = System.Drawing.Color.Firebrick;
            this.Final2.Font = new System.Drawing.Font("Harlow Solid Italic", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Final2.Location = new System.Drawing.Point(74, 37);
            this.Final2.Name = "Final2";
            this.Final2.Size = new System.Drawing.Size(300, 55);
            this.Final2.TabIndex = 17;
            this.Final2.Text = "Fin de la partida";
            this.Final2.Visible = false;
            // 
            // Gameplay2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 453);
            this.Controls.Add(this.Final2);
            this.Controls.Add(this.BTN_responder2);
            this.Controls.Add(this.Responder4);
            this.Controls.Add(this.Final_Btn);
            this.Controls.Add(this.EventoFinal);
            this.Controls.Add(this.LblABC);
            this.Controls.Add(this.Btn_Ok2);
            this.Controls.Add(this.Respuesta3);
            this.Controls.Add(this.CkNo);
            this.Controls.Add(this.CkSi);
            this.Controls.Add(this.SalirGame2);
            this.Controls.Add(this.ok_btn);
            this.Controls.Add(this.Respuesta1);
            this.Controls.Add(this.Lvl1);
            this.Controls.Add(this.Siguiente);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.intro);
            this.Name = "Gameplay2";
            this.Text = "Gameplay2";
            this.Load += new System.EventHandler(this.Gameplay2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox intro;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Siguiente;
        private System.Windows.Forms.Label Lvl1;
        private System.Windows.Forms.TextBox Respuesta1;
        private System.Windows.Forms.Button ok_btn;
        private System.Windows.Forms.Button SalirGame2;
        private System.Windows.Forms.CheckBox CkSi;
        private System.Windows.Forms.CheckBox CkNo;
        private System.Windows.Forms.Button Btn_Ok2;
        private System.Windows.Forms.TextBox Respuesta3;
        private System.Windows.Forms.Label LblABC;
        private System.Windows.Forms.Label EventoFinal;
        private System.Windows.Forms.Button Final_Btn;
        private System.Windows.Forms.Button BTN_responder2;
        private System.Windows.Forms.TextBox Responder4;
        private System.Windows.Forms.Label Final2;
    }
}