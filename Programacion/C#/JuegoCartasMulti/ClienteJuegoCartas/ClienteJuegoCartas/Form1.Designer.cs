﻿namespace ClienteJuegoCartas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbPalo = new System.Windows.Forms.ComboBox();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.grdCartasJugadas = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.txtNombreJugador = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.grdCartasJugadas)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbPalo
            // 
            this.cmbPalo.FormattingEnabled = true;
            this.cmbPalo.Location = new System.Drawing.Point(41, 463);
            this.cmbPalo.Name = "cmbPalo";
            this.cmbPalo.Size = new System.Drawing.Size(316, 39);
            this.cmbPalo.TabIndex = 8;
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(41, 402);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(316, 38);
            this.txtNumero.TabIndex = 7;
            // 
            // grdCartasJugadas
            // 
            this.grdCartasJugadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCartasJugadas.Location = new System.Drawing.Point(434, 72);
            this.grdCartasJugadas.Name = "grdCartasJugadas";
            this.grdCartasJugadas.RowTemplate.Height = 40;
            this.grdCartasJugadas.Size = new System.Drawing.Size(953, 476);
            this.grdCartasJugadas.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(41, 608);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(296, 107);
            this.button1.TabIndex = 5;
            this.button1.Text = "Jugar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtNombreJugador
            // 
            this.txtNombreJugador.Location = new System.Drawing.Point(41, 12);
            this.txtNombreJugador.Name = "txtNombreJugador";
            this.txtNombreJugador.Size = new System.Drawing.Size(316, 38);
            this.txtNombreJugador.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1454, 797);
            this.Controls.Add(this.txtNombreJugador);
            this.Controls.Add(this.cmbPalo);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.grdCartasJugadas);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdCartasJugadas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbPalo;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.DataGridView grdCartasJugadas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtNombreJugador;
    }
}

