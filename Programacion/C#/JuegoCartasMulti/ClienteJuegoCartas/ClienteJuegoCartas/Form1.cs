﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ClienteJuegoCartas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          

            cmbPalo.Items.Add(ServicioCartas.PaloCarta.Corazon.ToString());
            cmbPalo.Items.Add(ServicioCartas.PaloCarta.Espada.ToString());
            cmbPalo.Items.Add(ServicioCartas.PaloCarta.Trebol.ToString());
            cmbPalo.Items.Add(ServicioCartas.PaloCarta.Diamante.ToString());



            Thread mTr = new Thread(Actualizar); //Thread que actualiza la grilla siempre

            mTr.Start(); //Iniciamos el thread

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ServicioCartas.Carta mCarta = new ServicioCartas.Carta();
            mCarta.Jugador = txtNombreJugador.Text;

            switch (cmbPalo.SelectedItem.ToString())
            {
                case "Corazon":
                    mCarta.Palo = ServicioCartas.PaloCarta.Corazon;
                    break;
                case "Espada":
                    mCarta.Palo = ServicioCartas.PaloCarta.Espada;
                    break;
                case "Diamante":
                    mCarta.Palo = ServicioCartas.PaloCarta.Diamante;
                    break;
                default:
                    mCarta.Palo = ServicioCartas.PaloCarta.Trebol;
                    break;
            }
            mCarta.Numero = int.Parse(txtNumero.Text);

            ServicioCartas.JuegoCartasSoapClient mServicio = new ServicioCartas.JuegoCartasSoapClient();

            mServicio.Jugar(mCarta);

            
        }


        delegate void ActualizarGrillaDelegate(List<ServicioCartas.Carta> pCartas);

        void ActualizarGrilla(List<ServicioCartas.Carta> pCartas) //Como la actualización de la grilla se hace desde otri proceso, debemos garantizar que se hará mediante un Delegate
        {
            if (InvokeRequired) //Si la invocación de la actulización viene desde otro Thread, InvokeRequired será true
            {
                this.Invoke(new ActualizarGrillaDelegate(ActualizarGrilla), pCartas);
                return;
            }

            grdCartasJugadas.DataSource = null;
            grdCartasJugadas.DataSource = pCartas;

        }



        private void Actualizar()
        {
            while(true) //Bucle infinito que actualiza la grilla cada décima de segundo
            {
                ServicioCartas.JuegoCartasSoapClient mServicio = new ServicioCartas.JuegoCartasSoapClient();

                List<ServicioCartas.Carta> mCartasJugadas = mServicio.CartasJugadas().ToList<ServicioCartas.Carta>();

                ActualizarGrilla(mCartasJugadas);

                Thread.Sleep(100); //<--- Décima de segundo de espera

            }

            


        }
    }
}
