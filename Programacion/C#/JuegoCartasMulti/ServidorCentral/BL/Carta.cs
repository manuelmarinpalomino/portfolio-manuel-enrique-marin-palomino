﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Carta
    {
        public enum PaloCarta { Corazon, Espada, Diamante, Trebol}

        public PaloCarta Palo { get; set; }
        public int Numero { get; set; }
        public string Jugador { get; set; }

        /// <summary>
        /// Esta colección simula el guardado en base de datos en capas
        /// </summary>
        static List<Carta> mCartas = new List<Carta>();


        public static List<Carta> CartasJugadas()
        {
            return mCartas;
        }

        public static void Jugar(Carta pCarta)
        {
            mCartas.Add(pCarta);
        }

    }
}
