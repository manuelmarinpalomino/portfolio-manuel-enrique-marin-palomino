﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BL;

namespace Servicios
{
    /// <summary>
    /// Descripción breve de JuegoCartas
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class JuegoCartas : System.Web.Services.WebService
    {

        [WebMethod]
        public void Jugar(Carta pCarta)
        {
            Carta.Jugar(pCarta);
        }

        [WebMethod]
        public List<Carta> CartasJugadas()
        {
            return Carta.CartasJugadas();
        }


    }
}
