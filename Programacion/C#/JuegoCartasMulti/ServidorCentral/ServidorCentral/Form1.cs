﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace ServidorCentral
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Carta mCarta = new Carta();
            mCarta.Jugador = "Servidor";

            switch (cmbPalo.SelectedItem.ToString())
            {
                case "Corazon":
                    mCarta.Palo = Carta.PaloCarta.Corazon;
                    break;
                case "Espada":
                    mCarta.Palo = Carta.PaloCarta.Espada;
                    break;
                case "Diamante":
                    mCarta.Palo = Carta.PaloCarta.Diamante;
                    break;
                default:
                    mCarta.Palo = Carta.PaloCarta.Trebol;
                    break;
            }
            mCarta.Numero = int.Parse(txtNumero.Text);

            Carta.Jugar(mCarta);

            Actualizar();
        }


        private void Actualizar()
        {
            grdCartasJugadas.DataSource = null;
            grdCartasJugadas.DataSource = Carta.CartasJugadas();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cmbPalo.Items.Add(Carta.PaloCarta.Corazon.ToString());
            cmbPalo.Items.Add(Carta.PaloCarta.Espada.ToString());
            cmbPalo.Items.Add(Carta.PaloCarta.Trebol.ToString());
            cmbPalo.Items.Add(Carta.PaloCarta.Diamante.ToString());
        }
    }
}
