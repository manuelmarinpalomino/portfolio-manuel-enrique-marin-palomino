﻿
namespace ControlesPersonalizados
{
    partial class PanelMemotest
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPuntos = new System.Windows.Forms.Label();
            this.btnPista = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblPuntos
            // 
            this.lblPuntos.AutoSize = true;
            this.lblPuntos.Location = new System.Drawing.Point(31, 9);
            this.lblPuntos.Name = "lblPuntos";
            this.lblPuntos.Size = new System.Drawing.Size(13, 13);
            this.lblPuntos.TabIndex = 0;
            this.lblPuntos.Text = "0";
            // 
            // btnPista
            // 
            this.btnPista.Location = new System.Drawing.Point(34, 47);
            this.btnPista.Name = "btnPista";
            this.btnPista.Size = new System.Drawing.Size(75, 23);
            this.btnPista.TabIndex = 1;
            this.btnPista.Text = "Pista";
            this.btnPista.UseVisualStyleBackColor = true;
            this.btnPista.Click += new System.EventHandler(this.btnPista_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(141, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Reiniciar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // PanelMemotest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnPista);
            this.Controls.Add(this.lblPuntos);
            this.Name = "PanelMemotest";
            this.Size = new System.Drawing.Size(330, 86);
            this.Load += new System.EventHandler(this.PanelMemotest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPuntos;
        private System.Windows.Forms.Button btnPista;
        private System.Windows.Forms.Button button1;
    }
}
