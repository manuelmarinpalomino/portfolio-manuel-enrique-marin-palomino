﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlesPersonalizados
{
    public partial class PanelMemotest : UserControl
    {
        public PanelMemotest()
        {
            InitializeComponent();
        }
        int Contador = 0;
        private void PanelMemotest_Load(object sender, EventArgs e)
        {

        }
        
        public void sumarPuntosLbl()
        {
            Contador++;
            lblPuntos.Text = Contador.ToString();
        }
        public void restarPuntosLbl()
        {
            Contador -= 2;
            lblPuntos.Text = Contador.ToString();
        }

        private void btnPista_Click(object sender, EventArgs e)
        {
            Color valor = new Color();
            
            foreach (var item in this.Parent.Controls)
            {
                if(item is RecuadroMemotest)
                {
                    if (((RecuadroMemotest)item).Seleccionado && !((RecuadroMemotest)item).Acertado)
                    {
                        valor = ((RecuadroMemotest)item).Valor;
                    }
                }
            }
            foreach (var item in this.Parent.Controls)
            {
                if (item is RecuadroMemotest)
                {
                    if (!((RecuadroMemotest)item).Seleccionado && ((RecuadroMemotest)item).Valor == valor && !((RecuadroMemotest)item).Acertado)
                    {
                        ((RecuadroMemotest)item).jugarBoton(item, new EventArgs());
                        restarPuntosLbl();
                        return;
                    }
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(this.Parent != null)
            {
                foreach (Control item in this.Parent.Controls)
                {
                    if(item is RecuadroMemotest)
                    {
                        ((RecuadroMemotest)item).Seleccionado = false;
                        ((RecuadroMemotest)item).Acertado = false;
                        ((RecuadroMemotest)item).Enabled = true;
                        ((RecuadroMemotest)item).BackColor = Color.White;
                    }

                }
            }
            Contador = 0;
            lblPuntos.Text = Contador.ToString();
        }
    }
}
