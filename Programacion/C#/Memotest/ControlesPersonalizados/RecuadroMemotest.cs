﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlesPersonalizados
{
    
    public class RecuadroMemotest : Button
    {
        [Description("Es el valor que toma el recuadroMemotest "), Category("RecuadroMemotest")]
        public Color Valor { get; set; }

        public bool Acertado { get; set; }
        public bool Seleccionado { get; set; }
        public static int CantidadCoincidencias { get; set; }

        private static PanelMemotest panelMemotest;

        protected override void InitLayout()
        {
            base.InitLayout();
            
            this.Paint += graficoPersistentePaint;
            this.Text = "?";
            this.Click += jugarBoton;
            
        }
        public void jugarBoton(object sender , EventArgs e)
        {
            try
            {
                if(panelMemotest == null)
                {
                    if(Parent != null)
                    {
                        bool hayPanel = false;
                        foreach (Control item in Parent.Controls)
                        {
                            if(item is PanelMemotest)
                            {
                                hayPanel = true;
                                panelMemotest = (PanelMemotest)item;
                            }
                        }
                        if (!hayPanel)
                        {
                            panelMemotest = new PanelMemotest();
                            panelMemotest.Top = 100;
                            panelMemotest.Left = 100;
                            this.Parent.Controls.Add(panelMemotest);
                        }
                    }
                }
                if (validarCoincidencias())
                {
                    //this.Text = Valor;
                    Seleccionado = true;
                    verOtrosSeleccionados();
                    this.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se puede jugar con esta configuración");
            }

        }
        private void graficoPersistentePaint (object sender, PaintEventArgs e)
        {
            if (Seleccionado)
            {
                Pen pen = new Pen(Valor);
                Graphics grd = e.Graphics;
                grd.FillEllipse(pen.Brush,10,10,10,10);
            }
        }

        private void verOtrosSeleccionados()
        {
           int contador = 0; 
            List<RecuadroMemotest> listaDeCoincidencias = new List<RecuadroMemotest>();
            if(Parent != null)
            {
                foreach (Control item in Parent.Controls)
                {
                    if (item is RecuadroMemotest)
                    {
                        if (((RecuadroMemotest)item).Seleccionado && !((RecuadroMemotest)item).Acertado  )
                        {
                            listaDeCoincidencias.Add(((RecuadroMemotest)item));
                            if (((RecuadroMemotest)item).Valor == this.Valor)
                                contador++;
                        }
                    }
                }
            }
            if(contador == RecuadroMemotest.CantidadCoincidencias)
            {
                foreach (RecuadroMemotest item in listaDeCoincidencias)
                {
                    item.Acertado = true;
                    item.BackColor = Color.Green;
                    item.Enabled = false;
                }
                panelMemotest.sumarPuntosLbl();
            }
            else if(listaDeCoincidencias.Count == RecuadroMemotest.CantidadCoincidencias)
            {
                foreach (RecuadroMemotest item in listaDeCoincidencias)
                {
                    item.Seleccionado = false;
                    item.Text = "?";
                    item.Refresh();
                }
            }

        }

        private bool validarCoincidencias()
        {
            int contador = 0; 
            if(Parent != null)
            {
                foreach (Control item in Parent.Controls)
                {
                    if(item is RecuadroMemotest)
                    {
                        if(((RecuadroMemotest)item).Valor == this.Valor)
                        {
                            contador++;
                        }
                    }
                }
            }
            if (contador > 0 && contador % RecuadroMemotest.CantidadCoincidencias == 0)
            {
                return true;
            }
            else
            {
                throw new Exception();
            }
        }
    }
}
