﻿
namespace practicaPreParcial2_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.recuadroMemotest8 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest6 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest2 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest1 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest3 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest4 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest5 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest7 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest9 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest10 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest11 = new ControlesPersonalizados.RecuadroMemotest();
            this.recuadroMemotest12 = new ControlesPersonalizados.RecuadroMemotest();
            this.SuspendLayout();
            // 
            // recuadroMemotest8
            // 
            this.recuadroMemotest8.Acertado = false;
            this.recuadroMemotest8.Location = new System.Drawing.Point(414, 127);
            this.recuadroMemotest8.Name = "recuadroMemotest8";
            this.recuadroMemotest8.Seleccionado = false;
            this.recuadroMemotest8.Size = new System.Drawing.Size(131, 61);
            this.recuadroMemotest8.TabIndex = 6;
            this.recuadroMemotest8.Text = "?";
            this.recuadroMemotest8.UseVisualStyleBackColor = true;
            this.recuadroMemotest8.Valor = System.Drawing.Color.Lime;
            // 
            // recuadroMemotest6
            // 
            this.recuadroMemotest6.Acertado = false;
            this.recuadroMemotest6.Location = new System.Drawing.Point(419, 49);
            this.recuadroMemotest6.Name = "recuadroMemotest6";
            this.recuadroMemotest6.Seleccionado = false;
            this.recuadroMemotest6.Size = new System.Drawing.Size(131, 61);
            this.recuadroMemotest6.TabIndex = 4;
            this.recuadroMemotest6.Text = "?";
            this.recuadroMemotest6.UseVisualStyleBackColor = true;
            this.recuadroMemotest6.Valor = System.Drawing.Color.Red;
            // 
            // recuadroMemotest2
            // 
            this.recuadroMemotest2.Acertado = false;
            this.recuadroMemotest2.Location = new System.Drawing.Point(571, 127);
            this.recuadroMemotest2.Name = "recuadroMemotest2";
            this.recuadroMemotest2.Seleccionado = false;
            this.recuadroMemotest2.Size = new System.Drawing.Size(122, 61);
            this.recuadroMemotest2.TabIndex = 1;
            this.recuadroMemotest2.Text = "?";
            this.recuadroMemotest2.UseVisualStyleBackColor = true;
            this.recuadroMemotest2.Valor = System.Drawing.Color.Lime;
            // 
            // recuadroMemotest1
            // 
            this.recuadroMemotest1.Acertado = false;
            this.recuadroMemotest1.Location = new System.Drawing.Point(571, 49);
            this.recuadroMemotest1.Name = "recuadroMemotest1";
            this.recuadroMemotest1.Seleccionado = false;
            this.recuadroMemotest1.Size = new System.Drawing.Size(122, 61);
            this.recuadroMemotest1.TabIndex = 0;
            this.recuadroMemotest1.Text = "?";
            this.recuadroMemotest1.UseVisualStyleBackColor = true;
            this.recuadroMemotest1.Valor = System.Drawing.Color.Red;
            // 
            // recuadroMemotest3
            // 
            this.recuadroMemotest3.Acertado = false;
            this.recuadroMemotest3.Location = new System.Drawing.Point(414, 287);
            this.recuadroMemotest3.Name = "recuadroMemotest3";
            this.recuadroMemotest3.Seleccionado = false;
            this.recuadroMemotest3.Size = new System.Drawing.Size(131, 61);
            this.recuadroMemotest3.TabIndex = 12;
            this.recuadroMemotest3.Text = "?";
            this.recuadroMemotest3.UseVisualStyleBackColor = true;
            this.recuadroMemotest3.Valor = System.Drawing.Color.Lime;
            // 
            // recuadroMemotest4
            // 
            this.recuadroMemotest4.Acertado = false;
            this.recuadroMemotest4.Location = new System.Drawing.Point(419, 209);
            this.recuadroMemotest4.Name = "recuadroMemotest4";
            this.recuadroMemotest4.Seleccionado = false;
            this.recuadroMemotest4.Size = new System.Drawing.Size(131, 61);
            this.recuadroMemotest4.TabIndex = 11;
            this.recuadroMemotest4.Text = "?";
            this.recuadroMemotest4.UseVisualStyleBackColor = true;
            this.recuadroMemotest4.Valor = System.Drawing.Color.Red;
            // 
            // recuadroMemotest5
            // 
            this.recuadroMemotest5.Acertado = false;
            this.recuadroMemotest5.Location = new System.Drawing.Point(571, 287);
            this.recuadroMemotest5.Name = "recuadroMemotest5";
            this.recuadroMemotest5.Seleccionado = false;
            this.recuadroMemotest5.Size = new System.Drawing.Size(122, 61);
            this.recuadroMemotest5.TabIndex = 10;
            this.recuadroMemotest5.Text = "?";
            this.recuadroMemotest5.UseVisualStyleBackColor = true;
            this.recuadroMemotest5.Valor = System.Drawing.Color.Lime;
            // 
            // recuadroMemotest7
            // 
            this.recuadroMemotest7.Acertado = false;
            this.recuadroMemotest7.Location = new System.Drawing.Point(571, 209);
            this.recuadroMemotest7.Name = "recuadroMemotest7";
            this.recuadroMemotest7.Seleccionado = false;
            this.recuadroMemotest7.Size = new System.Drawing.Size(122, 61);
            this.recuadroMemotest7.TabIndex = 9;
            this.recuadroMemotest7.Text = "?";
            this.recuadroMemotest7.UseVisualStyleBackColor = true;
            this.recuadroMemotest7.Valor = System.Drawing.Color.Red;
            // 
            // recuadroMemotest9
            // 
            this.recuadroMemotest9.Acertado = false;
            this.recuadroMemotest9.Location = new System.Drawing.Point(78, 287);
            this.recuadroMemotest9.Name = "recuadroMemotest9";
            this.recuadroMemotest9.Seleccionado = false;
            this.recuadroMemotest9.Size = new System.Drawing.Size(131, 61);
            this.recuadroMemotest9.TabIndex = 16;
            this.recuadroMemotest9.Text = "?";
            this.recuadroMemotest9.UseVisualStyleBackColor = true;
            this.recuadroMemotest9.Valor = System.Drawing.Color.Lime;
            // 
            // recuadroMemotest10
            // 
            this.recuadroMemotest10.Acertado = false;
            this.recuadroMemotest10.Location = new System.Drawing.Point(83, 209);
            this.recuadroMemotest10.Name = "recuadroMemotest10";
            this.recuadroMemotest10.Seleccionado = false;
            this.recuadroMemotest10.Size = new System.Drawing.Size(131, 61);
            this.recuadroMemotest10.TabIndex = 15;
            this.recuadroMemotest10.Text = "?";
            this.recuadroMemotest10.UseVisualStyleBackColor = true;
            this.recuadroMemotest10.Valor = System.Drawing.Color.Red;
            // 
            // recuadroMemotest11
            // 
            this.recuadroMemotest11.Acertado = false;
            this.recuadroMemotest11.Location = new System.Drawing.Point(235, 287);
            this.recuadroMemotest11.Name = "recuadroMemotest11";
            this.recuadroMemotest11.Seleccionado = false;
            this.recuadroMemotest11.Size = new System.Drawing.Size(122, 61);
            this.recuadroMemotest11.TabIndex = 14;
            this.recuadroMemotest11.Text = "?";
            this.recuadroMemotest11.UseVisualStyleBackColor = true;
            this.recuadroMemotest11.Valor = System.Drawing.Color.Lime;
            // 
            // recuadroMemotest12
            // 
            this.recuadroMemotest12.Acertado = false;
            this.recuadroMemotest12.Location = new System.Drawing.Point(235, 209);
            this.recuadroMemotest12.Name = "recuadroMemotest12";
            this.recuadroMemotest12.Seleccionado = false;
            this.recuadroMemotest12.Size = new System.Drawing.Size(122, 61);
            this.recuadroMemotest12.TabIndex = 13;
            this.recuadroMemotest12.Text = "?";
            this.recuadroMemotest12.UseVisualStyleBackColor = true;
            this.recuadroMemotest12.Valor = System.Drawing.Color.Red;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.recuadroMemotest9);
            this.Controls.Add(this.recuadroMemotest10);
            this.Controls.Add(this.recuadroMemotest11);
            this.Controls.Add(this.recuadroMemotest12);
            this.Controls.Add(this.recuadroMemotest3);
            this.Controls.Add(this.recuadroMemotest4);
            this.Controls.Add(this.recuadroMemotest5);
            this.Controls.Add(this.recuadroMemotest7);
            this.Controls.Add(this.recuadroMemotest8);
            this.Controls.Add(this.recuadroMemotest6);
            this.Controls.Add(this.recuadroMemotest2);
            this.Controls.Add(this.recuadroMemotest1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest1;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest2;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest6;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest8;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest3;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest4;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest5;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest7;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest9;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest10;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest11;
        private ControlesPersonalizados.RecuadroMemotest recuadroMemotest12;
    }
}

