﻿namespace SocketClient
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label4 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDesconectar = new System.Windows.Forms.Button();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.txtPuerto = new System.Windows.Forms.TextBox();
            this.btnConectar = new System.Windows.Forms.Button();
            this.txtRecibido = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.txtEnviar = new System.Windows.Forms.TextBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(89, 271);
            this.Label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(202, 32);
            this.Label4.TabIndex = 12;
            this.Label4.Text = "Texto a enviar:";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.btnDesconectar);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Controls.Add(this.txtIP);
            this.GroupBox1.Controls.Add(this.txtPuerto);
            this.GroupBox1.Controls.Add(this.btnConectar);
            this.GroupBox1.Location = new System.Drawing.Point(36, 40);
            this.GroupBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.GroupBox1.Size = new System.Drawing.Size(1765, 112);
            this.GroupBox1.TabIndex = 10;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Socket de conexión";
            // 
            // btnDesconectar
            // 
            this.btnDesconectar.Enabled = false;
            this.btnDesconectar.Location = new System.Drawing.Point(1408, 38);
            this.btnDesconectar.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnDesconectar.Name = "btnDesconectar";
            this.btnDesconectar.Size = new System.Drawing.Size(229, 55);
            this.btnDesconectar.TabIndex = 6;
            this.btnDesconectar.Text = "Desconectar";
            this.btnDesconectar.UseVisualStyleBackColor = true;
            this.btnDesconectar.Click += new System.EventHandler(this.btnDesconectar_Click);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(53, 50);
            this.Label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(203, 32);
            this.Label3.TabIndex = 5;
            this.Label3.Text = "IP del servidor:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(712, 50);
            this.Label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(107, 32);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "Puerto:";
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(272, 43);
            this.txtIP.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(417, 38);
            this.txtIP.TabIndex = 2;
            // 
            // txtPuerto
            // 
            this.txtPuerto.Location = new System.Drawing.Point(837, 43);
            this.txtPuerto.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtPuerto.Name = "txtPuerto";
            this.txtPuerto.Size = new System.Drawing.Size(177, 38);
            this.txtPuerto.TabIndex = 1;
            // 
            // btnConectar
            // 
            this.btnConectar.Location = new System.Drawing.Point(1115, 38);
            this.btnConectar.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnConectar.Name = "btnConectar";
            this.btnConectar.Size = new System.Drawing.Size(221, 55);
            this.btnConectar.TabIndex = 0;
            this.btnConectar.Text = "Conectar";
            this.btnConectar.UseVisualStyleBackColor = true;
            this.btnConectar.Click += new System.EventHandler(this.btnConectar_Click);
            // 
            // txtRecibido
            // 
            this.txtRecibido.Location = new System.Drawing.Point(308, 326);
            this.txtRecibido.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtRecibido.Multiline = true;
            this.txtRecibido.Name = "txtRecibido";
            this.txtRecibido.ReadOnly = true;
            this.txtRecibido.Size = new System.Drawing.Size(1359, 521);
            this.txtRecibido.TabIndex = 9;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(89, 326);
            this.Label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(219, 32);
            this.Label2.TabIndex = 11;
            this.Label2.Text = "Datos recibidos:";
            // 
            // txtEnviar
            // 
            this.txtEnviar.Enabled = false;
            this.txtEnviar.Location = new System.Drawing.Point(308, 264);
            this.txtEnviar.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtEnviar.Name = "txtEnviar";
            this.txtEnviar.Size = new System.Drawing.Size(1113, 38);
            this.txtEnviar.TabIndex = 8;
            // 
            // btnEnviar
            // 
            this.btnEnviar.Enabled = false;
            this.btnEnviar.Location = new System.Drawing.Point(1444, 259);
            this.btnEnviar.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(229, 55);
            this.btnEnviar.TabIndex = 7;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1842, 955);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.txtRecibido);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.txtEnviar);
            this.Controls.Add(this.btnEnviar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Button btnDesconectar;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txtIP;
        internal System.Windows.Forms.TextBox txtPuerto;
        internal System.Windows.Forms.Button btnConectar;
        internal System.Windows.Forms.TextBox txtRecibido;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox txtEnviar;
        internal System.Windows.Forms.Button btnEnviar;
    }
}

