﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SocketCliente mSocket = new SocketCliente();

        private void Form1_Load(object sender, EventArgs e)
        {
            mSocket.SeRecibieronDatos += SeRecibieronDatosHandler;
            mSocket.Desconectado += DesconectadoHandlerHandler;

            //Remplazar estas dos lineas por invocación a controles mediante delegados, según lo visto en clase
            TextBox.CheckForIllegalCrossThreadCalls = false;
            Button.CheckForIllegalCrossThreadCalls = false;
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            mSocket.IPRemota = txtIP.Text;
            mSocket.PuertoRemoto = int.Parse(txtPuerto.Text);

            mSocket.Conectar();
            btnEnviar.Enabled = true;
            txtEnviar.Enabled = true;
            btnDesconectar.Enabled = true;
            btnConectar.Enabled = false;
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            mSocket.EnviarDatos(txtEnviar.Text);
            txtRecibido.AppendText("Cliente: " + txtEnviar.Text + Environment.NewLine);
        }

        private void SeConectoClienteHandler(string pIP)
        {
            txtRecibido.AppendText("Cliente conectado: " + pIP + Environment.NewLine);
        }

        private void SeRecibieronDatosHandler(string pDatos)
        {
            txtRecibido.AppendText("Servidor: " + pDatos + Environment.NewLine);
            
        }

        private void DesconectadoHandlerHandler()
        {
            txtRecibido.AppendText("Desconectado" + Environment.NewLine);
            btnEnviar.Enabled = false;
            txtEnviar.Enabled = false;
            btnDesconectar.Enabled = false;
            btnConectar.Enabled = true;
        }

        private void btnDesconectar_Click(object sender, EventArgs e)
        {
            mSocket.Desconectar();
        }
    }
}
