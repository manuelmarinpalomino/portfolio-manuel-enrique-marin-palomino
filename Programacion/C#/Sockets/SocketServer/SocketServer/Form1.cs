﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SocketServidor mSocket = new SocketServidor();

        private void Form1_Load(object sender, EventArgs e)
        {
            cmbDirecciones.Items.AddRange(mSocket.ObtenerDireccionesLocales());

            mSocket.SeConectoCliente += SeConectoClienteHandler;
            mSocket.SeRecibieronDatos += SeRecibieronDatosHandler;
            mSocket.SeDesconectoCliente += SeDesconectoClienteHandler;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int puerto;
            if (int.TryParse(txtPuerto.Text, out puerto))
            {
                mSocket.PuertoEscucha = puerto;
                mSocket.EscucharPuerto();
            }
            else
            {
                MessageBox.Show("El puerto debe ser numérico");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mSocket.DetenerEscucha();
        }

        private void SeConectoClienteHandler(string pIP)
        {
            ActualizarTextBox("Cliente conectado: " + pIP + Environment.NewLine);

            //txtRecibido.Text += "Cliente conectado: " + pIP + Environment.NewLine;

            HabilitarBoton(btnEnviar, true);
            HabilitarBoton(btnDetener, true);
            HabilitarBoton(btnEscuchar, false);
            HabilitarTextbox(txtEnviar, true);
        }

        private void SeRecibieronDatosHandler(string pDatos)
        {
            ActualizarTextBox("Cliente: " + pDatos + Environment.NewLine);

                    
        }

        private void SeDesconectoClienteHandler()
        {

            ActualizarTextBox("Cliente desconectado" + Environment.NewLine);

            HabilitarBoton(btnEnviar, false);
            HabilitarBoton(btnDetener, false);
            HabilitarBoton(btnEscuchar, true);
            HabilitarTextbox(txtEnviar, false);
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            mSocket.EnviarDatos(txtEnviar.Text);

            ActualizarTextBox("Servidor: " + txtEnviar.Text + Environment.NewLine);
        }



        delegate void ActualizarTextBoxDelegate(string pTexto);

        void ActualizarTextBox(string pTexto)
        {
            if (InvokeRequired)
            {
                this.Invoke(new ActualizarTextBoxDelegate(ActualizarTextBox), pTexto);
                return;
            }

            txtRecibido.AppendText(pTexto);
        }

        delegate void HabilitarBotonDelegate(Button pBoton, bool pHabilitado);

        void HabilitarBoton(Button pBoton, bool pHabilitado)
        {
            if (InvokeRequired)
            {
                this.Invoke(new HabilitarBotonDelegate(HabilitarBoton), pBoton, pHabilitado);
                return;
            }

            pBoton.Enabled = pHabilitado;
        }

        delegate void HabilitarTextboxDelegate(TextBox pTxt, bool pHabilitado);

        void HabilitarTextbox(TextBox pTxt, bool pHabilitado)
        {
            if (InvokeRequired)
            {
                this.Invoke(new HabilitarTextboxDelegate(HabilitarTextbox), pTxt, pHabilitado);
                return;
            }

            pTxt.Enabled = pHabilitado;
        }




    }
}
