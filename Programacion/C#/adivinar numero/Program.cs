﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adivinar_numero
{
    class Program
    {
        static void Main(string[] args)
        {
            string nombre;
            int numero;
            Random numeroMaquina = new Random();
            int aleatoreo = numeroMaquina.Next(0, 100);
            int intentos = 0;
            Console.WriteLine("ingrese su nombre:");
            nombre = Console.ReadLine();
            do
            {
                intentos++;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{nombre} por favor ingrese un numero: ");
                try
                {
                    numero = int.Parse(Console.ReadLine());
                }
                catch(FormatException e)
                {
                    Console.WriteLine(e.Message + " se tomo como valor 0 formatException");
                    numero=0;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + " se tomo como valor 0");
                    numero = 0;
                }
                if (numero < aleatoreo)
                {
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    Console.WriteLine($"{nombre} el numero que ingresaste es menor");
                }
                if (numero > aleatoreo)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    Console.WriteLine($"{nombre} el numero que ingresaste es mayor");

                }

            } while (numero != aleatoreo);
            Console.WriteLine($"felicitaciones {nombre}¡ adivinaste el numero");
            Console.WriteLine($"el numero era {aleatoreo}");
            Console.WriteLine($"cantidad de intentos: {intentos}");
            Console.ReadKey();

        }
    }
}
