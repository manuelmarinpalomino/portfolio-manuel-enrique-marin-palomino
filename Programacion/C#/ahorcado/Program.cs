﻿using core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ahorcado
{
    class Program
    {
     

        static void Main(string[] args)
        {

            Juego juego = new Juego();
            juego.Jugar();
            Console.ReadKey();
        }
        

    }
}
namespace core
{
     public class Juego
    {
        public void Jugar()
        {
            bool s = false;
            string[] linea = { "programacion", "clases", "herencia", "polimorfismo", "metodos", "interfaces", "string", "variable", "constante", "videojuegos" };
            do
            {
                Random random = new Random();
                int aleatorio = random.Next(0, 10);
                Tablero t = new Tablero("juan", linea[aleatorio]);
                bool salir = false;
                int estado = 0;

                do
                {

                    if (!t.HayGanador(estado))
                    {
                        t.Dibujar(estado);
                        Console.SetCursorPosition(13, 15);
                        Console.Write("¿cual es la letra?");
                        string palabra = Console.ReadLine();
                        if (palabra.Count() == 1)
                        {
                            estado = estado + t.AgregarLetra((char)palabra[0]);
                        }
                    }
                    else
                    {
                        salir = true;

                    }

                } while (!salir);
                t = null;
                char respuesta;
                Console.SetCursorPosition(13, 5);
                Console.Write("quiere volver a jugar? S para si, N para no: ");
                respuesta = Convert.ToChar(Console.ReadLine());
                if(respuesta == 'n'|| respuesta=='N')
                {
                    s = true;
                    
                }
                else
                {
                    s = false;
                    Console.Clear();
                }

            } while (!s);
        }
        public void MostrarMenu()
        {

        }
    }
    public class Tipito
    {
        public enum PartesDelCuerpo: int
        {
            Cabeza =1,
            Cuerpo=2,
            BrazoDerecho=3,
            BrazoIzquierdo=4,
            PiernaDerecha=5,
            piernaIzquierda=6
        }
        public int Estado { get; set; }
        public void Dibujar(int x, int y, int estado)
        {
            if((int)estado >= (int)PartesDelCuerpo.Cabeza)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(0);
            }
            if ((int)estado >= (int)PartesDelCuerpo.Cuerpo)
            {
                Console.SetCursorPosition(x, ++y);
                Console.Write("|");
                Console.SetCursorPosition(x, ++y);
                Console.Write("|");
            }
            if ((int)estado >= (int)PartesDelCuerpo.BrazoDerecho)
            {
                Console.SetCursorPosition(x, --y);
                Console.Write("|\\");
            }
            if ((int)estado >= (int)PartesDelCuerpo.BrazoIzquierdo)
            {
                Console.SetCursorPosition(--x, y);
                Console.Write("/|");
            }
            if ((int)estado >= (int)PartesDelCuerpo.PiernaDerecha)
            {
                y++;
                y++;
                x++;
                Console.SetCursorPosition(++x, y);
                Console.Write("\\");
            }
            if ((int)estado >= (int)PartesDelCuerpo.piernaIzquierda)
            {
                
                x--;
                Console.SetCursorPosition(--x, y);
                Console.Write("/ \\");
            }
        }
    } 
    public class Jugador
    {
        public string Nombre { get; set; }
        public int Puntaje { get; set; }
    }
    public class Tablero
    {

        
        #region "constructores"
        public Tablero()
        {

        }
        public Tablero(string nombreJugador,string palabra)
        {
            Palabra p = new Palabra();
            p.palabra = palabra;
            this.palabra = p;
            Jugador j = new Jugador();
            j.Nombre = nombreJugador;
            j.Puntaje = 0;
            jugador = j;


        }
        #endregion
        #region "propiedades"
        private Palabra palabra;
        private Jugador jugador { get; set; }
        
        #endregion
        #region "metodos"
        public int AgregarLetra(char letra)
        {
            bool existeLetra = palabra.HayLetra(letra);
            int estado = 0;
            if (existeLetra)
            {
                //la letra esta 
                Console.SetCursorPosition(13, 16);
                Console.Write("Felicitaciones adivinaste una letra");
                Console.ReadKey();
                return estado;
            }
            else
            {
                Console.SetCursorPosition(13, 16);
                Console.Write("upss no habia esa letra en la palabra");
                Console.ReadKey();
                estado = estado + 1;
                return estado ;
            }
        }
       
        public void Dibujar(int estado)
        {
            
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(13, 3);
            Console.Write("********");
            Console.SetCursorPosition(13, 4);
            Console.Write("*      *");
            Console.SetCursorPosition(13, 5);
            Console.Write("*      *");
            Console.SetCursorPosition(13, 6);
            Console.Write("*");
            Console.SetCursorPosition(13, 7);
            Console.Write("*");
            Console.SetCursorPosition(13, 8);
            Console.Write("*");
            Console.SetCursorPosition(13, 9);
            Console.Write("*");
            Console.SetCursorPosition(13, 10);
            Console.Write("*");
            Console.SetCursorPosition(13, 11);
            Console.Write("*");
            Console.SetCursorPosition(13, 12);
            Console.Write("**********");
            Tipito tipito = new Tipito();
            Console.ForegroundColor = ConsoleColor.Red;
            tipito.Dibujar(20,6,estado);
            tipito = null;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.SetCursorPosition(13, 13);
            Console.Write(palabra.palabraOculta);
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(13, 15);
            Console.Write("                                                      ");
            Console.SetCursorPosition(13, 16);
            Console.Write("                                                      ");
        }
        public Jugador Ganador()
        {
            return new Jugador();
        }
        public bool HayGanador(int estado)
        {
            int palabratieneespaciovacio = 0;
            for(int i = 0; i < palabra.palabraOculta.Count(); i++)
            {
                if (palabra.palabraOculta[i]== '_')
                {
                    palabratieneespaciovacio += 1;
                }

            }
            if(estado<7&& palabratieneespaciovacio == 0)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"{jugador.Nombre} ha ganado");
                Console.ReadLine();
                return true;
            }
            else if (estado==7 && palabratieneespaciovacio!= 0)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"{jugador.Nombre} ha perdido");
                Console.ReadLine();
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        
    }
    public class Palabra
    {
        private string _palabra;
        public string palabra
        {
            get { return _palabra; }
            set 
            {
                _palabra = value;
                CrearPalabraOculta();
            }
        }
        private string _palabraOculta;
        public string palabraOculta
        {
            get { return _palabraOculta; }
            private set
            {
                _palabraOculta = value;
            }
        }
        private void CrearPalabraOculta()
        {
            string p = "";
            for(int i=0;i<this.palabra.Count(); i++)
            {
                p += "_";
            }
            this.palabraOculta = p;
        }
        public bool HayLetra(char letra)
        {
            bool hay = false;
            char[] po = _palabraOculta.ToCharArray();
            //buscar la letra en la palabra
            for (int i = 0; i < palabra.Count(); i++)
            {
                if (char.ToUpper(_palabra[i]) == char.ToUpper(letra))
                {
                    //encontro una letra 
                    hay = true;
                    po[i] = letra;
                }
                _palabraOculta = new string(po); 
            }
            return hay;
        }

    }
}
