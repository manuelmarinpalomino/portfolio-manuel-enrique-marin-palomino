﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace c__oSucio
{
    class Program
    {
        static void Main(string[] args)
        {
            Core.Juego juego = new Core.Juego();
            juego.Menu();
        }
    }
}
namespace Core
{
    public class Juego
    {
        public void Menu()
        {
            bool salir = false;
            do
            {
                Console.WriteLine("Bienvenido al CuloSucio");
                Console.WriteLine("Jugar(Enter)");
                Console.WriteLine("Ayuda(A)");
                Console.WriteLine("Salir(S)");
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.Enter:
                        Jugar();
                        break;
                    case ConsoleKey.A:
                        Ayuda();
                        break;
                    case ConsoleKey.S:
                        salir = true;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("ingresaste una tecla incorrecta por favor intentelo nuevamente");
                        break;
                }
            } while (!salir);
        }
        private void Jugar()
        {
            bool salir = false;
            Tablero tablero = new Tablero(2);
            do
            {
                salir = tablero.Dibujar();
            } while (!salir);
            
        }
        private void Ayuda()
        {
            Console.WriteLine("si le dan sus nombres y luego apretan enter la maquina hace el resto.");
            Console.ReadLine();
        }
    }
    public class Jugador
    {
        public string Nombre { get; set; }

    }
    class Tablero
    {
        public Carta[] MazoAleatoreo = new Carta[48];
        public Mazo mazo1 = new Mazo();
        public Jugador jugador1 = new Jugador();
        public Jugador jugador2 = new Jugador();
        private int contador = 0;
        int turno = 0;
        public Tablero(int Jugadores)
        {
            string nombreJugador1;
            string nombreJugador2;
            Console.Clear();
            Console.WriteLine("ingrese el nombre del primer jugador:");
            nombreJugador1 = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("ingrese el nombre del segundo jugador:");
            nombreJugador2 = Console.ReadLine();
            jugador1.Nombre = nombreJugador1;
            jugador2.Nombre = nombreJugador2;
            mezclarCartas();
        }
       public bool Dibujar()
        {
            bool salir = false;
            string quiereSalir;
            
            Console.Clear();
            turno = darTurno(turno);
            if (turno == 0)
            {
                Console.WriteLine($"{jugador1.Nombre} agarre una carta(S para salir)");
               quiereSalir= Console.ReadLine();
            }
            else
            {
                Console.WriteLine($"{jugador2.Nombre} agarre una carta(S para salir)");
                quiereSalir = Console.ReadLine();
            }
            if(quiereSalir.ToUpper()== "S")
            {
                salir = true;
                Console.Clear();
                return salir;
            }
            Console.WriteLine("usted agarro " + MazoAleatoreo[contador].NumeroCarta + " " + MazoAleatoreo[contador].PaloCarta);
            if (MazoAleatoreo[contador].NumeroCarta == 1 && MazoAleatoreo[contador].PaloCarta == "oro")
            {
                Console.WriteLine("Has perdido lo siento.");
                Console.ReadLine();
                salir = true;
            }
            else
            {
                Console.WriteLine("Safaste, el juego continua");
                Console.ReadLine();
            }
            Console.Clear();
            contador++;
            return salir;

        }
        private int darTurno(int numero)
        {
            if (numero == 0)
            {
                numero++;
            }
            else
            {
                numero = 0;
            }
            return numero;
        }
        private void  mezclarCartas()
        {
            int[] numerosaleatorios = new int[48];
            Random random = new Random();
             int f = 0;
            for (int y =0; y < 48; y++)
            {
                numerosaleatorios[y] = -1;
                
            }
            for (int i = 0; i < MazoAleatoreo.Count(); i++)
            {
                bool salir = false;
                do
                {
                    int contador = 0;
                    f = random.Next(0, 48);
                    for(int z =0; z < 48; z++)
                    {
                        
                        if (numerosaleatorios[z] == f)
                        {
                            contador++;
                        }
                    }
                    if (contador == 0)
                    {
                        salir = true;
                        numerosaleatorios[i] = f;
                    }
                } while (!salir);
                
                
                MazoAleatoreo[i] = mazo1.cartas[f];
                
            }
        }
    }
    class Carta
    {
        public int NumeroCarta { get; set; }
        public string PaloCarta { get; set; }
        public Carta (int numeroCarta, string paloCarta)
        {
            NumeroCarta = numeroCarta;
            PaloCarta = paloCarta;
        }
    }
    class Mazo
    {
        public List<Carta> cartas = new List<Carta>();
        public Mazo()
        {
            #region oro
            Carta carta1 = new Carta(1, "oro");
            cartas.Add(carta1);
            Carta carta2 = new Carta(2, "oro");
            cartas.Add(carta2);
            Carta carta3 = new Carta(3, "oro");
            cartas.Add(carta3);
            Carta carta4 = new Carta(4, "oro");
            cartas.Add(carta4);
            Carta carta5 = new Carta(5, "oro");
            cartas.Add(carta5);
            Carta carta6 = new Carta(6, "oro");
            cartas.Add(carta6);
            Carta carta7 = new Carta(7, "oro");
            cartas.Add(carta7);
            Carta carta8 = new Carta(8, "oro");
            cartas.Add(carta8);
            Carta carta9 = new Carta(9, "oro");
            cartas.Add(carta9);
            Carta carta10 = new Carta(10, "oro");
            cartas.Add(carta10);
            Carta carta11 = new Carta(11, "oro");
            cartas.Add(carta11);
            Carta carta12 = new Carta(12, "oro");
            cartas.Add(carta12);
            #endregion
            #region Espada
            Carta carta13 = new Carta(1, "espada");
            cartas.Add(carta13);
            Carta carta14 = new Carta(2, "espada");
            cartas.Add(carta14);
            Carta carta15 = new Carta(3, "espada");
            cartas.Add(carta15);
            Carta carta16 = new Carta(4, "espada");
            cartas.Add(carta16);
            Carta carta17 = new Carta(5, "espada");
            cartas.Add(carta17);
            Carta carta18 = new Carta(6, "espada");
            cartas.Add(carta18);
            Carta carta19 = new Carta(7, "espada");
            cartas.Add(carta19);
            Carta carta20 = new Carta(8, "espada");
            cartas.Add(carta20);
            Carta carta21 = new Carta(9, "espada");
            cartas.Add(carta21);
            Carta carta22 = new Carta(10, "espada");
            cartas.Add(carta22);
            Carta carta23 = new Carta(11, "espada");
            cartas.Add(carta23);
            Carta carta24 = new Carta(12, "espada");
            cartas.Add(carta24);
            #endregion
            #region Copa
            Carta carta25 = new Carta(1, "copa");
            cartas.Add(carta25);
            Carta carta26 = new Carta(2, "copa");
            cartas.Add(carta26);
            Carta carta27 = new Carta(3, "copa");
            cartas.Add(carta27);
            Carta carta28 = new Carta(4, "copa");
            cartas.Add(carta28);
            Carta carta29 = new Carta(5, "copa");
            cartas.Add(carta29);
            Carta carta30 = new Carta(6, "copa");
            cartas.Add(carta30);
            Carta carta31 = new Carta(7, "copa");
            cartas.Add(carta31);
            Carta carta32 = new Carta(8, "copa");
            cartas.Add(carta32);
            Carta carta33 = new Carta(9, "copa");
            cartas.Add(carta33);
            Carta carta34 = new Carta(10, "copa");
            cartas.Add(carta34);
            Carta carta35 = new Carta(11, "copa");
            cartas.Add(carta35);
            Carta carta36 = new Carta(12, "copa");
            cartas.Add(carta36);
            #endregion
            #region Basto
            Carta carta37 = new Carta(1, "basto");
            cartas.Add(carta37);
            Carta carta38 = new Carta(2, "basto");
            cartas.Add(carta38);
            Carta carta39 = new Carta(3, "basto");
            cartas.Add(carta39);
            Carta carta40 = new Carta(4, "basto");
            cartas.Add(carta40);
            Carta carta41 = new Carta(5, "basto");
            cartas.Add(carta41);
            Carta carta42 = new Carta(6, "basto");
            cartas.Add(carta42);
            Carta carta43 = new Carta(7, "basto");
            cartas.Add(carta43);
            Carta carta44 = new Carta(8, "basto");
            cartas.Add(carta44);
            Carta carta45 = new Carta(9, "basto");
            cartas.Add(carta45);
            Carta carta46 = new Carta(10, "basto");
            cartas.Add(carta46);
            Carta carta47 = new Carta(11, "basto");
            cartas.Add(carta47);
            Carta carta48 = new Carta(12, "basto");
            cartas.Add(carta48);
            #endregion


        }


    }
}
