﻿
namespace _1parcialManuelMarin
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdTablero = new System.Windows.Forms.DataGridView();
            this.btnAgregarCarta = new System.Windows.Forms.Button();
            this.cmbTipoCarta = new System.Windows.Forms.ComboBox();
            this.txtbHealth = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbPower = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnJugarCarta = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdTablero)).BeginInit();
            this.SuspendLayout();
            // 
            // grdTablero
            // 
            this.grdTablero.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdTablero.Location = new System.Drawing.Point(225, 83);
            this.grdTablero.Name = "grdTablero";
            this.grdTablero.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdTablero.Size = new System.Drawing.Size(528, 257);
            this.grdTablero.TabIndex = 0;
            // 
            // btnAgregarCarta
            // 
            this.btnAgregarCarta.Location = new System.Drawing.Point(89, 223);
            this.btnAgregarCarta.Name = "btnAgregarCarta";
            this.btnAgregarCarta.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarCarta.TabIndex = 1;
            this.btnAgregarCarta.Text = "Agregar Carta";
            this.btnAgregarCarta.UseVisualStyleBackColor = true;
            this.btnAgregarCarta.Click += new System.EventHandler(this.btnAgregarCarta_Click);
            // 
            // cmbTipoCarta
            // 
            this.cmbTipoCarta.FormattingEnabled = true;
            this.cmbTipoCarta.Location = new System.Drawing.Point(64, 90);
            this.cmbTipoCarta.Name = "cmbTipoCarta";
            this.cmbTipoCarta.Size = new System.Drawing.Size(121, 21);
            this.cmbTipoCarta.TabIndex = 2;
            this.cmbTipoCarta.Text = "Elije Tipo Carta";
            // 
            // txtbHealth
            // 
            this.txtbHealth.Location = new System.Drawing.Point(64, 143);
            this.txtbHealth.Name = "txtbHealth";
            this.txtbHealth.Size = new System.Drawing.Size(100, 20);
            this.txtbHealth.TabIndex = 3;
            this.txtbHealth.Text = "100";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "health";
            // 
            // txtbPower
            // 
            this.txtbPower.Location = new System.Drawing.Point(64, 197);
            this.txtbPower.Name = "txtbPower";
            this.txtbPower.Size = new System.Drawing.Size(100, 20);
            this.txtbPower.TabIndex = 6;
            this.txtbPower.Text = "100";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Power";
            // 
            // btnJugarCarta
            // 
            this.btnJugarCarta.Location = new System.Drawing.Point(357, 358);
            this.btnJugarCarta.Name = "btnJugarCarta";
            this.btnJugarCarta.Size = new System.Drawing.Size(75, 23);
            this.btnJugarCarta.TabIndex = 8;
            this.btnJugarCarta.Text = "Jugar Carta";
            this.btnJugarCarta.UseVisualStyleBackColor = true;
            this.btnJugarCarta.Click += new System.EventHandler(this.btnJugarCarta_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnJugarCarta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtbPower);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtbHealth);
            this.Controls.Add(this.cmbTipoCarta);
            this.Controls.Add(this.btnAgregarCarta);
            this.Controls.Add(this.grdTablero);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdTablero)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdTablero;
        private System.Windows.Forms.Button btnAgregarCarta;
        private System.Windows.Forms.ComboBox cmbTipoCarta;
        private System.Windows.Forms.TextBox txtbHealth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbPower;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnJugarCarta;
    }
}

