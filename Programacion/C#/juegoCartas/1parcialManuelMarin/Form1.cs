﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace _1parcialManuelMarin
{
    public partial class Form1 : Form
    {
        public Tablero tablero = new Tablero();
        
        
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            cmbTipoCarta.Items.Add(TipoCarta.Hero);
            cmbTipoCarta.Items.Add(TipoCarta.Monster);
        }
         
        private void Actualizar()
        {
            grdTablero.DataSource = null;
            grdTablero.DataSource = tablero.cartas;
        }

        private void btnAgregarCarta_Click(object sender, EventArgs e)
        {
            try
            {
                if (tablero.cartas.Count <= 9)
                {
                    Carta carta = new Carta(Convert.ToInt32(txtbHealth.Text), Convert.ToInt32(txtbPower.Text));
                    if (cmbTipoCarta.SelectedItem.ToString() == "Hero")
                    {
                        carta.tipoCarta = TipoCarta.Hero;
                    }
                    else if (cmbTipoCarta.SelectedItem.ToString() == "Monster")
                    {
                        carta.tipoCarta = TipoCarta.Monster;
                    }
                    tablero.AgregarCarta(carta);
                    Actualizar();
                }
                else
                {
                    Carta carta = new Carta(Convert.ToInt32(txtbHealth.Text), Convert.ToInt32(txtbPower.Text));
                    if (cmbTipoCarta.SelectedItem.ToString() == "Hero")
                    {
                        carta.tipoCarta = TipoCarta.Hero;
                    }
                    else if (cmbTipoCarta.SelectedItem.ToString() == "Monster")
                    {
                        carta.tipoCarta = TipoCarta.Monster;
                    }
                    CartaExtendidaException ex = new CartaExtendidaException(carta);
                    throw ex;
                }
            }
            catch(CartaExtendidaException ex)
            {
                MessageBox.Show("la carta: " + ex.carta.tipoCarta + ". No se puede agregar al tablero porque supero las 10 cartas.");
            }

           ;
        }

        private void btnJugarCarta_Click(object sender, EventArgs e)
        {
            Carta carta = (Carta) grdTablero.SelectedRows[0].DataBoundItem ;
            
            tablero.PlayCard(carta);
            Actualizar();
        }
    }
}
