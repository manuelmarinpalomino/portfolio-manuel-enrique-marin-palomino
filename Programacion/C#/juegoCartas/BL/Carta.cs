﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL
{
    public enum TipoCarta
    {
        Hero,
        Monster
    }
    public class Carta
    {
        private TipoCarta mTipoCarta;
        public TipoCarta tipoCarta
        {
            get
            {
                return mTipoCarta;
            }
            set
            {
                mTipoCarta = value;
            }
        }
        private int mHealth;
        public int health
        {
            get
            {
                return mHealth;
            }
        }
        public int power { get; }

        private bool mDefeated;
        public bool defeated
        {
            get
            {
                return mDefeated;
            }
            internal set
            {
                mDefeated = value;
            }
        }
        public Carta(int health , int power)
        {
            this.mHealth = health;
            this.power = power;
        }
        public void EventSeJugoCarta (object Sender , EventArgs e)
        {
            if((((Carta) Sender).mTipoCarta == TipoCarta.Hero) && (this.mTipoCarta) == TipoCarta.Monster)
            {
                if(mHealth > 0 && ((Carta)Sender).mDefeated == false)
                {
                    this.mHealth -= ((Carta)Sender).power;
                }
              
                if(this.mHealth <= 0)
                {
                    mDefeated = true;
                }
            }
             else if ((((Carta)Sender).mTipoCarta == TipoCarta.Monster) && (this.mTipoCarta) == TipoCarta.Hero)
            {
                if (mHealth > 0 && ((Carta)Sender).mDefeated == false)
                {
                    this.mHealth -= ((Carta)Sender).power;
                }
                
                if (this.mHealth <= 0)
                {
                    mDefeated = true;
                }
            }
        }
    }
}
