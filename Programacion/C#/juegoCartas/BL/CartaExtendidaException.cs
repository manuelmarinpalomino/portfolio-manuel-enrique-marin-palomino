﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL
{
    public class CartaExtendidaException : Exception
    {
        public Carta carta { get; set; }
        public CartaExtendidaException(Carta carta)
        {
            this.carta = carta;
        }
    }
}
