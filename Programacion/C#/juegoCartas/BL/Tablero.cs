﻿using System;
using System.Collections.Generic;

namespace BL
{
    public class Tablero
    {
        private List<Carta> mCartas = new List<Carta>();

        public List<Carta> cartas
        {
            get
            {
                return mCartas;
            }
            set
            {
                mCartas = value;
            }
        }
        public event EventHandler JugoCarta;
        public void AgregarCarta(Carta carta)
        {
            JugoCarta += carta.EventSeJugoCarta;
            mCartas.Add(carta);
            
        }
        public void PlayCard(Carta carta)
        {
            JugoCarta(carta, null);
        }
    }
}
