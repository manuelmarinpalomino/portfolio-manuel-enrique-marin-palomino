﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLpreparcial2._2
{
    public class ATGM : Proyectil,IPerforadorBlindaje
    {
        public override int poderDano 
        { get
            {
                return 90; 
            } 
        } 
        public int nivelPerforacionBlindaje 
        {
            get
            {
                return 80;
            } 
        } 
    }
}
