﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLpreparcial2._2
{
    public class Challenger1 : Vehiculo
    {
        public override string nombre
        {
            get
            {
                return "Challenger1";
            }
        }
        public int blindaje
        { 
            get;
            internal set; 
        }

        public override void RecibirDano(Proyectil proyectil)
        {
            if (this.blindaje > 0 && proyectil is IPerforadorBlindaje)
            {
                this.blindaje-= ((IPerforadorBlindaje)proyectil).nivelPerforacionBlindaje;
            }
            else if (this.blindaje <= 0)
            {
                blindaje = 0;
                estado -= proyectil.poderDano;
            }
            if (estado <= 0)
            {
                this.estado = 0;
                this.destruido = true;
            }

        }
        public Challenger1():base()
        {
            this.blindaje = 100;
        }
    }
}
