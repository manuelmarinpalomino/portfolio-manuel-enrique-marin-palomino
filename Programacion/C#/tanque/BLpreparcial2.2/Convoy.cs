﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLpreparcial2._2
{
    public class Convoy:IEnumerable<Vehiculo>,ICloneable
    {
        private List<Vehiculo> vehiculos = new List<Vehiculo>();

        public IEnumerator<Vehiculo> GetEnumerator()
        {
            return vehiculos.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return vehiculos.GetEnumerator();
        }
        public void AgregarVehiculos(Vehiculo vehiculo)
        {
            DRecibirImpacto += vehiculo.RecibirDano;//se suscribe el vehiculo al delegate.
            vehiculos.Add(vehiculo);
        }
        public delegate void RecibirImpactoDelegate(Proyectil proyectil); //creoDelegado con su firma
        public RecibirImpactoDelegate DRecibirImpacto; //handler

        public void RecibirProyectil(Proyectil proyectil)
        {
            DRecibirImpacto(proyectil); //ejecuto el delegate 

        }

        public object Clone()
        {
            Convoy mConvoy = new Convoy();
            foreach (var v in vehiculos)
            {
                mConvoy.AgregarVehiculos((Vehiculo)v.Clone());
            }
            return mConvoy;
        }
    }
}
