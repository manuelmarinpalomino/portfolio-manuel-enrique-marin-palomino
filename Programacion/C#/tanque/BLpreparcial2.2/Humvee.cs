﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLpreparcial2._2
{
    public class Humvee : Vehiculo
    {
        public override string nombre 
        {
            get
            {
                return "Humvee";
            }
        }

        public override void RecibirDano(Proyectil proyectil)
        {
            
            this.estado -= proyectil.poderDano;
            if (estado <= 0)
            {
                this.estado = 0;
                this.destruido = true;
            }
        }
    }
}
