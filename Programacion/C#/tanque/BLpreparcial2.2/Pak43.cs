﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLpreparcial2._2
{
    public class Pak43 :Proyectil, IPerforadorBlindaje
    {
        public override int poderDano 
        {
            get
            {
                return 40;
            }
        }

        public int nivelPerforacionBlindaje 
        {
            get
            {
                return 50;
            } 
        } 
    }
}
