﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLpreparcial2._2
{
    public class RenaultSherpa : Vehiculo
    {
        public override string nombre 
        {
            get
            {
                return "RenaultSherpa";
            } 
        }

        public override void RecibirDano(Proyectil proyectil)
        {
            this.estado -= ((int)(proyectil.poderDano / 2));
            if (estado <= 0)
            {
                this.estado = 0;
                this.destruido = true;
            }
        }
    }
}
