﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLpreparcial2._2
{
    public class Stanag4172:Proyectil
    {
        private int mPoderDano;
        public override int poderDano {
            get
            {
                return mPoderDano;
            }
        }
        public static Stanag4172 operator +(Stanag4172 bala1 , Stanag4172 bala2)
        {
            Stanag4172 balaFinal = new Stanag4172();
            balaFinal.mPoderDano = bala1.mPoderDano + bala2.mPoderDano;
            return balaFinal;
        }
    }
}
