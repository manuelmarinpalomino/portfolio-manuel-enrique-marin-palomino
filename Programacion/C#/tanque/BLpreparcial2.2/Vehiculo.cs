﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLpreparcial2._2
{
    public abstract class Vehiculo:ICloneable
    {
        public int estado { get; protected set; }
        public bool destruido { get; set; }
        public abstract string nombre { get;}
        public abstract void RecibirDano(Proyectil proyectil);
        public static string regimiento { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public Vehiculo()
        {
            this.estado = 100;
        }

    }
}
