﻿
namespace preparcial2._2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnStanag4172 = new System.Windows.Forms.Button();
            this.DispararPak43 = new System.Windows.Forms.Button();
            this.btmATGM = new System.Windows.Forms.Button();
            this.GrdConvoy = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.GrdConvoy)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnStanag4172
            // 
            this.BtnStanag4172.Location = new System.Drawing.Point(112, 390);
            this.BtnStanag4172.Name = "BtnStanag4172";
            this.BtnStanag4172.Size = new System.Drawing.Size(132, 23);
            this.BtnStanag4172.TabIndex = 0;
            this.BtnStanag4172.Text = "Disparar Stanag4172";
            this.BtnStanag4172.UseVisualStyleBackColor = true;
            this.BtnStanag4172.Click += new System.EventHandler(this.BtnStanag4172_Click);
            // 
            // DispararPak43
            // 
            this.DispararPak43.Location = new System.Drawing.Point(343, 390);
            this.DispararPak43.Name = "DispararPak43";
            this.DispararPak43.Size = new System.Drawing.Size(127, 23);
            this.DispararPak43.TabIndex = 1;
            this.DispararPak43.Text = "Disparar Pak43";
            this.DispararPak43.UseVisualStyleBackColor = true;
            this.DispararPak43.Click += new System.EventHandler(this.DispararPak43_Click);
            // 
            // btmATGM
            // 
            this.btmATGM.Location = new System.Drawing.Point(579, 390);
            this.btmATGM.Name = "btmATGM";
            this.btmATGM.Size = new System.Drawing.Size(99, 23);
            this.btmATGM.TabIndex = 2;
            this.btmATGM.Text = "DispararATGM";
            this.btmATGM.UseVisualStyleBackColor = true;
            this.btmATGM.Click += new System.EventHandler(this.btmATGM_Click);
            // 
            // GrdConvoy
            // 
            this.GrdConvoy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdConvoy.Location = new System.Drawing.Point(13, 13);
            this.GrdConvoy.Name = "GrdConvoy";
            this.GrdConvoy.Size = new System.Drawing.Size(775, 352);
            this.GrdConvoy.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GrdConvoy);
            this.Controls.Add(this.btmATGM);
            this.Controls.Add(this.DispararPak43);
            this.Controls.Add(this.BtnStanag4172);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GrdConvoy)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnStanag4172;
        private System.Windows.Forms.Button DispararPak43;
        private System.Windows.Forms.Button btmATGM;
        private System.Windows.Forms.DataGridView GrdConvoy;
    }
}

