﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLpreparcial2._2;

namespace preparcial2._2
{
    public partial class Form1 : Form
    {
        public Convoy convoy = new Convoy();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Vehiculo.regimiento = "Regimiento Argentino";
            convoy.AgregarVehiculos(new Humvee());
            convoy.AgregarVehiculos(new Challenger1());
            convoy.AgregarVehiculos(new RenaultSherpa());
            GrdConvoy.Columns.Add("Nombre", "Nombre");
            GrdConvoy.Columns.Add("Regimiento", "Regimiento");
            GrdConvoy.Columns.Add("Estado", "Estado");
            GrdConvoy.Columns.Add("Blindaje", "Blindaje");
            GrdConvoy.Columns.Add("Destruido", "Destruido");
            actualizar();
        }
        public void actualizar()
        {
            GrdConvoy.Rows.Clear();
            foreach (var vehiculos in convoy)
            {
                if(vehiculos is Challenger1)
                {
                    GrdConvoy.Rows.Add(vehiculos.nombre,Vehiculo.regimiento, vehiculos.estado, ((Challenger1)vehiculos).blindaje, vehiculos.destruido);
                }
                else
                {
                    GrdConvoy.Rows.Add(vehiculos.nombre, Vehiculo.regimiento, vehiculos.estado, "-", vehiculos.destruido);
                }
            }
        }

        private void BtnStanag4172_Click(object sender, EventArgs e)
        {
            convoy.RecibirProyectil(new Stanag4172());
            actualizar();
        }

        private void DispararPak43_Click(object sender, EventArgs e)
        {
            convoy.RecibirProyectil(new Pak43());
            actualizar();
        }

        private void btmATGM_Click(object sender, EventArgs e)
        {
            convoy.RecibirProyectil(new ATGM());
            actualizar();
        }
    }
}
