﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tateti_por_mi
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            MostrarMenu();

        }
        static void MostrarMenu ()
        {
            int op;
            do
            {
                Console.Clear();
                Console.SetCursorPosition(15, 1);
                Console.WriteLine("1-Jugar");
                Console.SetCursorPosition(15, 2);
                Console.WriteLine("2-salir");
                Console.SetCursorPosition(15, 3);
                Console.WriteLine("3-Ayuda");
                Console.SetCursorPosition(15, 4);
                Console.WriteLine("-------");
                Console.SetCursorPosition(15, 5);
                Console.WriteLine("Seleccione una opción para continuar >? ");
                op = PedirNumero(15,6);
                switch (op)
                {
                    case 1:
                        //jugamos
                        Jugar();
                        break;
                    case 2:
                        //salimos del juego
                        break;
                    case 3:
                        //le damos información al jugador
                        MostrarMensaje("todos saben como jugar al tateti, solo tenes que especificar en que numero queres que ponga tu ficha");
                        break;
                    default:
                        //la opcion no es correcta
                        MostrarMensaje("La opcion elegida no es correcta, intente nuevamente");
                        break;


                }
            } while (op != 2);
            MostrarMensaje("El juego termino.Gracias por jugar :D");
        }
        static int PedirNumero(int fila, int columna)
        {
            //numero valido entre 0-9
            string s;
            int r = -1;
            Console.SetCursorPosition(fila, columna);
            s = Console.ReadLine();
            if (s.Length == 1)
            {
                if (Encoding.ASCII.GetBytes(s)[0]>=48 && Encoding.ASCII.GetBytes(s)[0]<=57)
                {
                    r = Convert.ToInt32(s);
                }
                else
                {
                    if (Encoding.ASCII.GetBytes(s)[0] == 83 || Encoding.ASCII.GetBytes(s)[0] <= 115)
                    {
                        r = -2;
                    }
                }
            }
            else
            {
                r = -1;
            }
            return r;
        }
        static void MostrarMensaje(string s)
        {
            Console.Clear();
            Console.SetCursorPosition(15, 6);
            Console.WriteLine(s);
            Console.ReadKey();
        }
        static void Jugar()
        {
            //preguntar nombres de cada jugador
            string[] jugador = new string[2];
            Console.Clear();
            Console.SetCursorPosition(15, 1);
            Console.Write("ingrese el nombre del primer jugador:");
            jugador[0] = Console.ReadLine();
            Console.Clear();
            Console.SetCursorPosition(15, 1);
            Console.Write("ingrese el nombre del segundo jugador: ");
            jugador[1] = Console.ReadLine();
            //variables para tablero
            char[] tablero = { '0', '1', '2', '3', '4', '5', '6', '7', '8' };
            char[] ficha = { 'x', 'o' };
            int op = 0;
            int turno = -1;
            do
            {
                //dibujar Tablero
                
                //gestionar Turnos
                DarTurno(ref turno);
                
                bool salir = false;
                do
                {
                    DibujarTablero(tablero);
                    Console.SetCursorPosition(15, 12);
                    Console.WriteLine("{0}donde desea colocar la ficha{1}?(S para salir) ", jugador[turno], ficha[turno]);
                    op = PedirNumero(15, 13);
                    if (op >= 0 && op <= 8)
                    {
                        if (tablero[op] != 'x' && tablero[op] != 'o')
                        {
                            tablero[op] = ficha[turno];
                            salir = true;
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("esa posición ya está ocupada. intente en otra posición");
                            Console.ReadKey();
                        }
                        
                    }
                    if (op == -2)
                    {
                        salir = true;
                        op = -2;
                    }
                    
                } while (!salir);
                op= CorroborarGanador(tablero,jugador,ficha,op,turno);
            } while (op != -2);
           
            //informar Ganador

        }
        static int CorroborarGanador(char[] tablero, string[] jugador, char[] ficha,int op,int turno)  
        {
            if ((tablero[0] == 'x' && tablero[1] == 'x' && tablero[2] == 'x')|| (tablero[0] == 'o' && tablero[1] == 'o' && tablero[2] == 'o'))
            {
                Console.Clear();
                DibujarTableroGanador(tablero);
                Texto("el jugador " + jugador[turno] + " gano ", 1, 2, 0, 15, 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.ReadKey();
                op = -2;
                return op;
            }
            if ((tablero[3] == 'x' && tablero[4] == 'x' && tablero[5] == 'x') || (tablero[3] == 'o' && tablero[4] == 'o' && tablero[5] == 'o'))
            {
                Console.Clear();
                DibujarTableroGanador(tablero);
                Texto("el jugador " + jugador[turno] + " gano ", 1, 2, 0, 15, 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.ReadKey();
                op = -2;
                return op;
            }
            if ((tablero[6] == 'x' && tablero[7] == 'x' && tablero[8] == 'x') || (tablero[6] == 'o' && tablero[7] == 'o' && tablero[8] == 'o'))
            {
                Console.Clear();
                DibujarTableroGanador(tablero);
                Texto("el jugador " + jugador[turno] + " gano ", 1, 2, 0, 15, 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.ReadKey();
                op = -2;
                return op;
            }
            if ((tablero[0] == 'x' && tablero[3] == 'x' && tablero[6] == 'x') || (tablero[0] == 'o' && tablero[3] == 'o' && tablero[6] == 'o'))
            {
                Console.Clear();
                DibujarTableroGanador(tablero);
                Texto("el jugador " + jugador[turno] + " gano ", 1, 2, 0, 15, 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.ReadKey();
                op = -2;
                return op;
            }
            if ((tablero[1] == 'x' && tablero[4] == 'x' && tablero[7] == 'x') || (tablero[1] == 'o' && tablero[4] == 'o' && tablero[7] == 'o'))
            {
                Console.Clear();
                DibujarTableroGanador(tablero);
                Texto("el jugador " + jugador[turno] + " gano ", 1, 2, 0, 15, 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.ReadKey();
                op = -2;
                return op;
            }
            if ((tablero[2] == 'x' && tablero[5] == 'x' && tablero[8] == 'x') || (tablero[2] == 'o' && tablero[5] == 'o' && tablero[8] == 'o'))
            {
                Console.Clear();
                DibujarTableroGanador(tablero);
                Texto("el jugador " + jugador[turno] + " gano ", 1, 2, 0, 15, 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.ReadKey();
                op = -2;
                return op;
            }
            if ((tablero[0] == 'x' && tablero[4] == 'x' && tablero[8] == 'x') || (tablero[0] == 'o' && tablero[4] == 'o' && tablero[8] == 'o'))
            {
                Console.Clear();
                DibujarTableroGanador(tablero);
                Texto("el jugador " + jugador[turno] + " gano ", 1, 2, 0, 15, 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.ReadKey();
                op = -2;
                return op;
            }
            if ((tablero[2] == 'x' && tablero[4] == 'x' && tablero[6] == 'x') || (tablero[2] == 'o' && tablero[4] == 'o' && tablero[6] == 'o'))
            {
                Console.Clear();
                DibujarTableroGanador(tablero);
                Texto("el jugador " + jugador[turno] + " gano ", 1, 2, 0, 15, 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.ReadKey();
                op = -2;
                return op;
            }
            if ((tablero[2] == 'x' && tablero[4] == 'x' && tablero[6] == 'x') || (tablero[2] == 'o' && tablero[4] == 'o' && tablero[6] == 'o'))
            {
                Console.Clear();
                DibujarTableroGanador(tablero);
                Texto("el jugador " + jugador[turno] + " gano ", 1, 2, 0, 15, 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.ReadKey();
                op = -2;
                return op;
            }
            if ((tablero[0]=='x'||tablero[0]=='o')&& (tablero[1] == 'x' || tablero[1] == 'o')&& (tablero[2] == 'x' || tablero[2] == 'o')&& (tablero[3] == 'x' || tablero[3] == 'o')&& (tablero[4] == 'x' || tablero[4] == 'o')&& (tablero[5] == 'x' || tablero[5] == 'o')&& (tablero[6] == 'x' || tablero[6] == 'o')&& (tablero[7] == 'x' || tablero[7] == 'o')&& (tablero[8] == 'x' || tablero[8] == 'o'))
            {
                Console.Clear();
                DibujarTablero(tablero);
                Texto("empataron hijos de puta", 1, 2, 0, 15, 1);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.ReadKey();
                op = -2;
                return op;
            }
            else
            {
                return op;
            }

        }
        static void DarTurno(ref int turno)
        {
            if (turno == -1)
            {
                //comienza el juego
                int semilla = Environment.TickCount;
                Random rnd = new Random(semilla);
                turno = rnd.Next(0, 2);
            }
            if (turno == 0)
            {
                turno = 1;
            }
            else
            {
                turno = 0;
            }
                
        }
       static void DibujarTableroGanador(char[]tablero)
        {
            if ((tablero[0] == 'x' && tablero[1] == 'x' && tablero[2] == 'x')|| (tablero[0] == 'o' && tablero[1] == 'o' && tablero[2] == 'o'))
            {
                Console.Clear();
                Texto(" " + tablero[0] + "| " + tablero[1] + "| " + tablero[2], 1, 2, 1, 15, 1);
                Texto("----------", 1, 1, 1, 15, 2);
                Texto(" " + tablero[3] + "| " + tablero[4] + "| " + tablero[5], 1, 1, 1, 15, 3);
                Texto("----------", 1, 1, 1, 15, 4);
                Texto(" " + tablero[6] + "| " + tablero[7] + "| " + tablero[8], 1, 1, 1, 15, 5);
            }
            if ((tablero[3] == 'x' && tablero[4] == 'x' && tablero[5] == 'x') || (tablero[3] == 'o' && tablero[4] == 'o' && tablero[5] == 'o'))
            {
                Console.Clear();
                Texto(" " + tablero[0] + "| " + tablero[1] + "| " + tablero[2], 1, 1, 1, 15, 1);
                Texto("----------", 1, 1, 1, 15, 2);
                Texto(" " + tablero[3] + "| " + tablero[4] + "| " + tablero[5], 1, 2, 1, 15,3);
                Texto("----------", 1, 1, 1, 15, 4);
                Texto(" " + tablero[6] + "| " + tablero[7] + "| " + tablero[8], 1, 1, 1, 15, 5);
            }
            if ((tablero[6] == 'x' && tablero[7] == 'x' && tablero[8] == 'x') || (tablero[6] == 'o' && tablero[7] == 'o' && tablero[8] == 'o'))
            {
                Console.Clear();
                Texto(" " + tablero[0] + "| " + tablero[1] + "| " + tablero[2], 1, 1, 1, 15, 1);
                Texto("----------", 1, 1, 1, 15, 2);
                Texto(" " + tablero[3] + "| " + tablero[4] + "| " + tablero[5], 1, 1, 1, 15, 3);
                Texto("----------", 1, 1, 1, 15, 4);
                Texto(" " + tablero[6] + "| " + tablero[7] + "| " + tablero[8], 1, 2, 1, 15, 5);
                Console.ForegroundColor = ConsoleColor.Green;
            }
            if ((tablero[0] == 'x' && tablero[3] == 'x' && tablero[6] == 'x') || (tablero[0] == 'o' && tablero[3] == 'o' && tablero[6] == 'o'))
            {
                Console.Clear();
                Texto(" " + tablero[0], 2, 2, 1, 15, 1);
                Texto("| " + tablero[1] + "| " + tablero[2], 1, 1, 0, 0, 0);
                Texto("--", 2, 2, 1, 15, 2);
                Texto("--------", 1, 1, 0, 0, 0);
                Texto(" " + tablero[3], 2, 2, 1, 15, 3);
                Texto("| " + tablero[4] + "| " + tablero[5], 1, 1, 0, 0, 0);
                Texto("--", 2, 2, 1, 15, 4);
                Texto("--------", 1, 1, 0, 0, 0);
                Texto(" " + tablero[6], 2, 2, 1, 15, 5);
                Texto("| " + tablero[7] + "| " + tablero[8], 1, 1, 0, 0, 0);
            }
            if ((tablero[1] == 'x' && tablero[4] == 'x' && tablero[7] == 'x') || (tablero[1] == 'o' && tablero[4] == 'o' && tablero[7] == 'o'))
            {
                Console.Clear();
                Texto(" " + tablero[0] + "| ", 2, 1, 1, 15, 1);
                Texto(Convert.ToString(tablero[1]), 2, 2, 0, 0, 0);
                Texto("| " + tablero[2], 1, 1, 0, 0, 0);
                Texto("---", 2, 1, 1, 15, 2);
                Texto("--", 2, 2, 0, 0, 0);
                Texto("-----", 1, 1, 0, 0, 0);
                Texto(" " + tablero[3] + "| ", 2, 1, 1, 15, 3);
                Texto(Convert.ToString(tablero[4]), 2, 2, 0, 0, 0);
                Texto("| " + tablero[5], 1, 1, 0, 0, 0);
                Texto("---", 2, 1, 1, 15, 4);
                Texto("--", 2, 2, 0, 0, 0);
                Texto("-----", 1, 1, 0, 0, 0);
                Texto(" " + tablero[6] + "| ",2, 1, 1, 15, 5);
                Texto(Convert.ToString(tablero[7]), 2, 2, 0, 0, 0);
                Texto("| " + tablero[8], 1, 1, 0, 0, 0);
            }
            if ((tablero[2] == 'x' && tablero[5] == 'x' && tablero[8] == 'x') || (tablero[2] == 'o' && tablero[5] == 'o' && tablero[8] == 'o'))
            {
                Console.Clear();
                Texto(" " + tablero[0] + "| " + tablero[1] + "| ", 2, 1, 1, 15, 1);
                Texto(Convert.ToString(tablero[2]), 1, 2, 0, 0, 0);
                Texto("------", 2, 1, 1, 15, 2);
                Texto("----", 1, 2, 0, 0, 0);
                Texto(" " + tablero[3] + "| " + tablero[4] + "| ", 2, 1, 1, 15, 3);
                Texto(Convert.ToString(tablero[5]), 1, 2, 0, 0, 0);
                Texto("------", 2, 1, 1, 15, 4);
                Texto("----", 1, 2, 0, 0, 0);
                Texto(" " + tablero[6] + "| " + tablero[7] + "| ", 2, 1, 1, 15, 5);
                Texto(Convert.ToString(tablero[8]), 1, 2, 0, 0, 0);
                Console.ForegroundColor = ConsoleColor.Green;
            }
            if ((tablero[0] == 'x' && tablero[4] == 'x' && tablero[8] == 'x') || (tablero[0] == 'o' && tablero[4] == 'o' && tablero[8] == 'o'))
            {
                Console.Clear();
                Texto(" " + tablero[0], 2, 2, 1, 15, 1);
                Texto("| " + tablero[1] + "| " + tablero[2], 1, 1, 0, 0, 0);
                Texto("--", 2, 2, 1, 15, 2);
                Texto("--------", 1, 1, 0, 0, 0);
                Texto(" " + tablero[3] + "| ", 2, 1, 1, 15, 3);
                Texto(Convert.ToString(tablero[4]), 2, 2, 0, 0,0);
                Texto("| " + tablero[5], 1, 1, 0, 0, 0);
                Texto("---", 2, 1, 1, 15, 4);
                Texto("--", 2, 2, 0, 0, 0);
                Texto("-----", 1, 1, 0, 0, 0);
                Texto(" " + tablero[6] + "| " + tablero[7] + "| ", 2, 1, 1, 15, 5);
                Texto(Convert.ToString(tablero[8]), 1, 2, 0, 0, 0);
                Console.ForegroundColor = ConsoleColor.Green;
            }
            if ((tablero[2] == 'x' && tablero[4] == 'x' && tablero[6] == 'x') || (tablero[2] == 'o' && tablero[4] == 'o' && tablero[6] == 'o'))
            {
                Console.Clear();
                Texto(" " + tablero[0] + "| " + tablero[1] + "| ", 2, 1, 1, 15, 1);
                Texto(Convert.ToString(tablero[2]), 1, 2, 0, 0, 0);
                Texto("------", 2, 1, 1, 15, 2);
                Texto("----", 2, 2, 0, 0, 0);
                Texto(" " + tablero[3] + "| ", 2, 1, 1, 15, 3);
                Texto(Convert.ToString(tablero[4]), 2, 2, 0, 0, 0);
                Texto("| " + tablero[5], 1, 1, 0, 0, 0);
                Texto("---", 2, 1, 1, 15, 4);
                Texto("--", 2, 2, 0, 0, 0);
                Texto("-----", 1, 1, 0, 0, 0);
                Texto(" " + tablero[6], 2, 2, 1, 15, 5);
                Texto("| " + tablero[7] + "| " + tablero[8], 1, 1, 0, 0, 0);
            }
        }
        static void DibujarTablero(char[]tablero)
        {
            Console.Clear();
            Texto(" " + tablero[0] + "| " + tablero[1] + "| " + tablero[2], 1, 1,1, 15, 1);
            Texto("----------", 1, 1,1, 15, 2);
            Texto(" " + tablero[3] + "| " + tablero[4] + "| " + tablero[5], 1, 1,1, 15, 3);
            Texto("----------",1,1,1,15,4);
            Texto(" " + tablero[6] + "| " + tablero[7] + "| " + tablero[8], 1, 1,1, 15, 5);
            
        }
        static void Texto (string texto,int QueTipoDeTextoQuieres, int color,int QueresPosicion,int x, int y)
        {
            //funcion para imprimir texto en pantalla que te permite imprimirlo con un color dado, elegir que posicion en pantalla imprimirlo
            if(QueresPosicion==1)
            Console.SetCursorPosition(x, y);
            if (color == 1)
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            if(color==2)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            if(QueTipoDeTextoQuieres==1)
            {
                Console.WriteLine(texto);
            }
            if (QueTipoDeTextoQuieres==2)
            {
                Console.Write(texto);
            }
        }
    }
    
}
