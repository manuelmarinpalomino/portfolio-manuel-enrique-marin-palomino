#pragma once
#ifndef TABLERO
#define TABLERO
#include <iostream>
#include "Game.h"


struct Celda
{
	int fila;
	int Columna;

};


struct Ganador {
	bool ganador;
	char ficha;
	int fila;
};


Celda ConvertirPos_a_Celda(int pos) {

	Celda celda;


	switch (pos)
	{
	case 0:
		celda.fila = 0;
		celda.Columna = 0;
		break;
	case 1:
		celda.fila = 0;
		celda.Columna = 1;
		break;
	case 2:
		celda.fila = 0;
		celda.Columna = 2;
		break;
	case 3:
		celda.fila = 1;
		celda.Columna = 0;
		break;
	case 4:
		celda.fila = 1;
		celda.Columna = 1;
		break;
	case 5:
		celda.fila = 1;
		celda.Columna = 2;
		break;
	case 6:
		celda.fila = 2;
		celda.Columna = 0;
		break;
	case 7:
		celda.fila = 2;
		celda.Columna = 1;
		break;
	case 8:
		celda.fila = 2;
		celda.Columna = 2;
		break;

	default:
		break;
	}
	return celda;
}
void TableroInicializar(unsigned char tablero[3][3]) {

	tablero[0][0] = '0';
	tablero[0][1] = '1';
	tablero[0][2] = '2';
	tablero[1][0] = '3';
	tablero[1][1] = '4';
	tablero[1][2] = '5';
	tablero[2][0] = '6';
	tablero[2][1] = '7';
	tablero[2][2] = '8';


}
void TableroInicializar(char c, unsigned char tablero[3][3]) {

	
	for (int f = 0; f < 3; f++) {
		for (int j = 0; j < 3; j++) {
			
				tablero[f][j]=c;
			
		}
	}
	

}

Ganador ControlarFicha(char ficha, unsigned char tablero[3][3]) {

	Ganador g;
	g.ganador = false;
	g.ficha = ficha;
	g.fila = 0;
	bool ganador = false;
	int fila = 0;

	//Fila
	if (tablero[0][0] == ficha && tablero[0][1] == ficha && tablero[0][2] == ficha) {
		g.ganador = true;
		g.fila = 1;
	}
	else {
		if (tablero[1][0] == ficha && tablero[1][1] == ficha && tablero[1][2] == ficha) {
			g.ganador = true;
			g.fila = 2;
		}
		else {

			if (tablero[2][0] == ficha && tablero[2][1] == ficha && tablero[2][2] == ficha) {
				g.ganador = true;
				g.fila = 3;
			}

		}

	}
	//Columna

	if (tablero[0][0] == ficha && tablero[1][0] == ficha && tablero[2][0] == ficha) {
		g.ganador = true;
		g.fila = 4;
	}
	else {
		if (tablero[0][1] == ficha && tablero[1][1] == ficha && tablero[2][1] == ficha) {
			g.ganador = true;
			g.fila = 5;
		}
		else {

			if (tablero[0][2] == ficha && tablero[1][2] == ficha && tablero[2][2] == ficha) {
				g.ganador = true;
				g.fila = 6;
			}

		}

	}

	//diagonales
	if (tablero[0][0] == ficha && tablero[1][1] == ficha && tablero[2][2] == ficha) {
		g.ganador = true;
		g.fila = 7;
	}
	else {
		if (tablero[0][1] == ficha && tablero[1][1] == ficha && tablero[2][0] == ficha) {
			g.ganador = true;
			g.fila = 8;
		}


	}


	return g;

}

bool TableroCeldaEstaOcupada(int pos, unsigned char tablero[3][3]) {

	bool ocupado = false;
	Celda celda;

	if (pos >= 0 && pos <= 8) {
		celda = ConvertirPos_a_Celda(pos);
		if (tablero[celda.fila][celda.Columna] == 'X' || tablero[celda.fila][celda.Columna] == 'O') {
			ocupado = true;
		}
	}
	else {
		//error por valor fuera de rango
		ocupado = true;
	}
	return ocupado;
}

void SeleccionarColor(int pos, int color,unsigned char tablero[3][3]) {
	if (TableroCeldaEstaOcupada(pos, tablero)) {
		SetColor(color);
	}
}

void TableroDibujarMaterix(int X, int Y, int colorNeutro, int colorActivo, unsigned char tablero[3][3]) {
	setcursor(0, 0);
	cls();
	SetColor(colorNeutro);
	// FILA 1
	gotoxy(X, Y);

	SeleccionarColor(0,colorActivo, tablero);
	printf("%c", tablero[0][0]);
	SetColor(colorNeutro);
	gotoxy(X + 1, Y);
	printf("%c", 179);
	gotoxy(X + 2, Y);
	SeleccionarColor(1,colorActivo, tablero);
	printf("%c", tablero[0][1]);
	SetColor(colorNeutro);
	gotoxy(X + 3, Y);
	printf("%c", 179);
	gotoxy(X + 4, Y);
	SeleccionarColor(2, colorActivo, tablero);
	printf("%c", tablero[0][2]);
	SetColor(colorNeutro);
	//dubujar fila sepador
	gotoxy(X, Y + 1);
	printf("%c", 196);
	gotoxy(X + 1, Y + 1);
	printf("%c", 197);
	gotoxy(X + 2, Y + 1);
	printf("%c", 196);
	gotoxy(X + 3, Y + 1);
	printf("%c", 197);
	gotoxy(X + 4, Y + 1);
	printf("%c", 196);

	//FILA 2
	gotoxy(X, Y + 2);
	SeleccionarColor(3, colorActivo, tablero);
	printf("%c", tablero[1][0]);
	gotoxy(X + 1, Y + 2);
	SetColor(colorNeutro);
	printf("%c", 179);
	gotoxy(X + 2, Y + 2);
	SeleccionarColor(4, colorActivo, tablero);
	printf("%c", tablero[1][1]);
	SetColor(colorNeutro);
	gotoxy(X + 3, Y + 2);
	printf("%c", 179);
	gotoxy(X + 4, Y + 2);
	SeleccionarColor(5, colorActivo, tablero);
	printf("%c", tablero[1][2]);
	SetColor(colorNeutro);

	//dubujar fila sepador
	gotoxy(X, Y + 3);
	printf("%c", 196);
	gotoxy(X + 1, Y + 3);
	printf("%c", 197);
	gotoxy(X + 2, Y + 3);
	printf("%c", 196);
	gotoxy(X + 3, Y + 3);
	printf("%c", 197);
	gotoxy(X + 4, Y + 3);
	printf("%c", 196);

	//FILA 3
	gotoxy(X, Y + 4);
	SeleccionarColor(6, colorActivo, tablero);
	printf("%c", tablero[2][0]);
	SetColor(colorNeutro);
	gotoxy(X + 1, Y + 4);
	printf("%c", 179);
	gotoxy(X + 2, Y + 4);
	SeleccionarColor(7, colorActivo, tablero);
	printf("%c", tablero[2][1]);
	SetColor(colorNeutro);
	gotoxy(X + 3, Y + 4);
	printf("%c", 179);
	gotoxy(X + 4, Y + 4);
	SeleccionarColor(8, colorActivo, tablero);
	printf("%c", tablero[2][2]);
	SetColor(colorNeutro);
}

void TableroDibujar(int X, int Y, unsigned char tablero[3][3]) {

	TableroDibujarMaterix(X, Y, 15,9, tablero);

}

void TableroLLenarFilaGanadora(int fila, unsigned char tablero[3][3]) {

	switch (fila)
	{
	case 1:
		tablero[0][0] = 'X';
		tablero[0][1] = 'X';
		tablero[0][2] = 'X';		
		break;
	case 2:
		
		tablero[1][0] = 'X';
		tablero[1][1] = 'X';
		tablero[1][2] = 'X';
		break;
	case 3:
		tablero[2][0] = 'X';
		tablero[2][1] = 'X';
		tablero[2][2] = 'X';
		break;
	case 4:
		tablero[0][0] = 'X';
		tablero[1][0] = 'X';
		tablero[2][0] = 'X';
		break;
	case 5:
		tablero[0][1] = 'X';
		tablero[1][1] = 'X';
		tablero[2][1] = 'X';
		break;
	case 6:
		tablero[0][2] = 'X';
		tablero[1][2] = 'X';
		tablero[2][2] = 'X';
		break;
	case 7:
		tablero[0][0] = 'X';
		tablero[1][1] = 'X';
		tablero[2][2] = 'X';
		break;
	case 8:
		tablero[0][2] = 'X';
		tablero[1][1] = 'X';
		tablero[2][0] = 'X';
		break;

	default:
		break;
	}

}

void TableroDibujarFilaGanadora(int X, int Y, char ficha, unsigned char tablero[3][3]) {

	Ganador g;
	g = ControlarFicha(ficha, tablero);
	
	TableroInicializar(' ', tablero);
	TableroLLenarFilaGanadora(g.fila,tablero);

	TableroDibujarMaterix(X, Y,15,10, tablero);

}



void TableroColocarFicha(int pos, char ficha, unsigned char tablero[3][3]) {

	Celda celda;
	celda = ConvertirPos_a_Celda(pos);
	tablero[celda.fila][celda.Columna] = ficha;

}

char TableroTurno(char ficha) {

	if (ficha == 'X') {
		ficha = 'O';
	}
	else {
		ficha = 'X';
	}

	return ficha;
}





bool TableroLleno(unsigned char tablero[3][3]) {
	bool lleno = false;
	int cont = 0;
	for (int f = 0; f < 3; f++) {
		for (int j = 0; j < 3; j++) {
			if (tablero[f][j] == 'X' || tablero[f][j] == 'O') {
				cont += 1;
			}
		}
	}

	if (cont == 8) {
		lleno = true;
	}

	return lleno;

}

char TableroHayGanador(unsigned char tablero[3][3]) {

	char ficha='*';
	Ganador g;
	g = ControlarFicha('X', tablero);
	
	if (g.ganador ) {
		//gano X
		ficha = 'X';
	}

	g = ControlarFicha('O', tablero);

	if (g.ganador ) {
			//gano X
			ficha = 'O';
	}
	
	return ficha;

}
#endif // !TABLERO

