#pragma once
#ifndef CICLO
#define CICLO
#include <iostream>
#include <locale.h>
#include "Tablero.h"
#include "Game.h"

using namespace std;
void Inicializar() {
	 
}
void Cargar() {
	SetColor(15);
	cls();
	gotoxy(10, 8);
	printf("Juguemos al ");
	gotoxy(10, 9);
	printf("T A T E T I");

	gotoxy(10, 10);
	printf("Presione una tecla para continuar ....");

	cin.ignore();
	cin.get();

}
void Dibujar(unsigned char tablero[3][3]) {
	setlocale(LC_ALL, "C");
	TableroDibujar(18,8, tablero);
	
}
int Actualizar(char turno, unsigned char tablero[3][3]) {
	setlocale(LC_ALL, "es_ES");
	
	
	int op;
	bool salir = false;
	
	
	do {
		gotoxy(10, 15);
		setcursor(1, 10);
		cout << "D�nde desea poner la ficha " << turno << " ? ";
		cin >> op;
		if (!TableroCeldaEstaOcupada(op, tablero)) {
			salir = true;
		}
		else {
		//muestro el error
			gotoxy(10, 17);
			setcursor(1, 10);
			cout << "La celda esta Ocupada";
		}

	} while (!salir);

	
	
	TableroColocarFicha(op, turno, tablero);

	//? si hay ganador
	if (TableroHayGanador(tablero) != '*' || TableroLleno(tablero)) {
		
		op = -1;
	}

	return op;
}
void Descargar() {
	 
}
void Finalizar() {

	SetColor(15);
	cls();
	gotoxy(10, 8);
	printf("Gracias por jugar al ");
	gotoxy(10, 9);
	printf("T A T E T I");

	gotoxy(10, 10);
	printf("Presione una tecla para continuar ....");

	cin.ignore();
	cin.get();
}


void Play() {

	Inicializar();
	Cargar();

	bool salir = false;

	unsigned char tablero[3][3];
	TableroInicializar(tablero);
	char turno = 'X';
	int op=0;
	do {
	
		Dibujar(tablero);
		if (op == -1) {
			
			if (TableroHayGanador(tablero) != '*') {
				//avisr quien gano
				//TableroDibujar(18, 8, tablero);
				cls();
				TableroDibujarFilaGanadora(18, 8, TableroHayGanador(tablero), tablero);
				gotoxy(10, 19);
				cout << "Gano la " << TableroHayGanador(tablero);
				
			}
			else {
				gotoxy(10, 19);
				cout << "Hay un empate" << TableroHayGanador(tablero);

			}

			cin.ignore();
			cin.get();
			salir = true;
		}
		else {
			op = Actualizar(turno, tablero);
			turno = TableroTurno(turno);
		}
		
		
	} while (!salir);

	Descargar();
	Finalizar();


}



#endif // !CICLO

