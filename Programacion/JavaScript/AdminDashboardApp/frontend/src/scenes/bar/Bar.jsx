import React from 'react'
import { Box } from '@mui/material'
import Header from '../../components/Header'
import BarChart from '../../components/BarChart'

const Bar = () => {
  return (
    <Box
      display="flex"
      justifyContent="space-between"
      flexDirection="column"
      m="10px"
    >
      <Header title="Bar Chart" subtitle="Simple Bar Chart" />
      <Box width="100% " display="flex" justifyContent="center">
        <Box height="75vh" width="76vw !important">
          <BarChart />
        </Box>
      </Box>
    </Box>
  )
}

export default Bar
