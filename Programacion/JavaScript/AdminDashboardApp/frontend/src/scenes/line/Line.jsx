import React from 'react'
import { Box } from '@mui/material'
import Header from '../../components/Header'
import LineChart from '../../components/LineChart'

const Line = () => {
  return (
    <Box m="20px">
      <Header title="Line Chart" subtitle="Simple Line Chart" />
      <Box width="100% " display="flex" justifyContent="center">
      <Box height="75vh" width="76vw !important">
        <LineChart />
      </Box>
      </Box>
    </Box>
  )
}

export default Line
