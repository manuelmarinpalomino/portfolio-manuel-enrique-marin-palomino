import React from 'react'
import { Box } from '@mui/material'
import Header from '../../components/Header'
import PieChart from '../../components/PieChart'

const Pie = () => {
  return (
    <Box m="20px">
      <Header title="Pie Chart" subtitle="Simple Pie Chart" />
      <Box width="100% " display="flex" justifyContent="center">
      <Box height="75vh" width="76vw !important">
        <PieChart />
        </Box>
      </Box>
    </Box>
  )
}

export default Pie
