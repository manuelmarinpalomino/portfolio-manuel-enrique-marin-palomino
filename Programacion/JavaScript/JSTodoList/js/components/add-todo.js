import Alert from "./alert.js";
export default class AddTodo {
    constructor(){
        this.btn = document.querySelector('#add');
        this.title = document.querySelector('#title');
        this.description = document.querySelector('#description');
        this.Alert = new Alert('alert');
    }
    onclick(callback){
        this.btn.onclick = ()=>{
            if (title.value === '' || description.value === '') {
                this.Alert.show('Title and description are required');
            }
            else{
                this.Alert.hide();
                callback(this.title.value, this.description.value);
            }
        }
    }
}