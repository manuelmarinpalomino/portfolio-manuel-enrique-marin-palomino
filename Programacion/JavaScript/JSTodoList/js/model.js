 export default class Model {
    constructor(){
        this.view = null;
        this.Todos = JSON.parse(localStorage.getItem('todos'));
        if(!this.Todos || this.Todos.length < 1){
            this.Todos = [
            ];
            this.currentId = 1;
        } else {
            this.currentId = this.Todos[this.Todos.length - 1].id +1;
        }
    }
    setView(view){
        this.view;
    }
    save() {
        localStorage.setItem('todos', JSON.stringify(this.Todos));
    }
    getTodos(){
        return this.Todos;
    }
    addTodo(title, description){
        const todo = {
            id: this.currentId++,
            title,
            description,
            completed: false,
        }
        this.Todos.push(todo);
        this.save();
        return {...todo};
    }
    editTodo(id,values){
        const Todo = this.Todos[this.findTODO(id)];
        Todo.title = values.title;
        Todo.description = values.description;
        Todo.completed = values.completed;
        this.save();
    }
    findTODO (id){
        return this.Todos.findIndex((todo) => todo.id ===id);
    }
    toggleCompleted(id){
        const todo = this.Todos[this.findTODO(id)];
        todo.completed = !todo.completed;
        this.save();
    }
    removeTodo(id){
        this.Todos.splice(this.findTODO(id),1);
        this.save();
    }
}