/* eslint-disable react-hooks/exhaustive-deps */
import './App.css'
import { useEffect, useState, useRef, useCallback } from 'react'
import { Movies } from './components/movies'
import { useMovies } from './hooks/useMovies'
import debounce from 'just-debounce-it'
function useSearch (){
  const [search, updateSearch] = useState('')
  const [error, setError] = useState(null)
  const isFirstInput = useRef(true)
  useEffect(()=>{
    if(isFirstInput.current){
      isFirstInput.current= search === ''
      return
    }
    if(search===''){
      setError('no se puede buscar una pelicula vacia')
      return
    }
    if(search.match(/^\d+$/)){
      setError('no se puede buscar una pelicula con un numero')
      return
    }
    if (search.length <3){
      setError('la busqueda debe tener al menos 3 caracteres')
      return
    }
    setError(null)
},[search])
  return {search, updateSearch, error}
}
export function App() {
  const [sort,setSort] = useState(false)
  const {search, updateSearch, error} = useSearch()
  const {movies, getMovies, loading} = useMovies({search, sort})
  const debouncedGetMovies = useCallback( debounce((search) =>{
    getMovies({search})
  },500),[])
  const handleSubmit = (event) => {
    event.preventDefault()
    getMovies({search})
  }
  const handleSort = () =>{
    setSort(!sort)
  }
  const handleChange = (event) => {
    const newSearch = event.target.value
    updateSearch(newSearch)
    debouncedGetMovies(newSearch)
  }

  return (
    <div className="page">
      <header>
        <h1>Buscador Peliculas:</h1>
        <form className='form' onSubmit={handleSubmit}>
          <input name='query' onChange={handleChange} value={search}  placeholder='Avengers, Star Wars, The Matrix...'/>
          <input name='sort' onChange={handleSort} value={sort} type='checkbox' />
          <button type='submit'>Buscar</button>
        </form>
        {error && <p style={{color:'red'}}>{error}</p>}
      </header>
      <main>
        {loading ? (<p>cargando...</p>): <Movies movies={movies}></Movies>}

      </main>
    </div>
  )
}

export default App
