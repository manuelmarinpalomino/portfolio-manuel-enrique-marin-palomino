/* eslint-disable react/prop-types */

function ListOfMovies ({movies}) {
    return (
            <ul className='movies'>
              {
                movies.map(movie => {
                  return(
                    <li key={movie.id}>
                    <h3>{movie.title}</h3>
                    <p>{movie.year}</p>
                    <img src={movie.poster} className="poster" alt={movie.title} />
                  </li>
                )
                })
              }
            </ul>)
}

function NoMoviesResults(){
    return(
        <p> No se encuentran peliculas para esta busqueda</p>
    )
}

export function Movies({movies}){
    const hasMovies = movies?.length > 0
    return(
        hasMovies? (<ListOfMovies movies={movies}/>) : (<NoMoviesResults/>)
    )
}