import './App.css'
import { Suspense, lazy } from 'react'
import Router from './Router'
import { Route } from './Route'

const AboutPage = lazy(() => import('./pages/AboutMe.jsx'))
const HomePage = lazy(() => import('./pages/Home'))
const Search = lazy(() => import('./pages/Search'))
const Page404 = lazy(() => import('./pages/404'))

const routes = [
  {
    path: '/',
    component: HomePage
  },
  {
    path: '/about',
    component: AboutPage
  },
  {
    path: '/search/:query',
    component: Search
  }
]

function App () {
  return (
    <main>
      <Suspense fallback={<div>Loading...</div>}>

        <Router routes={routes} DefaultComponent={Page404}>
          <Route path='/Home' component={HomePage} />
        </Router>
      </Suspense>
    </main>
  )
}

export default App
