/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import Events from './consts'

function navigate (href) {
  window.history.pushState({}, '', href)
  const navigationEvent = new Event(Events.PUSHSTATE)
  window.dispatchEvent(navigationEvent)
}

export default function Link ({ target, to, ...props }) {
  const handleClick = (e) => {
    const isMainEvent = e.button === 0
    const isModifiedEvent = e.metaKey || e.altKey || e.ctrlKey || e.shiftKey
    const isManageableEvent = target === undefined || target === '_self'
    if (isMainEvent && isManageableEvent && !isModifiedEvent) {
      e.preventDefault()
      navigate(to)
    }
  }
  return (
    <>
      <a href={to} target={target} onClick={handleClick} {...props} />
    </>
  )
}
