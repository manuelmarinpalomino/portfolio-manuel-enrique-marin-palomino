import React, { useEffect, useState, Children } from 'react'
import Events from './consts'
import { match } from 'path-to-regexp'
import { getCurrentPath } from './utils'

export default function Router ({ children, routes = [], DefaultComponent = () => (<div>404</div>) }) {
  const [currentPath, setCurrentPath] = useState(getCurrentPath())
  useEffect(() => {
    const navigationEvent = () => setCurrentPath(getCurrentPath())
    window.addEventListener(Events.PUSHSTATE, navigationEvent)
    window.addEventListener(Events.POPSTATE, navigationEvent)
    return () => {
      window.removeEventListener(Events.PUSHSTATE, navigationEvent)
      window.removeEventListener(Events.POPSTATE, navigationEvent)
    }
  }, [])
  const RoutesFromChildren = Children.map(children, (children) => {
    const { props, type } = children
    const { name } = type
    const isRoute = name === 'Route'
    return isRoute ? props : null
  })
  routes = routes.concat(RoutesFromChildren).filter(Boolean)
  let PathParams = {}
  const Page = routes.find(route => {
    if (route.path === currentPath) {
      return true
    }
    const matcherURL = match(route.path, { decode: decodeURIComponent })
    const matchResult = matcherURL(currentPath)
    if (!matchResult) return false
    PathParams = matchResult.params
    return true
  })?.component || DefaultComponent

  return <Page routeParams={PathParams} />
}
