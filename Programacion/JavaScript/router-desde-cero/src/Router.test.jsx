import { describe, it, expect, beforeEach, vi } from 'vitest'
import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import Router from './Router.jsx'
import { Route } from './Route.jsx'
import Link from './Link.jsx'
import { getCurrentPath } from './utils.js'
vi.mock('./utils.js', () => ({
  getCurrentPath: vi.fn()
}))
describe('Router', () => {
  beforeEach(() => {
    cleanup()
    vi.clearAllMocks()
  })
  it('should render without problems', () => {
    render(<Router routes={[]} />)
    expect(true).toBeTruthy()
  })
  it('should render 404 if no routes match', () => {
    render(<Router routes={[]} DefaultComponent={() => (<div>404</div>)} />)
    expect(screen.getByText('404')).toBeTruthy()
  })
  it('should render the component of the first route that matches', () => {
    getCurrentPath.mockReturnValue('/about')
    const routes = [
      {
        path: '/',
        component: () => (<div>Home</div>)
      },
      {
        path: '/about',
        component: () => (<div>About</div>)
      }
    ]
    render(<Router routes={routes} />)
    expect(screen.getByText('About')).toBeTruthy()
  })
  it('should navigate using Links', async () => {
    getCurrentPath.mockReturnValueOnce('/')
    render(
      <Router>
        <Route path='/' component={() => (<Link to='/about'>About</Link>)} />
        <Route path='/about' component={() => (<div>About</div>)} />
      </Router>
    )
    const button = screen.getByText(/About/)
    fireEvent.click(button)
    const aboutTitle = await screen.findByText('About')
    expect(aboutTitle).toBeTruthy()
  })
})
