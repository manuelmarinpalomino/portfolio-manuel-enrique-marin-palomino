const Events = {
  PUSHSTATE: 'pushstate',
  POPSTATE: 'popstate'
}
export default Events
