import Link from '../Link'
export default function AboutPage () {
  return (
    <>
      <h1>Sobre nosotros</h1>
      <div>
        <img src='https://picsum.photos/200/300' alt='imagen de prueba' />
      </div>
      <p>Esta es una pagina de ejemplo para crear un React Router desde cero</p>
      <Link to='/'>Ir a Home</Link>
    </>
  )
}
