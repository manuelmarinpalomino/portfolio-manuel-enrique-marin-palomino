import { useEffect } from 'react'

export default function Search ({ routeParams }) {
  useEffect(() => {
    document.title = 'search' + routeParams.query
  }, [])
  return (
    <div>
      <h1>Search {routeParams.query}</h1>
    </div>
  )
}
