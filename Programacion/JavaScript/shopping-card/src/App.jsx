import { Products } from "./components/Products"
import {products as initialProducts} from './mocks/products'
import { Header } from "./components/Header"
import { useFilters } from "./hooks/useFilters"
import Footer from "./components/Footer"
import { Cart } from "./components/Cart"
import { CartProvider } from "./context/cart"
function App() {
  const {filterProducts} = useFilters()
  const filteredProducts = filterProducts(initialProducts)
  return (
    <CartProvider>
      <Header />
      <Cart/>
      <h1>Shopping card</h1>
      <Products products={filteredProducts} />
      <Footer/>
    </CartProvider>
    
  )
}

export default App
