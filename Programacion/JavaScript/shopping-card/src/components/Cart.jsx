/* eslint-disable no-unused-vars */
import { useId } from "react";
import { CartIcon, ClearCartIcon, RemoveFromCartIcon } from "./Icons";
import '../css/Cart.css'
import { useCart } from "../hooks/useCart";
import CartItem from "./CartItem";

export function Cart(){
  const cartCheckboxId = useId()
  const { cart, addToCart, clearCart } = useCart();
  return (
    <>
      <label className="cart-button" htmlFor={cartCheckboxId}>
        <CartIcon />
      </label>
      <input id={cartCheckboxId} type="checkbox" hidden />
      <aside className="cart">
        <ul>
          {cart.map((item) => {
            return (
              <CartItem 
                key={item.id}
                addToCart={()=>{addToCart(item)}}
                {...item}/>
            );
          })}
        </ul>
        <button onClick={() => clearCart()}>
          <ClearCartIcon />
        </button>
      </aside>
    </>
  );
}