import { useReducer } from "react";
import { reducer, cartInitialState, CART_ACTION_TYPES } from "../reducers/cartReducer.js";
export function useCartReducer() {
  const [cart, dispatch] = useReducer(reducer, cartInitialState);
  const addToCart = (product) =>
    dispatch({
      type: CART_ACTION_TYPES.ADD_TO_CART,
      payload: product,
    });
  const removeFromCart = (product) =>
    dispatch({
      type: CART_ACTION_TYPES.REMOVE_FROM_CART,
      payload: product,
    });
  const clearCart = () =>
    dispatch({
      type: CART_ACTION_TYPES.CLEAR_CART
    });
    return{cart,addToCart,removeFromCart,clearCart}
}
