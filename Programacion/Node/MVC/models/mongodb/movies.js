import { randomUUID } from 'node:crypto'
import { readJSON } from '../../utils.js'
import { MongoClient } from 'mongodb'

const movies = readJSON('./movies.json')
const url = 'mongodb://localhost:27017'
const dbName = 'Movies'
const folder = 'movies'
export class MovieModel {
  static async getAll ({ genre }) {
    const client = new MongoClient(url)
    await client.connect()
    const db = client.db(dbName)
    const collection = db.collection(folder)
    if (genre) {
      //conectar a la base de datos mongodb
      

      const movies = await collection.find({ genre: genre }).toArray()
      await client.close()
      return movies.filter(
        movie => movie.genre.some(g => g.toLowerCase() === genre.toLowerCase())
      )
    }
    const movies = await collection.find({}).toArray()
    await client.close()
    return movies
  }

  static async getById ({ id }) {
    const client = new MongoClient(url)
    await client.connect()
    const db = client.db(dbName)
    const collection = db.collection(folder)
    const movie = await collection.findOne({ id })
    await client.close()
    return movie
  }

  static async create ({ input }) {
    let newMovie = {
      id: randomUUID(),
      ...input
    }
    const client = new MongoClient(url)
    await client.connect()
    const db = client.db(dbName)
    const collection = db.collection(folder)
    await collection.insertOne(newMovie)
    newMovie= await collection.findOne({ id: newMovie.id })
    await client.close()
    return newMovie
  }

  static async delete ({ id }) {
    const client = new MongoClient(url)
    await client.connect()
    const db = client.db(dbName)
    const collection = db.collection(folder)
    //Eliminar el documento con el id especificado
    const result = await collection.deleteOne({ id })
    await client.close()
    if (result.deletedCount === 0) return false
    return true
  }

  static async update ({ id, input }) {
    const client = new MongoClient(url)
    await client.connect()
    const db = client.db(dbName)
    const collection = db.collection(folder)
    //traer la pelicula con el id especificado
    const movie = await collection.findOne({ id })
    if (!movie) return null
    //actualizar la pelicula con el id especificado tomando los datos de input
    const movieActualizado = { ...movie, ...input }
    await collection.updateOne({ id }, { $set: movieActualizado })
    //traer todas las peliculas
    const movies = await collection.find({}).toArray()
    await client.close()
    return movies
  }
}
