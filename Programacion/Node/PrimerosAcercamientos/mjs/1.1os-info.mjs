import { type, release, platform, arch, totalmem, freemem, uptime } from 'node:os'
console.log('Hola,Mundo!')

console.log('Informacion del sistema operativo:')
console.log('-----------------------------------')
console.log('nombre del sistema operativo:', type())
console.log('version del sistema operativo:', release())
console.log('plataforma:', platform())
console.log('arquitectura del sistema:', arch())
console.log('memoria total:', totalmem() / 1024 / 1024)
console.log('memoria libre:', freemem() / 1024 / 1024)
console.log('tiempo activo:', uptime() / 60 / 60 / 24)
