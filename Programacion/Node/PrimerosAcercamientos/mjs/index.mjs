import { sum, substract, multiply, divide } from './sum.mjs'

console.log('Hola,Mundo!')
console.log(sum(1, 2)) // 3
console.log(substract(1, 2)) // -1
console.log(multiply(1, 2)) // 2
console.log(divide(1, 2)) // 0.5
