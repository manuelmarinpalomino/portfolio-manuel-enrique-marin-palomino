
from distutils.log import debug, error
from msilib.schema import Error
from urllib import response
from flask import Flask, jsonify, request
from flaskext.mysql import MySQL
from pymysql import OperationalError
from pymysql.cursors import DictCursor
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

mysql = MySQL(app)
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'Av.demayo1377'
app.config['MYSQL_DATABASE_DB'] = 'granja2'

try:
    cursor = mysql.connect().cursor(cursor=DictCursor)
except OperationalError as e:
    code, msg = e.args
    print('_'*100)
    print(msg)
    print('verificar si la DB está ejecutandose')
    print('_'*100)

def instertIntoDB(sql):
    conn= mysql.connect()
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()
    cursor.close()
    conn.close()
    return cursor.lastrowid

@app.route('/')
def index():
    return '<h2>Esto es una API.<h2>\
        <p><a href="/products">/products</a></p>\
        <p><a href="/orders">/orders</a></p>'


@app.route('/products')
def products():
    sql = 'select * from products where is_deleted=0'
    response ={"error":False,"data":None}
    try:
        cursor.execute(sql)
        response['data']=cursor.fetchall()
    except:
        response['error']=True
    return jsonify(response)

@app.route('/products/<string:id>')
def getProductById(id):
    sql = f'select * from products where id={id} and is_deleted=0;'
    response ={"error": False,"data":None}
    try:
        cursor.execute(sql)
        response['data']=cursor.fetchall()
    except:
        response['error'] = True
    return jsonify(response)

@app.route('/orders', methods = ['GET'])
def orders():
    sql='select * from orders'
    sql2='select * from order_details'
    response={'error': False, 'data':[]}
    try:
        cursor.execute(sql)
        response['data'].append(cursor.fetchall())
        cursor.execute(sql2)
        response['data'].append(cursor.fetchall())
    except BaseException as e:
        response['data']=None
        response['error'] =True
    return jsonify(response)

@app.route('/orders', methods = ['POST'])
def createOrders():
    res = request.json
    response = {'error':False, 'data':res }
    try:
        sql = f'insert into orders(total,customer_id) values ({res["total"]},1);'
        orderId=instertIntoDB(sql)
        for prod in res["products"]:
            sql2 = f'insert into order_details(order_id,product_id,quantity)values({orderId},{int(prod["id"])},{int(prod["quantity"])});'
            instertIntoDB(sql2)
    except BaseException as e:
        print(e)
        response['error'] = True
        response['data'] = None
    return response

if __name__ == '__main__':
    app.run(debug=True)
