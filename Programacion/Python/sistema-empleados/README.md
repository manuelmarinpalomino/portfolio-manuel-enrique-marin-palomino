# Sistema Empleados
## Requerimientos:

    autopep8==1.6.0
    cffi==1.15.0
    click==8.0.3
    colorama==0.4.4
    cryptography==36.0.1
    Flask==2.0.3
    Flask-MySQL==1.5.2
    itsdangerous==2.0.1
    Jinja2==3.0.3
    MarkupSafe==2.0.1
    pip==21.2.4
    pycodestyle==2.8.0
    pycparser==2.21
    PyMySQL==1.0.2
    setuptools==58.1.0
    toml==0.10.2
    Werkzeug==2.0.3
## Instalación:
Crear Database empleados en MySQL con el siguiente codigo:
create database if not exists empleados;
use empleados;

    create table empleados (
	    id int not null auto_increment,
        nombre varchar(255),
        correo varchar(255),
        foto varchar(5000),
        primary key(id)
    );
