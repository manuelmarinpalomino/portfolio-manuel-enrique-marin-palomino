from tkinter import PROJECTING
from flask import Flask,render_template,redirect, request, url_for, send_from_directory,flash
from flaskext.mysql import MySQL
from datetime import datetime
from pymysql import cursors
import os
app = Flask(__name__)
mysql = MySQL()
app.config['MSQL-DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'Av.demayo1377'
app.config['MYSQL_DATABASE_DB'] = 'empleados'
app.config['SECRET_KEY']='codoacodo'
UPLOADS = os.path.join('uploads')
app.config['UPLOADS'] = UPLOADS
mysql.init_app(app)


def queryMySql(query,datos = ()):
    conn = mysql.connect()
    cursor = conn.cursor(cursor=cursors.DictCursor)
    cursor.execute(query,datos)
    queryResult = cursor.fetchall()
    conn.commit()
    cursor.close()
    conn.close()
    return queryResult

@app.route('/')
def index():
    sql = "select * FROM empleados"
    empleados = queryMySql(sql)

    return render_template('empleados/index.html.j2',empleados = empleados)

@app.route('/create')
def create():
    return render_template('empleados/create.html.j2')

@app.route('/store' , methods = ["POST"])
def store():
    nombre = request.form['txtNombre']
    correo = request.form['txtCorreo']
    image = request.files['txtFoto']
    now = datetime.now()
    tiempo = now.strftime("%Y%H%M%S")
    nuevoNombreFoto = ''
    if nombre=='' or correo == '':
        flash('el nombre y el correo son obligatorios.')
        return redirect(url_for('create'))
    if image.filename != '':
        nuevoNombreFoto = f'{tiempo}-{image.filename}'
        image.save(".\\uploads\\"+ nuevoNombreFoto)
    sql = "insert into empleados (nombre,correo, foto) values (%s,%s,%s);"
    datos = (nombre, correo, nuevoNombreFoto)
    queryMySql(sql,datos)
    return redirect('/')

@app.route('/delete/<int:id>')
def delete(id):
    sql='select foto from empleados where id=%s'
    nombreFoto = queryMySql(sql,id)[0]['foto']
    try:
        os.remove(os.path.join(app.config['UPLOADS'], nombreFoto))
    except:
        pass
    sql = 'delete from empleados where id= %s'
    queryMySql(sql,id)
    return redirect('/')

@app.route('/modify/<int:id>')
def modify(id):
    sql = "select * from empleados where id=%s"
    empleado=queryMySql(sql,id)[0]
    return render_template('empleados/edit.html.j2', empleado =empleado)

@app.route('/update', methods=['POST'])
def update():
    nombre = request.form['txtNombre']
    correo = request.form['txtCorreo']
    image = request.files['txtFoto']
    id = request.form['txtId']
    now = datetime.now()
    tiempo = now.strftime("%Y%H%M%S")
    nuevoNombreFoto = ''
    sql=''
    datos=()
    if image.filename != '':
        nuevoNombreFoto = f'{tiempo}-{image.filename}'
        image.save(".\\uploads\\" + nuevoNombreFoto)
        sql = "select foto from empleados where id=%s"
        datos = (id)
        nombreFoto=queryMySql(sql,datos)[0]['foto']
        try:
            os.remove(os.path.join(app.config['UPLOADS'],nombreFoto))
        except:
            pass
        sql = "update empleados set nombre=%s, correo=%s, foto=%s where id=%s"
        datos = (nombre, correo, nuevoNombreFoto, id)
    else:
        sql = "update empleados set nombre=%s, correo=%s where id=%s"
        datos = (nombre, correo,id)
    queryMySql(sql,datos)
    return redirect('/')

@app.route('/userpic/<path:userpic>')
def uploads(userpic):
    return send_from_directory(os.path.join('uploads'), userpic)

if __name__ == '__main__':
    app.run(debug=True)
