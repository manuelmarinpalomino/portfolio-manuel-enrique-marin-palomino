import { Toaster } from "sonner";
import "./App.css";
import { CreateNewUser } from "./components/CreateNewUser";
import ListOfUsers from "./components/ListOfUsers";

function App() {
	return (
		<div className="tremor-theme w-full h-full">
			<ListOfUsers />
			<CreateNewUser />
			<Toaster richColors />
		</div>
	);
}

export default App;
