import { Badge, Button, Card, TextInput, Title } from "@tremor/react";
import { useState } from "react";
import { useUserActions } from "../hooks/useUserActions";
export function CreateNewUser() {
	const { addNewUser } = useUserActions();
	const [result, setResult] = useState<"ok" | "ko" | null>(null);
	return (
		<Card>
			<Title>Create New User</Title>
			<form
				className=""
				onSubmit={(e: React.FormEvent<HTMLFormElement>) => {
					e.preventDefault();
					setResult(null);
					const form = e.target as HTMLFormElement;
					const formData = new FormData(form);
					const name = formData.get("Name") as string;
					const email = formData.get("Email") as string;
					const github = formData.get("Github") as string;
					if (!name || !email || !github) return setResult("ko");
					addNewUser({ name, email, github });
					setResult("ok");
					form.reset();
				}}
			>
				<TextInput name="Name" placeholder="Name" className="mb-2" />
				<TextInput name="Email" placeholder="Email" className="mb-2" />
				<TextInput name="Github" placeholder="Github" className="mb-2" />
				<div className="flex flex-col	 justify-center">
					<span className="w-full flex justify-center items-center mb-2">
						{result === "ok" && (
							<Badge className="w-full p-5" color="green">
								Success
							</Badge>
						)}
						{result === "ko" && (
							<Badge color="red" className="w-full h-1/2">
								Error
							</Badge>
						)}
					</span>
					<Button className="w-full mb-2" type="submit">
						Create New User
					</Button>
				</div>
			</form>
		</Card>
	);
}
