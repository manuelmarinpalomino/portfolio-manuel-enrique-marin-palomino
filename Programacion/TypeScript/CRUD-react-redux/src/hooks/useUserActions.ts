import { useAppDispatch, useAppSelector } from "../hooks/store";
import { User, addUser, deleteUserById } from "../store/users/slice";

export const useUserActions = () => {
	const users = useAppSelector((state) => state.users);
	const dispatch = useAppDispatch();
	const deleteUser = (id: number) => {
		dispatch(deleteUserById(id));
	};
	const addNewUser = (user: User) => {
		dispatch(addUser(user));
	};
	return { users, deleteUser, addNewUser };
};
