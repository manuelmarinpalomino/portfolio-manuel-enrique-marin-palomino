import { configureStore, type Middleware } from "@reduxjs/toolkit";
import { toast } from "sonner";
import usersReducer, { UsersState, rollbackUser } from "./users/slice";

const PersistanceLocalStorageMiddleware: Middleware =
	(store) => (next) => (action) => {
		next(action);
		localStorage.setItem("reduxState", JSON.stringify(store.getState()));
	};
const syncWithDataBaseMiddleware: Middleware =
	(store) => (next) => (action) => {
		const { type, payload } = action;
		const previosState = store.getState();

		console.log({ action, state: store.getState() });
		next(action);
		if (type === "users/deleteUserById") {
			const userToDelete: UsersState = previosState.users.find(
				(user: UsersState) => user.id === payload,
			);
			fetch(`https://jsonplaceholder.typicode.com/users/${payload}`, {
				method: "DELETE",
			})
				.then((response) => {
					if (response.ok) return toast.success("User deleted successfully");
					throw new Error("Error deleting user");
				})
				.catch(() => {
					if (userToDelete) store.dispatch(rollbackUser(userToDelete));
					toast.error("Error deleting user");
				});
		}
	};
export const Store = configureStore({
	reducer: {
		// Add reducer here
		users: usersReducer,
	},
	middleware: [PersistanceLocalStorageMiddleware, syncWithDataBaseMiddleware],
});

export type RootState = ReturnType<typeof Store.getState>;
export type AppDispatch = typeof Store.dispatch;
