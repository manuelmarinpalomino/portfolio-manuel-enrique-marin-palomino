import { PayloadAction, createSlice } from "@reduxjs/toolkit";

type userID = number;
const defaultState: UsersState[] = [
	{
		id: 1,
		name: "John Doe",
		email: "John_Doe@gmail.com",
		github: "John_Doe",
	},
	{
		id: 2,
		name: "Jane Doe",
		email: "Jane_Doe@gmail.com",
		github: "Jane_Doe",
	},
	{
		id: 3,
		name: "John Smith",
		email: "John_Smith@gmail.com",
		github: "John_Smith",
	},
	{
		id: 4,
		name: "Jane Doe",
		email: "Jane_Doe@gmail.com",
		github: "Jane_Doe",
	},
	{
		id: 5,
		name: "Robert Johnson",
		email: "Robert_Johnson@gmail.com",
		github: "Robert_Johnson",
	},
	{
		id: 6,
		name: "Emily Davis",
		email: "Emily_Davis@gmail.com",
		github: "Emily_Davis",
	},
	{
		id: 7,
		name: "Chris Brown",
		email: "Chris_Brown@gmail.com",
		github: "Chris_Brown",
	},
	{
		id: 8,
		name: "Alexandra White",
		email: "Alexandra_White@gmail.com",
		github: "Alexandra_White",
	},
	{
		id: 9,
		name: "Michael Green",
		email: "Michael_Green@gmail.com",
		github: "Michael_Green",
	},
	{
		id: 10,
		name: "Sophia Martinez",
		email: "Sophia_Martinez@gmail.com",
		github: "Sophia_Martinez",
	},
];
export interface User {
	name: string;
	email: string;
	github: string;
}
export interface UsersState extends User {
	id: userID;
}
const initialState: UsersState[] = (() => {
	const reduxState = localStorage.getItem("reduxState");
	if (reduxState) return JSON.parse(reduxState).users;
	return defaultState;
})();

export const usersSlice = createSlice({
	name: "users",
	initialState: initialState,
	reducers: {
		deleteUserById: (state, action: PayloadAction<userID>) => {
			const id = action.payload;
			return state.filter((user) => user.id !== id);
		},
		addUser: (state, action: PayloadAction<User>) => {
			const { name, email, github } = action.payload;
			const id = state[state.length - 1].id + 1;
			return [...state, { id, name, email, github }];
		},
		editUser: (state, action: PayloadAction<UsersState>) => {
			const { id, name, email, github } = action.payload;
			const index = state.findIndex((user) => user.id === id);
			state[index] = { id, name, email, github };
		},
		rollbackUser: (state, action: PayloadAction<UsersState>) => {
			const isUserAlreadyExist = state.some(
				(user) => user.id === action.payload.id,
			);
			if (isUserAlreadyExist) return state;
			return [...state, action.payload];
		},
	},
});

export default usersSlice.reducer;
export const { deleteUserById, addUser, editUser, rollbackUser } =
	usersSlice.actions;
