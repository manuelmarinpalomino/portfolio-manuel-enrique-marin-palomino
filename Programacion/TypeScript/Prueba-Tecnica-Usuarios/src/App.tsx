import './App.css'
import { useState, useMemo } from 'react'
import { SortBy, type User } from './types.d'
import { UsersLists } from './components/UserList'
import { useQueryClient } from '@tanstack/react-query'
import { useUsers } from './hooks/useUsers'
import { Results } from './components/Results'

function App () {
  const [showColors, setShowColors] = useState(false)
  const [sorting, setSorting] = useState<SortBy>(SortBy.None)
  const [filterCountry, setFilterCountry] = useState('')
  const queryClient = useQueryClient()
  const { isLoading, isError, data, refetch, fetchNextPage, hasNextPage } = useUsers()
  const users = data?.pages?.flatMap(page => page.users) ?? []
  const usersFiltered = useMemo(() => { return filterCountry !== '' ? users.filter(user => user.location.country.toLowerCase().includes(filterCountry.toLowerCase())) : users }, [users, filterCountry])
  const sortedUsers = useMemo(() => {
    console.log(sorting)
    if (sorting === SortBy.None) return usersFiltered
    const comparePropeties: Record<string, (a: User, b: User) => number> = {
      [SortBy.FirstName]: (a, b) => a.name.first.localeCompare(b.name.first),
      [SortBy.LastName]: (a, b) => a.name.last.localeCompare(b.name.last),
      [SortBy.location]: (a, b) => a.location.country.localeCompare(b.location.country)
    }
    return usersFiltered.toSorted(comparePropeties[sorting])
  }, [usersFiltered, sorting])
  const handleDeleteUser = (index: string, userIndex: number) => {
    const current = queryClient.getQueryData<{ pageParams: any[], pages: Array<{ nextCursor: number, users: User[] }> }>(['users'])
    const currentpages = current?.pages
    const calculatePage = Math.floor(userIndex / 10)
    const currentPage = currentpages?.[calculatePage]
    const newPage = currentPage?.users.filter(user => user.email !== index)
    const newPages = currentpages?.map((page, index) => {
      if (index === calculatePage) {
        return {
          ...page,
          users: newPage ?? []
        }
      }
      return page
    })

    queryClient.setQueryData<{ pageParams: any[], pages: Array<{ nextCursor: number, users: User[] }> }>(['users'], {
      pageParams: current?.pageParams ?? [],
      pages: newPages ?? []
    })
  }
  const handleRecoversUsers = () => {
    void refetch()
  }
  const handleChangeSort = (sort: SortBy) => {
    sorting !== sort ? setSorting(sort) : setSorting(SortBy.None)
  }
  return (
    <>
    <header style={{ height: '100px' }}>
      <button onClick={() => { setShowColors(!showColors) }}>Cambiar fila</button>
      <button onClick={() => { sorting !== SortBy.location ? setSorting(SortBy.location) : setSorting(SortBy.None) }}>Ordenar por pais</button>
      <button onClick={() => { handleRecoversUsers() }}>Recobrar Usuarios</button>
      <input type="text" style={{ height: '36%', border: '1px solid #ccc', borderRadius: '5px', boxShadow: '0 0 5px rgba(0, 0, 0, 0.1)' }} placeholder='Filtra Por Pais' onChange={(e) => { setFilterCountry(e.target.value) }}/>
    </header>
    <main>
      <Results></Results>
      {users.length > 0 && <UsersLists users={sortedUsers} handleChangeSort ={handleChangeSort} handleDeleteUser={handleDeleteUser} showColors={showColors}/>}
      {isLoading && <p>Cargando...</p>}
      {!isLoading && isError && <p>Hubo un error</p>}
      {!isLoading && !isError && users.length === 0 && <p>No hay usuarios</p>}
    </main>
    <footer>
      {!isLoading && !isError && hasNextPage === true && <button onClick={() => { void fetchNextPage() }}>cargar mas resultados</button>}
      {!isLoading && !isError && hasNextPage === false && <p>No hay mas resultados</p>}
    </footer>
    </>
  )
}

export default App
