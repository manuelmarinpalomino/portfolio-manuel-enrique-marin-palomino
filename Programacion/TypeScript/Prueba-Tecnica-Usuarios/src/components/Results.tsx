import { useUsers } from '../hooks/useUsers'
export const Results = () => {
  const { data } = useUsers()
  return (
    <h3>Results: {data?.pages.flatMap(page => page.users).length}</h3>
  )
}
