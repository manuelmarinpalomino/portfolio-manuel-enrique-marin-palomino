import { type User, SortBy } from '../types.d'

export function UsersLists ({ users, showColors, handleDeleteUser, handleChangeSort }: { users: User[], showColors: boolean, handleDeleteUser: (index: string, userIndex: number) => void, handleChangeSort: (sort: SortBy) => void }) {
  return (
    <table>
      <thead>
        <tr>
          <th>Foto</th>
          <th onClick={() => { handleChangeSort(SortBy.FirstName) }}>Nombre</th>
          <th onClick={() => { handleChangeSort(SortBy.LastName) }}>Apellido</th>
          <th onClick={() => { handleChangeSort(SortBy.location) }}>Pais</th>
          <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
          {
            users.map((user, index) => {
              const backgroundColor = showColors ? index % 2 === 0 ? '#333' : '#555' : 'transparent'
              return (
              <tr style={{ backgroundColor }} key={user.email}>
                <td><img src={user.picture.thumbnail} alt={`${user.name.first} ${user.name.last}`}/></td>
                <td>{user.name.first}</td>
                <td>{user.name.last}</td>
                <td>{user.location.country}</td>
                <td>
                  <button onClick={() => { handleDeleteUser(user.email, index) }}>Eliminar</button>
                </td>
              </tr>
              )
            }
            )
          }
        </tbody>
            </table>
  )
}
