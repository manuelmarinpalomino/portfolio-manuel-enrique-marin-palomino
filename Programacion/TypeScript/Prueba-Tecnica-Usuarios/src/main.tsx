import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import { QueryClientProvider, QueryClient } from '@tanstack/react-query'
const root = document.getElementById('root')
const queryClient = new QueryClient()
if (root !== null) {
  ReactDOM.createRoot(root).render(

    <QueryClientProvider client={queryClient}>
      <App />
    </QueryClientProvider>
  )
}
