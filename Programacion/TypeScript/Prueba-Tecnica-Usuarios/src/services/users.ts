export const fetchUsers = async ({ pageParam = 1 }: { pageParam?: number }) => {
  return await fetch(`https://randomuser.me/api?results=10&seed=manuel&page=${pageParam}`).then(async res => {
    if (!res.ok) throw new Error('Error al obtener los datos')
    return await res.json()
  }).then(res => {
    return {
      users: res.results,
      nextCursor: res.info.page > 10 ? undefined : res.info.page + 1
    }
  })
}
