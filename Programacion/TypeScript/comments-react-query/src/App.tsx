import './App.css'
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query'
import { getComments, type CommentWithId, postComment, Comment } from './service/comments'
import { FormInput, FormTextArea } from './components/Form'
import { Results } from './components/Results'

function App () {
  const { data, isLoading, error } = useQuery<CommentWithId[]>(
    ['comments'],
    getComments
  )
  const queryClient = useQueryClient()
  const { mutate, isLoading: isLoadingMutation } = useMutation({
    mutationFn: postComment,
    onMutate: async (newComment) => {
      await queryClient.cancelQueries(['comments'])
      const previousData = queryClient.getQueryData(['comments'])
      await queryClient.setQueryData(['comments'], (oldData?: Comment[]) => {
        if (oldData == null) return [newComment]
        return [...oldData, newComment]
      })
      return { previousData }
    },
    onError: (_error, _newComment, context) => {
      if (context?.previousData != null) {
        queryClient.setQueryData(['comments'], context?.previousData)
      } else {
        queryClient.setQueryData(['comments'], [])
      }
    },
    onSuccess: async (newComment) => {
      await queryClient.invalidateQueries(['comments'])
    }
  })

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    if (isLoadingMutation) return
    const data = new FormData(event.currentTarget)
    const message = data.get('message')?.toString() ?? ''
    const title = data.get('title')?.toString() ?? ''
    if (title !== '' && message !== '') {
      mutate({ title, message })
    }
  }

  return (
    <main className='grid grid-cols-2 h-screen'>
      <div className='col-span-1 bg-white p-8'>

        {isLoading && <strong>Cargando...</strong>}
        {error != null && <strong>Algo ha ido mal</strong>}
        <Results data={data} />

      </div>
      <div className='col-span-1 bg-black p-8'>
        <form className={`${isLoadingMutation ? 'opacity-40' : ''} max-w-xl m-auto block px-4`} onSubmit={handleSubmit}>

          <FormInput />
          <FormTextArea />

          <button
            disabled={isLoadingMutation}
            type='submit' className='mt-4 px-12 text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm py-2.5 text-center mr-2 mb-2'
          >
            {isLoadingMutation ? 'Enviando Comentario...' : 'Enviar comentario'}
          </button>
        </form>
      </div>
    </main>
  )
}

export default App
