export interface Comment {
  title: string
  message: string
}

export interface CommentWithId extends Comment {
  id: string
}

export const getComments = async () => {
  const response = await fetch('https://api.jsonbin.io/v3/b/659c8ae3dc746540188efe44', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-Access-Key': '$2a$10$IggF71XvPLQviaW/8R7.0e8Zuhx.8gINY1cJaQnrZNT5kT1RNXBbi'
    }
  })

  if (!response.ok) {
    throw new Error('Failed to fetch comments.')
  }

  const json = await response.json()

  return json?.record
}

export const postComment = async (comment: Comment) => {
  const comments = await getComments()

  const id = crypto.randomUUID()
  const newComment = { ...comment, id }
  const commentsToSave = [...comments, newComment]

  const response = await fetch('https://api.jsonbin.io/v3/b/659c8ae3dc746540188efe44', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'X-Access-Key': '$2a$10$IggF71XvPLQviaW/8R7.0e8Zuhx.8gINY1cJaQnrZNT5kT1RNXBbi'
    },
    body: JSON.stringify(commentsToSave)
  })

  if (!response.ok) {
    throw new Error('Failed to post comment.')
  }

  return newComment
}
