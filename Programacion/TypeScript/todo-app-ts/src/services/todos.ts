import { type TodoList } from '../types'

const API_URL = 'https://api.jsonbin.io/v3/b/64d7ec92b89b1e2299cfbc7d'

interface Todo {
  id: string
  title: string
  completed: boolean
  order: number
}

export const fetchTodos = async (): Promise<Todo[]> => {
  const res = await fetch(API_URL, { headers: { 'X-Master-Key': '$2b$10$8thaUXKVRMQvFgJbWlh8M.Ij3YgMSCIIvtbnxc/S7uKk0icbuVrgy' } })
  console.log(res)
  if (!res.ok) {
    console.error('Error fetching todos')
    return []
  }

  const { record: todos } = await res.json() as { record: Todo[] }
  return todos
}

export const updateTodos = async ({ todos }: { todos: TodoList }): Promise<boolean> => {
  const res = await fetch(API_URL, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'X-Master-Key': '$2b$10$8thaUXKVRMQvFgJbWlh8M.Ij3YgMSCIIvtbnxc/S7uKk0icbuVrgy'
    },
    body: JSON.stringify(todos)
  })

  return res.ok
}
