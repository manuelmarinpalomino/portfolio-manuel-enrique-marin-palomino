module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "standard-with-typescript",
        "plugin:react/recommended"
    ],
    "parserOptions": {
        "project": 'c:\\users\\marin\\OneDrive\\Documentos\\Porfolio manu\\portfolio-manuel-enrique-marin-palomino\\Programacion\\TypeScript\\traductor-with-gpt\\tsconfig.json',
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
      "react/react-in-jsx-scope": "off",
      "react/prop-types": "off",
      "@typescript-eslint/explicit-function-return-type": "off"
    }
}
