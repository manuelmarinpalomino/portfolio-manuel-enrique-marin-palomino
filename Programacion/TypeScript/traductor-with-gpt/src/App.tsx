import 'bootstrap/dist/css/bootstrap.min.css'
import { Container, Row, Col, Button, Stack } from 'react-bootstrap'
import useStore from './hooks/useStore'
import { AUTO_LANGUAGE } from './constants'
import { ArrowsIcon } from './components/Icons'
import { LenguageSelector } from './components/LanguageSelector'
import { type FromLenguage, type Language, SectionType } from './types.d'
import { TextArea } from './components/TextArea'
import { translate } from './services/translate'
import { useEffect } from 'react'
const App = () => {
  const { state, InterchangeLanguages, SetFromLanguage, SetToLanguage, SetFromText, SetResult } = useStore()
  useEffect(() => {
    if (state.text === '') return
    const fromLanguage: FromLenguage = state.fromLenguage
    const toLanguage: Language = state.toLenguage
    const text: string = state.text
    translate({ fromLanguage, toLanguage, text }).then(result => {
      if (result == null) return
      SetResult(result as string)
    }).catch(error => {
      console.error(error)
    })
  }, [state.text])
  return (
    <div className='app'>
      <h1>Google Translate</h1>
      <Container fluid>
        <Row>
          <Col>
            <Stack gap={2}>
              <LenguageSelector type={SectionType.From} value={state.fromLenguage} onChange={SetFromLanguage}/>
              <TextArea type={SectionType.From} value={state.text} onChange={SetFromText}/>
              {state.text}
            </Stack>
          </Col>
          <Col xs='auto'>
            <Button variant="link" disabled={state.fromLenguage === AUTO_LANGUAGE} onClick={() => { InterchangeLanguages() }}><ArrowsIcon/></Button>
          </Col>
          <Col>
            <Stack gap={2}>
              <LenguageSelector type={SectionType.To} value={state.toLenguage} onChange={SetToLanguage}/>
              <TextArea loading={state.loading} type={SectionType.To} value={state.result} onChange={SetResult} />
            </Stack>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default App
