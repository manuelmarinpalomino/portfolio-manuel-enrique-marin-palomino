import { Form } from 'react-bootstrap'
import { SUPPORTED_LANGUAGES, AUTO_LANGUAGE } from '../constants'
import { SectionType, type FromLenguage, type Language } from '../types.d'

type Props = { type: SectionType.From, value: FromLenguage, onChange: (language: FromLenguage) => void }
| { type: SectionType.To, value: Language, onChange: (language: Language) => void }
export const LenguageSelector: React.FC<Props> = ({ onChange, type, value }) => {
  return (
    <Form.Select aria-label="selecciona el idioma" value={value} onChange={(e: React.ChangeEvent<HTMLSelectElement>) => { onChange(e.target.value as Language) }}>
      {type === SectionType.From && <option value={AUTO_LANGUAGE}>Detectar idioma</option>}
      {Object.entries(SUPPORTED_LANGUAGES).map(([key, value]) => (
        <option key={key} value={key}>{value}</option>
      ))}
    </Form.Select>
  )
}
