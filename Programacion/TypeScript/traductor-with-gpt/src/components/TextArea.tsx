import { Form } from 'react-bootstrap'
import { SectionType } from '../types.d'
interface Props { type: SectionType, loading?: boolean, value: string, onChange: (value: string) => void }
const commonStyles: React.CSSProperties = { border: 0, height: '200px', resize: 'none' }
const getPlaceholder = ({ type, loading }: { type: SectionType, loading?: boolean }) => {
  if (type === SectionType.From) return 'Introducir texto'
  if (loading === true) return 'Cargando...'
  return 'Traducción'
}
export const TextArea: React.FC<Props> = ({ type, loading, value, onChange }) => {
  const styles = type === SectionType.From ? commonStyles : { ...commonStyles, backgroundColor: '#f5f5f5' }
  return (
    <Form.Control disabled={type === SectionType.To} as="textarea" style={styles} autoFocus={type === SectionType.From} placeholder={getPlaceholder({ type, loading })}rows={3} value={value} onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => { onChange(e.target.value) }} />
  )
}
