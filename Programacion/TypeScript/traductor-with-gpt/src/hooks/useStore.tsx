import { useReducer } from 'react'
import { type State, type Action, type Language, type FromLenguage } from '../types'
import { AUTO_LANGUAGE } from '../constants'
const initialState: State = {
  fromLenguage: 'auto',
  toLenguage: 'en',
  text: '',
  result: '',
  loading: false
}
const reducer = (state: State, action: Action) => {
  const { type } = action
  if (type === 'INTERCHANGE_LENGUAGES') {
    if (state.fromLenguage === AUTO_LANGUAGE) return state
    return {
      ...state,
      fromLenguage: state.toLenguage,
      toLenguage: state.fromLenguage
    }
  }

  if (type === 'SET_FROM_LANGUAGE') {
    return {
      ...state,
      fromLenguage: action.payload
    }
  }

  if (type === 'SET_TO_LANGUAGE') {
    return {
      ...state,
      toLenguage: action.payload
    }
  }
  if (type === 'SET_FROM_TEXT') {
    return {
      ...state,
      loading: true,
      text: action.payload,
      result: ''
    }
  }
  if (type === 'SET_RESULT') {
    return {
      ...state,
      result: action.payload
    }
  }
  return state
}

export default function useStore () {
  const [state, dispatch] = useReducer(reducer, initialState)
  console.log(state)
  console.log(dispatch)
  const InterchangeLanguages = () => { dispatch({ type: 'INTERCHANGE_LENGUAGES' }) }
  const SetFromLanguage = (fromLenguage: FromLenguage) => { dispatch({ type: 'SET_FROM_LANGUAGE', payload: fromLenguage }) }
  const SetToLanguage = (toLenguage: Language) => { dispatch({ type: 'SET_TO_LANGUAGE', payload: toLenguage }) }
  const SetFromText = (text: string) => { dispatch({ type: 'SET_FROM_TEXT', payload: text }) }
  const SetResult = (result: string) => { dispatch({ type: 'SET_RESULT', payload: result }) }
  return { state, SetFromLanguage, SetToLanguage, SetFromText, SetResult, InterchangeLanguages }
}
