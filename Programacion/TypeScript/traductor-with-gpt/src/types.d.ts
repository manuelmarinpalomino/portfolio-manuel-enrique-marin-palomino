import { type SUPPORTED_LANGUAGES, type AUTO_LANGUAGE } from './constants'

export interface State {
  fromLenguage: FromLenguage
  toLenguage: Language
  text: string
  result: string
  loading: boolean
}
export type Action = { type: 'SET_FROM_LANGUAGE', payload: FromLenguage } | { type: 'INTERCHANGE_LENGUAGES' } | { type: 'SET_TO_LANGUAGE', payload: Language } | { type: 'SET_FROM_TEXT', payload: string } | { type: 'SET_RESULT', payload: string }

export type Language = keyof typeof SUPPORTED_LANGUAGES
export type AutoLanguage = typeof AUTO_LANGUAGE
export type FromLenguage = Language | AutoLanguage
export enum SectionType {
  From = 'from',
  To = 'to'
}
