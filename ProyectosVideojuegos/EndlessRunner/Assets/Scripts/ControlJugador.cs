﻿
using UnityEngine;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{
    private Animator animator;
    private bool puedeSaltar=true;
    public Text text;
    
    void Start()
    {
        puedeSaltar=true;
        animator = GetComponent<Animator>();
        text.text = " ";
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && puedeSaltar ==true)
        {
            animator.SetBool("enPiso", true);
        }
       
       if(transform.position.x == 10 && Input.GetKey(KeyCode.LeftArrow) && puedeSaltar == true)
        {
            animator.SetInteger("posicionJugador", 2);
            animator.SetBool("vienePorIzquierda", true);
        }
       else if (transform.position.x == 0 && Input.GetKey(KeyCode.LeftArrow) && puedeSaltar == true)
        {
            animator.SetInteger("posicionJugador", 1);
        }
        else if (transform.position.x == -10 && Input.GetKey(KeyCode.RightArrow) && puedeSaltar == true)
        {
            animator.SetInteger("posicionJugador", 2);  
        }
        else if (transform.position.x == 0 && Input.GetKey(KeyCode.RightArrow) && puedeSaltar == true)
        {
            animator.SetInteger("posicionJugador", 3);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            puedeSaltar = true;
            animator.SetBool("enPiso", false);   
        }
        if (collision.gameObject.CompareTag("Pared"))
        {
            Time.timeScale = 0f;
            text.text = "Perdiste";
        }
    }
   

}
