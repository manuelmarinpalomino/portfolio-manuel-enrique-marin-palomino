﻿
using System.Collections;
using UnityEngine;

public class GeneradorParedes : MonoBehaviour
{
    public GameObject Pared1;
    public GameObject Pared2;
    public Transform transform1;
    public Transform transform2;
    public Transform transform3;
    

    private float tiempoRestante;
    void Start()
    {
        InstanciarPared();
    }

    // Update is called once per frame
    void Update()
    {
        if(tiempoRestante == 0)
        {
            InstanciarPared();
        }
        
    }

    private void InstanciarPared()
    {
        int numeroPrefabs = Random.Range(0, 2);
        int posicion = Random.Range(0, 3);
        if (numeroPrefabs == 0 && posicion == 0)
        {
            Instantiate(Pared1, transform1.position, Quaternion.identity);

        }
         else if (numeroPrefabs == 0 && posicion == 1)
        {

            Instantiate(Pared1, transform2.position, Quaternion.identity);

        }
        else if (numeroPrefabs == 0 && posicion == 2)
        {

            Instantiate(Pared1, transform3.position, Quaternion.identity);

        }
        else if (numeroPrefabs == 1 && posicion == 0)
        {

            Instantiate(Pared2, transform1.position, Quaternion.identity);

        }
        else if (numeroPrefabs == 1 && posicion == 1)
        {

            Instantiate(Pared2, transform2.position, Quaternion.identity);

        }
        else if (numeroPrefabs == 1 && posicion == 2)
        {

            Instantiate(Pared2, transform3.position, Quaternion.identity);

        }
        StartCoroutine(ComenzarCronometro(5));


    }
    public IEnumerator ComenzarCronometro(float valorCronometro = 5)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }

}
