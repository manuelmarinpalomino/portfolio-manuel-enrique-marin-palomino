﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{
    public GameObject objeto1;
    public GameObject objeto2;
    public GameObject objeto3;
    public GameObject objeto4;
    public GameObject objeto5;
    public GameObject objeto6;
    public GameObject objeto7;


    public Camera camaraPrimeraPersona;
    private Rigidbody rb;
    private int contador, contador2, contador3;
    private int saltoActual = 0;
    public float rapidezDesplazamiento = 10.0f;
    public float salto;
    private bool enElSuelo = true;
    private float x, y, z;
    bool sacarPrefabs = false;

    public Animator anim;
    public string animacionAbrir;

    public Text textoCantidadRecolectados;
    public Text textoRecolectarCuboAzul;
    public GameObject imagenes;
    public Text timerText;
    private float startTime;
    float minutes = 0;
    float seconds = 0;
    public Text tiempoUltimaPartida ;
    public float minUltimaPartida;
    public float segundosUltimaPartida;
    public GestorPersistencia gestorPersistencia;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        startTime = Time.time;
        rb = GetComponent<Rigidbody>();
        x = transform.position.x;
        y = transform.position.y;
        z = transform.position.z;
        textoRecolectarCuboAzul.text = "";
        setearTextos();
        objeto3.GetComponent<MeshRenderer>().enabled = true;
        objeto4.GetComponent<MeshRenderer>().enabled = true;
        objeto5.GetComponent<MeshRenderer>().enabled = true;
        objeto6.GetComponent<MeshRenderer>().enabled = true;
        objeto7.GetComponent<MeshRenderer>().enabled = false;
        imagenes.SetActive(false);





    }

    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Cantidad recolectados: " + contador2.ToString();
        if (contador2 >= 18)
        {
            textoRecolectarCuboAzul.text = "Recolectar Cubo Azul";
        }

    }

    void Update()
    {
        minUltimaPartida = gestorPersistencia.data.minutos;
        segundosUltimaPartida = gestorPersistencia.data.segundos;
        tiempoUltimaPartida.text = "Tiempo Ultima Partida: " + minUltimaPartida + "." + segundosUltimaPartida;
        float t = Time.time - startTime;
        minutes = ((int)t / 60); 
        seconds = (t % 60);
        timerText.text = "Tiempo:   " + minutes + ":" + seconds + "     ";

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape")&& contador3==0)
        {
            Cursor.lockState = CursorLockMode.None;
            contador3++;
        }
        if(Input.GetKeyDown("escape") && contador3 == 1)
        {
            Cursor.lockState = CursorLockMode.Locked;
            contador3++;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
          if(contador == 0&&sacarPrefabs==false)
            {

                pro = Instantiate(objeto1, ray.origin + (camaraPrimeraPersona.transform.forward * 5), transform.rotation);

                Rigidbody rb = pro.GetComponent<Rigidbody>();
                
            }
            if (contador == 1&&sacarPrefabs==false)
            {
                pro = Instantiate(objeto2, ray.origin + (camaraPrimeraPersona.transform.forward * 4), transform.rotation);

                Rigidbody rb = pro.GetComponent<Rigidbody>();
                
            }
            if (sacarPrefabs == true&&contador2==18)
            {
                RaycastHit hit;

                if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 10)
                {
                    Debug.Log("El rayo tocó al objeto: " + hit.collider.name);

                    if (hit.collider.name == "Plane")
                    {

                        anim.SetBool("abrir", true);
                        anim.Play(animacionAbrir);
                        textoRecolectarCuboAzul.text = "vaya al Portal";
                    }

                }
            }
        }
        if ( Input.GetMouseButtonDown(1)) 
        {
            if (contador == 0)
            {
                contador = 1;
                objeto3.GetComponent<MeshRenderer>().enabled = false;
                objeto4.GetComponent<MeshRenderer>().enabled = false;
                objeto5.GetComponent<MeshRenderer>().enabled = false;
                objeto6.GetComponent<MeshRenderer>().enabled = false;
                objeto7.GetComponent<MeshRenderer>().enabled = true;
            }
            else
            {
                contador = 0;
                objeto3.GetComponent<MeshRenderer>().enabled = true;
                objeto4.GetComponent<MeshRenderer>().enabled = true;
                objeto5.GetComponent<MeshRenderer>().enabled = true;
                objeto6.GetComponent<MeshRenderer>().enabled = true;
                objeto7.GetComponent<MeshRenderer>().enabled = false;
            }
        }
        if (Input.GetButtonDown("Jump") && enElSuelo )
        {

            rb.velocity = new Vector3(0f, salto, 0f * Time.deltaTime);
            rb.AddForce(Vector3.up * salto, ForceMode.Impulse);
            enElSuelo = false;
            saltoActual++;

        }

    }
    private  void OnCollisionEnter(Collision collision)
    {
        enElSuelo = true;
        saltoActual = 0;
        if (collision.gameObject.CompareTag("Coleccionable"))
        {
            Destroy(collision.gameObject);
            contador2++;
            setearTextos();
        }
        if (collision.gameObject.CompareTag("SacarPrefabs")&&contador2==18)
        {
            Destroy(collision.gameObject);
            sacarPrefabs=true;
            objeto3.GetComponent<MeshRenderer>().enabled = false;
            objeto4.GetComponent<MeshRenderer>().enabled = false;
            objeto5.GetComponent<MeshRenderer>().enabled = false;
            objeto6.GetComponent<MeshRenderer>().enabled = false;
            objeto7.GetComponent<MeshRenderer>().enabled = false;
            imagenes.SetActive(true);
            textoRecolectarCuboAzul.text = "Dispare A la Mano";

        }

        if (collision.gameObject.CompareTag("NoPodesTocar"))
        {
            gameObject.transform.position = new Vector3(x, y, z);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Checkpoint"))
        {

            x = gameObject.transform.position.x;
            y = gameObject.transform.position.y;
            z = gameObject.transform.position.z;
        }
        if (other.gameObject.CompareTag("PasarNivel"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

            gestorPersistencia.data.minutos = minutes;
            gestorPersistencia.data.segundos = seconds;
            gestorPersistencia.GuardarDataPersistencia();
        }
    }
}
