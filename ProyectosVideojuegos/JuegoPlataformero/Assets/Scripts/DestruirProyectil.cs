﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestruirProyectil : MonoBehaviour
{
    int contador=0;
    GameObject gameObject1;
    // Start is called before the first frame update
    void Start()
    {
        gameObject1 = gameObject;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            contador++;
        }
        if (contador == 4)
        {
            Destroy(gameObject1);
        }
    }
}
