﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravedad : MonoBehaviour
{
    private Rigidbody rb;
    public float x=0f, y=0f, z=0f;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Physics.gravity = new Vector3(x, y, z);
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
Physics.gravity = new Vector3(0, -20, 0);
    }
}
