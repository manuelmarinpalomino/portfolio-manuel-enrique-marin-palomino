﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{

    public static bool gamePaused = false;
    public GameObject pauseMenuUI;
    public GameObject textoGanaste;
    public GameObject textoRecolectados;
    
   
    public GameObject texto;
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (gamePaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
   public void Resume()
    { 
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gamePaused = false;
        textoGanaste.SetActive(true);
        textoRecolectados.SetActive(true);
        
        texto.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;

    }
    void Pause()
    {
        Cursor.lockState = CursorLockMode.None;
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gamePaused = true;
        textoGanaste.SetActive(false);
        textoRecolectados.SetActive(false);
        
        texto.SetActive(false);

    }
    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Saliendo Del juego");
    }
}
