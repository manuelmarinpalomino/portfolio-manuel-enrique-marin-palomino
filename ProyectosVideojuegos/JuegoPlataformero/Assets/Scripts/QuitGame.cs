﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
    }
    public void quitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }
    
}
