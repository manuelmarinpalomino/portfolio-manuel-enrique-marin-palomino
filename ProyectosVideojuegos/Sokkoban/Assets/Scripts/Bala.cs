﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    public float velocidad;
    private void Update()
    {
        if (!SokobanGameManager.gameOver)
        {
            transform.position = new Vector3(transform.position.x + velocidad * Time.deltaTime, transform.position.y, transform.position.z);
            if (transform.position.x == 100)
            {
                Destroy(this.gameObject);
            }
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "jugador")
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
            SokobanGameManager.gameOver = true;
        }
    }
}
