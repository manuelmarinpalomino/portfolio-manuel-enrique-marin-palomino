﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Contador : MonoBehaviour
{
    private float TiempoF;
    public float TiempoI;
    public Text txtTiempo;

    void Start()
    {
        TiempoF = TiempoI;
    }

    void Update()
    {
        txtTiempo.text = "Tiempo Restante: " + TiempoF.ToString("00");
        if (SokobanGameManager.gameOver == false)
        {
            TiempoF -= Time.deltaTime;
        }

        if (TiempoF <= 0.5f)
        {
            Time.timeScale = 0;
            SokobanGameManager.gameOver = true;
        }
        else Time.timeScale = 1;
    }
}
