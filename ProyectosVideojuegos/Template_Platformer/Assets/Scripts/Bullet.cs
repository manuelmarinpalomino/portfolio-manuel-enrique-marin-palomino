﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public bool Direction;
    public float speed;
    private Rigidbody rb;


    private void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody>();
    }
    void Update()
    {
        if (Direction)
        {
            rb.AddForce(Vector3.left * speed,ForceMode.Force);
        }
        else
        {
            rb.AddForce(Vector3.right * speed, ForceMode.Force);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Bullet" || collision.gameObject.tag  == "Enemy1")
        {
            Destroy(this.gameObject);
        }
    }
}
