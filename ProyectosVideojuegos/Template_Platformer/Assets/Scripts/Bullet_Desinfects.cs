﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Desinfects : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Water")
        {
            Water water = other.gameObject.GetComponent<Water>();
            water.waterIsInfected = false;
            Destroy(this.gameObject);
        }
    }
}
