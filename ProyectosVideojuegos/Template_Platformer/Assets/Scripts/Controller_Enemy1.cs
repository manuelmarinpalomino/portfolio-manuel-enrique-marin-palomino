﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy1 : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bullet;
    public bool fire;


    private void Start()
    {
        StartCoroutine(TimeCounter((float)Random.Range(1, 6)));
    }
    private void Update()
    {
        if (fire)
        {
            fire = false;
            Instantiate(bullet, firePoint.position,Quaternion.identity);
            StartCoroutine(TimeCounter((float)Random.Range(1, 6)));
        }
    }
    public IEnumerator TimeCounter(float chonometerValue)
    {

        yield return new WaitForSeconds(chonometerValue);
        fire = true;
    }
}
