﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy2 : MonoBehaviour
{
    public Animator anim;
    public GameObject Switch2;
    public GameObject water;
    private Water water2;
    private Switch Switch;

    private void Start()
    {
        Switch = Switch2.GetComponent<Switch>();
        water2 = water.GetComponent<Water>();
    }
    private void Update()
    {
        if (Switch.SwitchActive)
        {
            anim.SetBool("Enemy2Down", false);
            anim.SetBool("Enemy2Up", true);
        }
        else
        {
            anim.SetBool("Enemy2Down", true);
            anim.SetBool("Enemy2Up", false);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Water")
        {
            water2.waterIsInfected = true;
        }
    }
    
}
