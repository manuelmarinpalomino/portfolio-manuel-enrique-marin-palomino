﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy3 : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            StartCoroutine(TimeOfLive(5.0f));
        }
    }
    public IEnumerator TimeOfLive (float Time)
    {
        yield return new WaitForSeconds(Time);
        Destroy(this.gameObject);
    }
}
