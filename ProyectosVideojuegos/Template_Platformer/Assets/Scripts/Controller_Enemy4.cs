﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy4 : MonoBehaviour
{
    public bool Player10IsInTheEnemy = false;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Controller_Player cp = collision.gameObject.GetComponent<Controller_Player>();
            if (cp.playerNumber == 10)
            {
                Renderer renderer = this.gameObject.GetComponent<Renderer>();
                renderer.enabled = true;
                Player10IsInTheEnemy = true;
            }
            else
            {
                if (!Player10IsInTheEnemy)
                {
                    BoxCollider box = this.gameObject.GetComponent<BoxCollider>();
                    box.isTrigger = true;
                }
            }
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (!Player10IsInTheEnemy)
            {
                BoxCollider box = this.gameObject.GetComponent<BoxCollider>();
                box.isTrigger = true;
            }
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Controller_Player cp = collision.gameObject.GetComponent<Controller_Player>();
            if (cp.playerNumber == 10)
            {
                Renderer renderer = this.gameObject.GetComponent<Renderer>();
                renderer.enabled = false;
                Player10IsInTheEnemy = false;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            BoxCollider box = this.gameObject.GetComponent<BoxCollider>();
            box.isTrigger = false;
        }
    }
}
