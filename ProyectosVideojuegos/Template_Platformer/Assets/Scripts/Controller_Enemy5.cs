﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy5 : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Controller_Player cP = collision.gameObject.GetComponent<Controller_Player>();
            if(cP.playerNumber == 11)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Destroy(collision.gameObject);
                GameManager.gameOver = true;
            }
        }
    }
}
