﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_DesinfectsWater : Controller_Player
{
    public Transform firePoint;
    public GameObject BulletDesinfects;
    public override void Update()
    {
        base.Update();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject go = Instantiate(BulletDesinfects, firePoint.position, Quaternion.identity);
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.AddForce(Vector3.right * 2, ForceMode.Impulse);

        }
    }
}
