﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_InmmuneToShots : Controller_Player
{
    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Water"))
        {
            Destroy(this.gameObject);
            GameManager.gameOver = true;
        }
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Floor"))
        {
            onFloor = true;
        }
    }
}
