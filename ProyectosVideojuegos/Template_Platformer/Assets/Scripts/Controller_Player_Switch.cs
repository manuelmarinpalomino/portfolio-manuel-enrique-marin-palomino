﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Switch : Controller_Player
{

   
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 11)
        {
            Switch Switch1 = other.gameObject.GetComponent<Switch>();
            Switch1.SwitchActive = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 11)
        {
            Switch Switch1= other.gameObject.GetComponent<Switch>();
            Switch1.SwitchActive = false;
        }
    }
}
