﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    public bool SwitchActive = false;
    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Player8")
        {
            SwitchActive = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Player8")
        {
            SwitchActive = true;
        }
    }
}
