﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    public bool waterIsInfected = false;
    public Material materialWater;
    public Material materialWaterInfected;
    private Renderer renderer1;
    private void Start()
    {
        renderer1 = this.gameObject.GetComponent<Renderer>();
    }
    private void Update()
    {
        if (waterIsInfected)
        {
            renderer1.material = materialWaterInfected;
        }
        else
        {
            renderer1.material = materialWater;
        }
    }
}
