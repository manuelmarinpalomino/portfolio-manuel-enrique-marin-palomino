﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bullet : MonoBehaviour
{
    public bool inSphere;
    public Text textPunctuation;

    private void Start()
    {
        GameObject gameObject = GameObject.Find("TextPunctuation");
        textPunctuation = gameObject.GetComponent<Text>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Sphere")
        {
            this.transform.SetParent(collision.transform);
            inSphere = true;
            GameOver_Mecanica13.contador += 1;
            textPunctuation.text = "Punctuation: " + GameOver_Mecanica13.contador;
        }
        if(collision.gameObject.tag == "Bullet")
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
            GameOver_Mecanica13.contador -= 1;
            textPunctuation.text = "Punctuation: " + GameOver_Mecanica13.contador;
        }
    }
    void Update()
    {
        if(!inSphere)
            transform.Translate(Vector3.forward * 5 * Time.deltaTime);
    }
}
