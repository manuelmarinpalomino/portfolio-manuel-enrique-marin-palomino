﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameOver_Mecanica13 : MonoBehaviour
{
    public static int contador;
    public Text textGameOver;

    private void Awake()
    {
        contador = 0;
        textGameOver.gameObject.SetActive(false);
    }
    void Start()
    {
        contador = 0;
        textGameOver.gameObject.SetActive(false);
    }

    void Update()
    {
        if (contador<=-3)
        {
            Time.timeScale = 0;
            textGameOver.text = "GameOver";
            textGameOver.gameObject.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
