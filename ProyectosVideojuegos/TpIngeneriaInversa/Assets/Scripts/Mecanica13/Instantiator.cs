﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiator : MonoBehaviour
{
    public GameObject bullet;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(bullet);
        }
    }
}
