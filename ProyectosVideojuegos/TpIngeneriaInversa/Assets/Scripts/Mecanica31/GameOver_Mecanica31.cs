﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver_Mecanica31 : MonoBehaviour
{
    public Text textGameOver;
    public Text textGoal;

    private void Awake()
    {
        textGameOver.gameObject.SetActive(false);
        textGoal.gameObject.SetActive(false);
    }
    private void Start()
    {

        textGameOver.gameObject.SetActive(false);
        textGoal.gameObject.SetActive(false);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "GameOver")
        {
            Time.timeScale = 0;
            textGameOver.gameObject.SetActive(true);
            textGoal.gameObject.SetActive(false);
        }
        if(collision.gameObject.tag == "Goal")
        {
            Time.timeScale = 0;
            textGameOver.gameObject.SetActive(false);
            textGoal.gameObject.SetActive(true);
        }
    }
}
