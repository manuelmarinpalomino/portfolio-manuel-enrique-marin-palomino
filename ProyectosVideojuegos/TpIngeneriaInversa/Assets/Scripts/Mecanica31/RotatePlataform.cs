﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlataform : MonoBehaviour
{
    public Animator anim;

    private void awake()
    {
        anim.SetBool("RotatePlataform1", false);
        anim.SetBool("RotatePlataform3", false);
    }

    private void Start()
    {
        anim.SetBool("RotatePlataform1", false);
        anim.SetBool("RotatePlataform3", false);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!anim.GetBool("RotatePlataform1"))
            {
                anim.SetBool("RotatePlataform1", true);
                anim.SetBool("RotatePlataform3", false);
            }
            else if (!anim.GetBool("RotatePlataform3"))
            {
                anim.SetBool("RotatePlataform1", false);
                anim.SetBool("RotatePlataform3", true);
            }

        }
    }
}
