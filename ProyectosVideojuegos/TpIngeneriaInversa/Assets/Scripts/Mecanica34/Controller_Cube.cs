﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Cube : MonoBehaviour
{
    public Material material1;
    public Material material2;
    public Rigidbody rigidbody;
    public float speed;
    private Renderer renderer;
    private bool up;
    
    private void Awake()
    {
        renderer = this.gameObject.GetComponent<Renderer>();
        up = true;
    }
    private void Start()
    {
        renderer = this.gameObject.GetComponent<Renderer>();
        up = true;
    }
    private void OnMouseDown()
    {
        renderer.material = material2;
        up = false;
    }
    private void Update()
    {
        if(up)
        {
            rigidbody.AddForce(Vector3.up * Time.deltaTime * speed, ForceMode.Force);
        }
        if(!up)
        {
            rigidbody.AddForce(-Vector3.up * Time.deltaTime * speed, ForceMode.Force);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Floor")
        {
            renderer.material = material1;
            up = true;
        }
    }
}
