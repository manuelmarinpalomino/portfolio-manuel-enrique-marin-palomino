﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controler_Instantiator35 : MonoBehaviour
{
    public List<Transform> Points;
    public float timeInicial;
    private float time;
    private int contador;
    public GameObject EnemyCube;
    private void Start()
    {
        time = timeInicial;
        contador = 0;
    }
    public void Awake()
    {
        time = timeInicial;
        contador = 0;
    }
    private void Update()
    {
        time--;
        if (time <= 0)
        {
            Instantiate(EnemyCube, Points[Random.Range(0,Points.Count)].position,Quaternion.identity);
            time = timeInicial;
            contador++;
        }
        if(contador >= 4)
        {
            contador = 0;
            timeInicial -= 20;
        }
    }
}
