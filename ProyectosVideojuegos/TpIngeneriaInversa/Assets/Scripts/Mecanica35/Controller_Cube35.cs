﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Cube35 : MonoBehaviour
{
    private Renderer renderer;
    private bool up;
    public Rigidbody rb;
    public Material material1;
    public Material material2;
    public float Speed;
    public Text textGameOver;
    
    private void Awake()
    {
        renderer = this.gameObject.GetComponent<Renderer>();
        textGameOver.gameObject.SetActive(false);
        up = false;
    }
    private void Start()
    {
        renderer = this.gameObject.GetComponent<Renderer>();
        textGameOver.gameObject.SetActive(false);
        up = false;
    }
    private void OnMouseDown()
    {
        if (up)
        {
            up = false;
            renderer.material = material1;
        }
        else
        {
            up = true;
            renderer.material = material2;
        }
    }
    private void Update()
    {
        if (up)
            rb.AddForce(Vector3.up * Speed, ForceMode.Force);
        else
            rb.AddForce(-Vector3.up * Speed, ForceMode.Force);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "GameOver")
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
            Time.timeScale = 0;
            textGameOver.gameObject.SetActive(true);
        }
    }
}
