﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public Rigidbody rb;
    private void Update()
    {
        rb.AddForce(Vector3.left,ForceMode.Force);
        if(this.transform.position.x < -30)
        {
            Destroy(this.gameObject);
        }
    }
}
