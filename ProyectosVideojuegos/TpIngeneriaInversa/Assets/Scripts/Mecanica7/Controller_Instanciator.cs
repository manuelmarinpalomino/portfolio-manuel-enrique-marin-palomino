﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Instanciator : MonoBehaviour
{
    public GameObject walls;
    private float Time;
    private float minTime;
    private float count;
    private bool stopwatchStarted;
    private void Awake()
    {
        Time = 6.0f;
        count = 0;
        stopwatchStarted = false;
    }
    private void Start()
    {
        InstantiateWals();

    }
    private void Update()
    {
        if (stopwatchStarted == false)
        {
            InstantiateWals();
        }
    }
    private void InstantiateWals()
    {

            float numero = UnityEngine.Random.Range(5.0f, 16.1f);
            Instantiate(walls, new Vector3(0, numero, 0), Quaternion.identity);
            stopwatchStarted = true;
            count++;
            StartCoroutine(chronometer(Time));
    }

    IEnumerator chronometer (float TimeChronometer)
    {
        yield return new WaitForSeconds(TimeChronometer);
        stopwatchStarted = false;
        if(count == 4)
        {
            count = 0;
            if (Time >= 2)
                Time -= 0.5f;
        }
    }
}
