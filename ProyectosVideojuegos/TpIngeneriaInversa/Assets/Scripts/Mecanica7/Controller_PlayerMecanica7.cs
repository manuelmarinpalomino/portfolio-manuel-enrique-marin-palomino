﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PlayerMecanica7 : MonoBehaviour
{
    public Rigidbody rb;
    public float jumpForce;
    public GameOver_Mecanica7 gameOver_Mecanica7;
    private event System.EventHandler EGameOver;

    private void Awake()
    {
        EGameOver += gameOver_Mecanica7.EventGameOver;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Walls"))
        {
            EGameOver(this, null);
        }
    }
}
