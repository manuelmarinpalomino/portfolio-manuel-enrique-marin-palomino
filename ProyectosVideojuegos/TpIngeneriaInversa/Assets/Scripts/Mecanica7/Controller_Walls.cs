﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Walls : MonoBehaviour
{
    public static float horizontalMovement;

    public void Awake()
    {
        horizontalMovement = 5;
    }
    private void Update()
    {
        this.transform.Translate(new Vector3(0, 0,-horizontalMovement * Time.deltaTime));
        if(this.transform.position.z <= -50)
        {
            Destroy(this.gameObject);
        }
    }

}
