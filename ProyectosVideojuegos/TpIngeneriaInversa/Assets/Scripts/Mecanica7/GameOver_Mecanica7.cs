﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver_Mecanica7 : MonoBehaviour
{
    public Text textGameOver;
    private void Awake()
    {
        textGameOver.gameObject.SetActive(false);
    }
    private void Start()
    {
        textGameOver.gameObject.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            SceneManager.LoadScene("Menu");
        }
    }
    public void EventGameOver(object sender,System.EventArgs evenArgs)
    {
        textGameOver.gameObject.SetActive(true);
        Time.timeScale = 0;

    }
}
