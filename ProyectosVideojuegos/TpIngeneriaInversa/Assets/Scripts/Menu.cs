﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void Mecanica7()
    {
        SceneManager.LoadScene("Mecanica7");
    }
    public void Mecanica13()
    {
        SceneManager.LoadScene("Mecanica13");
    }
    public void Mecanica31()
    {
        SceneManager.LoadScene("Mecanica31");
    }
    public void Mecanica34()
    {
        SceneManager.LoadScene("Mecanica34");
    }
    public void Mecanica35()
    {
        SceneManager.LoadScene("Mecanica35");
    }
    public void Quit()
    {
        Application.Quit();
    }
}
