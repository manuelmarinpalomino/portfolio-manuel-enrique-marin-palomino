﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balas : MonoBehaviour//Andres tiene qe usar este codigo para hacer polimorfismo y heredar de esta clase
{
    public float velocidad;
    public Renderer Render;
    public GameObject[] target;
    public int CurPos = 0;
    public int NexPos = 1;
    public bool Push;
    public Material MatAzul;
    public Material MatRojo;
    public Material MatVerde;
    public Material MatAmarillo;
    public int ColorCant;
    void Start()
    {
        Render = GetComponent<Renderer>();
        ColorCant = Random.Range(1, 5);
        TargetController();
       
    }
    protected virtual void TargetController()
    {
        if (transform.parent.name == "Punto1")
        {
            target[0] = GameObject.Find("Punto1Final");
        }
        if (transform.parent.name == "Punto2")
        {
            target[0] = GameObject.Find("Punto2Final");
        }
        if (transform.parent.name == "Punto3")
        {
            target[0] = GameObject.Find("Punto3Final");
        }
        if (transform.parent.name == "Punto4")
        {
            target[0] = GameObject.Find("Punto4Final");
        }
    }
    protected virtual void Color()
    {
        switch (ColorCant)
        {
            case 1:
                Render.material = MatAzul;
                gameObject.tag = "Azul";
                break;
            case 2:
                Render.material = MatAmarillo;
                gameObject.tag = "Amarillo";
                break;
            case 3:
                Render.material = MatRojo;
                gameObject.tag = "Rojo";
                break;
            case 4:
                Render.material = MatVerde;
                gameObject.tag = "Verde";
                break;
            default:
                break;
        }


    }
    protected virtual void Mover()
    {

        if (Push == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, target[NexPos].transform.position, velocidad * Time.deltaTime);
            if (Vector3.Distance(transform.position, target[NexPos].transform.position) <= 0)
            {

                CurPos = NexPos;
                NexPos++;

                if (NexPos > target.Length - 1)
                {
                    NexPos = 0;
                }
            }
        }

    }

    void Update()
    {
        Color();
        Mover();

    }
}
