﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CambioColor : MonoBehaviour//Heredar para tener el comportameinto de esta clase en otro cambio de color
{
    public Renderer Render;
    public Material MatAzul;
    public Material MatRojo;
    public Material MatVerde;
    public Material MatAmarillo;
    public int MaxCant;
    public int ColorCant;

        void Start()
    {
        Render = GetComponent<Renderer>();
    }
    protected virtual void Aumentar()
    {
        ColorCant += 1;
    }
    protected virtual void CambioDeColor()
    {
        if (ColorCant > MaxCant)
        {
            ColorCant = 0;
        }
        switch (ColorCant)
        {
            case 1:
                Render.material = MatAzul;
                gameObject.tag = "Azul";
                break;
            case 2:
                Render.material = MatAmarillo;
                gameObject.tag = "Amarillo";
                break;
            case 3:
                Render.material = MatRojo;
                gameObject.tag = "Rojo";
                break;
            case 4:
                Render.material = MatVerde;
                gameObject.tag = "Verde";
                break;
            default:
                break;

        }
    }
    void Update()
    {
        CambioDeColor();
    }
    public void OnMouseDown()
    {
        Aumentar();
    }
}
