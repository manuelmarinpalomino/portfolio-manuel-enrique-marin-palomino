﻿using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.Net;
using System.Net.Sockets;
public class Servidor 
{
    IPHostEntry Host;
    IPAddress Ip;
    IPEndPoint endPoint;

    Socket S_Server;
    Socket S_Cliente;
    public Servidor(string ip, int port)
    {
        Host = Dns.GetHostEntry(ip);
        Ip = Host.AddressList[0];
        endPoint = new IPEndPoint(Ip, port);

        S_Server = new Socket(Ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        S_Server.Bind(endPoint);
        S_Server.Listen(3);

    }
  public void Start ()
    {
        byte[] buffer = new byte[1024];
        string mensaje;
        S_Cliente = S_Server.Accept();

        S_Cliente.Receive(buffer);
        mensaje = Encoding.ASCII.GetString(buffer);
        Debug.Log("Se recivio mensaje: " + mensaje);
    }
}
