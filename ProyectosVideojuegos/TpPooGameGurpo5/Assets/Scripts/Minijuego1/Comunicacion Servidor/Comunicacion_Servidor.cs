﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comunicacion_Servidor : MonoBehaviour
{

    public  Bases_Manager_Otro_Cliente managerBasesOtroCliente;
    public  Controller_Otro_Cliente managerControllerOtroCliente;
    public static Conexion conexionServidor;
    string informacionServidor = "";
    public static void PasarInformacion(string informacion)
    {
        conexionServidor.SendInformacion(informacion);
    }
    private void Awake()
    {
        
        conexionServidor =  GameObject.Find("Conexion").GetComponent<Conexion>();
    }
    private void Update()
    {
        if (managerBasesOtroCliente != null && managerControllerOtroCliente)
        {

        }
        if(informacionServidor != "")
        {
            MandarInformacion(informacionServidor);
            informacionServidor = "";
        }
    }
    public void RecibirInformacionDelServidor(string informacion)
    {
        informacionServidor = informacion;
        
    }
    public  void MandarInformacion(string informacion) //ACA RECIBE LA INFORMACION Y LA DISTRIBUYE. EN EL SERVIDOR MANDAMOS EL NICK DE LA CONEXION JUNTO AL STRING PARA ASEGURAR CUAL TENEMOS QUE MODIFICAR
    {
        //try
        //{
            char[] charSplit = new char[2];
            charSplit[0] = ';';
            charSplit[1] = ':'; 
            string[] queRecibi = informacion.Split(charSplit,System.StringSplitOptions.RemoveEmptyEntries);
            if(queRecibi[0] ==LeerImput.imput)
            {

            }
            else
            {

                switch (queRecibi[1]) 
                {
                    case "c": //Tuve que poner un espacio antes de la letra porque en el servidor no se quien puso un espacio despues de los dos puntos.
                        managerBasesOtroCliente.CambiarColorEnBase(informacion);
                        break;
                    case "b": //Tuve que poner un espacio antes de la letra porque en el servidor no se quien puso un espacio despues de los dos puntos.
                        managerControllerOtroCliente.InstanciarBalas(informacion);
                        break;
                    default:
                        break;
                }
            }
           
        //}
        //catch (System.Exception)
        //{
        //    Debug.Log("Fallo en el recibimiento de informacion");
        //    throw;
        //}
    }
}

