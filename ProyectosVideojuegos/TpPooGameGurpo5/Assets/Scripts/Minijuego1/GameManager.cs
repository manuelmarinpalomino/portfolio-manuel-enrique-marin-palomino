﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour,Interfaz
{
   
    public string TextoPerder { get; set; }
    public Bases_Controlador Controlador;

    public TextMeshProUGUI TextPerder;
   
    public Persiste Data;
    public bool perder;

   

    public  void Dialogo( string textoperder)
    {
     
        TextoPerder = textoperder;

        TextPerder.text = TextoPerder.ToString();
     

    }


    /// <summary>
    /// Agregar la llega del evento de perder 
    /// Recibe los evento de ganar y perder de todos los juegos
    /// Estara en todas las escenas 
    /// </summary>
    // Start is called before the first frame update
 public void Start()
    {

        Controlador = GameObject.FindObjectOfType<Bases_Controlador>();
        Data = GameObject.FindObjectOfType<Persiste>();
        

        TextoPerder = "GAME OVER";
        Dialogo(TextoPerder);

    }

    // Update is called once per frame
   public void Update()
    {

        if (perder == true)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Time.timeScale = 1;
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);


            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                Time.timeScale = 1;
                SceneManager.LoadScene("Menu");

            }
        } 
    }
    // hacer handler para victoria y derota 

    public void PunteroGameOver(object Sender, System.EventArgs E)
    {
        Time.timeScale = 0;
        TextPerder.gameObject.SetActive(true);
        if(SceneManager.GetActiveScene().name == "Minijuego1")
        {
            if (Data.data.MayorRecordjuego1.Count < 10 && Bases_Controlador.contador > 0)
            {
                Data.data.AgregarRecordJuego1(Bases_Controlador.contador);
                Data.data.OrdenarMayorRecordJuego1();
                
            }
            else
            {
                bool rompioAlgunRecord = false;
                foreach (var item in Data.data)
                {
                    if(Bases_Controlador.contador > item)
                    {
                        rompioAlgunRecord = true;
                    }
                }
                if (rompioAlgunRecord == true)
                {
                    Data.data.OrdenarMayorRecordJuego1();
                    Data.data.EliminarUltimoElementoJuego1();
                    Data.data.AgregarRecordJuego1(Bases_Controlador.contador);
                    Data.data.OrdenarMayorRecordJuego1();
                }
            }
         
        }
        else if(SceneManager.GetActiveScene().name == "Minijuego2")
        {
            if (Data.data.MayorRecordjuego2.Count < 10&& Bases_Controlador.contador > 0)
            {
                Data.data.AgregarRecordJuego2(Bases_Controlador.contador);
                Data.data.OrdenarMayorRecordJuego2();
            }
            else
            {
                bool rompioAlgunRecord = false;
                foreach (var item in Data.data.MayorRecordjuego2)
                {
                    if (Bases_Controlador.contador > item)
                    {
                        rompioAlgunRecord = true;
                    }
                }
                if (rompioAlgunRecord == true)
                {
                    Data.data.OrdenarMayorRecordJuego2();
                    Data.data.EliminarUltimoElementoJuego2();
                    Data.data.AgregarRecordJuego2(Bases_Controlador.contador);
                    Data.data.OrdenarMayorRecordJuego2();
                }

            }
        }
        Data.GuardarDataPersistencia();
        perder = true;
    }


    
}

