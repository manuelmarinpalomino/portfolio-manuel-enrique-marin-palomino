﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class Instanciador_Nivel1 : Controller_Instanciador
{
    protected override void aumentarDificultad()
    {
        base.aumentarDificultad();
    }
    protected override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        base.Update();
    }
    protected override IEnumerator Espera()
    {
        return base.Espera();
    }
    protected override IEnumerator Espera(float tiempo)
    {
        return base.Espera(tiempo);
    }
    protected override void InstanciarBalas()
    {
        int numero = Random.Range(0, 4);
        GameObject refe = Instantiate(mBala, mPuntos[numero].transform.position, mPuntos[numero].transform.rotation,mPuntos[numero].transform);
         StartCoroutine(Espera(mTiempo));
   
    }

}
