﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balas_Otro_Cliente : MonoBehaviour
{
    public float velocidad=1;
    public Renderer Render = new Renderer();
    public GameObject[] target;
    public int CurPos = 0;
    public int NexPos = 1;
    public bool Push;
    public string ColorCant;
    public int puntoDeSalida;

    public Material MatAzul;
    public Material MatRojo;
    public Material MatVerde;
    public Material MatAmarillo;

    public Balas_Otro_Cliente()
    {
    }

    private void Awake()
    {
        Render = this.GetComponent<Renderer>();
    }
    void Start()
    {
        Render = GetComponent<Renderer>();
        TargetController();

    }
    protected virtual void TargetController()
    {
        if (transform.parent.name == "Punto1")
        {
            target[0] = GameObject.Find("Punto1FinalM");
        }
        if (transform.parent.name == "Punto2")
        {
            target[0] = GameObject.Find("Punto2FinalM");
        }
        if (transform.parent.name == "Punto3")
        {
            target[0] = GameObject.Find("Punto3FinalM");
        }
        if (transform.parent.name == "Punto4")
        {
            target[0] = GameObject.Find("Punto4FinalM");
        }
    }
    public void Color(string informacion)
    {
        switch (informacion)
        {
            case "b":
                Render.material = MatAzul;
                gameObject.tag = "Azul";
                break;
            case "y":
                Render.material = MatAmarillo;
                gameObject.tag = "Amarillo";
                break;
            case "r":
                Render.material = MatRojo;
                gameObject.tag = "Rojo";
                break;
            case "g":
                Render.material = MatVerde;
                gameObject.tag = "Verde";
                break;
            default:
                break;
        }


    }
    protected virtual void Mover()
    {
            if (Push == false)
            {
                transform.position = Vector3.MoveTowards(transform.position, target[NexPos].transform.position, velocidad * Time.deltaTime);
                if (Vector3.Distance(transform.position, target[NexPos].transform.position) <= 0)
                {

                    CurPos = NexPos;
                    NexPos++;

                    if (NexPos > target.Length - 1)
                    {
                        NexPos = 0;
                    }
                }
            }
    }

    void Update()
    {
        Mover();
    }
}
