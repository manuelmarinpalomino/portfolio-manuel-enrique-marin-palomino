﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Otro_Cliente : MonoBehaviour
{
    [SerializeField]
    public List<Transform> mPuntosMimic;

    [SerializeField]
    public GameObject mBala_Otro_Cliente;

    public float mTiempo;
    public bool mInstanciarBala;
    public int mContador;

    string informacionAnterior = "";
    private void Awake()
    {
        mTiempo = 4;
        mInstanciarBala = true;
        mContador = 0;

    }
    protected virtual void Start()
    {
        mContador = 0;
        //if (mInstanciarBala == true)
        //{
        //    mInstanciarBala = false;
        //    InstanciarBalas();
        //}
        aumentarDificultad();
    }

    protected virtual void aumentarDificultad()
    {
        if ((mContador%2) == 0)
        {
            if (mTiempo > 1.0f)
            {
                mTiempo -= 0.5f;
            }
        }
    }
    public virtual void Update()
    {
        //if (mInstanciarBala == true)
        //{
        //    mInstanciarBala = false;
        //    mContador++;
        //    InstanciarBalas();
        //}
    }
    public void InstanciarBalas(string informacion)
    {
        int posicion;
        char[] charSplit = new char[2];
        charSplit[0] = ';';
        charSplit[1] = ':';
        string[] separado;
        separado = informacion.Split(charSplit, System.StringSplitOptions.RemoveEmptyEntries);
        if (separado[1] + separado[2] + separado[3] == informacionAnterior)
        {

        }
        else
        {
            posicion = DevolverPosicion(separado[2]); //Por como spliteaste, recibimos tambien el nickname. Asi que estamos trabajando con 4 pos en vez de 3. 


            GameObject refe = Instantiate(mBala_Otro_Cliente, mPuntosMimic[posicion].transform.position, mPuntosMimic[posicion].transform.rotation, mPuntosMimic[posicion].transform);
            Balas_Otro_Cliente temp = refe.GetComponent<Balas_Otro_Cliente>();
            temp.Color(separado[3]); //Por como spliteaste, recibimos tambien el nickname. Asi que estamos trabajando con 4 pos en vez de 3. 
                                     //StartCoroutine(Espera(mTiempo));
            mContador++;
            informacionAnterior = separado[1] + separado[2] + separado[3];
        }


    }

    //protected virtual IEnumerator Espera(float tiempo)
    //{
    //    yield return new WaitForSeconds(tiempo);
    //    mInstanciarBala = true;
    //}
    //protected virtual IEnumerator Espera()
    //{
    //    yield return new WaitForSeconds(6.0f);
    //    mInstanciarBala = true;
    //}

    private int DevolverPosicion(string informacion) // Si esto esta raro es por como esta posicionado en la escena
    {
        int posicion=0;

        switch (informacion)
        {
            case "u":
                posicion = 0;
                break;
            case "r":
                posicion = 1;
                break;
            case "d":
                posicion = 2;
                break;
            case "l":
                posicion = 3;
                break;
            default:
                Debug.Log(informacion);
                break;
        }
        return posicion;
    }
}
