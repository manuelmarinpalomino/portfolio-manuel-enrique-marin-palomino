﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bases_Manager_Otro_Cliente : MonoBehaviour
{
    public List<CambiarColorOtroCliente> bases = new List<CambiarColorOtroCliente>();

    
    public void CambiarColorEnBase(string informacion)
    {
        string[] separado;
        separado = informacion.Split(';');
        switch (separado[1])
        {
            case "u":
                bases[0].CambioDeColor(separado[2]);
                break;
            case "r":
                bases[1].CambioDeColor(separado[2]);
                break;
            case "d":
                bases[2].CambioDeColor(separado[2]);
                break;
            case "l":
                bases[3].CambioDeColor(separado[2]);
                break;
            default:
                Debug.Log(informacion);
                break;
        }
    }
}
