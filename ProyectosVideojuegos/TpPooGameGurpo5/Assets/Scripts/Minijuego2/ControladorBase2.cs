﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControladorBase2 : MonoBehaviour
{
    public GameManager Controlador;
    public Bases_Controlador Evento;
    void Start()
    {
        Controlador = GameObject.FindObjectOfType<GameManager>();
        Evento = GameObject.FindObjectOfType<Bases_Controlador>();
    }

    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name== "BalaMinJueg2(Clone)")
        {
            Destroy(other.gameObject);
         
            Evento.ProducirGameOver += Controlador.PunteroGameOver;
            Evento.OnProducirGameOver();
            
        }
    }
}
