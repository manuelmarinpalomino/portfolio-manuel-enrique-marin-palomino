﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class General_InputManager : MonoBehaviour
{
    void Update()
    {
        if(Input.GetKey(KeyCode.R))
        {
            string nombreEscenaActual = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(nombreEscenaActual);
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
