﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorNivel2 : Controller_Instanciador
{
    protected override void Start()
    {
        base.Start();
    }
    public override void Update()
    {
        base.Update();
    }
    protected override void InstanciarBalas()
    {
        base.InstanciarBalas();
    }
    protected override void aumentarDificultad()
    {
        base.aumentarDificultad();
    }
    protected override IEnumerator Espera()
    {
        return base.Espera();
    }
    protected override IEnumerator Espera(float tiempo)
    {
        return base.Espera(tiempo);
    }
}
