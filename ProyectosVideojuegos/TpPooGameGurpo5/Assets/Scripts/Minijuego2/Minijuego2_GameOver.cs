﻿using UnityEngine;

public class Minijuego2_GameOver : MonoBehaviour
{
    private void Start()
    {
        Time.timeScale = 1;
    }

    void Update()
    {
        GameOver();
    }

    public static void GameOver()
    {
        Time.timeScale = 0;
    }
}
