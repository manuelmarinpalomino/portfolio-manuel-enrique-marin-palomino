﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller_Instanciador : MonoBehaviour
{
    [SerializeField]
    public List<Transform> mPuntos;

    [SerializeField]
    public GameObject mBala;
    
    public float mTiempo;
    public bool mInstanciarBala;
    public int mContador;

    private void Awake()
    {
        mTiempo = 4;
        mInstanciarBala = true;
        mContador = 0;
        
    }
    protected virtual void Start()
    {
        mContador = 0;
        if (mInstanciarBala == true)
        {
            mInstanciarBala = false;
            InstanciarBalas();
        }
    }
    public virtual void Update()
    {
        if (mInstanciarBala == true)
        {
            mInstanciarBala = false;
            mContador++;
            InstanciarBalas();
        }
        aumentarDificultad();
    }
    protected virtual void aumentarDificultad()
    {
        if (mContador == 2)
        {
            mContador = 0;
            if (mTiempo > 1.0f)
            {
                mTiempo -= 0.5f;
            }
        }
    }
    protected  virtual void InstanciarBalas()
    {
        int numero = Random.Range(0, mPuntos.Count);
        GameObject refe = Instantiate(mBala, mPuntos[numero].transform.position,Quaternion.identity);
        StartCoroutine(Espera(mTiempo));
    }

    protected virtual IEnumerator Espera(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        mInstanciarBala = true;
    }
    protected virtual IEnumerator Espera()
    {
        yield return new WaitForSeconds(6.0f);
        mInstanciarBala = true;
    }
}
