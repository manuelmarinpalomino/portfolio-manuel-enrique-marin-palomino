﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class Bases_Controlador : MonoBehaviour
{
    public static int contador;
    public static int contador2;
    public Persiste Data;
    public TextMeshProUGUI Puntos;
    public TextMeshProUGUI MayorPuntuacion;
    public GameManager Controlador;
    public Instanciador_Nivel1 cont;
    public GameObject vfxEffectErroneo;
    public GameObject vfxEffectAcertado;

    public bool Vivir;
    public delegate void GameOverEventHandler(object source, System.EventArgs args);
    public event GameOverEventHandler ProducirGameOver;

    public void OnProducirGameOver()
    {
        ProducirGameOver?.Invoke(this, System.EventArgs.Empty);
    }
    public static  void PuntajeMas()
    {
        contador++;
    }
    public static void PuntajeMenos()
    {
        if (contador > 0)
            contador--;
        else
            contador = 0;
    }

    void Start()
    {
        Controlador = GameObject.FindObjectOfType<GameManager>();
        contador = 0;
        Data = GameObject.FindObjectOfType<Persiste>();

        cont = GameObject.FindObjectOfType<Instanciador_Nivel1>();
    }
    private void Update()
    {
        
        puntos();
        if (cont.Cont == 20)
        {
            ProducirGameOver += Controlador.PunteroGameOver;
            OnProducirGameOver();
        }
    }
    protected  virtual void puntos()
    {
        MayorPuntuacion.text = "Mayor Puntaje " + Data.data.MayorRecord.ToString("f0");
        Puntos.text = "Puntos: "+contador.ToString();
        Data.data.Record = contador;
      
        if (Data.data.Record >= Data.data.MayorRecord)
        {
            Data.data.MayorRecord = Data.data.Record;
            MayorPuntuacion.text = "Mayor Puntaje " + Data.data.MayorRecord.ToString("f0");
            Data.GuardarDataPersistencia();
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {


        if (gameObject.tag == "Rojo")
        {
            if (other.CompareTag("Rojo"))
            {
                PuntajeMas();
                Destroy(other.gameObject);
                GameObject vfxInstanciado = Instantiate(vfxEffectAcertado,this.gameObject.transform.position,Quaternion.LookRotation(Vector3.up));
                Destroy(vfxInstanciado, 1f);
            }
            else 
            {
                PuntajeMenos();
                Destroy(other.gameObject);
                GameObject vfxInstanciado = Instantiate(vfxEffectErroneo, this.gameObject.transform.position, Quaternion.LookRotation(Vector3.up));
                Destroy(vfxInstanciado, 1f);
            }
        }

        if (gameObject.tag == "Azul")
        {
            if (other.CompareTag("Azul"))
            {
                PuntajeMas();
                Destroy(other.gameObject);
                GameObject vfxInstanciado = Instantiate(vfxEffectAcertado, this.gameObject.transform.position, Quaternion.LookRotation(Vector3.up));
                Destroy(vfxInstanciado, 1f);
            }
            else
            {
                PuntajeMenos();
                Destroy(other.gameObject);
                GameObject vfxInstanciado = Instantiate(vfxEffectErroneo, this.gameObject.transform.position, Quaternion.LookRotation(Vector3.up));
                Destroy(vfxInstanciado, 1f);
            }
        }

        if (gameObject.tag == "Verde")
        {
            if (other.CompareTag("Verde"))
            {
                PuntajeMas();
                Destroy(other.gameObject);
                GameObject vfxInstanciado = Instantiate(vfxEffectAcertado, this.gameObject.transform.position, Quaternion.LookRotation(Vector3.up));
                Destroy(vfxInstanciado, 1f);
            }
            else
            {
                PuntajeMenos();
                Destroy(other.gameObject);
                GameObject vfxInstanciado = Instantiate(vfxEffectErroneo, this.gameObject.transform.position, Quaternion.LookRotation(Vector3.up));
                Destroy(vfxInstanciado, 1f);
            }
        }

        if (gameObject.tag == "Amarillo")
        {
            if (other.CompareTag("Amarillo"))
            {
                PuntajeMas();
                Destroy(other.gameObject);
                GameObject vfxInstanciado = Instantiate(vfxEffectAcertado, this.gameObject.transform.position, Quaternion.LookRotation(Vector3.up));
                Destroy(vfxInstanciado, 1f);
            }
            else
            {
                PuntajeMenos();
                Destroy(other.gameObject);
                GameObject vfxInstanciado = Instantiate(vfxEffectErroneo, this.gameObject.transform.position, Quaternion.LookRotation(Vector3.up));
                Destroy(vfxInstanciado, 1f);

            }
        }

    }
}
