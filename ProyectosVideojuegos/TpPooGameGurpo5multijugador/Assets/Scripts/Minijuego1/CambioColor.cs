﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CambioColor : MonoBehaviour//Heredar para tener el comportameinto de esta clase en otro cambio de color
{
    public Renderer Render;
    public Material MatAzul;
    public Material MatRojo;
    public Material MatVerde;
    public Material MatAmarillo;
    public int MaxCant;
    public int ColorCant;
    public string nombreBase;
    public string colorBase;

        void Start()
    {
        Render = GetComponent<Renderer>();
    }
    protected virtual void Aumentar()
    {
        ColorCant += 1;
    }
    protected virtual void CambioDeColor()
    {
        if (ColorCant > MaxCant)
        {
            ColorCant = 1;
        }
        switch (ColorCant)
        {
            case 1:
                Render.material = MatAzul;
                gameObject.tag = "Azul";
                colorBase = "b";
                break;
            case 2:
                Render.material = MatAmarillo;
                gameObject.tag = "Amarillo";
                colorBase = "y";
                break;
            case 3:
                Render.material = MatRojo;
                gameObject.tag = "Rojo";
                colorBase = "r";
                break;
            case 4:
                Render.material = MatVerde;
                gameObject.tag = "Verde";
                colorBase = "g";
                break;
            default:
                break;

        }
    }
    void Update()
    {
       // if (GameManager.Activo == true)
       // {


            if (Input.GetKeyDown(KeyCode.W))
            {
                if (nombreBase == "u")
                {
                    Aumentar();
                    CambioDeColor();
                    Comunicacion_Servidor.PasarInformacion("c;" + nombreBase + ";" + colorBase);
                }
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                if (nombreBase == "l")
                {
                    Aumentar();
                    CambioDeColor();
                    Comunicacion_Servidor.PasarInformacion("c;" + nombreBase + ";" + colorBase);
                }
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                if (nombreBase == "d")
                {
                    Aumentar();
                    CambioDeColor();
                    Comunicacion_Servidor.PasarInformacion("c;" + nombreBase + ";" + colorBase);
                }
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                if (nombreBase == "r")
                {
                    Aumentar();
                    CambioDeColor();
                    Comunicacion_Servidor.PasarInformacion("c;" + nombreBase + ";" + colorBase);
                }
            }
        //}


    }
}
