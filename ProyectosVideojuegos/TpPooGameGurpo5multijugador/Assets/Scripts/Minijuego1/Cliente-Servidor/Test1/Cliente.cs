﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

public class Cliente 
{
    IPHostEntry Host;
    IPAddress Ip;
    IPEndPoint endPoint;

    Socket S_Server;
    Socket S_Cliente;
    public Cliente(string ip, int port)
    {
        Host = Dns.GetHostEntry(ip);
        Ip = Host.AddressList[0];
        endPoint = new IPEndPoint(Ip, port);

        S_Cliente = new Socket(Ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

    }
    public void Start()
    {
        S_Cliente.Connect(endPoint);
    }
    public void sent(string msg)
    {
		        byte[] ByteMsg = Encoding.ASCII.GetBytes(msg);
		if(msg != null)
		{
			
        S_Cliente.Send(ByteMsg);
      Debug.Log("Mensaje envido ");
        Console.Out.Flush();
		}

    }
    public string receive()
    {
        byte[] Buffer = new byte[1040];
        S_Cliente.Receive(Buffer);
        return Buffer.ToString();


    }
}
