﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour,Interfaz
{
    Cleinte cliente;
    public static bool Activo;
    public Comunicacion_Servidor comunicacion_Servidor;
    public string TextoPerder { get; set; }
    public Bases_Controlador Controlador;

    public TextMeshProUGUI TextPerder;
    public TextMeshProUGUI Ganador;
    public Persiste Data;
    public bool perder;

    public bool partidaTermino;

    public string mGanador;

    public List<string> ganadores = new List<string>();

    public void CargarTexto(string ganador, int puntajeGanador, string fechaActual)
    {
        Ganador.text = ganador + puntajeGanador.ToString() + "A las " + fechaActual.ToString();

        ganadores.Add(Ganador.text);
    }

    public void Dialogo( string textoperder)
    {
     
        TextoPerder = textoperder;

        TextPerder.text = TextoPerder.ToString();
     
    }


    /// <summary>
    /// Agregar la llega del evento de perder 
    /// Recibe los evento de ganar y perder de todos los juegos
    /// Estara en todas las escenas 
    /// </summary>
    // Start is called before the first frame update
 public void Start()
    {
        Time.timeScale = 1;
        cliente = new Cleinte(ref comunicacion_Servidor);
        Controlador = GameObject.FindObjectOfType<Bases_Controlador>();
        Data = GameObject.FindObjectOfType<Persiste>();
        

        TextoPerder = "GAME OVER";
        Dialogo(TextoPerder);

    }

   public void Update()
    {
       // if (Cliente.> 1)
       // {
         //   Activo = true;
         //   Time.timeScale = 1;
        //}
        //else
       // {
         //   Activo = false;
           // Time.timeScale = 0;
        //}
        if (true)
        {

        }
        if (perder == true)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                Time.timeScale = 1;
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                
                SceneManager.LoadScene("Menu");
                Time.timeScale = 1;
            }
        }
    }
    // hacer handler para victoria y derota 

    public void PunteroGameOver(object Sender, System.EventArgs E)
    {
        Time.timeScale = 0;
        TextPerder.gameObject.SetActive(true);
        if(SceneManager.GetActiveScene().name == "Minijuego1")
        {
            if (Data.data.MayorRecordjuego1.Count < 10 && Bases_Controlador.contador > 0)
            {
                Data.data.AgregarRecordJuego1(Bases_Controlador.contador);
                Data.data.OrdenarMayorRecordJuego1();
                
            }
            else
            {
                bool rompioAlgunRecord = false;
                foreach (var item in Data.data)
                {
                    if(Bases_Controlador.contador > item)
                    {
                        rompioAlgunRecord = true;
                    }
                }
                if (rompioAlgunRecord == true)
                {
                    Data.data.OrdenarMayorRecordJuego1();
                    Data.data.EliminarUltimoElementoJuego1();
                    Data.data.AgregarRecordJuego1(Bases_Controlador.contador);
                    Data.data.OrdenarMayorRecordJuego1();
                }
            }
            if (Bases_Controlador.contador > Bases_Controlador.contador2)
            {
                Ganador.gameObject.SetActive(true);
                //Ganador.text = "Ganador: Jugador1";
                if (!partidaTermino)
                {
                    string tiempoActual = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
                    CargarTexto("Ganó el Jugador 1, con ", Bases_Controlador.contador, tiempoActual);
                    Comunicacion_Servidor.PasarInformacion("Gane");
                    partidaTermino = true;
                }

            }
            else
            {
                Ganador.gameObject.SetActive(true);
                //Ganador.text = "Ganador: Jugador2";
                if (!partidaTermino)
                {
                    string tiempoActual = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
                    CargarTexto("Ganó el Jugador 2, con ", Bases_Controlador.contador2, tiempoActual);
                    Comunicacion_Servidor.PasarInformacion("Perdi");
                    partidaTermino = true;
                }
            }
         
        }
        else if(SceneManager.GetActiveScene().name == "Minijuego2")
        {
            if (Data.data.MayorRecordjuego2.Count < 10&& Bases_Controlador.contador > 0)
            {
                Data.data.AgregarRecordJuego2(Bases_Controlador.contador);
                Data.data.OrdenarMayorRecordJuego2();
            }
            else
            {
                bool rompioAlgunRecord = false;
                foreach (var item in Data.data.MayorRecordjuego2)
                {
                    if (Bases_Controlador.contador > item)
                    {
                        rompioAlgunRecord = true;
                    }
                }
                if (rompioAlgunRecord == true)
                {
                    Data.data.OrdenarMayorRecordJuego2();
                    Data.data.EliminarUltimoElementoJuego2();
                    Data.data.AgregarRecordJuego2(Bases_Controlador.contador);
                    Data.data.OrdenarMayorRecordJuego2();
                }

            }
        }
        Data.GuardarDataPersistencia();
        perder = true;
    }
    
}