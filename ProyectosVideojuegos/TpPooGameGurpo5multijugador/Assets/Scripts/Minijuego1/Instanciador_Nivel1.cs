﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public  class Instanciador_Nivel1 : Controller_Instanciador
{
    public int Cont;
    public TextMeshProUGUI TextCont;
    public delegate void GameOverEventHandler(object source, System.EventArgs args);
    public event GameOverEventHandler ProducirGameOver;
    public void OnProducirGameOver()
    {
        ProducirGameOver?.Invoke(this, System.EventArgs.Empty);
    }
    protected override void aumentarDificultad()
    {
        base.aumentarDificultad();
    }
    protected override void Start()
    {
        base.Start();
      
    }
    public override void Update()
    {
        base.Update();
      
    }
    protected override IEnumerator Espera()
    {
        return base.Espera();
    }
    protected override IEnumerator Espera(float tiempo)
    {
        return base.Espera(tiempo);
    }
    protected override void InstanciarBalas()
    {
        int numero = Random.Range(0, 4);
        GameObject refe = Instantiate(mBala, mPuntos[numero].transform.position, mPuntos[numero].transform.rotation, mPuntos[numero].transform);
        Cont += 1;
        TextCont.text = Cont.ToString() + "/50";
        StartCoroutine(Espera(mTiempo));
    }
}
