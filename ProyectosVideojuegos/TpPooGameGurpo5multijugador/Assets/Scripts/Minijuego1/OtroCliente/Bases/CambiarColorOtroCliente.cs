﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarColorOtroCliente : MonoBehaviour
{


    public Renderer Render;
    public Material MatAzul;
    public Material MatRojo;
    public Material MatVerde;
    public Material MatAmarillo;


    void Awake()
    {
        Render = GetComponent<Renderer>();
        
    }

    public void CambioDeColor(string color)
    {
        switch (color)
        {
            case "b":
                Render.material = MatAzul;
                gameObject.tag = "Azul";
                break;
            case "y":
                Render.material = MatAmarillo;
                gameObject.tag = "Amarillo";
                break;
            case "r":
                Render.material = MatRojo;
                gameObject.tag = "Rojo";
                break;
            case "g":
                Render.material = MatVerde;
                gameObject.tag = "Verde";
                break;
            default:
                break;
        }
    }
}
