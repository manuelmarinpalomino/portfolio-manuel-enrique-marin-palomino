﻿using UnityEngine;

public class CambioColorPlayerMinJue2 : CambioColor
{
    void Start()
    {
        Render = GetComponent<Renderer>();
    }
    protected override void Aumentar()
    {
        
    }
    protected override void CambioDeColor()
    {

        if (ColorCant > MaxCant)
        {
            ColorCant = 0;
        }
        if (ColorCant < 0)
        {
            ColorCant = 4;
        }
        if (Input.GetButtonDown("Fire1") || Input.GetKeyDown(KeyCode.Space))
        {
            ColorCant += 1;
        }
        if (Input.GetButtonDown("Fire2") )
        {
            ColorCant -= 1;
        }
        switch (ColorCant)
        {
            case 1:
                Render.material = MatAzul;
                gameObject.tag = "Azul";
                break;
            case 2:
                Render.material = MatAmarillo;
                gameObject.tag = "Amarillo";
                break;
            case 3:
                Render.material = MatRojo;
                gameObject.tag = "Rojo";
                break;
            case 4:
                Render.material = MatVerde;
                gameObject.tag = "Verde";
                break;
            default:
                break;
        }
    }
}

    


