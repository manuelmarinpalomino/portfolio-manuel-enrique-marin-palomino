﻿using UnityEngine;

public class MiniJuego2_ControlJugador : Bases_Controlador
{
    public float velocidadMovimiento = 25f;

    void Update()
    {
        MovimientoJugador();
        puntos();
    }


    protected override void puntos()
    {
        MayorPuntuacion.text = "Mayor Puntaje " + Data.data.MayorRecord2.ToString();
        Puntos.text = "Puntos: " + contador.ToString();
        Data.data.Record = contador;
    
        if (Data.data.Record >= Data.data.MayorRecord2)
        {
         
            Data.data.MayorRecord2 = Data.data.Record;
            MayorPuntuacion.text = "Mayor Puntaje " + Data.data.MayorRecord2.ToString();
           
            Data.GuardarDataPersistencia();


        }
    }
    public void MovimientoJugador()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal") * velocidadMovimiento;

        movimientoHorizontal *= Time.deltaTime;

        transform.Translate(-movimientoHorizontal, 0, 0);
        
    }
    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        
    }
}
