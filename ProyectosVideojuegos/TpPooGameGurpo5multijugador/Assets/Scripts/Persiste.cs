﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Persiste : MonoBehaviour
{
    public static Persiste instancia;
    public DataPersistencia data;

    string archivoDatos = "save2.dat";

    private void Awake()
    {
        if (instancia == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instancia = this;
        }
        else if (instancia != this)
            Destroy(this.gameObject);

        CargarDataPersistencia();
        data.OrdenarMayorRecordJuego1();
        data.OrdenarMayorRecordJuego2();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G)) GuardarDataPersistencia();
    }

    public void GuardarDataPersistencia()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();

    }

    public void CargarDataPersistencia()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            DataPersistencia cargado = (DataPersistencia)bf.Deserialize(file);
            data = cargado;
            file.Close();
            Debug.Log("Datos cargados");
        }
    }
}
[System.Serializable]
public class DataPersistencia:IEnumerable<float>
{
    public float Record;
    public float MayorRecord;
    public float Record2;
    public float MayorRecord2;
    public List<float> MayorRecordjuego1;
    public List<float> MayorRecordjuego2;

    public DataPersistencia()
    {
        Record = 0;
        MayorRecord = 0;
        Record2 = 0;
        MayorRecord2 = 0;
    }

    public void AgregarRecordJuego1(float record)
    {
        MayorRecordjuego1.Add(record);
    }
    public void AgregarRecordJuego2(float record)
    {
        MayorRecordjuego2.Add(record);
    }

    public IEnumerator<float> GetEnumerator()
    {
        return ((IEnumerable<float>)MayorRecordjuego1).GetEnumerator();
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable)MayorRecordjuego1).GetEnumerator();
    }

   
    public void EliminarUltimoElementoJuego1()
    {
        MayorRecordjuego1.Remove(MayorRecordjuego1[MayorRecordjuego1.Count - 1]);
    }
    public void EliminarUltimoElementoJuego2()
    {
        MayorRecordjuego2.Remove(MayorRecordjuego2[MayorRecordjuego2.Count - 1]);
    }

    public void OrdenarMayorRecordJuego1()
    {
        CompararLista compararLista = new CompararLista();
        MayorRecordjuego1.Sort(compararLista);

    }
    public void OrdenarMayorRecordJuego2()
    {
        CompararLista compararLista = new CompararLista();
        MayorRecordjuego2.Sort(compararLista);

    }
}

public class CompararLista : IComparer<float>
{
    public int Compare(float x, float y)
    {
        if (x > y)
        {
            return -1;
        }
        else if (x < y)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

