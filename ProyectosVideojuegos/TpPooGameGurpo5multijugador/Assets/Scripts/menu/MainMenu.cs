﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class MainMenu : MonoBehaviour
{

    public TextMeshProUGUI Text1;
    public TextMeshProUGUI Text2;

    public Persiste Data;
    private void Awake()
    {
        Data.CargarDataPersistencia();
        int contador = 0;
        Data.data.OrdenarMayorRecordJuego1();
        Data.data.OrdenarMayorRecordJuego2();
        foreach (var item in Data.data)
        {
            contador++;
            Text1.text += "\n record " + contador + ": " + item;  
        }
        contador = 0;
        foreach (var item in Data.data.MayorRecordjuego2)
        {
            contador++;
            Text2.text += "\n record " + contador + ": " + item;
        }
    }
    private void Start()
    {
    }
    public void ComenzarJuego1()
    {
      
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void ComenzarJuego2()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
    }

    public void CerrarJuego()
    {
        Application.Quit();
    }
}
