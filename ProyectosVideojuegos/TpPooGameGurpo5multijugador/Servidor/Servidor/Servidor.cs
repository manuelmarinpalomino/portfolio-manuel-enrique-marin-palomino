﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
namespace Servidor
{
    class Servidor
    {
        private TcpListener Server;
        private TcpClient Cliente = new TcpClient();
        private IPEndPoint End = new IPEndPoint(IPAddress.Any, 8000);
        private List<connection> list = new List<connection>();
        connection con;

        private struct connection
        {
            public NetworkStream stream;
            public StreamWriter streamW;
            public StreamReader streamR;
            public string Nick;

        }
        public Servidor()
        {
            Inicio();

        }
        public void Inicio()
        {


            Console.WriteLine("Servidor iniciado Lol AA");
            Server = new TcpListener(End);
            Server.Start();
            while (true)
            {
                Cliente = Server.AcceptTcpClient();
                con = new connection();
                con.stream = Cliente.GetStream();
                con.streamR = new StreamReader(con.stream);
                con.streamW = new StreamWriter(con.stream);

                con.Nick = con.streamR.ReadLine();
                list.Add(con);
                Console.WriteLine(con.Nick + " Se ha conectado.");

                Thread T = new Thread(EscucharConnection);
                T.Start();
            }
        }
        public void EscucharConnection()
        {
            connection hcon = con;
            do
            {
                try
                {
                    string tmp = hcon.streamR.ReadLine();
                    MandarInformacion(Cliente, tmp);
                    Console.WriteLine(hcon.Nick + " : " + tmp);
                    foreach (connection c in list)
                    {
                        try
                        {
                            c.streamW.WriteLine(hcon.Nick + " : " + tmp);
                            c.streamW.Flush();
                        }
                        catch
                        {


                        }
                    }
                }
                catch
                {

                    list.Remove(hcon);
                    Console.WriteLine(con.Nick + "Se desconecto.");
                }
            } while (true);
        }
        public void MandarInformacion(TcpClient con, string info)
        {
            NetworkStream netStream = con.GetStream();
            StreamWriter Escribir = new StreamWriter(netStream);
            Escribir.WriteLine(info);
            Escribir.Flush();
        }

    }

}
}
