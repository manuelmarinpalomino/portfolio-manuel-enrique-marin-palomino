﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;


namespace Servidor
{

    public class DAO
    {
        public SqlConnection sqlConnection = new SqlConnection(@"Data Source=MANUELMARIN\MANUELSQLSERVER;Initial Catalog=TpProgramacion;Integrated Security=True");

        public int ejecutar(string pSQL)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(pSQL, sqlConnection);
                sqlConnection.Open();
                return sqlCommand.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
        public DataSet Obtener(string pSQL)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(pSQL, sqlConnection);
            DataSet dataSet = new DataSet();

            try
            {
                sqlDataAdapter.Fill(dataSet);
                return dataSet;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public int ObtenerIdCliente(string nombre)
        {
            string mSQL = "SELECT id FROM Clientes where Nombre= '" + nombre +"'";
            DataSet mDs = Obtener(mSQL);

            return int.Parse(mDs.Tables[0].Rows[0][0].ToString());
        }
    }

}
