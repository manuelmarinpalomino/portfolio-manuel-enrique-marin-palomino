﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Data;
namespace Servidor
{
    public class Servidor
    {
        private TcpListener Server;
        private TcpClient Cliente = new TcpClient();
        private IPEndPoint End = new IPEndPoint(IPAddress.Any, 8000);
        private List<connection> list = new List<connection>();
        connection con;
        public DAO dAO = new DAO();
        private struct connection
        {
            public NetworkStream stream;
            public StreamWriter streamW;
            public StreamReader streamR;
            public string Nick;

        }
        public Servidor()
        {
            Inicio();

        }
        public void Inicio()
        {


            Console.WriteLine("Servidor iniciado");
            Server = new TcpListener(End);
            Server.Start();
            while (true)
            {
                Cliente = Server.AcceptTcpClient();
                con = new connection();
                con.stream = Cliente.GetStream();
                con.streamR = new StreamReader(con.stream);
                con.streamW = new StreamWriter(con.stream);

                con.Nick = con.streamR.ReadLine();
                list.Add(con);
                Console.WriteLine(con.Nick + "Se ha conectado.");

                Thread T = new Thread(EscucharConnection);
                T.Start();
                int id = dAO.ObtenerIdCliente(con.Nick);
                dAO.ejecutar("insert into HistorialConexión (IdCliente,HorarioConexion) values (" + id + ",GETDATE())");

            }
        }
        public void EscucharConnection()
        {
            connection hcon = con;
            do
            {
                try
                {
                    string tmp = hcon.streamR.ReadLine(); if (tmp == "Necesito las ultimas 10 conexiones")
                    {
                        ultimas10conexiones(hcon);

                    }
                    else if (tmp == "Necesito las ultimas 10 partidas")
                    {
                        ultimas10partidas(hcon);
                    }
                    else if (tmp == "Gane")
                    {
                        string nombreDelOtro = "";
                        foreach (connection c in list)
                        {
                            if (!con.Nick.Equals(c.Nick))
                            {
                                nombreDelOtro = c.Nick;
                            }
                        }
                        int idDelOtro = dAO.ObtenerIdCliente(nombreDelOtro);
                        int idPropio = dAO.ObtenerIdCliente(hcon.Nick);
                        dAO.ejecutar("insert into HistorialPartidas(idCliente1,IdCliente2,QuienGano) values (" + idPropio + "," + idDelOtro + ",'" + hcon.Nick + "')");
                    }
                    else if (tmp == "perdio")

                    {
                        string nombreDelOtro = "";
                        foreach (connection c in list)
                        {
                            if (!con.Nick.Equals(c.Nick))
                            {
                                nombreDelOtro = c.Nick;
                            }
                        }
                        int idDelOtro = dAO.ObtenerIdCliente(nombreDelOtro);
                        int idPropio = dAO.ObtenerIdCliente(hcon.Nick);
                        dAO.ejecutar("insert into HistorialPartidas(idCliente1,IdCliente2,QuienGano) values (" + idPropio + "," + idDelOtro + ",'" + nombreDelOtro + "')");
                    }
                    string info = hcon.Nick + " :" + tmp;
                    MandarInformacion(Cliente, info);
                    Console.WriteLine(hcon.Nick + " :" + tmp);
                    foreach (connection c in list)
                    {
                            c.streamW.WriteLine(hcon.Nick + ":" + tmp);
                            c.streamW.Flush();
                    }
                }
                catch
                {

                    list.Remove(hcon);
                    Console.WriteLine(con.Nick + "Se desconecto.");
                }
            } while (true);
        }

        private void ultimas10partidas(connection hcon)
        {
            string info = "";
            int id = dAO.ObtenerIdCliente(hcon.Nick);
            DataSet dSet = dAO.Obtener("Select c.QuienGano from HistorialPartidas as c Join cliente as cl on c.IdCliente1 where c.IdCliente1 =" + id + "order by c.Id Desc ");

            for (int i = 0; i < 10; i++)
            {
                info += ";" + dSet.Tables[0].Rows[i].ToString();
            }

            foreach (connection c in list)
            {
                if (hcon.Nick.Equals(c.Nick))
                {
                    c.streamW.Write(hcon.Nick + ";" + info);
                    c.streamW.Flush();
                }

            }
            hcon.streamW.Flush();
        }

        private void ultimas10conexiones(connection hcon)
        {
            string info = "";
            int id = dAO.ObtenerIdCliente(hcon.Nick);
            DataSet dSet = dAO.Obtener("Select c.HorarioConexión from HistorialConexión as c where c.IdCliente =" + id + "order by c.Id Desc");

            for (int i = 0; i < 10; i++)
            {
                info += ";" + dSet.Tables[0].Rows[i].ToString();
            }

            foreach (connection c in list)
            {
                if (hcon.Nick.Equals(c.Nick))
                {
                    c.streamW.Write(hcon.Nick + ";" + info);
                    c.streamW.Flush();
                }
            }

            hcon.streamW.Flush();
        }
        public void MandarInformacion(TcpClient con, string info)
        {
          
            NetworkStream netStream  =con.GetStream();
            StreamWriter Escribir = new StreamWriter(netStream);
            Escribir.WriteLine(info);
            Escribir.Flush();
        }

    }


}
	

