﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Bullet : MonoBehaviour
{
    
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>(); //Get the rigidbody component from the bullet
    }

    void Update()
    {
        rb.AddForce(new Vector3(Controller_Enemy.enemyVelocity, 0, 0), ForceMode.Force);  // add a force to the bullet so that it moves on the x axis
        TimeOfLife();
    }

    public void TimeOfLife() //Method that destroys the object when it reaches a given position
    {
        Destroy(this.gameObject, 3);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("SpecialEnemy")) // if it collides with specialEnemy both are destroyed
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
    }

}
