﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>(); //Get the rigidbody component from the enemy
    }

    void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force);  // add a force to the enemy so that it moves on the x axis
        OutOfBounds();
    }

    public void OutOfBounds() //Method that destroys the object when it reaches a given position
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }
}
