﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{

  
    public static float maxDistance = 25;
    public Text distanceText;
    public Text gameOverText;
    public Text recordDistanceText;
    public static float distance = 0;
    public Image pogressBar;
    

    void Start()
    {
        
        distance = 0;
        distanceText.text = distance.ToString("0.00");
        gameOverText.gameObject.SetActive(false);
        recordDistanceText.text = PersistenceManager.instance.data.recordDistance.ToString("0.00");// I get the record distance of the data persistence
    }

    void Update()
    {
        PogressBar();
        GameOver();
    }
    private void GameOver()
    {
        if (GameManager.gameOver) // When GameOver is true what it does is stop the time and show the totaly distance on the screen and I see if I beat the record .
        {
            Time.timeScale = 0;
           
            gameOverText.gameObject.SetActive(true);
            if(PersistenceManager.instance.data.recordDistance < distance)//if the player breaks his record it is saved in the file save.dat and tells him that he broke the record
            {
                PersistenceManager.instance.data.recordDistance = distance;
                gameOverText.text = "Game Over \n Total Distance: " + distance.ToString("0.00") + "\n you have exceeded the record¡ ";
                PersistenceManager.instance.SavePersistenceData();
            }
            else if (PersistenceManager.instance.data.recordDistance > distance) //otherwise it warns you that you did not break the record
            {
                gameOverText.text = "Game Over \n Total Distance: " + distance.ToString("0.00") + "\n you have not exceeded the record¡ ";
            }
           
           
            
        }
        else // if gameOver is false, the else is executed that causes the distance variable to be increased and displayed on the screen.
        {
            gameOverText.gameObject.SetActive(false);
           
            distance += Time.deltaTime;
            distanceText.text = distance.ToString("0.00");

            
        }
    }
    private void PogressBar()// Method for the progress bar to decrease over time
    {
        
        maxDistance = Mathf.Clamp(maxDistance-Time.deltaTime, 0, 25); // Mathf.Clamp makes a float always be between certain values, with this I make sure that it does not increase too much with the nitro
        pogressBar.fillAmount = maxDistance / 25;
        if(maxDistance == 0) // if the maxDistance variable reaches 0 the player loses
        {
            GameManager.gameOver = true;
        }
    }
}
