﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public GameObject instantiatePos;
    public float respawningTimer;
    private float time = 0;

    void Start()
    {
        Controller_Enemy.enemyVelocity = 2;
    }

    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
    }

    private void ChangeVelocity() // change the enemy's speed
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f); //speed up as time goes by
    }

    private void SpawnEnemies() //Spawn enemies once in a while in a certain time range
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            int instantiatedEnemy = UnityEngine.Random.Range(0, enemies.Count);
            Instantiate(enemies[ instantiatedEnemy], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
            
        }
    }
}
