﻿using UnityEngine;
using UnityEngine.InputSystem;
public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    public GameObject bullet;
    private float initialSize;
    private int i = 0;
    private bool floored;
    public PlayerInput playerInput;

    private void Start()
    {
        rb = GetComponent<Rigidbody>(); //gets the player's rigidbody component
        initialSize = rb.transform.localScale.y;//saves the y-axis scale of the player object
    }

   
    public  void Shoot()// method to make the player shoot 
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(bullet, this.transform.position, this.transform.rotation);
        }
        
    }

    public  void Jump() // method to make the player jump
    {
        if (floored) // ask if it's on the floor
        {
           
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse); //add a force to player so they can jump
            
        }
    }

    public void Duck() //Method for the player to crouch
    {
        if (floored) // ask if it's on the floor
        {
            if (Input.GetKey(KeyCode.S)) //if you hold down the s the player remains crouched
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z); //scale the player to appear to be crouching if on the floor
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z); // When the player stops pressing the s and is on the floor, it returns to its original height
                    i = 0;
                }
            }
        }
        else // if it is not on the ground, the else is executed
        {
            
                rb.AddForce(new Vector3(0, -jumpForce*2, 0), ForceMode.Impulse); //if the player presses the s and is jumping, it causes a force to be added to make it go down faster
            
        }
    }

  
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Floor")) //When it comes out of colliding with the floor change the variable floored to false
        {
            floored = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("SpecialEnemy")) // When it collides with the enemy it destroys the player and tells the GameOver variable of the controller_Hud to be true.
        {
            this.gameObject.SetActive(false);
            Destroy(other.gameObject);
            GameManager.gameOver = true;
        }
        if (other.gameObject.CompareTag("Floor")) //When it collides with the ground it changes the floored variable to true
        {
            floored = true;
        }

        if (other.gameObject.CompareTag("Nitro")) // when it collides with the nitro increase the max distance variable to increase the progress bar
        {
            Controller_Hud.maxDistance += 10;
            Destroy(other.gameObject);

        }
    }
}
