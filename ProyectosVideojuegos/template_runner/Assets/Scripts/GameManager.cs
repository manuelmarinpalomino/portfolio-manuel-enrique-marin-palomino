﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager  : MonoBehaviour
{
    public GameObject player;
    public static bool gameOver = false;
   

    
    public void Restart() //reset the game, eliminate the enemies if they are instantiated and return to the initial values ​​to play again
    {

       
            if (GameObject.FindWithTag("Enemy") != null)
            {
                Destroy(GameObject.FindWithTag("Enemy"));
            }
            if (GameObject.FindWithTag("SpecialEnemy") != null)
            {
                Destroy(GameObject.FindWithTag("SpecialEnemy"));
            }
            if (GameObject.FindWithTag("Bullet") != null)
            {
                Destroy(GameObject.FindWithTag("Bullet"));
            }
            if (GameObject.FindWithTag("Bullet") != null)
            {
                 Destroy(GameObject.FindWithTag("Bullet"));
            }
            if (GameObject.FindWithTag("Bullet") != null)
             {
                Destroy(GameObject.FindWithTag("Bullet"));
             }
        if (GameObject.FindWithTag("Nitro") != null)
            {
                Destroy(GameObject.FindWithTag("Nitro"));
            }
            gameOver = false;
            Controller_Hud.distance = 0;
            player.transform.position = new Vector3(player.transform.position.x, 0.1f, player.transform.position.z);
            player.SetActive(true);
            
            Time.timeScale = 1;
            //SceneManager.LoadScene(0);
        }
    }

 