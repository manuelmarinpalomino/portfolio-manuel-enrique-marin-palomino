﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;

    void Start()
    {
        //startPos = transform.position.x;
        //length = GetComponent<SpriteRenderer>().bounds.size.x; //doubt
    }

    void Update()
    {
        ParallaxEffect();
    }
    private void ParallaxEffect()
    {
        if(GameManager.gameOver == false)
        {
            transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z); //Makes the sprite move
        }
        
        if (transform.localPosition.x < -20)
        {
            transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z); //Reset the sprite position on the x axis = 20
        }
    }
}
