﻿
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class PersistenceManager: MonoBehaviour
{
    public static PersistenceManager instance;
    public DataPersistence data;
    string dataFile = "save.dat";

    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instance = this;
        }
        else if (instance != this)
            Destroy(this.gameObject);

        LoadDataPersistence();
    }

 

    public void SavePersistenceData()
    {
        string filePath = Application.persistentDataPath + "/" + dataFile;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Datos guardados");
    }

    public void LoadDataPersistence()
    {
        string filePath = Application.persistentDataPath + "/" + dataFile;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            DataPersistence cargado = (DataPersistence)bf.Deserialize(file);
            data = cargado;
            file.Close();
            Debug.Log("Datos cargados");
        }
    }
    [System.Serializable]
    public class DataPersistence
    {
        public float recordDistance;

        public DataPersistence()
        {
            recordDistance = 0.0f;
            
        }
    }

}
