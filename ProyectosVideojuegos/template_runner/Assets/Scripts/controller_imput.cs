﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class controller_imput : MonoBehaviour
{
    //Script that deals with the game's input, creates the methods for the system's input
    public PlayerInput playerInput;
    public Controller_Player Jugador;
    public GameManager GameManager;

    public void OnJump(InputAction.CallbackContext value)
    {
        Jugador.Jump();
    }
    public void OnDuck(InputAction.CallbackContext value)
    {
        Jugador.Duck();
    }
  public void OnShoot(InputAction.CallbackContext value)
    {
        Jugador.Shoot();
    }
    public void OnRestart(InputAction.CallbackContext value)
    {
        GameManager.Restart();
    }

}
