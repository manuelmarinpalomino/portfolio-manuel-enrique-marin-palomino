﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camera : MonoBehaviour
{
    public GameObject player;

    void Update()
    {
        this.transform.position= new Vector3(player.transform.position.x, this.transform.position.y, player.transform.position.z); //the camera follows the player
    }
}
