﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camera2 : MonoBehaviour
{
    public List<Transform> targets;

    private Vector3 velocity = Vector3.one*2;
    public float smoothTime = 0.5f;
    public float maxZoom;
    public float minZoom;
    public Camera cam;
    public GameObject player1;
    public GameObject player2;
    private void LateUpdate()
    {
        if(GameObject.Find("Player")!= null && GameObject.Find("Player2") != null)
        {
            if (targets.Count == 0)
            {
                return;
            }
            Vector3 centerPoint = getCenterPoint();

            transform.position = new Vector3(centerPoint.x, this.transform.position.y, centerPoint.z);
            Zoom();
        }
        else if(GameObject.Find("Player") != null && GameObject.Find("Player2") == null)
        {
            transform.position = new Vector3(player1.transform.position.x, this.transform.position.y, player1.transform.position.z);
            cam.orthographicSize = 9.0f;
        }
        else if(GameObject.Find("Player") == null && GameObject.Find("Player2") != null)
        {
            transform.position = new Vector3(player2.transform.position.x,this.transform.position.y,player2.transform.position.z);
            cam.orthographicSize = 9.0f;
        }
    }
        

    private void Zoom() //Calculate the camera zoom so that both players can see each other
    {
        Vector3 v3 = getDistance();
        float newZoom;
        if (v3.x > v3.z)
        {
            newZoom = Mathf.Lerp(minZoom, maxZoom, v3.x/20);
        }
        else
        {
            newZoom = Mathf.Lerp(minZoom, maxZoom, v3.z/20);
        }
        cam.orthographicSize = newZoom;
    }


    private Vector3 getDistance() //get the distance between the two players
    {
        if (targets.Count == 1)
        {
            return targets[0].position;
        }
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }
        return bounds.size;
    }
    private Vector3 getCenterPoint() //Calculate the midpoint of the distance between player1 and player2
    {
        if(targets.Count == 1)
        {
            return targets[0].position;
        }
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for(int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }
        return bounds.center;
    }
}
