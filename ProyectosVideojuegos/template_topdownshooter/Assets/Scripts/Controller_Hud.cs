﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Controller_Hud : MonoBehaviour
{
    public static int gameOver;
    public static int points;
    private Ammo ammo;
    public Text gameOverText;
    public Text pointsText;
    public Text powerUpText;
    private void Awake()
    {
        if(GameObject.Find("Player2") == null) // for game over to work on both one player and two players. if it does not find player 2 then it is set gameOver = 1 otherwise it is = 0
        {
            gameOver = 1;
        }
        else
        {
            gameOver = 0;
        }
    }
    void Start()
    {
        Restart._Restart.OnRestart += Reset;
        gameOverText.gameObject.SetActive(false);
        points = 0;
    }

    private void Reset()
    {
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            gameOver = 1;
        }
        else
        {
            gameOver = 0;
        }
        gameOverText.gameObject.SetActive(false);
        points = 0;
    }

    void Update()
    {
        if (gameOver==2)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over";
            gameOverText.gameObject.SetActive(true);
        }

        switch (Controller_Shooting.ammo)
        {
            case Ammo.Normal:
                powerUpText.text = "Gun: Normal - Ammo:∞";
                break;
            case Ammo.Shotgun:
                powerUpText.text = "Gun: Shotgun - Ammo:" + Controller_Shooting.ammunition.ToString();
                break;
            case Ammo.Cannon:
                powerUpText.text = "Gun: Cannon - Ammo:" + Controller_Shooting.ammunition.ToString();
                break;
            case Ammo.Bumeran:
                powerUpText.text = "Gun: Bumeran - Ammo:" + Controller_Shooting.ammunition.ToString();
                break;
        }

        pointsText.text = "Score: " + points.ToString();
    }

    private void OnDisable()
    {
        Restart._Restart.OnRestart -= Reset;
    }
}
