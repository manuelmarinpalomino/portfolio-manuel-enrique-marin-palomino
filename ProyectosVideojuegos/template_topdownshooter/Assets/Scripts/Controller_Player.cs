﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{

    private Rigidbody rb;
    private Renderer render;
    public static Controller_Player _Player;
    private Vector3 movement;
    private Vector3 movement1;
    private Vector3 mousePos;
    internal Vector3 shootAngle;
    private Vector3 startPos;
    private bool started = false;
    public float speed = 5;

    private void Start()
    {
        if (_Player == null)
        {
            _Player = this.gameObject.GetComponent<Controller_Player>();
        }
        startPos = this.transform.position;
        rb = GetComponent<Rigidbody>();
        render = GetComponent<Renderer>();
        Restart._Restart.OnRestart += Reset;//adds an item to the restart event queue
        started = true;
        Controller_Shooting._ShootingPlayer.OnShooting += Shoot;
    }

    private void OnEnable()
    {
        if (started)
            Restart._Restart.OnRestart += Reset;//adds an item to the restart event queue when initialized
    }

    private void Reset() // when the reset event occurs, this method is called to reset initial parameters
    {
        this.transform.position = startPos;
    }

    private void Update()
    {
        if (this.gameObject.CompareTag("Player"))
        {
            movement.x = Input.GetAxis("Horizontal");
            movement.z = Input.GetAxis("Vertical");
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        if (this.gameObject.CompareTag("Player2"))
        {
            movement.x = Input.GetAxis("Horizontal2");
            movement.z = Input.GetAxis("Vertical2");
            if (Input.GetKey(KeyCode.J))
            {
                movement1.x = -1;
            }
            if (Input.GetKey(KeyCode.L))
            {
                movement1.x = 1;
            }
            if (Input.GetKey(KeyCode.K))
            {
                movement1.z = -1;
            }
            if (Input.GetKey(KeyCode.I))
            {
                movement1.z = 1;
            }
        }
       
    }

    public virtual void FixedUpdate()
    {
        Movement();
      
    }

    private void Movement() //method that makes the player move and rotate according to where the mouse is pointing
    {
        rb.MovePosition(rb.position + movement * speed * Time.fixedDeltaTime);
        if(this.gameObject.CompareTag("Player"))
            transform.LookAt(new Vector3(mousePos.x, 1, mousePos.z));
        if (this.gameObject.CompareTag("Player2"))
            transform.LookAt(new Vector3(movement1.x,1,movement1.z)); 
    }
   
    public Vector3 GetLastAngle() //get the player's last angle
    {
        if (Input.GetKey(KeyCode.W)|| Input.GetKey(KeyCode.I))
        {
            shootAngle = Vector3.forward;
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.J))
        {
            shootAngle = Vector3.left;
        }
        if (Input.GetKey(KeyCode.S) ||Input.GetKey(KeyCode.K))
        {
            shootAngle = Vector3.back;
        }
        if (Input.GetKey(KeyCode.D)|| Input.GetKey(KeyCode.L))
        {
            shootAngle = Vector3.right;
        }
        return shootAngle;
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("EnemyProjectile") || collision.gameObject.CompareTag("EnemyBoss")) //if it collides with an enemy, boss or enemyPreyectile, the player disappears and also changes the GameObject variable that is used for when the player loses
        {
            gameObject.SetActive(false);
            Controller_Hud.gameOver++;
        }
        if (collision.gameObject.CompareTag("PowerUp")) //if it collides with the powerup it randomly assigns the player the special weapon
        {
            if (this.gameObject.CompareTag("Player")){
                int rnd = UnityEngine.Random.Range(1, 3);
                if (rnd == 1)
                {
                    Controller_Shooting.ammo = Ammo.Shotgun;
                    Controller_Shooting.ammunition = 5;
                }
                else if (rnd == 2)
                {
                    Controller_Shooting.ammo = Ammo.Cannon;
                    Controller_Shooting.ammunition = 5;
                }
                else
                {
                    Controller_Shooting.ammo = Ammo.Bumeran;
                    Controller_Shooting.ammunition = 1;
                }
            }
            else
            {
                int rnd = UnityEngine.Random.Range(1, 3);
                if (rnd == 1)
                {
                    Controller_Shooting2.ammo = Ammo1.Shotgun;
                    Controller_Shooting2.ammunition = 5;
                }
                else if (rnd == 2)
                {
                    Controller_Shooting2.ammo = Ammo1.Cannon;
                    Controller_Shooting2.ammunition = 5;
                }
                else
                {
                    Controller_Shooting2.ammo = Ammo1.Bumeran;
                    Controller_Shooting2.ammunition = 1;
                }
            }
                Destroy(collision.gameObject);
            
        }

        if (collision.gameObject.CompareTag("Bumeran")) //if the boomerang returns to the player it is reassigned again
        {
            if (this.gameObject.CompareTag("Player"))
            {
                Controller_Shooting.ammo = Ammo.Bumeran;
                Controller_Shooting.ammunition = 1;
                Destroy(collision.gameObject);
            }
        }
        if (collision.gameObject.CompareTag("Bumeran2")) //if the boomerang returns to the player it is reassigned again
        {
            if (this.gameObject.CompareTag("Player2"))
            {
                Controller_Shooting2.ammo = Ammo1.Bumeran;
                Controller_Shooting2.ammunition = 1;
                Destroy(collision.gameObject);
            }
        }
    }

    void OnDisable() //when the object is disable it is removed from the restart queue and shooting queue
    {
        Controller_Shooting._ShootingPlayer.OnShooting -= Shoot;
        Restart._Restart.OnRestart -= Reset;
    }

    private void Shoot()
    {
        if (Controller_Shooting.ammo == Ammo.Cannon)
        {
            rb.AddForce(this.transform.forward * -4f, ForceMode.Impulse);
        }
    }
}
