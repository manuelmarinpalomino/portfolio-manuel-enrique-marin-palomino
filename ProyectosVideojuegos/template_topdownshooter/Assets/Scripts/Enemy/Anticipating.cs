﻿using UnityEngine;
public class Anticipating : Controller_Enemy //Anticipating inherits the Controller_Enemy class
{
    public GameObject player1;
 
    private void Update()
    {
        if(SmokeGranade.playerIsInTheSmoke == false)//If the player is not inside the smoke grenade, the if is executed
        {
            agent.speed = enemySpeed;
            AnticipatingBehaviour();
        }
        else //if he is inside the smoke grenade, he tells him not to move
        {
            agent.speed = 0;
        }

    }

    private void AnticipatingBehaviour() //method that makes the mechanics that anticipate the movement of the enemy
    {
        if (player1 != null )
        {
           
            Controller_Player p1 = player1.GetComponent<Controller_Player>();
            agent.SetDestination(player1.transform.position + p1.GetLastAngle() * 2);
        }
        
    }
}
