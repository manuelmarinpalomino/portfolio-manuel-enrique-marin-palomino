﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Boss1 : MonoBehaviour
{
    private int life=100;
    public GameObject bullet;
    public GameObject boss1;
    private float time;
    private float shooting;
    public Transform firePoint;
    public Transform firePoint2;
    public Transform firePoint3;
    public Transform firePoint4;

    private void Start()
    {
        life = 100;
        shooting = 0;

    }
    
    private void OnCollisionEnter(Collision collision) //if it collides with some of these munitions life lowers
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
            life -= 10;
            if(life <= 0)
            {
                Controller_Hud.points += 10;
            }
            
        }
        if (collision.gameObject.CompareTag("CannonBall"))
        {
            life -= 10;
            if (life <= 0)
            {
                Controller_Hud.points += 10;
            }
        }
        if (collision.gameObject.CompareTag("Bumeran"))
        {
            life -= 10;
            if (life <= 0)
            {
                Controller_Hud.points += 10;
            }
        }
    }
  
    private void Update()
    {
        IsAlive();
        shoot();
    }
    private void IsAlive() //method that checks if the boss is still alive
    {
        if (life <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    public void shoot() //method to generate the boss shots
    {
        if (shooting == 0)
        {
            shooting++;
            StartCoroutine(chronometer(30.0f));
        }
        
    }
    public IEnumerator chronometer(float chronometerValue) //coroutine to instantiate the boss's shots from time to time
    {
        time = chronometerValue;
        while (time > 0)
        {
            GameObject bullet1 = Instantiate(bullet, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet1.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * 10, ForceMode.Impulse);
            GameObject bullet2 = Instantiate(bullet, firePoint2.position, firePoint2.rotation);
            Rigidbody rb2 = bullet2.GetComponent<Rigidbody>();
            rb2.AddForce(firePoint2.forward * 10, ForceMode.Impulse);
            GameObject bullet3 = Instantiate(bullet, firePoint3.position, firePoint3.rotation);
            Rigidbody rb3 = bullet3.GetComponent<Rigidbody>();
            rb3.AddForce(firePoint3.forward * 10, ForceMode.Impulse);
            GameObject bullet4 = Instantiate(bullet, firePoint4.position, firePoint4.rotation);
            Rigidbody rb4 = bullet4.GetComponent<Rigidbody>();
            rb4.AddForce(firePoint.forward * 10, ForceMode.Impulse);
            boss1.transform.Rotate(0, transform.rotation.y + 20, 0);

            yield return new WaitForSeconds(0.3f);

            time--;
        }
        if (time <= 0)
        {
            shooting = 0;

        }
    }
 
}
