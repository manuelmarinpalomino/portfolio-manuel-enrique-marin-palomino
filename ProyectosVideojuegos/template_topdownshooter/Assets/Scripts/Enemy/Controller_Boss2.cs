﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Controller_Bot2 : MonoBehaviour
{
    public NavMeshAgent agent;
    public Transform transform;
    public Transform transform2;
    public Transform transform3;
    public Transform transform4;
    public Transform transform5;
    public GameObject enemy;
    private float time;
    private int life;

    private void Start()
    {
        time = 0;
        transform.position = new Vector3(0, 2, 0);
        life = 100;
        Restart._Restart.OnRestart += Reset;
    }
    public void Reset() // when the reset event occurs, this method is called to destroy this gameobject
    {
        Destroy(this.gameObject);
    }

    void Update()
    {
        IsAlive();
        if(time ==0 )
        {
            StartCoroutine(chronometer(5.0f));

        }
       
    }
    private void IsAlive() //check if the boss is alive
    {
        if (life <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    public IEnumerator chronometer(float chronometerValue) // coroutine to execute the boss mechanics (go to a point and instantiate an object)
    {
        time = chronometerValue;
        while (time > 0)
        {
            yield return new WaitForSeconds(1.0f);
            time--;
        }
        if (time == 0)
        {
            int r = Random.Range(0,3);
            if (r == 0)
            {
                agent.SetDestination(transform.position);
                Instantiate(enemy,transform5);
                
            }
            else if (r == 1)
            {
                agent.SetDestination(transform2.position);
                Instantiate(enemy, transform5);
            }
            else if (r == 2)
            {
                agent.SetDestination(transform3.position);
                Instantiate(enemy, transform5);
            }
           else if (r == 3)
            {
                agent.SetDestination(transform4.position);
                Instantiate(enemy, transform5);
            }

        }
    }
    private void OnCollisionEnter(Collision collision) // detect collisions of gun shots
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
            life -= 10;
            if (life <= 0)
            {
                Controller_Hud.points += 10;
            }

        }
        if (collision.gameObject.CompareTag("CannonBall"))
        {
            life -= 10;
            if (life <= 0)
            {
                Controller_Hud.points += 10;
            }
        }
        if (collision.gameObject.CompareTag("Bumeran"))
        {
            life -= 10;
            if (life <= 0)
            {
                Controller_Hud.points += 10;
            }
        }
    }
    private void OnDisable() //when the object is destroyed it is removed from the restart queue
    {
        Restart._Restart.OnRestart -= Reset;
    }
}
