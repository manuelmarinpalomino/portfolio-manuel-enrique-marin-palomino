﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Boss3 : MonoBehaviour
{
    public GameObject player;
    public int salto;
    public float time;
    public int life;
    
    void Start()
    {
        salto = 0;
        life = 100;
    }
    private void Reset() // when the reset event occurs, this method is called to reset initial parameters
    {
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        IsAlive();
        if (salto == 0)
        {
            salto++;
            StartCoroutine(chronometer(30.0f));
            
        }
    }
    private void IsAlive() //method that checks if the boss is still alive
    {
        if (life <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    public IEnumerator chronometer(float chronometerValue) //coroutine that makes the boss rise after a while on the floor and then teleport
    {
        time = chronometerValue;
        while(time > 0)
        {
            if(time <= 10 && time>1)
            {
              
                this.transform.position = new Vector3(this.transform.position.x, 5.0f, this.transform.position.z);
            
            }
            else if(time==1)
            {
                this.transform.position = new Vector3(Random.Range(-26f,26f),1.5f, Random.Range(-26f, 26f));
            }
            yield return new WaitForSeconds(0.3f);
            time--;


        }
        if (time == 0)
        {
            salto = 0;
            
        }
    }
    private void OnCollisionEnter(Collision collision) //if it collides with some of these munitions life lowers
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
            life -= 10;
            if (life <= 0)
            {
                Controller_Hud.points += 10;
            }

        }
        if (collision.gameObject.CompareTag("CannonBall"))
        {
            life -= 10;
            if (life <= 0)
            {
                Controller_Hud.points += 10;
            }
        }
        if (collision.gameObject.CompareTag("Bumeran"))
        {
            life -= 10;
            if (life <= 0)
            {
                Controller_Hud.points += 10;
            }
        }
    }
  
}
