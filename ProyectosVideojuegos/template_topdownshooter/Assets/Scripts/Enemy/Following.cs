﻿public class Following : Controller_Enemy //Following inherits the Controller_Enemy class
{
    private void Update()
    {
        if (player != null && SmokeGranade.playerIsInTheSmoke == false)//If the player is not inside the smoke grenade, the if is executed
        {
            FollowingBehaviour();
        }
        else//if he is inside the smoke grenade, he tells him not to move
        {
            agent.speed = 0;
        }
    }

    private void FollowingBehaviour() //method that makes the mechanics so that the object follows the player
    {
        agent.speed = enemySpeed;
        agent.SetDestination(player.transform.position);
    }
}
