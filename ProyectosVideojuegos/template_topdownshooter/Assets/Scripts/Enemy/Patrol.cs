﻿using UnityEngine;

public class Patrol : Controller_Enemy//Patrol inherits the Controller_Enemy class
{

    private void Update()
    {

        if (player != null && SmokeGranade.playerIsInTheSmoke == false)//If the player is not inside the smoke grenade, the if is executed
        {
            agent.speed = enemySpeed;
            Patroling();
        }
        else //if he is inside the smoke grenade, he tells him not to move
        {
            agent.speed = 0;
        }
        
    }

    private void Patroling()  //method that evaluates if the enemy is close to the player to chase him, if he is not, it calls the patrolBehaviour method
    {
           
        var heading = player.transform.position - this.transform.position;
        var distance = heading.magnitude;
        if (distance < patrolDistance)
        {
            agent.SetDestination(player.transform.position);
        }
        else
        {
            PatrolBehaviour();
        }
        
       
    }

    private void PatrolBehaviour() //method that makes the enemy move to a random position
    {
        agent.SetDestination(destination);
        destinationTime -= Time.deltaTime;
        if (destinationTime < 0)
        {
            destination = new Vector3(Random.Range(-10, 12), 1, Random.Range(-12, 9));
            destinationTime = 4;
        }
    }
}
