﻿using UnityEngine;

public class RandomEnemy : Controller_Enemy //RandomEnemy inherits the Controller_Enemy class
{
    private void Update()
    {
        if (SmokeGranade.playerIsInTheSmoke == false) //If the player is not inside the smoke grenade, the if is executed
        {
            agent.speed = enemySpeed;
            PatrolBehaviour();
        }
        else //if he is inside the smoke grenade, he tells him not to move
        {
            agent.speed = 0;
        }
    }

    private void PatrolBehaviour() //method that gives the mechanics that the enemy goes anywhere
    {
        
            agent.SetDestination(destination);
            destinationTime -= Time.deltaTime;
            if (destinationTime < 0)
            {
                destination = new Vector3(Random.Range(-10, 12), 1, Random.Range(-12, 9));
                destinationTime = 4;
            }
        
       
    }
}
