﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartBosses : MonoBehaviour
{
    private void Start() //adds an item to the restart event queue
    {
        Restart._Restart.OnRestart += Reset;
    }
    public void Reset()// when the reset event occurs, this method is called detroy this gameObject
    {
        Destroy(this.gameObject);
    }
    private void OnDestroy() //when the object is destroyed, remove the enemy from the enemies list and  removed from the restart queue
    {
        Instantiator.RestartBosses.Remove(this);
        Restart._Restart.OnRestart -= Reset;
    }
 
}
