﻿using System.Collections.Generic;
using UnityEngine;

public class Instantiator : MonoBehaviour
{
    public static List<Controller_Enemy> enemies;
    public static List<RestartBosses> RestartBosses;
    public List<GameObject> enemy;
    public List<GameObject> bosses;
    public List<GameObject> positions;
    public GameObject powerUp;
    private GameObject powerUpInstance;
    private float initialWaveDuration, initialAumentedWaveDuration, initialPowerUpTime;
    public int wave = 1;
    public int roundCounter;
    public float waveDuration = 5, aumentedWaveDuration = 3, powerUpTime = 10;
    public GameObject transform1;

    private void Start()
    {
        enemies = new List<Controller_Enemy>();
        RestartBosses = new List<RestartBosses>();
        initialWaveDuration = waveDuration;
        initialAumentedWaveDuration = aumentedWaveDuration;
        initialPowerUpTime = powerUpTime;
        Restart._Restart.OnRestart += Reset; //adds an item to the restart event queue
        SpawnEnemies();
    }

    public void Reset() //when the restart event happens, this method is called to restart the enemies
    {
        //Reset initial parameters
        waveDuration = initialWaveDuration;
        aumentedWaveDuration = initialAumentedWaveDuration;
        powerUpTime = initialPowerUpTime;
        wave = 1;
        //destroy objects in the scene
        if (powerUpInstance != null) 
            Destroy(powerUpInstance);
        foreach (Controller_Enemy c in enemies)
        {
            c.Reset();
            
        }
        foreach (RestartBosses c in RestartBosses)
        {
            c.Reset();
        }
        SpawnEnemies();
    }

    private void Update()
    {
        powerUpTime -= Time.deltaTime;
     
        if (powerUpTime < 0) // if the spawn time is 0 call the SpawnPowerUp method
        {
            SpawnPowerUp();
        }
        if(roundCounter%6 ==0 && roundCounter != 0) //if roundCounter is divisible by 6 and it is not the first round, it spawns a boss
        {
            SpawnBosses();
            roundCounter++;
        }
        else //if you don't spawn a boss, they spawn common enemies
        {
            waveDuration -= Time.deltaTime;
            if (waveDuration < 0)
            {
                SpawnEnemies();
                roundCounter++;
            }
        }
     
    }
    private void SpawnBosses() //Method that spawns bosses randomly
    {
        if (Controller_Hud.gameOver < 2)
        {
            transform1.transform.position = new Vector3(UnityEngine.Random.Range(-26.0f, 26.0f), 1.0f, UnityEngine.Random.Range(-26.0f, 26.0f));
            GameObject enemyInstance = Instantiate(bosses[UnityEngine.Random.Range(0, bosses.Count)],transform1.transform.position,Quaternion.identity) ;
            RestartBosses.Add(enemyInstance.GetComponent<RestartBosses>());
        }
    }

    private void SpawnEnemies() //method that spawns different common enemies
    {
        if (Controller_Hud.gameOver<2)
        {
            int enemiesCount = wave * 2;
            for (int i = 0; i < enemiesCount; i++)
            {
                int random = UnityEngine.Random.Range(0, positions.Count);
                GameObject enemyInstance = Instantiate(enemy[UnityEngine.Random.Range(0, enemy.Count)], positions[random].transform.position, Quaternion.identity);
                enemies.Add(enemyInstance.GetComponent<Controller_Enemy>());
            }
            aumentedWaveDuration += 0.3f;
            waveDuration = aumentedWaveDuration;
            wave++;
        }
    }

    private void SpawnPowerUp()  //method that spawns the powerup
    {
        Vector3 randomizer = new Vector3(UnityEngine.Random.Range(-7, 7), 1, UnityEngine.Random.Range(-7, 7));
        powerUpInstance = Instantiate(powerUp, randomizer, Quaternion.identity);
        Destroy(powerUpInstance, 10);
        powerUpTime = 20;
    }

    private void OnDisable() //when the object is destroyed it is removed from the restart queue
    {
        Restart._Restart.OnRestart -= Reset;
    }
}
