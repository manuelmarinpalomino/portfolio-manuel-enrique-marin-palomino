﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void GoToOnePlayerLevel()
    {
        SceneManager.LoadScene(1);
    }
    public void GoToTwoPlayerLevel()
    {
        SceneManager.LoadScene(2);
    }
}
