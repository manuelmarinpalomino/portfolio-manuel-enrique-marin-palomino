﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public static Restart _Restart;
    public delegate void Restarting();
    public event Restarting OnRestart;
    private GameObject gameObject1;
    private GameObject gameObject2;

    private void Awake()
    {
        gameObject1 = GameObject.Find("Player");
        gameObject2 = GameObject.Find("Player2");

        if (_Restart == null)
        {
            _Restart = this.gameObject.GetComponent<Restart>();
        }
        else
        {
            Destroy(this);
        }
    }

    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            OnRestarting();
        }
    }

    public void OnRestarting()
    {
        Time.timeScale = 0;
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            gameObject1.SetActive(true);
        }
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            gameObject1.SetActive(true);
            gameObject2.SetActive(true);
        }
        OnRestart();
        Time.timeScale = 1;
    }
}
