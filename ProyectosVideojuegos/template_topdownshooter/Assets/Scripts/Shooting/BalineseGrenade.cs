﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalineseGrenade : MonoBehaviour
{

    public GameObject bullet;
    public GameObject boss1;
    private float time;
    private float shooting;
    public Transform firePoint;
   

    private void Start()
    {
        Restart._Restart.OnRestart += Reset;//adds an item to the restart event queue
        shooting = 0;
    }
    public void Reset() //adds an item to the restart event queue when initialized
    {
        Destroy(this.gameObject);
    }
   

    private void Update()
    {
        Destroy(this.gameObject, 10.0f); //the object is destroyed after 10 seconds
        shoot();
    }
  
    public void shoot() 
    {
        if (shooting == 0) //if so that it only fires a single bullet per frame
        {
            shooting++;
            StartCoroutine(chronometer(10.0f));
        }

    }
    public IEnumerator chronometer(float chronometerValue)//coroutine that invites the Balinese of the Granada every so often
    {
        time = chronometerValue;
        while (time > 0)
        {
            GameObject Balinese = Instantiate(bullet, firePoint.position, firePoint.rotation);
            Rigidbody rb = Balinese.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * 10, ForceMode.Impulse);
            boss1.transform.Rotate(0, transform.rotation.y + 15, 0);
            yield return new WaitForSeconds(0.05f);
            time--;
        }
        if (time <= 0)
        {
            shooting = 0;

        }
    }
    private void OnDisable() //when the object is destroyed it is removed from the restart queue
    {
        Restart._Restart.OnRestart -= Reset;
    }
}
