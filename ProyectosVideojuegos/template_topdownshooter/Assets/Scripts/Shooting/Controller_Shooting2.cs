﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Shooting2 : MonoBehaviour
{
    public delegate void Shooting();
    public event Shooting OnShooting;
    public static Ammo1 ammo;
    public static int ammunition;
    public static Controller_Shooting _ShootingPlayer;
    public Transform firePoint;
    public Transform firePoint2;
    public GameObject bulletPrefab;
    public GameObject cannonPrefab;
    public GameObject bumeranPrefab;
    public GameObject wallPrefab;
    public GameObject smokeGranadePrefab;
    public GameObject balineseGrenadePrefab;
    public float bulletForce = 20f;
    private bool started = false;
    private int tightNumber;
    private Rigidbody rbplayer;

    private void Awake()
    {
        if (_ShootingPlayer == null)
        {
            _ShootingPlayer = this.gameObject.GetComponent<Controller_Shooting>();
            Debug.Log("Shooting es nulo");
        }
        else
        {
            Destroy(this);
        }
        tightNumber = 1;

    }

    private void Start()
    {
        if (_ShootingPlayer == null)
        {
            _ShootingPlayer = this.gameObject.GetComponent<Controller_Shooting>();
        }
        Restart._Restart.OnRestart += Reset;//adds an item to the restart event queue
        started = true;
        ammo = Ammo1.Bumeran;
        ammunition = 1;
        tightNumber = 1;
        rbplayer = this.gameObject.GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        if (started)
            Restart._Restart.OnRestart += Reset;//adds an item to the restart event queue when initialized
    }

    private void Reset() // when the reset event occurs, this method is called to reset initial parameters
    {
        ammo = Ammo1.Bumeran;
        ammunition = 1;
        tightNumber = 1;
    }

    void Update()
    {
        GetImput();
        if (Input.GetButtonDown("Fire2"))
        {

            Shoot();
            CheckAmmo();
        }
    }
    private void GetImput() //get the input of the choice of the weapon
    {
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            tightNumber = 1;
        }
        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            tightNumber = 2;
        }
        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            tightNumber = 3;
        }
        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            tightNumber = 4;
        }
        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            tightNumber = 5;
        }
    }
    private void CheckAmmo() //Check the ammo
    {
        if (ammunition <= 0)
        {
            ammo = Ammo1.Normal;
        }
    }

    private void Shoot()
    {
        if (OnShooting != null)
        {
            OnShooting();
        }

        switch (tightNumber)// swich so that the game knows what to shoot according to the player's choice
        {
            case 1:
                if (ammo == Ammo1.Normal)
                {
                    GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                    Rigidbody rb1 = bullet.GetComponent<Rigidbody>();
                    rb1.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
                }
                else if (ammo == Ammo1.Shotgun)
                {
                    Rigidbody rb2;
                    for (float i = -0.2f; i < 0.2f; i += 0.1f)
                    {
                        rb2 = null;

                    }
                    ammunition--;
                }
                else if (ammo == Ammo1.Cannon)
                {
                    GameObject bullet = Instantiate(cannonPrefab, firePoint.position, firePoint.rotation);
                    Rigidbody rb3 = bullet.GetComponent<Rigidbody>();
                    rb3.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
                    ammunition--;
                }
                else if (ammo == Ammo1.Bumeran)
                {
                    GameObject bullet = Instantiate(bumeranPrefab, firePoint.position, firePoint.rotation);
                    Controller_bumeran2 bm = bullet.GetComponent<Controller_bumeran2>();
                    bm.startPos = firePoint.position;
                    Rigidbody rb4 = bullet.GetComponent<Rigidbody>();
                    rb4.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
                    ammunition--;
                }
                break;
            case 2:
                GameObject wall = Instantiate(wallPrefab, firePoint2.position, firePoint2.rotation);
                Rigidbody rb = wall.GetComponent<Rigidbody>();
                rb.AddForce(firePoint2.forward * bulletForce * 2, ForceMode.Impulse);
                break;
            case 3:
                rbplayer.AddForce(firePoint2.forward * 1000, ForceMode.Impulse);
                break;
            case 4:
                Instantiate(balineseGrenadePrefab, firePoint2.position, firePoint2.rotation);
                break;
            case 5:
                Instantiate(smokeGranadePrefab, firePoint2.position, firePoint2.rotation);
                break;
        }

    }

    private void OnDisable() //when the object is destroyed it is removed from the restart queue
    {
        Restart._Restart.OnRestart -= Reset;
    }
}
public enum Ammo1
{
    Normal,
    Shotgun,
    Cannon,
    Bumeran
}



