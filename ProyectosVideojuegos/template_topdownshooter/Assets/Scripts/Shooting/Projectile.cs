﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float xLimit = 1000;
    public float yLimit = 1000;

    private void Start()
    {
        Restart._Restart.OnRestart += Reset;//adds an item to the restart event queue
    }

    private void Reset() // when the reset event occurs,destroy the projectiles 
    {
        Destroy(this.gameObject);
    }

    virtual public void Update()
    {
        CheckLimits();
    }

    internal virtual void OnCollisionEnter(Collision collision) //if it collides with the walls of the level or with the power up it is destroyed
    {
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("PowerUp"))
        {
            Destroy(this.gameObject);
        }
    }

    internal virtual void CheckLimits() //if it passes the limits of space it is destroyed
    {
        if (this.transform.position.x > xLimit)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.x < -xLimit)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.z > yLimit)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.z < -yLimit)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDisable()
    {
        Restart._Restart.OnRestart -= Reset; //when the object is destroyed it is removed from the restart queue
    }
}
