﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeGranade : MonoBehaviour
{
    public static bool playerIsInTheSmoke = false;
    private void Start()
    {
        playerIsInTheSmoke = false;
        Restart._Restart.OnRestart += Reset;//adds an item to the restart event queue

    }

    private void Reset() // when the reset event occurs, this method is called to destroy the smoke grenades when rebooted
    {
        Destroy(this.gameObject);
        playerIsInTheSmoke = false;
    }

    private void Update()
    {
        Destroy(this.gameObject, 10.0f); //is destroyed after 10 seconds of initialization
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player")|| other.gameObject.CompareTag("Player2")) //if the player stays within the trigger zone change the value of the variable to true
        {
            playerIsInTheSmoke = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player")|| other.gameObject.CompareTag("Player2")) //if the player exit within the trigger zone change the value of the variable to false
        {
            playerIsInTheSmoke = false;
        }
    }
    private void OnDisable() //when the object is destroyed it is removed from the restart queue and return the variable playerIsInTheSmoke to false
    {
        Restart._Restart.OnRestart -= Reset;
        playerIsInTheSmoke = false;
    }
}
