﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
   

    private void Start()
    {
        Restart._Restart.OnRestart += Reset;//adds an item to the restart event queue

    }

    private void Reset()// when the reset event occurs, this method is called to  destroy this gameobject
    {
        Destroy(this.gameObject);
    }
    private void Update()
    {
        Destroy(this.gameObject, 3.0f);
    }

    internal virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall")) //if it collides with the walls of the level it is destroyed
        {
            Destroy(this.gameObject);
        }
    }



    private void OnDisable()
    {
        Restart._Restart.OnRestart -= Reset;//when the object is destroyed it is removed from the restart queue
    }
}
