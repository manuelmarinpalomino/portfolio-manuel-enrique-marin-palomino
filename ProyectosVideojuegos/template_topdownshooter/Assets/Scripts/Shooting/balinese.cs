﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balinese : MonoBehaviour
{
    private void Start()
    {
        Restart._Restart.OnRestart += Reset; //adds an item to the restart event queue
    }
    private void Reset() // when the reset event occurs,this method is called to destroy this gameobject
    {
        Destroy(this.gameObject);
    }
    void Update()
    {
        Destroy(this.gameObject, 1);
    }
    public virtual void OnCollisionEnter(Collision collision) //if it collides with the enemy gameobject it destroys it
    {
        if (collision.gameObject.CompareTag("Enemy")) 
        {
            Destroy(collision.gameObject);
            Controller_Hud.points++;
           
        }
    }
    private void OnDisable() //when the object is destroyed it is removed from the restart queue
    {
        Restart._Restart.OnRestart -= Reset;
    }
}
