﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slow_Motion : MonoBehaviour
{
    private float time;
    
    private int noaddtime = 0;
    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U) && noaddtime == 0 )
        {
            noaddtime++;
            SlowMotion();
            
        }
    }

    private void SlowMotion() //method that slows down the game
    {
        StartCoroutine(chronometer(3));
    }
    public IEnumerator chronometer(float chronometerValue)
    {
        time = chronometerValue;
        while (time > 0)
        {
            Time.timeScale = 0.33f;
            yield return new WaitForSeconds(1.0f);
            time--;
        }
        if(time <= 0)
        {
            Time.timeScale = 1;
            noaddtime = 0;
        }
    }
}
